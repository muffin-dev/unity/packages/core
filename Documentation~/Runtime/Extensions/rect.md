# Core - Runtime - `RectExtensions`

Extension functions for `Rect` values.

## Public API

```cs
public static class RectExtensions { }
```

### Functions

#### `HoldInArea()`

```cs
public static void HoldInArea(this ref Rect rect, Rect area)
```

Holds this `Rect` inside the given area.

- `this ref Rect rect`: The `Rect` you want to hold in the given area.
- `Rect area`: The available area in which the given rect can be placed.

#### `HoldInScreenSpace()`

```cs
public static void HoldInScreenSpace(this ref Rect rect)
```

Holds this `Rect` inside the screen space.

- `this ref Rect rect`: The `Rect` you want to hold in the screen space.

---

[<= Back to summary](./README.md)