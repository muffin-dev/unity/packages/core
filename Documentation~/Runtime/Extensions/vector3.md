# Core - Runtime - `Vector3Extensions`

Extension functions for `Vector3` values.

## Public API

```cs
public static class Vector3Extensions { }
```

### Functions

#### `Min()`

```cs
public static Vector3 Min(this Vector3 vector, float min)
```

Returns a new `Vector3` with its values superior or equal to the given minimum value.

- `this Vector3 vector`: The input vector to compute.
- `float min`: The minimum value of the given vector.

Returns the computed vector.

#### `Max()`

```cs
public static Vector3 Max(this Vector3 vector, float max)
```

Returns a new `Vector3` with its values inferior or equal to the given maximum value.

- `this Vector3 vector`: The input vector to compute.
- `float max`: The maximum value of the given vector.

Returns the computed vector.

#### `Translate()`

```cs
public static Vector3[] Translate(this Vector3[] points, Vector3 translation, bool inPlace = false)
```

Translates the given points.

- `this Vector3[] points`: The points you want to translate.
- `Vector3 translation`: The traslation vector.
- `bool inPlace = false`: If enabled, the input array is modified in-place, instead of returning a new array

Returns the translated points.

#### `Scale()`

```cs
public static Vector3[] Scale(this Vector3[] points, float scale)
```

Scales a shape.

- `this Vector3[] points`: The points that represent the shape to scale.
- `float scale`: The scale factor to apply.

Returns the scaled shape's points.

#### `To2D()`

```cs
public static Vector2[] To2D(this IList<Vector3> points)
```

Converts this list or `Vector3` into a `Vector2` array.

- `this Vector3[] points`: The points you want to convert.

Returns the processed array.

#### `Barycentre()`

```cs
public static Vector3 Barycentre(IList<Vector3> points);
public static Vector3 Barycentre(IList<(Vector3, float)> weightedPoints);
```

Computes the barycentre of the given points.

Barycentre formula:

```txt
                ->                ->
->    weightA * OA + weightN... * ON...
OG = ___________________________________
            weightA + weightN...
```

- `IList<Vector3> points`: The points from which you want to compute the barycentre.
- `IList<(Vector3, float)> weightedPoints`: The points from which you want to compute the barycentre, and their associated weight. The more the weight value, the closer the barycentre to this point.

---

[<= Back to summary](./README.md)