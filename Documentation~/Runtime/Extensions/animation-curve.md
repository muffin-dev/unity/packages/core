# Core - Runtime - `AniamtionCurveExtensions`

Extension functions for `AniamtionCurve` instances.

## Public API

```cs
public static class AniamtionCurve { }
```

### Functions

#### `ComputeDuration()`

```cs
public static float ComputeDuration(this AnimationCurve curve)
```

Calculates the duration of an `AnimationCurve`, using its first and last keyframes on X axis.

- `this AnimationCurve curve`: The curve of which you want to compute the duration.

Returns the calculated duration of the `AnimationCurve`. Note that it will return 0 if the `AnimationCurve` counts less than 2 keyframes.

#### `ComputeRange()`

```cs
public static float ComputeRange(this AnimationCurve curve)
```

Calculates the value range of an `AnimationCurve`, using its lowest and highest keyframes on Y axis.

- `this AnimationCurve curve`: The curve of which you want to compute the range.

Returns the calculated value range of the `AnimationCurve`. Note that it will return 0 if the `AnimationCurve` counts less than 2 keyframes.

#### `GetFirstKeyframe()`

```cs
public static Keyframe GetFirstKeyframe(this AnimationCurve curve)
```

Gets the first keyframe on X axis of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the first keyrame.

Returns the found keyframe, or the default one if there's no keyframe on the curve.

#### `GetLastKeyframe()`

```cs
public static Keyframe GetLastKeyframe(this AnimationCurve curve)
```

Gets the last keyframe on X axis of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the last keyrame.

Returns the found keyframe, or the default one if there's no keyframe on the curve.

#### `GetMinKeyframe()`

```cs
public static Keyframe GetMinKeyframe(this AnimationCurve curve)
```

Gets the lowest keyframe on Y axis of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the minimum keyrame.

Returns the found keyframe, or the default one if there's no keyframe on the curve.

#### `GetMaxKeyframe()`

```cs
public static Keyframe GetMaxKeyframe(this AnimationCurve curve)
```

Gets the highest keyframe on Y axis of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the maximum keyrame.

Returns the found keyframe, or the default one if there's no keyframe on the curve.

#### `GetMinTime()`

```cs
public static float GetMinTime(this AnimationCurve curve)
```

Gets the minimum time of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the min time value.

Returns the found minimum time, or default keyframe's if there's no keyframe on the curve.

#### `GetMaxTime()`

```cs
public static float GetMaxTime(this AnimationCurve curve)
```

Gets the maximum time of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the max time value.

Returns the found maximum time, or default keyframe's if there's no keyframe on the curve.

#### `GetMinValue()`

```cs
public static float GetMinValue(this AnimationCurve curve)
```

Gets the minimum value of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the min value.

Returns the found minimum value, or default keyframe's if there's no keyframe on the curve.

#### `GetMaxValue()`

```cs
public static float GetMaxalue(this AnimationCurve curve)
```

Gets the maximum value of this `AnimationCurve`.

- `this AnimationCurve curve`: The curve of which you want to get the max value.

Returns the found maximum value, or default keyframe's if there's no keyframe on the curve.

#### `Loop()`

```cs
public static AnimationCurve Loop(this AnimationCurve curve)
```

Makes this `AnimationCurve` repeat (loop) before its first frame, and after its last frame.

- `this AnimationCurve curve`: The curve you want to set the wrap mode.

Returns the updated input curve.

#### `PingPong()`

```cs
public static AnimationCurve PingPong(this AnimationCurve curve)
```

Makes this `AnimationCurve` ping-pong before its first frame, and after its last frame.

- `this AnimationCurve curve`: The curve you want to set the wrap mode.

Returns the updated input curve.

### Properties

#### `DefaultKeyframe`

```cs
public static Keyframe DefaultKeyframe => new Keyframe(0f, 0f);
```

Default `Keyframe` value.

---

[<= Back to summary](./README.md)