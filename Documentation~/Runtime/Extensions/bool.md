# Core - Runtime - `BoolExtensions`

Extension functions for boolean values.

## Public API

```cs
public static class BoolExtensions { }
```

### Constants

#### `TRUE_LABEL`

```cs
public const string TRUE_LABEL = "true";
```

#### `FALSE_LABEL`

```cs
public const string FALSE_LABEL = "false";
```

### Functions

#### `ToLabel()`

```cs
public static string ToLabel(this bool value, string labelIfTrue = TRUE_LABEL, string labelIfFalse = FALSE_LABEL)
```

Converts this boolean into a label.

- `this bool value`: The boolean value to convert.
- `string labelIfTrue = TRUE_LABEL`: The output label if the boolean is true.
- `string labelIfFalse = FALSE_LABEL`: The output label if the boolean is false.

Returns the appropriate label.

---

[<= Back to summary](./README.md)