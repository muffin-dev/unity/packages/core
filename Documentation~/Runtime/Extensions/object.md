# Core - Runtime - `ObjectExtensions`

Extension functions for `Object` instances.

## Public API

```cs
public static class ObjectExtensions { }
```

### Functions

#### `GetTransform()`

```cs
public static Transform GetTransform(this Object obj)
```

Gets the `Transform` component of this object if it can be placed in the scene.

- `Object obj`: The object from which you want to get the `Transform` component.

Returns the found `Transform` component.

#### `TryGetTransform()`

```cs
public static bool TryGetTransform(this Object obj, out Transform transform)
```

Tries to get the `Transform` component of this object if it can be placed in the scene.

- `Object obj`: The object from which you want to get the `Transform` component.
- `out Transform transform`: Outputs the found `Transform` component.

Returns `true` if the given object has a `Transform` component, otherwise `false`.

#### `GetGameObject()`

```cs
public static GameObject GetGameObject(this Object obj)
```

Gets the `GameObject` to which this object is attached.

- `Object obj`: The object of which you want to get the attached `GameObject`.

Returns the found attcahed `GameObject`.

#### `TryGetGameObject()`

```cs
public static bool TryGetGameObject(this Object obj, out GameObject gameObject)
```

Tries to get `GameObject` to which this object is attached.

- `Object obj`: The object of which you want to get the attached `GameObject`.
- ` out GameObject gameObject`: Outputs the found attached `GameObject`.

Returns `true` if this object is attached to a `GameObject`, otherwise `false`.

---

[<= Back to summary](./README.md)