# Core - Runtime - `GameObjectExtensions`

Extension functions for `GameObject` instances.

## Public API

```cs
public static class GameObjectExtensions { }
```

### Functions

#### `ComputeSize()`

```cs
public static Vector3 ComputeSize(this GameObject obj, bool applyScale = false)
```

Computes the local bounds of an object, before it's transformed in the scene.

This function will attempt to use the `MeshFilter` component in order to get the original mesh bounds (in local space), then multiply it by the object's scale if required.

- `this GameObject obj`: The object you want to get the bounds.
- `bool applyScale = false`: If enabled, multiply the local bounds by the object's scale.

Returns the local bounds of the object.

---

[<= Back to summary](./README.md)