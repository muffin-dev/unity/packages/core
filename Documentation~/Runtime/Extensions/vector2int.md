# Core - Runtime - `Vector2IntExtensions`

Extension functions for `Vector2Int` values.

## Public API

```cs
public static class Vector2IntExtensions { }
```

### Functions

#### `Min()`

```cs
public static Vector2Int Min(this Vector2Int vector, float min)
```

Returns a new `Vector2Int` with its values superior or equal to the given minimum value.

- `this Vector2Int vector`: The input vector to compute.
- `float min`: The minimum value of the given vector.

Returns the computed vector.

#### `Max()`

```cs
public static Vector2Int Max(this Vector2Int vector, float max)
```

Returns a new `Vector2Int` with its values inferior or equal to the given maximum value.

- `this Vector2Int vector`: The input vector to compute.
- `float max`: The maximum value of the given vector.

Returns the computed vector.

---

[<= Back to summary](./README.md)