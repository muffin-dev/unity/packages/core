# Core - Runtime - `IntExtensions`

Extension functions for `int` values.

## Public API

```cs
public static class IntExtensions { }
```

### Functions

#### `Pad()`

```cs
public static string Pad(this int number, int length)
```

"Pads" this value by adding leading 0s.

- `this int number`: The number you want to pad.
- `int length`: The expected length of the output string, after adding leading 0 if applicable.

Returns the padded number as `string`.

#### `Ratio()`

```cs
public static float Ratio(this int value, float min, float max, float ratio = 1f);
public static float Ratio(this int value, float max, float ratio = 1f);
```

Apply proportions this value, between a given min and max, and report it to the given ratio.

- `this int value`: The value of you want to compute proportions.
- `float min`: The minimum value.
- `float max`: The maximum value.
- `float ratio = 1f`: The proportion operand.

Returns the computed ratio.

##### Example

```cs
Ratio(50, 0, 100, 1);     // Outputs 0.5
Ratio(50, 0, 100, 20);    // Outputs 10
Ratio(50, -100, 100, 1);  // Outputs 0.75
Ratio(-10, 0, 10, 1);     // Outputs -1
```

#### `Percents()`

```cs
public static float Percents(this int value, float min, float max);
public static float Percents(this int value, float max);
```

Apply proportions to this value, between a given min and max, and report it to a percentage.

- `this int value`: The value of you want to compute proportions.
- `float min`: The minimum value.
- `float max`: The maximum value.

Returns the computed percentage.

#### `Clamp()`

```cs
public static int Clamp(this int value, int min, int max)
```

Clamps this value between given min and max.

- `this int value`: The value to clamp..
- `int min`: The minimum possible value.
- `int max`: The maximum possible value.

Returns the clamped value.

---

[<= Back to summary](./README.md)