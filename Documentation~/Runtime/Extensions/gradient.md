# Core - Runtime - `GradientExtensions`

Extension functions for `Gradient` instances.

## Public API

```cs
public static class GradientExtensions { }
```

### Functions

#### `Clone()`

```cs
public static Gradient Clone(this Gradient gradient)
```

Clones this `Gradient`.

- `this Gradient gradient`: The gradient to clone.

Returns the gradient's clone.

#### `Reverse()`

```cs
public static Gradient Reverse(this Gradient gradient)
```

Reverses the gradient color and alpha keys.

- `this Gradient gradient`: The original gradient you want to reverse.

Returns a new `Gradient` instance with the reversed keys of the input `Gradient`.

---

[<= Back to summary](./README.md)