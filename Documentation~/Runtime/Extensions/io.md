# Core - Runtime - `IOExtensions`

Extension functions for working with IO features.

## Public API

```cs
public static class IOExtensions { }
```

### Functions

#### `CopyTo()`

```cs
public static bool CopyTo(this DirectoryInfo source, string outputDirectory)
```

Copies all the files of the source directory to the output directory.

- `this DirectoryInfo source`: The source directory.
- `string outputDirectory`: The output directory. If relative path given, it's resolved from this project's path.

Returns `true` if at least one file has been copied to the output directory, otherwise `false`.

---

[<= Back to summary](./README.md)