# Core - Runtime - `ColorExtensions`

Extension functions for `Color` and `Color32` values.

## Public API

```cs
public static class ColorExtensions { }
```

### Constants

#### `MAX_COLOR_VALUE`

```cs
public const float MAX_COLOR_VALUE = 255f;
```

### Functions

#### `Set()`

```cs
public static void Set(this ref Color color, float r, float g, float b, float a);
public static void Set(this ref Color color, float value, float alpha);
public static void Set(this ref Color color, float value);
public static void Set(this ref Color32 color, byte r, byte g, byte b, byte a);
public static void Set(this ref Color32 color, byte value, byte alpha);
public static void Set(this ref Color32 color, byte value);
```

Sets the components of the given color.

- `this ref Color color`: The color of which you want to set the values.
- `float r`: The red value of the color, between 0 and 1.
- `float g`: The green value of the color, between 0 and 1.
- `float b`: The blue value of the color, between 0 and 1.
- `float a`: The alpha value of the color, between 0 and 1.
- `float value`: The value for all color channels of the color, between 0 and 1.
- `float alpha`: The alpha value of the color, between 0 and 1.
- `this ref Color32 color`: The color of which you want to set the values.
- `byte r`: The red value of the color, between 0 and 255.
- `byte g`: The green value of the color, between 0 and 255.
- `byte b`: The blue value of the color, between 0 and 255.
- `byte a`: The alpha value of the color, between 0 and 255.
- `byte value`: The blue value of the color, between 0 and 255.
- `byte alpha`: The alpha value of the color, between 0 and 255.

#### `ToHexRGB()`

```cs
public static string ToHexRGB(this Color color);
public static string ToHexRGB(this Color32 color);
```

Converts this color into an hexadecimal string value.

- `this Color color`: The color to convert.
- `this Color32 color`: The color to convert.

Returns the *color string* with format `RRGGBB`.

#### `ToHexRGBA()`

```cs
public static string ToHexRGBA(this Color color);
public static string ToHexRGBA(this Color32 color);
```

Converts this color into an hexadecimal string value.

- `this Color color`: The color to convert.
- `this Color32 color`: The color to convert.

Returns the *color string* with format `RRGGBBAA`.

#### `FromHex()`

```cs
public static bool FromHex(this ref Color color, string hexString);
public static bool FromHex32(this ref Color32 color, string hexString);
```

Sets this color values from the given hexadecimal string.

- `this ref Color color`: The color to override.
- `string hexString`: The hexadecimal string (like `F9F9F9`, `F9F9F9F9`, `#F9F9F9`, `#F9F9F9F9`).
- `this ref Color32 color`: The color to override.

Returns `true` if the string has been parsed successfully, otherwise `false`.

---

[<= Back to summary](./README.md)