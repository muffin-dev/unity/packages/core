# Core - Runtime - `GUIStyleExtensions`

Extension functions for `GUIStyle` instances.

## Public API

```cs
public static class GUIStyleExtensions { }
```

### Constants

#### `MIN_FONT_SIZE`

```cs
public const int MIN_FONT_SIZE = 1;
```

#### `MAX_FONT_SIZE`

```cs
public const int MAX_FONT_SIZE = 255;
```

### Functions

#### `WordWrap()`

```cs
public static GUIStyle WordWrap(this GUIStyle style, bool enable)
```

Copies the input style, and enables/disables word wrapping.

- `this GUIStyle style`: The style you want to update.
- `bool enable`: Is word wrapping enabled?

#### `RichText()`

```cs
public static GUIStyle RichText(this GUIStyle style, bool enable)
```

Copies the input style, and enables/disables rich text mode.

- `this GUIStyle style`: The style you want to update.
- `bool enable`: Is word rich text enabled?

#### `StretchWidth()`

```cs
public static GUIStyle StretchWidth(this GUIStyle style, bool enable)
```

Copies the input style, and enables/disables width stretching.

- `this GUIStyle style`: The style you want to update.
- `bool enable`: Is stretching enabled?

#### `StretchHeight()`

```cs
public static GUIStyle StretchHeight(this GUIStyle style, bool enable)
```

Copies the input style, and enables/disables height stretching.

- `this GUIStyle style`: The style you want to update.
- `bool enable`: Is stretching enabled?

#### `Stretch()`

```cs
public static GUIStyle Stretch(this GUIStyle style, bool stretchWidth, bool stretchHeight)
```

Copies the input style, and enables/disables width and height stretching.

- `this GUIStyle style`: The style you want to update.
- `bool stretchWidth`: Is width stretching enabled?
- `bool stretchHeight`: Is height stretching enabled?

#### `FontSize()`

```cs
public static GUIStyle FontSize(this GUIStyle style, int fontSize)
```

Copies the input style, and sets the given font size.

- `this GUIStyle style`: The style you want to update.
- `int fontSize`: The font size to set.

#### `FontColor()`

```cs
public static GUIStyle FontColor(this GUIStyle style, Color color)
```

Copies the input style, and sets the given font color on normal state.

- `this GUIStyle style`: The style you want to update.
- `Color color`: The font color to set.

#### `TextAlignment()`

```cs
public static GUIStyle TextAlignment(this GUIStyle style, TextAnchor alignment)
```

Copies the input style, and sets the text alignment.

- `this GUIStyle style`: The style you want to update.
- `TextAnchor alignment`: The text alignment to set.

#### `FontStyle()`

```cs
public static GUIStyle FontStyle(this GUIStyle style, FontStyle fontStyle)
```

Copies the input style, and sets the font style.

- `this GUIStyle style`: The style you want to update.
- `FontStyle fontStyle`: The font style to set.

#### `Margin()`

```cs
public static GUIStyle Margin(this GUIStyle style, RectOffset margins);
public static GUIStyle Margin(this GUIStyle style, int left, int right, int top, int bottom);
public static GUIStyle Margin(this GUIStyle style, int horizontal, int vertical);
```

Copies the input style, and sets the margins.

- `this GUIStyle style`: The style you want to update.
- `RectOffset margin`: The margin to set.
- `int left`: The left margin.
- `int right`: The right margin.
- `int top`: The top margin.
- `int bottom`: The bottom margin.
- `int horizontal`: The left and right margin.
- `int vertical`: The top and bottom margin.

#### `Padding()`

```cs
public static GUIStyle Padding(this GUIStyle style, RectOffset padding);
public static GUIStyle Padding(this GUIStyle style, int horizontalOffset, int verticalOffset);
public static GUIStyle Padding(this GUIStyle style, int left, int right, int top, int bottom);
```

Copies the input style, and sets the padding.

- `this GUIStyle style`: The style you want to update.
- `RectOffset padding`: The padding to set.
- `int left`: The left padding.
- `int right`: The right padding.
- `int top`: The top padding.
- `int bottom`: The bottom padding.
- `int horizontal`: The left and right padding.
- `int vertical`: The top and bottom padding.

---

[<= Back to summary](./README.md)