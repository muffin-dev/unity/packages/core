# Core - Runtime - `EnumExtensions`

Extension functions for enum values.

## Public API

```cs
public static class EnumExtensions { }
```

### Functions

#### `IsValid()`

```cs
public static bool IsValid<TEnum>(this TEnum enumValue)
  where TEnum : Enum
```

Checks if the given enumeration value matches a defined item.

- `<TEnum>`: The enumeration type.
- `this TEnum enumValue`: The enumeration value to check.

Returns `true` if the given enumeration value matches a defined item, otherwise `false`.

#### `Invert()`

```cs
public static void Invert<TEnum>(this ref TEnum enumValue)
  where TEnum : struct, Enum
```

Invert the given enumeration value: enabled flags become disabled flags, and vice-versa.

- `<TEnum>`: The enumeration type.
- `this ref TEnum enumValue`: The enumeration value to invert.

#### `AddFlag()`

```cs
public static void AddFlag<TEnum>(this ref TEnum enumValue, TEnum flag)
  where TEnum : struct, Enum
```

Adds the given flag to the enum value.

- `<TEnum>`: The enumeration type.
- `this ref TEnum enumValue`: The enumeration value to which you want to add the flag.
- `TEnum flag`: The flag to add.

#### `RemoveFlag()`

```cs
public static void RemoveFlag<TEnum>(this ref TEnum enumValue, TEnum flag)
  where TEnum : struct, Enum
```

Removes the given flag from the enum value.

- `<TEnum>`: The enumeration type.
- `this ref TEnum enumValue`: The enumeration value from which you want to remove the flag.
- `TEnum flag`: The flag to remove.
        
---

[<= Back to summary](./README.md)