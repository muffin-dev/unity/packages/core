# Core - Runtime - `EColorExtensions`

Extension functions for [`EColor`](../Enums/ecolor.md) values.

## Public API

```cs
public static class EColorExtensions { }
```

### Constants

#### `MAX_COLOR_VALUE`

```cs
public const byte MAX_COLOR_VALUE = 255;
```

#### `HALF_COLOR_VALUE`

```cs
public const byte HALF_COLOR_VALUE = MAX_COLOR_VALUE / 2;
```

### Function

#### `ToColor()`

```cs
public static Color ToColor(this EColor color, bool ignoreAlpha = false);
public static Color ToColor(this EColor color, float alpha);
```

Gets the color value from this color enum.

- `this EColor color`: The enum value of which you want to get the color.
- `bool ignoreAlpha = false`: If enabled, returns a color with 100% alpha.
- `float alpha`: The fixed alpha value (between 0 and 1) of the color (ignoring the alpha value from the enum).

Returns the processed color.

#### `ToColor32()`

```cs
public static Color32 ToColor32(this EColor color, bool ignoreAlpha = false);
public static Color32 ToColor32(this EColor color, byte alpha);
public static Color32 ToColor32(this EColor color, float alpha);
```

Gets the color value from this color enum.

- `this EColor color`: The enum value of which you want to get the color.
- `bool ignoreAlpha = false`: If enabled, returns a color with 100% alpha.
- `byte alpha`: The fixed alpha value (between 0 and 255) of the color (ignoring the alpha value from the enum).
- `float alpha`: The fixed alpha value (between 0 and 1) of the color (ignoring the alpha value from the enum).

Returns the processed color.

---

[<= Back to summary](./README.md)