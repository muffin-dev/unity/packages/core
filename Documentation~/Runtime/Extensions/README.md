# Core - Documentation - Extensions

## Summary

- [`CameraExtensions`](./camera.md): Extensions for `Camera` instances.
- [`BoolExtensions`](./bool.md): Extensions for boolean values.
- [`CameraExtensions`](./camera.md): Extensions for `Camera` instances.
- [`ColorExtensions`](./color.md): Extensions for `Color` and `Color32` values.
- [`ComponentExtensions`](./component.md): Extensions for `Component` instances.
- [`EColorExtensions`](./ecolor.md): Extensions for [`EColor`](../Enums/ecolor.md) values.
- [`EnumExtensions`](./enum.md): Extensions for enum values.
- [`ExceptionExtensions`](./exception.md): Extensions for [`Exception`](https://learn.microsoft.com/dotnet/api/system.exception) instances.
- [`FloatExtensions`](./float.md): Extensions for `float` values.
- [`GameObjectExtensions`](./game-object.md): Extensions for `GameObject` instances.
- [`GradientExtensions`](./gradient.md): Extensions for `Gradient` instances.
- [`GUISkinExtensions`](./guiskin.md): Extensions for `GUISkin` instances.
- [`GUIStyleExtensions`](./guistyle.md): Extensions for `GUIStyle` instances.
- [`IDictionaryExtensions`](./idictionary.md): Extensions for `IDictionary<T, U>` instances.
- [`IEnumerableExtensions`](./ienumerable.md): Extension functions for [`IEnumerable`](https://learn.microsoft.com/dotnet/api/system.collections.ienumerable) instances.
- [`IListExtensions`](./ilist.md): Extensions for `IList<T>` instances.
- [`IntExtensions`](./int.md): Extensions for `int` values.
- [`MonoBehaviourExtensions`](./mono-behaviour.md): Extensions for `MonoBehaviour` instances.
- [`ObjectExtensions`](./object.md): Extensions for `Object` instances.
- [`RectExtensions`](./rect.md): Extensions for `Rect` values.
- [`StringExtensions`](./string.md): Extensions for `string` instances.
- [`TransformExtensions`](./transform.md): Extensions for `Transform` instances.
- [`TypeExtensions`](./type.md): Extensions for `Type` instances.
- [`Vector2Extensions`](./vector2.md): Extensions for `Vector2` value.
- [`Vector2IntExtensions`](./vector2int.md): Extensions for `Vector2Int` value.
- [`Vector3Extensions`](./vector3.md): Extensions for `Vector3` value.
- [`Vector3IntExtensions`](./vector3Int.md): Extensions for `Vector3Int` value.
- [`Vector4Extensions`](./vector4.md): Extensions for `Vector4` value.

---

[<= Back to *Runtime* summary](../README.md)