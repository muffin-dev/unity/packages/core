# Core - Runtime - `CameraExtensions`

Extension functions for `Camera` instances.

## Public API

```cs
public static class CameraExtensions { }
```

### Functions

#### `GetExtentsOrthographic()`

```cs
public static Vector2 GetExtentsOrthographic(this Camera camera)
```

Calculates the camera render extents, using orthographic mode.

The extents are Vector2(orthographic size * aspect ratio, orthographic size). You can use this value to automatically define screen limits.

- `this Camera camera`: The `Camera` of which you want to get the extents.

Returns the computed extents.

#### `GetBoundsOrthographic()`

```cs
public static Vector2 GetBoundsOrthographic(this Camera camera)
```

Calculates the camera render extends using `GetExtentsOrthographic()` and double it to get bounds, using orthographic mode.

- `this Camera camera`: The `Camera` of which you want to get the bounds.

Returns the computed bounds.

---

[<= Back to summary](./README.md)