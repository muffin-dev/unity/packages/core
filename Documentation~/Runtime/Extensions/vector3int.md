# Core - Runtime - `Vector3intExtensions`

Extension functions for `Vector3int` values.

## Public API

```cs
public static class Vector3intExtensions { }
```

### Functions

#### `Min()`

```cs
public static Vector3Int Min(this Vector3Int vector, float min)
```

Returns a new `Vector3Int` with its values superior or equal to the given minimum value.

- `this Vector3Int vector`: The input vector to compute.
- `float min`: The minimum value of the given vector.

Returns the computed vector.

#### `Max()`

```cs
public static Vector3Int Max(this Vector3Int vector, float max)
```

Returns a new `Vector3Int` with its values inferior or equal to the given maximum value.

- `this Vector3Int vector`: The input vector to compute.
- `float max`: The maximum value of the given vector.

Returns the computed vector.

---

[<= Back to summary](./README.md)