# Core - Runtime - `Exception`

Extension functions for [`Exception`](https://learn.microsoft.com/dotnet/api/system.exception) instances.

## Public API

```cs
public static class ExceptionExtensions { }
```

### Functions

#### `ToLogEntry()`

```cs
public static ExceptionLogEntry ToLogEntry(this Exception exception, ELogLevel logLevel = ELogLevel.Fatal);
public static ExceptionLogEntry ToLogEntry(this Exception exception, string message, ELogLevel logLevel = ELogLevel.Fatal);
public static ExceptionLogEntry ToLogEntry(this Exception exception, object data, ELogLevel logLevel = ELogLevel.Fatal);
public static ExceptionLogEntry ToLogEntry(this Exception exception, string message, object data, ELogLevel logLevel = ELogLevel.Fatal);
```

Creates a log entry that contains this exception (see [*Log* feature documentation](../Utilities/Log/README.md)).

- `Exception exception`: The handled exception.
- `string message`: The additional message to the exception.
- `object data`: Additional data that complete exception information.
- [`ELogLevel logLevel = ELogLevel.Fatal`](../Utilities/Log/log-level.md): The level of the log entry.

Returns the created log entry.
        
---

[<= Back to summary](./README.md)