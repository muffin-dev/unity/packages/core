# Core - Runtime - `ComponentExtensions`

Extension functions for `Component` instances.

## Public API

```cs
public static class ComponentExtensions { }
```

### Functions

#### `CopyTo()`

```cs
public static void CopyTo(this Component original, Component target, string[] filterProperties = null, bool ignoreFilterProperties = false);
public static void CopyTo<T>(this T original, T target, bool ignoreFilterProperties, params string[] filterProperties)
  where T : Component;
```

Copy the values of the original component onto another one. Note that if the types doesn't match, only the matching properties will be copied.

- `this Component original`: The original component.
- `Component target`: The target component, on which to set the original values.
- `string[] filterProperties = null`: If `true`, all properties and fields will be copied BUT the target properties list.
- `bool ignoreFilterProperties = false`: Defines the names of the properties or fields you want to copy.
- `<T>`: The type of the component to copy.
- `this T original`: The component to copy.
- `T target`: The target component, on which to set the original values.
- `bool ignoreFilterProperties`: Defines the names of the properties or fields you want to copy.
- `params string[] filterProperties`: If `true`, all properties and fields will be copied BUT the target properties list.
        
---

[<= Back to summary](./README.md)