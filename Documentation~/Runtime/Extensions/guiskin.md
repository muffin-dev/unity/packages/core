# Core - Runtime - `GUISkinExtensions`

Extension functions for `GUISkin` instances.

## Public API

```cs
public static class GUISkinExtensions { }
```

### Functions

#### `FindStyle()`

```cs
public static GUIStyle FindStyle(this GUISkin skin, string styleName, GUIStyle fallbackStyle)
```

Finds a style on this `GUISkin` asset.

- `this GUISkin skin`: The `GUISkin` on which you want to find the style.
- `string styleName`: The name of the style you want to find.
- `GUIStyle fallbackStyle`: The default style to use if the named style can't be found in the `GUISkin` asset.

Returns the found style, or the given fallback if the named style doesn't exist on the `GUISkin` asset.

---

[<= Back to summary](./README.md)