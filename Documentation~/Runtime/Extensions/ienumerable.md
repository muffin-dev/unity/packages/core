# Core - Runtime - `IEnumerableExtensions`

Extension functions for [`IEnumerable`](https://learn.microsoft.com/dotnet/api/system.collections.ienumerable) instances.

## Public API

```cs
public static class IEnumerableExtensions { }
```

### Delegates

#### `MapPredicateDelegate<TInput, TOutput>`

```cs
public delegate TOutput MapPredicateDelegate<TInput, TOutput>(TInput input);
```

Called for each item when using the `Map()` function.

- `<TInput>`: The type of the elements in the list.
- `<TOutput>`: The type of the output data for each item.
- `TInput input`: The original list item.

Returns the "mapped" value.

### Functions

#### `Join<T>()`

```cs
public static string Join(this IEnumerable enumerable, string separator);
public static string Join<T>(this IEnumerable<T> enumerable, string separator, MapPredicateDelegate<T, string> mapFunc);
```

Joins the items in the given collection into a single string using a separator.

- `<T>`: The type of an item in the input list.
- `this IEnumerable | IEnumerable<T> enumerable`: The collection to pack into a single string.
- `string separator`: The character(s) that separates each elements in the output text.
- `MapPredicateDelegate<T, string> mapFunc`: The function called for every item to convert it into a string.

Returns the processed string.

#### `Map<TInput, TOutput>()`

```cs
public static TOutput[] Map<TInput, TOutput>(this IEnumerable<TInput> enumerable, MapPredicateDelegate<TInput, TOutput> mapFunc)
```

Creates a new array populated with the results of calling a given function every item in the given list.

- `<TInput>`: The type of an item in the input list.
- `<TOutput>`: The type of the output data when calling the action function on each item.
- `this IEnumerable<TInput> enumerable`: The collection you want to "map".
- `MapPredicateDelegate<TInput, TOutput> mapFunc`: The function to call to provide a data for each item.

Returns the mapped items array.
        
---

[<= Back to summary](./README.md)