# Core - Runtime - `IListExtensions`

Extension functions for `IList` instances.

## Public API

```cs
public static class IListExtensions { }
```

### Delegates

#### `IsSimilarDelegate<TListItem>`

```cs
public delegate bool IsSimilarDelegate<TListItem>(TListItem item, TListItem other);
```

Called to check if the given items are similar.

- `<TListItem>`: The type of the items to compare.
- `TListItem item`: The first item to compare.
- `TListItem other`: The second item to compare.

Returns true if the items are similar, otherwise false.

### Functions

#### `AddOnce<T>()`

```cs
public static bool AddOnce<T>(this IList<T> list, T item);
```

Adds the given item to the list only if it's not already in.

- `<T>`: The type of the elements in the list.
- `this IList<T>`: The item to add.
- `T item`: The item to add.

Returns true if the item has been added, or false if the item is already in the list.

#### `Move<T>()`

```cs
public static bool Move<T>(this IList<T> list, int movedIndex, int targetIndex)
```

Moves an item in the list in-place.

- `<T>`: The type of the elements in the list.
- `this IList<T> list`: The list in which you want to move items.
- `int movedIndex`: The index of the item to move in the list, clamped between `0` and `list.Count - 1`.
- `int targetIndex`: The new index of the item, clamped between `0` and `list.Count - 1`.

#### `IsInRange<T>()`

```cs
public static bool IsInRange<T>(this IList<T> list, int index)
```

Checks if the given index is in this list's range.

- `<T>`: The type of an item in the input list.
- `this IList<T> list`: The list in which you want to check the range.
- `int index`: The index you want to check.

Returns `true` if the given index in the list's range, otherwise `false`.

#### `Shuffle<T>()`

```cs
public static void Shuffle<T>(this IList<T> list)
```

Shuffles the list in-place, using `Random.Range()`.

Original version at https://stackoverflow.com/questions/273313/randomize-a-listt

- `<T>`: The type of an item in the input list.
- `this IList<T> list`: The list you want to shuffle.

#### `ShuffleCrypto<T>()`

```cs
public static void ShuffleCrypto<T>(this IList<T> list)
```

Shuffles the list in-place, using Cryptography random number generators.

This function is slower than `Shuffle()`, but provides a better randomness quality.

Original version at https://stackoverflow.com/questions/273313/randomize-a-listt

- `<T>`: The type of an item in the input list.
- `this IList<T> list`: The list you want to shuffle.

#### `Slice()`

```cs
public static T[] Slice<T>(this IList<T> list, int start);
public static T[] Slice<T>(this IList<T> list, int start, int end);
```

Extracts a range of items from a list.

- `<T>`: The type of the items in the list.
- `this IList<T> list`: The list from which you want to extract the items.
- `int start`: The index at which to start extraction (included). If negative value given, the index is count from the end of the list.
- `int end`: The index at which to end extraction (excluded). If negative value given, the index is count from the end of the list.

Returns the extracted items.

##### Example

```cs
using UnityEngine;
using MuffinDev.Core;

public class SliceExample : MonoBehaviour
{
  private static readonly string[] Items = new string[] { "A", "B", "C", "D", "E" };

  private void Start()
  {
    Debug.Log(string.Join("", Items.Slice(0)));  // Outputs "ABCDE"
    Debug.Log(string.Join("", Items.Slice(0, 5)));  // Outputs "ABCDE"
    Debug.Log(string.Join("", Items.Slice(1)));  // Outputs "BCDE"
    Debug.Log(string.Join("", Items.Slice(1, 4)));  // Outputs "BCD"
    Debug.Log(string.Join("", Items.Slice(0, -2))); // Outputs "ABC"
    Debug.Log(string.Join("", Items.Slice(-3))); // Outputs "CDE"
    Debug.Log(string.Join("", Items.Slice(-3, -1))); // Outputs "CD"
    Debug.Log(string.Join("", Items.Slice(4, 2))); // Outputs "" (invalid because end < start)
    Debug.Log(string.Join("", Items.Slice(10, 20))); // Outputs "" (invalid because start > length)
  }
}
```

#### `Paginate<T>()`

```cs
public static T[] Paginate<T>(this IList<T> list, out Pagination pagination, int page, int nbElementsPerPage = Pagination.DEFAULT_NB_ELEMENTS_PER_PAGE);
public static T[] Paginate<T>(this IList<T> list, int page, int nbElementsPerPage = Pagination.DEFAULT_NB_ELEMENTS_PER_PAGE);
```

Creates a sub-list of the given list that contains only the elements that should be displayed using the given pagination settings.

- `<T>`: The type of an item in the input list.
- `this IList<T> list`: The list that is paginated.
- `out Pagination pagination`: The `Pagination` infos of the operation.
- `int page`: The current page.
- `int nbElementsPerPage = Pagination.DEFAULT_NB_ELEMENTS_PER_PAGE`: The number of elements displayed per page.

Returns the sub-list of the elements to display.

#### `RemoveDoubles()`

```cs
public static int RemoveDoubles<T>(this IList<T> list)
  where T : IComparable<T>;
public static int RemoveDoubles<T>(this IList<T> list, IsSimilarDelegate<T> comparator);
```

Removes the duplicates in a list so only the first occurrence of an item is kept in the list.

- `<T>`: The type of an item in the input list.
- `this IList<T> list`: The list from which you want to remove the doubles.
- `IsSimilarDelegate<T> comparator`: The function that compares an item to another to check if they are similar.

Returns the number of doubles removed from the list.

---

[<= Back to summary](./README.md)