# Core - Runtime - `StringExtensions`

Extension functions for `string` instances.

## Public API

```cs
public static class StringExtensions { }
```

### Constants

```cs
public const byte ASCII_0 = (byte)'0';
public const byte ASCII_9 = (byte)'9';
public const byte ASCII_A = (byte)'A';
public const byte ASCII_Z = (byte)'Z';
public const byte ASCII_a = (byte)'a';
public const byte ASCII_z = (byte)'z';
```

### Functions

#### `Split()`

```cs
public static string[] Split(this string str, string separator, StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries)
```

Shortcut for using `string.Split()` with one string as a separator.

- `this string str`: The string to split.
- `string separator`: The separator to use for splitting the string.
- `StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries`: Eventual split options.

Returns the splitted string.

#### `SplitLine()`

```cs
public static string[] SplitLines(this string str, StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries)
```

Splits a string by newline characters.

- `this string str`: The string to split.
- `StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries`: Eventual split options.

Returns the splitted string.

#### `Repeat()`

```cs
public static string Repeat(this string str, int iterations)
```

Creates a string that contains several iterations of the given input string.

- `this string str`: The string to repeat.
- `int iterations`: The number of times the input string is repeated in the output.

Returns the input string repeated the given number of times.

#### `Occurrences()`

```cs
public static int Occurrences(this string str, string pattern)
```

Counts the number of occurrences of a given string into another.

- `this string str`: The string in which you want to count the occurrences.
- `string pattern`: The pattern you want to count the occurrences.

Returns the number of occurrences found.

#### `Slice()`

```cs
public static string Slice(this string str, int start);
public static string Slice(this string str, int start, int end);
```

Extracts a substring, from an index to another.

- `this string str`: The string you want to slice.
- `int start`: The start index of the string to extract. If negative, it's used as `str.Length - start`.
- `int end`: The end index of the string to extract (exclusive). If negative, it's used as `str.Length - end`. If the end index is lower than the start index, start and end are inverted.

Returns the substring, from the given start index to the given end one.

#### `RemoveDiacritics()`

```cs
public static string RemoveDiacritics(this string str)
```

Remove all diacritic characters, keeping their base character in the output string. Examples: "àBCDË?" -> "aBCDE?".

- `this string str`: The string from which you want to remove the diacritic characters

Returns the processed string.

#### `RemoveSpecialChars()`

```cs
public static string RemoveSpecialChars(this string str, string allowedChars)
```

Removes all characters that are not letters or digits in the given string.

- `this string str`: The string from which you want to remove the characters.
- `string allowedChars`: The allowed characters in the output string that won't be removed.

Returns the processed string.

#### `Path()`

```cs
public static string Path(this string str);
public static string Path(this string str, char separator);
```

Ensures given string represents a path, so it doesn't contain forbidden characters, and uses only slash as drectory separator.

- `string str`: The string to format.
- `char separator`: The character to use as directory separator.

Returns the formatted string.

#### `Paths()`

```cs
public static void Paths(this IList<string> strs);
public static void Paths(this IList<string> strs, char separator);
```

Ensures given strings represent paths, so they don't contain forbidden characters, and use only slash as directory separator.

- `IList<string> strs`: The string to format.
- `char separator`: The character to use as directory separator.

Returns the formatted string.

#### `AbsolutePath()`

```cs
public static string AbsolutePath(this string path)
```

If the given string represents a relative path, it's combined with [`PathUtlity.ProjectPath`](../Utilities/path-utility.md) (which is the path to the current project in the editor, and [`PathUtlity.PeristentDataPath`](../Utilities/path-utility.md) in build) to make it absolute.

- `this string path`: The path string to convert.

Returns the absolute path, or an empty string if the input path is not valid.

#### `RelativePath()`

```cs
public static string RelativePath(this string path)
```

If the given string represents an absolute path to a project file or directory, this function makes it relative to [`PathUtlity.ProjectPath`](../Utilities/path-utility.md) (which is the path to the current project in the editor, and [`PathUtlity.PeristentDataPath`](../Utilities/path-utility.md) in build). If the given string is already relative or doesn't target the project, the input string is returned as is.

- `this string path`: The path string to convert.

Returns the relative path, or an empty string if the input path is not valid.

#### `IsProjectPath()`

```cs
public static bool IsProjectPath(this string path)
```

Checks if this path is relative to the current Unity project's root directory.

- `string path`: The path you want to check. If it's a relative path, this function always returns `true`.

Returns `true` if this path is relative to the current Unity project's root directory, otherwise `false`.

#### `CombinePaths()`

```cs
public static string CombinePaths(this string path, params string[] relPaths)
```

Combines and resolves this path with relative ones.

- `string path`: The path from which you want to resolve the relative paths. Note that if this path is relative, the path is resolved from the directory where the code is executed. Also, if the path targets a file, relative paths are resolved from the last directory.
- `params string[] relPaths`: The relative paths to resolve.

Returns the processed path.

#### `GetDirectoryPath()`

```cs
public static string GetDirectoryPath(this string path)
```

Gets the path to the parent directory of this one. If this path already targets a directory, this function returns the input path as is.

- `string path`: The path of which you want to get the directory path.

Returns the processed path.

---

[<= Back to summary](./README.md)