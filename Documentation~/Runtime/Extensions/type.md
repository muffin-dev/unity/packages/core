# Core - Runtime - `TypeExtensions`

Extension functions for `Type` instances.

## Public API

```cs
public static class TypeExtensions { }
```

### Functions

#### `GetFullNameWithAssembly()`

```cs
public static string GetFullNameWithAssembly(this Type type)
```

Gets the full `Type` name string, including the assmebly name. The output string looks like:

```txt
TypeNamespace.TypeName, AssemblyName, Version=0.0.0.0, Culture=neutral, PublicKeyKoken=null
```

- `this Type type`: The `Type` of which you want to get the full name string.

Returns the computed full name string, or null if the given `Type` is `null`.

#### `IsReallyPrimitive()`

```cs
public static bool IsReallyPrimitive(this Type type)
```

 Checks if the given `Type` is "really" a primitive. This function is meant to replace the `Type.IsPrimitive` property, which will return false even if the type is a `string`, `decimal`, `long`, `ulong`, `short` or `ushort`.

- `this Type type`: The `Type` you want to check as primitive.

Returns `true` if the `Type` is "really" a primitive, otherwise `false`.

#### `TryGetAttribute()`

```cs
public static bool TryGetAttribute<T>(this Type type, out T attribute)
  where T : Attribute;
public static bool TryGetAttribute(this Type type, Type attributeType, out Attribute attribute)
```

Tries to get a class attribute on this type.

- `<T>`: The type of the attribute you want to get.
- `this Type type`: The type from which you want to get the class attribute.
- `out T attribute`: Outputs the found attribute.
- `Type attributeType`: The type of the attribute you want to get.

Returns `true` if this type has the expected attribute, otherwise `false`.

#### `HasAttribute()`

```cs
public static bool HasAttribute<T>(this Type type)
  where T : Attribute;
public static bool HasAttribute(this Type type, Type attributeType);
```

Checks if this type has a given class attribute.

- `<T>`: The type of the attribute you want to get.
- `this Type type`: The type from which you want to check for the class attribute.
- `Type attributeType`: The type of the attribute you want to get.

Returns `true` if this type has the expected attribute, otherwise `false`.

---

[<= Back to summary](./README.md)