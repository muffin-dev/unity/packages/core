# Core - Runtime - `FloatExtensions`

Extension functions for `float` values.

## Public API

```cs
public static class FloatExtensions { }
```

### Functions

#### `Approximately()`

```cs
public static bool Approximately(this float a, float b);
public static bool Approximately(this float a, float b, float epsilon);
```

Checks the two float values are close enough to be considered equals. This is meant to prevent float imprecisions.

- `float epsilon`: The approximation value. Basically, this function returns `true` if `a > (b - epsilon)` and `a < (b + epsilon)`.

Returns `true` if `b` is equals (or very close) to `a`.

#### `Ratio()`

```cs
public static float Ratio(this float value, float min, float max, float ratio = 1f);
```

Apply proportions this value, between a given min and max, and report it to the given ratio.

- `this float value`: The value of you want to compute proportions.
- `float min`: The minimum value.
- `float max`: The maximum value.
- `float ratio = 1f`: The proportion operand.

Returns the computed ratio.

##### Example

```cs
Ratio(50, 0, 100, 1);     // Outputs 0.5
Ratio(50, 0, 100, 20);    // Outputs 10
Ratio(50, -100, 100, 1);  // Outputs 0.75
Ratio(-10, 0, 10, 1);     // Outputs -1
```

#### `Percents()`

```cs
public static float Percents(this float value, float min, float max);
public static float Percents(this float value, float max);
```

Apply proportions to this value, between a given min and max, and report it to a percentage.

- `this float value`: The value of you want to compute proportions.
- `float min`: The minimum value.
- `float max`: The maximum value.

Returns the computed percentage.

#### `Clamp()`

```cs
public static float Clamp(this float value, float min, float max)
```

Clamps this value between given min and max.

- `this float value`: The value to clamp..
- `float min`: The minimum possible value.
- `float max`: The maximum possible value.

Returns the clamped value.

---

[<= Back to summary](./README.md)