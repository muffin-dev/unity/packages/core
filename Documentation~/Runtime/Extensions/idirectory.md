# Core - Runtime - `IDictionaryExtensions`

Extensions functions for `IDictionary<T, U>` instances.

## Public API

```cs
public static class IDictionaryExtensions { }
```

### Functions

#### `AddOrSet<TKey, TValue>`

```cs
public static void AddOrSet<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
```

Adds a new entry in the dictionary if the key doesn't exist yet, or replace the previous value otherwise.

- `<TKey>`: The key type of this dictionary.
- `<TValue>`: The value type of this dictionary.
- `this IDictionary<TKey, TValue> dictionary`: The dictionary in which you want to add or set the value.
- `TKey key`: The target key.
- `TValue value`: The new value.

#### `AddOrPush<TKey, TItem>`

```cs
public static void AddOrPush<TKey, TItem>(this IDictionary<TKey, List<TItem>> dictionary, TKey key, TItem item)
```

Adds a new entry in the dictionary if the key doesn't yet, or adds the given value to the list otherwise.

- `<TKey>`: The key type of this dictionary.
- `<TValue>`: The type of a single item in the list.
- `this IDictionary<TKey, List<TItem>> dictionary`: The dictionary in which you want to add or push the value.
- `TKey key`: The target key.
- `TItem item`: The item to add.

---

[<= Back to summary](./README.md)