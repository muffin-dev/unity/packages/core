# Core - Runtime - `TransformExtensions`

Extension functions for `Transform` instances.

## Public API

```cs
public static class TransformExtensions { }
```

### Functions

#### `ClearHierarchy()`

```cs
public static void ClearHierarchy(this Transform transform)
```

Destroys all child `GameObject` of this `Transform`.

---

[<= Back to summary](./README.md)