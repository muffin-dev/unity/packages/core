# Core - Runtime - `MonoBehaviourExtensions`

Extension functions for `MonoBehaviour` instances.

## Public API

```cs
public static class MonoBehaviourExtensions { }
```

### Constants

#### `FIND_METHOD_FLAGS`

```cs
private const BindingFlags FIND_METHOD_FLAGS = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
```

### Functions

#### `Invoke()`

```cs
public static void Invoke(this MonoBehaviour obj, string methodName, float delay, params object[] @params)
```

Invokes the named function on the given object after a delay. Does the same as `MonoBehaviour.Invoke()`, but allow you to pass parameters to the invoked function.

- `this MonoBehaviour obj`: The component on which you want to invoke the function.
- `string methodName`: The name of the function to call. Note that you can use the `nameof()` keyword in order to avoid naming errors.
- `float delay`: The delay after which you want the function to be invoked. If this value is less or equal to 0, calls the function at the end of the frame. In this case, you should call the function directly.
- `params object[] @params`: The parameters you want to pass to the named function.

#### `InvokeCoroutine()`

```cs
private static IEnumerator InvokeCoroutine(this MonoBehaviour obj, string methodName, float delay, params object[] @params)
```

Invokes the named function on the given object after a delay.

- `this MonoBehaviour obj`: The component on which you want to invoke the function.
- `string methodName`: The name of the function to call. Note that you can use the `nameof()` keyword in order to avoid naming errors.
- `float delay`: The delay after which you want the function to be invoked. If this value is less or equal to 0, calls the function at the end of the frame. In this case, you should call the function directly.
- `params object[] @params`: The parameters you want to pass to the named function.

---

[<= Back to summary](./README.md)