# Core - Runtime - `Vector4Extensions`

Extension functions for `Vector4` values.

## Public API

```cs
public static class Vector4Extensions { }
```

### Functions

#### `Min()`

```cs
public static Vector4 Min(this Vector4 vector, float min)
```

Returns a new `Vector4` with its values superior or equal to the given minimum value.

- `this Vector4 vector`: The input vector to compute.
- `float min`: The minimum value of the given vector.

Returns the computed vector.

#### `Max()`

```cs
public static Vector4 Max(this Vector4 vector, float max)
```

Returns a new `Vector4` with its values inferior or equal to the given maximum value.

- `this Vector4 vector`: The input vector to compute.
- `float max`: The maximum value of the given vector.

Returns the computed vector.

---

[<= Back to summary](./README.md)