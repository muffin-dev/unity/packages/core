# Core - Runtime - `Vector2Extensions`

Extension functions for `Vector2` values.

## Public API

```cs
public static class Vector2Extensions { }
```

### Functions

#### `Min()`

```cs
public static Vector2 Min(this Vector2 vector, float min)
```

Returns a new `Vector2` with its values superior or equal to the given minimum value.

- `this Vector2 vector`: The input vector to compute.
- `float min`: The minimum value of the given vector.

Returns the computed vector.

#### `Max()`

```cs
public static Vector2 Max(this Vector2 vector, float max)
```

Returns a new `Vector2` with its values inferior or equal to the given maximum value.

- `this Vector2 vector`: The input vector to compute.
- `float max`: The maximum value of the given vector.

Returns the computed vector.

#### `Barycentre()`

```cs
public static Vector2 Barycentre(IList<Vector2> points);
public static Vector2 Barycentre(IList<(Vector2, float)> weightedPoints);
```

Computes the barycentre of the given points.

Barycentre formula:

```txt
                ->                ->
->    weightA * OA + weightN... * ON...
OG = ___________________________________
            weightA + weightN...
```

- `IList<Vector2> points`: The points from which you want to compute the barycentre.
- `IList<(Vector2, float)> weightedPoints`: The points from which you want to compute the barycentre, and their associated weight. The more the weight value, the closer the barycentre to this point.

---

[<= Back to summary](./README.md)