# Core - Runtime - `IOUtility`

Miscellaneous functions for working with IO features.

## Public API

```cs
public static class IOUtility { }
```

### Functions

#### `CopyDirectory`

```cs
public static void CopyDirectory(string sourceDirectory, string targetDirectory)
```

Copies the content of a directory to another recursively.

- `string sourceDirectory`: The path of the directory you want to copy. If relative path given, it's resolved from the project's absolute path.
- `string targetDirectory`: The path of the directory where you want to copy the contents. If relative path given, it's resolved from the project's absolute path.

---

[<= Back to summary](./README.md)