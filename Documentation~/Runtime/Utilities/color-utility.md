# Core - Runtime - `ColorUtility`

Miscellanous functions for working with `Color` and `Color32` values.

## Public API

```cs
public static class ColorUtility { }
```

### Constants

#### `MAX_COLOR_VALUE`

```cs
public const int MAX_COLOR_VALUE = 255;
```

#### `MAX_COLOR_VALUE_F`

```cs
public const float MAX_COLOR_VALUE_F = 255f;
```

### Functions

#### `ToHexRGB()`

```cs
public static string ToHexRGB(Color color);
public static string ToHexRGB(Color32 color);
```

Converts the given color into an hexadecimal string value.

- `Color color`: The color to convert.
- `Color32 color`: The color to convert.

Returns the *color string* with format `RRGGBB`.

#### `ToHexRGBA()`

```cs
public static string ToHexRGBA(Color color);
public static string ToHexRGBA(Color32 color);
```

Converts the given color into an hexadecimal string value.

- `Color color`: The color to convert.
- `Color32 color`: The color to convert.

Returns the *color string* with format `RRGGBBAA`.

#### `FromHex()`

```cs
public static bool FromHex(ref Color color, string hexString);
public static bool FromHex32(ref Color32 color, string hexString);
```

Sets the given color values from the given hexadecimal string.

- `ref Color color`: The color to override.
- `string hexString`: The hexadecimal string (like `F9F9F9`, `F9F9F9F9`, `#F9F9F9`, `#F9F9F9F9`).
- `ref Color32 color`: The color to override.

Returns `true` if the string has been parsed successfully, otherwise `false`.

```cs
public static Color FromHex(string hexString);
public static Color32 FromHex(string hexString);
```

Creates a color value from the given hexadecimal string.

- `string hexString`: The hexadecimal string (like `F9F9F9`, `F9F9F9F9`, `#F9F9F9`, `#F9F9F9F9`).

Returns the created color value.

---

[<= Back to summary](./README.md)