# Core - Runtime - `Math`

Miscellaneous functions for mathematics.

## Public API

```cs
public static class Math { }
```

### Constants

#### `APPROXIMATION_5`

```cs
public const float APPROXIMATION_5 = 0.00001f;
```

#### `APPROXIMATION_8`

```cs
public const float APPROXIMATION_8 = 0.00000001f;
```

#### `PERCENTS`

```cs
public const float PERCENTS = 100f;
```

### Functions

#### `Approximately()`

```cs
public static bool Approximately(float a, float b);
public static bool Approximately(float a, float b, float epsilon);
```

Checks the two float values are close enough to be considered equals. This is meant to prevent float imprecisions.

- `float epsilon`: The approximation value. Basically, this function returns `true` if `a > (b - epsilon)` and `a < (b + epsilon)`.

Returns `true` if `b` is equals (or very close) to `a`.

#### `Ratio()`

```cs
public static float Ratio(float value, float min, float max, float ratio = 1f);
```

Apply proportions to a given value, between a given min and max, and report it to the given ratio.

- `float value`: The value of you want to compute proportions.
- `float min`: The minimum value.
- `float max`: The maximum value.
- `float ratio = 1f`: The proportion operand.

Returns the computed ratio.

##### Example

```cs
Ratio(50, 0, 100, 1);     // Outputs 0.5
Ratio(50, 0, 100, 20);    // Outputs 10
Ratio(50, -100, 100, 1);  // Outputs 0.75
Ratio(-10, 0, 10, 1);     // Outputs -1
```

#### `Percents()`

```cs
public static float Percents(float value, float min, float max);
public static float Percents(float value, float max);
```

Apply proportions to a given value, between a given min and max, and report it to a percentage.

- `float value`: The value of you want to compute proportions.
- `float min`: The minimum value.
- `float max`: The maximum value.

Returns the computed percentage.

---

[<= Back to summary](./README.md)