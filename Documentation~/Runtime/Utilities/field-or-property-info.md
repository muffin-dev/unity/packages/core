# Core - Runtime - `FieldOrPropertyInfo`

Groups informations about a field or a property being processed through C# reflection.

This is used by [`ReflectionUtility.GetFieldsAndProperties()`](./reflection-utility.md), and is useful to unifies operations over both fields and properties, avoiding code duplications.

## Public API

```cs
public class FieldOrPropertyInfo { }
```

### Fields

#### `Field`

```cs
public FieldInfo Field;
```

Informations about the field. `null` if the element is a property.

#### `Property`

```cs
public PropertyInfo Property;
```

Informations about the property. `null` if the element is a field.

### Functions

#### `GetValue()`

```cs
public object GetValue(object target)
```

Gets the value of this field or property.

- `object target`: The object that owns the field or property to read.

Returns the value of the field or property.

#### `SetValue()`

```cs
public void SetValue(object target, object value)
```

Sets the value of this field or property.

- `object target`: The object that owns the field or property to set.
- `object value`: The value you want to set for the field or property.

#### `TryGetCustomAttribute<T>()`

```cs
public bool TryGetCustomAttribute<T>(out T attribute, bool inherit = false)
  where T : Attribute
```

Tries to get a custom attribute on the current field or property.

- `<T>`: The type of the attribute.
- `out T attribute`: Outputs the found attribute.
- `bool inherit = false`: If enabled, checks if the attribute exists on the parent values.

Returns `true` if the expected attribute has been found on the element, otherwise `false`.

### Properties

#### `Name`

```cs
public string Name { get; }
```

Gets the name of the element.

---

[<= Back to summary](./README.md)