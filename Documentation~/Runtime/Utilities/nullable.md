# Core - Runtime - `Nullable<T>`

Implements `NullObject` pattern.

This is mostly meant to use "null" keys for Dictionaries, which is useful to create default or fallback values. The implementation is inspired by an answer from Fabio Marreco: https://stackoverflow.com/a/22261282

To be clear, a dictionary uses hash codes to avoid doubles. But since `null` doesn't implement the `GetHashCode()`, you can't use `null` as a key value for a dictionary. So, instead of declaring `Dictitonary<MyType, int>`, prefer using `Dictionary<Nullable<MyType>, int>` if you need a `null` key!

## Public API

```cs
public struct Nullable<T> { }
```

### Constructors

```cs
public Nullable(T item) : this(item, item == null)
private Nullable(T item, bool isNull) : this()
```
- `T item`: The nullable item.
- `bool isNull`: Defines if the item is `null` or not.

### Properties

#### `Null`

```cs
public static Nullable<T> Null { get; }
```

Gets a default `Nullable<T>` item.

#### `Item`

```cs
public T Item { get; }
```

Gets the nullable item.

#### `IsNull`

```cs
public bool IsNull { get; }
```

Checks if the item is null.

### Operators

#### `implicit T(Nullable<T>)`

```cs
public static implicit operator T(Nullable<T> nullable)
```

Implicit conversion into the generic type.

#### `implicit Nullable<T>(T)`

```cs
public static implicit operator Nullable<T>(T item)
```

Implicit conversion into a nullable item.

#### `Nullable<T> == object`

```cs
public static bool operator ==(Nullable<T> item, object other)
```

Checks if the nullable item is equal to the other.

#### `Nullable<T> != object`

```cs
public static bool operator !=(Nullable<T> item, object other)
```

Checks if the nullable item is different from the other.

#### `Equals()`

```cs
public override bool Equals(object obj)
```

Checks if the given object is equals to this one.

#### `GetHashCode()`

```cs
public override int GetHashCode()
```

Gets this nullablle item's hash code.

Returns 0 if this nullable is `null`, or get the item's hash code otherwise.

---

[<= Back to summary](./README.md)