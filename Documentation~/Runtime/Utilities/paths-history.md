# Core - Runtime - `PathsHistory`

Keeps a history of selected paths to files and directories.

## Usage

The basic usage of `PathsHistory` is really simple: simply add a path when you select a file or a folder, and get a path to get the one in history.

But the main goal of this utility is to get histories for your own tools. And to do so, you must first create a custom paths history before using it.

We recommend creating an "init" file with `InitializeOnLoad` attribute that creates the histories you need right when the editor launches.

```cs
using UnityEditor;
using MuffinDev.Core;

[InitializeOnLoad]
public static class CustomToolInit
{
    static CustomToolInit()
    {
        PathsHistory.CreateHistory("CustomTool_HistoryKey", 1, EPathOrigin.Relative);
    }
}
```

Having this script in your project will create a new history with key `CustomTool_HistoryKey` that can contain a maximum of 1 path, and store them in relative form if possible.

## Public API

```cs
[Save(EDataScope.User)]
[System.Serializable]
public class PathsHistory : Singleton<PathsHistory>, IEnumerable<PathsCollection> { }
```

### Functions

#### `CreateHistory()`

```cs
public static PathsCollection CreateHistory(string key, int limit = 0, EPathOrigin origin = EPathOrigin.Source);
public static bool CreateHistory(string key, out PathsCollection history, int limit = 0, EPathOrigin origin = EPathOrigin.Source);
```

Creates a new paths collection in this history, identified by the given key.

- `string key`: The identifier of the paths collection to create.
- `int limit = 0,`: The maximum number of paths that can be registered in the collection to create. `0` or negative means unlimited.
- `PathOrigin origin = EPathOrigin.Source`: Defines where the path strings of the collection should start in the collection to create.
- `out PathsCollection history`: Outputs the create paths collection.

Returns the created paths collection, or `null` if the key is already used.

#### `GetHistory()`

```cs
public static PathsCollection GetHistory();
```

Gets the default paths collection.

Returns the default paths collection.

---

```cs
public static PathsCollection GetHistory(string key);
public static bool GetHistory(string key, out PathsCollection history);
```

Gets the paths collection identified by the given key in this history.

- `string key`: The identifier of the paths collection to get.
- `out PathsCollection history`: Outputs the found paths collection.

Returns `true` if a paths collection has been found successfully, otherwise `false`.

---

```cs
public static PathsCollection GetHistory(string key, int limit, EPathOrigin origin = EPathOrigin.Source);
public static void GetHistory(string key, out PathsCollection history, int limit, EPathOrigin origin = EPathOrigin.Source);
```

Gets the paths collection identified by the given key in this history, or create it if it doesn't exist yet.

- `string key`: The identifier of the paths collections to get or create.
- `int limit`: The maximum number of paths that can be registered in the collection to create. `0` or negative means unlimited.
- `EPathOrigin origin = EPathOrigin.Source`: Defines where the path strings of the collection should start in the collection to create.
- `out PathsCollection history`: Outputs the found paths collection.

Returns the found or created paths collection.

#### `ClearHistory()`

```cs
public static bool ClearHistory()
```

Clears the default paths collection.

---

```cs
public static bool ClearHistory(string key)
```

Clears the paths collection identified by the given key in this history.

- `string key`: The identifier of the paths collection to clear.

Returns `true` if a paths collection has been cleared successfully, otherwise `false`.

#### `RemoveHistory()`

```cs
public static bool RemoveHistory(string key)
```

Removes the paths collection identified by the given key from this history.

- `string key`: The identifier of the paths collection to remove.

Returns `true` if a paths collection has been removed successfully, otherwise `false`.

#### `IndexOf()`

```cs
public static int IndexOf(string key)
```

Gets the index of the paths collection identified by the given key in this history.

- `string key`: The identifier of the paths collection to find.

Returns the found paths collection index in this history, or `-1` if no paths collection identified by the given key has been found.

#### `AddPath()`

```cs
public static int AddPath(string path);
public static int AddPaths(IEnumerable<string> paths);
```

Adds given path(s) to the default collection.

- `string path`: The path(s) to add.

Returns `true` if the path(s) has been added successfully, otherwise `false`.

---

```cs
public static bool AddPath(string key, string path);
public static bool AddPaths(string key, IEnumerable<string> paths);
```

Adds given path(s) to the collection identified by the given key in this history.

- `string key`: The identifier of the paths collection in which to add the path.
- `string path`: The path(s) to add.

Returns `true` if the path(s) has been added successfully, or `false` if no paths collection identified by the given key has been found.

#### `GetPath()`

```cs
public static string GetPath(EPathOrigin origin = EPathOrigin.Source, string defaultPath = null);
public static string[] GetPaths(EPathOrigin origin = EPathOrigin.Source);
```

Gets the first path (or all paths) from the default collection.

- `EPathOrigin origin = EPathOrigin.Source`: Defines where the output path string should start.
- `string defaultPath = null`: The value to return if the found paths collection is empty.

Returns the first path (or all paths) from the default paths collection, or the default path if the default collection is empty.

---

```cs
public static string GetPath(string key, string defaultPath = null, EPathOrigin origin = EPathOrigin.Source);
public static bool GetPath(string key, out string path, string defaultPath = null, EPathOrigin origin = EPathOrigin.Source);
public static string[] GetPaths(string key, EPathOrigin origin = EPathOrigin.Source);
public static bool GetPaths(string key, out string[] paths, EPathOrigin origin = EPathOrigin.Source);
```

Gets the first path (or all paths) from the collection identified by the given key in this history.

- `string key`: The identifier of the collection of which to get the path(s).
- `EPathOrigin origin = EPathOrigin.Source`: Defines where the output path string(s) should start.
- `string defaultPath = null`: The value to return if the found paths collection is empty.
- `out string | string[] paths`: Outputs the path(s) from the found collection.

Returns the first path (or all paths) from the found paths collection, or the default path if the found collection is empty or if no collection identified with the given key has been found.

#### `GetPathInfos()`

```cs
public static PathsCollection.PathInfo[] GetPathInfos()
```

Gets the complete paths informations in the default collection.

Returns the default collection's paths informations.

---

```cs
public static PathsCollection.PathInfo[] GetPathInfos(string key);
public static bool GetPathInfos(string key, out PathsCollection.PathInfo[] pathInfos);
```

Gets the complete paths informations in the collection identified by the given key in this history.

- `string key`: The identifier of the collection of which to get the paths informations.
- `out PathsCollection.PathInfo[] pathInfos`: Outputs the paths informations from the found collection.

Returns the found collection's paths informations, or `null` if no paths collection identified by the given key has been found.

#### `SetLimit()`

```cs
public static void SetLimit(int limit)
```

Sets the maximum number of paths accepted by the default collection.

- `int limit)`: The maximum number of paths accepted by the collection. `0` or negative means unlimited.

---

```cs
public static bool SetLimit(string key, int limit)
```

Sets the maximum number of paths accepted by the collection identified by the given key in this history.

- `string key`: The identifier of the collection of which to set the limit.
- `int limit)`: The maximum number of paths accepted by the collection. `0` or negative means unlimited.

Returns `true` if the paths collection's limit has been set successfully, or `false` if no collection identified by the given key has been found.

#### `RemovePath()`

```cs
public static bool RemovePath(string path);
public static bool RemovePaths(IEnumerable<string> paths);
```

Removes the given path(s) from the default collection.

- `string | IEnumerable<string> path`: The path string(s) to remove.

Returns `true` if a path has been removed successfully, or `false` if the path was not registered.

---

```cs
public static bool RemovePath(string key, string path);
public static bool RemovePaths(string key, IEnumerable<string> paths);
```

Removes the given path(s) from the collection identified by the given key in this history.

- `string key`: The identifier of the collection of which to remove the path(s).
- `string | IEnumerable<string> path`: The path string(s) to remove.

Returns `true` if a path has been removed successfully, or `false` if the path was not registered.

### Properties

#### `this[int]`

```cs
public PathsCollection this[int index] { get; }
```

Gets the paths collection at the given index in this history.

- `int index`: The index of the paths collection to get.

Returns the paths collection at the given index in this history.

#### `this[string]`

```cs
public PathsCollection this[string key] { get; }
```

Gets the paths collection identified by the given key in this history.

- `string key`: The identifier of the paths collection to get.

Returns `true` if a paths collection has been found successfully, otherwise `false`.

---

```cs
public PathsCollection this[string key, int limit, EPathOrigin origin = EPathOrigin.Source] { get; }
```

Gets the paths collection identified by the given key in this history, or create it if it doesn't exist yet.

- `string key`: The identifier of the paths collections to get or create.
- `int limit`: The maximum number of paths that can be registered in the collection to create. `0` or negative means unlimited.
- `EPathOrigin origin = EPathOrigin.Source`: Defines where the path strings of the collection should start in the collection to create.

Returns the found or created paths collection.

#### `this[string, EPathOrigin]`

```cs
public string this[string key, EPathOrigin origin] { get; }
public string this[string key, string defaultPath, EPathOrigin origin] { get; }
```

Gets the first path in the collection identified by the given key in this history.

- `string key`: The identifier of the collection of which to get the first path.
- `EPathOrigin origin`: Defines where the output path string should start.
- `string defaultPath`: The value to return if the found paths collection is empty or if no paths collection identified by the given key has been found in this history.

Returns the first path of the found paths collection, or the default path if the found collection is empty or if no collection identified with the given key has been found.

#### `PathsCollections`

```cs
public static PathsCollection[] PathCollections { get; }
```

The path collections in this history.

#### `Count`

```cs
public static int Count { get; }
```

Gets the number of paths collections in this history.

---

[<= Back to summary](./README.md)