# Core - Runtime - `CultureScope`

Sets the culture of a thread. This is meant to be used temporarily, in a `using` statement.

## Usage

```cs
private void ToFile(string path, string text)
{
  // Create a culture invariant scope to serialize data
  using (CultureScope.Invariant)
  {
    File.WriteAllText(path, text);
  }
}
```

## Public API

```cs
public class CultureScope : IDisposable { }
```

### Constructors

```cs
public CultureScope()
```

Applies [`CultureInfo.InvariantCulture`](https://learn.microsoft.com/en-us/dotnet/api/system.globalization.cultureinfo.invariantculture) to the current thread.

---

```cs
public CultureScope(CultureInfo culture)
```

Applies the given culture to the current thread.

- `CultureInfo culture`: The culture to apply.

---

```cs
public CultureScope(Thread thread)
```

Applies [`CultureInfo.InvariantCulture`](https://learn.microsoft.com/en-us/dotnet/api/system.globalization.cultureinfo.invariantculture) to the given thread.

- `Thread thread`: The thread you want to set the culture.

---

```cs
public CultureScope(Thread thread, CultureInfo culture)
```

Applies the given culture to the given thread.

- `Thread thread`: The thread you want to set the culture.
- `CultureInfo culture`: The culture to apply.

### Functions

#### `Dispose()`

```cs
public void Dispose()
```

Called when this object is disposed.

### Properties

#### `Invariant`

```cs
public static CultureScope Invariant { get; }
```

Applies [`CultureInfo.InvariantCulture`](https://learn.microsoft.com/en-us/dotnet/api/system.globalization.cultureinfo.invariantculture) to the current thread.

---

[<= Back to summary](./README.md)