# Core - Documentation - Scopes

Set the state of a code block temporarily. They are meant to be used in `using` statements.

## Summary

- [`CultureScope`](./culture-scope.md): Sets the culture of a thread.

---

[<= Back to *Utilities* summary](../README.md)