# Core - Runtime - `SaveUtility`

Utility class for saving data to files or prefs.

## Usage

### Manual save and load

Here is an example of how to manually save and load date from file.

```cs
using UnityEngine;
using MuffinDev.Core;

[Save("PlayerData.json", EDataScope.Player)]
public class Player : MonoBehaviour
{
  public string playerName = "RandomPlayer";
  public int score = 10000;
  public int level = 20;
  public float experience = 500;

  private void Start()
  {
    SaveUtility.Load(this);
  }

  private void OnDestroy()
  {
    SaveUtility.Save(this);
    Debug.Log("File saved at " + SaveUtility.GetFilePath(this));
  }
}
```

The first time this component is loaded, the save file doesn't exist. So in its `Start()` function `SaveUtility.Load()` does nothing.

If you change the scene or change the play mode in the editor, the `OnDestroy()` function will be called, and so is `SaveUtility.Save()`. This will create a new file at the logged location (on Windows, in your `/AppData` directory). This location is set by the `[Save]` attribute on the class, which defines the scope and the file name. And since we use `EDataScope.Player` in this example, the file will be stored in the persistent data path.

After the file has been created, `SaveUtility.Load()` will now uses the saved file, and so reload the data from that file instead of using the values you may have changed from the editor.

### Auto-save

Auto-save behavior is setup by default if you use [`Singleton<T>`](../Patterns/singleton.md) or [`SOSingleton<T>`](../Patterns/so-singleton.md) classes. But you can apply the same behavior to your own instances by using `SaveUtility.Register()` and  `SaveUtility.Unregister()` functions.

These functions are mostly meant to be used for custom editor tools that has settings that should be saved when before play mode change, before scripts are reloaded, or even before the editor is closed. In game, you'll have to call `SaveUtility.SaveAll()` in order to save the registered items, and so get the same behavior.

## Public API

```cs
public static class SaveUtility { }
```

### Constants

#### `PROJECT_SETTINGS_DIR`

```cs
private const string PROJECT_SETTINGS_DIR = "ProjectSettings";
```

The name of the project settings directory.

#### `USER_SETTINGS_DIR`

```cs
private const string USER_SETTINGS_DIR = "UserSettings";
```

The name of the user settings directory.

#### `TEMP_DIR`

```cs
private const string TEMP_DIR = "Temp";
```

The name of the temporary directory.

### Functions

#### `Save()`

```cs
public static bool Save(object data)
```

Saves the given data following the rules defines from [`SaveAttribute`](../Attributes/save.md) and [`DataScopeAttribute`](../Attributes/data-scope.md) on the object.

- `object data`: The data to save.

Returns `true` if the data has been saved successfully, otherwise `false`.

---

```cs
public static bool Save(object data, string filePath);
public static bool Save(object data, EDataScope scope);
public static bool Save(object data, string filePath, EDataScope scope);
```

Saves the given data into a file at the given path. If `filePath` is not defined, uses the informations from [`SaveAttribute`](../Attributes/save.md) and [`DataScopeAttribute`](../Attributes/data-scope.md) set on the object. And if no file path is defined at all, the data will be saved in [`PlayerPrefs`](https://docs.unity3d.com/ScriptReference/PlayerPrefs.html) if the scope is set to [`EDataScope.Player`](../Enums/data-scope.md), or in [`EditorPrefs`](https://docs.unity3d.com/ScriptReference/EditorPrefs.html) otherwise if available.

If the file path is defined:

- it's resolved from the `/ProjectSettings` directory of this project if the scope is [`EDataScope.Project`](../Enums/data-scope.md)
- it's resolved from the `/UserSettings` directory of this project if the scope is [`EDataScope.User`](../Enums/data-scope.md)
- it's resolved from the [`PathUtility.PersistentDataPath`](./path-utility.md) directory in any other cases

- `object data`: The data to save.
- `string filePath`: The path to the file where the data will be saved. If invalid path given, saves the data in [`PlayerPrefs`](https://docs.unity3d.com/ScriptReference/PlayerPrefs.html) if the scope is set to [`EDataScope.Player`](../Enums/data-scope.md), or in [`EditorPrefs`](https://docs.unity3d.com/ScriptReference/EditorPrefs.html) otherwise if available.
- `EDataScope scope`: The scope of the data to save.

Returns `true` if the data has been saved successfully, otherwise `false`.

#### `Load()`

```cs
public static bool Load<T>(out T obj)
  where T : ScriptableObject;
public static bool Load(out ScriptableObject obj, Type objType);
```

Loads data by overriding a created asset, using serialized content from file or prefs.

This function will try to get the path to the file that contain the serialized content, using [`SaveAttribute`](../Attributes/save.md) and [`DataScopeAttribute`](../Attributes/data-scope.md) attributes defined in the data class.

- `<T>`: The type of the object to create and load.
- `out T |ScriptableObject obj`: Outputs the created object.
- `Type objType`: The type of the object to create and load.

Returns `true` if serialized content has been loaded successfully, otherwise `false`.

---

```cs
public static bool Load(object data);
```

Loads data by overriding the given object, using serialized content from file or prefs.

This function will try to get the path to the file that contain the serialized content, using [`SaveAttribute`](../Attributes/save.md) and [`DataScopeAttribute`](../Attributes/data-scope.md) attributes defined in the data class.

- `object data`: The data object to overwrite.

Returns `true` if serialized content has been loaded successfully, otherwise `false`.

---

```cs
public static bool Load<T>(out T obj, string filePath)
  where T : ScriptableObject;
public static bool Load(out ScriptableObject obj, Type objType, string filePath);
```

Loads data by overriding a created asset, using serialized content from file or prefs.

- `<T>`: The type of the object to create and load.
- `out T |ScriptableObject obj`: Outputs the created object.
- `Type objType`: The type of the object to create and load.
- `string filePath`: The path to the file that contains the serialized content to load.

Returns `true` if serialized content has been loaded successfully, otherwise `false`.

---

```cs
public static bool Load(object data, string filePath);
```

Loads data by overriding the given object, using serialized content from file or prefs.

- `object data`: The data object to overwrite.
- `string filePath`: The path to the file that contains the serialized content to load.

Returns `true` if serialized content has been loaded successfully, otherwise `false`.

#### `Register()`

```cs
public static bool Register(object savableObject);
```

Registers an object that can be saved in batch by calling `SaveAll()`.

- `object savableObject`: The object to register.

Returns `true` if the object has been registered successfully, or `false` if it was already registered.

#### `Unregister()`

```cs
public static bool Unregister(object savableObject);
```

Unregisters an object from the batch save list.

- `object savableObject`: The object to unregister.

Returns `true` if the object has been unregistered successfully, otherwise `false`.

#### `SaveAll()`

```cs
public static void SaveAll();
```

Saves all registered objects.

#### `GetFilePath()`

```cs
public static string GetFilePath(object data);
public static string GetFilePath(object data, EDataScope scope);
```

Gets the path to the file where the data should be saved. This function uses [`SaveAttribute`](../Attributes/save.md) and [`DataScopeAttribute`](../Attributes/data-scope.md) to get that file path.

- `object data`: The object of which to get the save file path.
- `EDataScope scope`: The scope of the data to save. This value overrides the ones from [`SaveAttribute`](../Attributes/save.md) or [`DataScopeAttribute`](../Attributes/data-scope.md) if defined on the given data object.

Returns the found save file path.

---

[<= Back to summary](./README.md)