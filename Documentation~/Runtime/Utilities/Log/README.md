# Core - Documentation - *Log*

Utility for tracking informations about a running process.

This feature is meant to store logs of a whole process and display it all at once instead of logging messages directly in Unity console. It also provides functions to write log messages into a file.

## Summary

- [`ELogLevel`](./log-level.md): Represents the level of a log entry.
- [`ExceptionLogEntry`](./exception-log-entry.md): Represents a log entry that contains an exception.
- [`ILogEntry`](./i-log-entry.md): Represents an entry in a [`Log`](./log.md) instance.
- [`Log`](./log.md): Utility for tracking informations about a running process.
- [`LogEntry`](./log-entry.md): Represents a basic log entry in a [`Log`](./log.md) instance.

---

[<= Back to *Utilities* summary](../README.md)