# Core - Documentation - `LogEntry`

Represents a basic log entry in a [`Log`](./log.md) instance.

## Public API

```cs
public interface ILogEntry { }
```

### Constructors

```cs
public LogEntry(string message, ELogLevel level = ELogLevel.None);
public LogEntry(string message, object data, ELogLevel logLevel = ELogLevel.None);
```

- `string message`: The message that describes this log entry.
- `object data`: Additional data for the log entry.
- `ELogLevel logLevel = ELogLevel.None`: The level of this log entry.

### Properties

#### `LogLevel`

```cs
public ELogLevel LogLevel { get; }
```

The level of this log entry.

#### `Data`

```cs
public object Data { get; }
```

Additional data.

#### `Message`

```cs
public string Message { get; }
```

The message that describes the log entry.

#### `MessageWithDetails`

```cs
public string MessageWithDetails { get; }
```

The message that describes the log entry, including eventual additional informations. This is mainly used to write full log informations into a file or display call stacks.

---

[<= Back to *Log* summary](./README.md)