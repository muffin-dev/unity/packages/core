# Core - Documentation - `ExceptionLogEntry`

Represents a log entry that contains an exception.

## Public API

```cs
public class ExceptionLogEntry : ILogEntry { }
```

### Constructors

```cs
public ExceptionLogEntry(Exception exception, ELogLevel logLevel = ELogLevel.Fatal);
public ExceptionLogEntry(Exception exception, string message, ELogLevel logLevel = ELogLevel.Fatal);
public ExceptionLogEntry(Exception exception, object data, ELogLevel logLevel = ELogLevel.Fatal);
public ExceptionLogEntry(Exception exception, string message, object data, ELogLevel logLevel = ELogLevel.Fatal);
```

- `Exception exception`: The handled exception.
- `string message`: The additional message to the exception.
- `object data`: Additional data that complete exception information.
- `ELogLevel logLevel = ELogLevel.Fatal`: The level of the log entry.

### Properties

#### `LogLevel`

```cs
public ELogLevel LogLevel { get; }
```

The log level of this entry.

#### `Message`

```cs
public string Message { get; }
```

Gets the custom message, or the exception message.

#### `Data`

```cs
public object Data { get; }
```

Additional data.

#### `Exception`

```cs
public Exception Exception { get; }
```

The exception of this log entry.

#### `MessageWithDetails`

```cs
public string MessageWithDetails { get; }
```

Gets the full content of the exception, with eventual additional message.

---

[<= Back to *Log* summary](./README.md)