# Core - Documentation - `ILogEntry`

Represents an entry in a [`Log`](./log.md) instance.

## Public API

```cs
public interface ILogEntry { }
```

### Properties

#### `LogLevel`

```cs
ELogLevel LogLevel { get; }
```

The level of this log entry.

#### `Message`

```cs
string Message { get; }
```

The message that describes the log entry.

#### `MessageWithDetails`

```cs
string MessageWithDetails { get; }
```

The message that describes the log entry, including eventual additional informations. This is mainly used to write full log informations into a file or display call stacks.

---

[<= Back to *Log* summary](./README.md)