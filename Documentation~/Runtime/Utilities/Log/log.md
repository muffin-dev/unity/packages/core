# Core - Documentation - `LogEntry`

Utility for tracking informations about a running process.

Its purpose is to handle messages and context objects, so you can use and display these informations as you want.

## Public API

```cs
public class Log : IEnumerable<ILogEntry> { }
```

### Functions

#### `Add()`

```cs
public void Add(string message, ELogLevel logLevel = ELogLevel.None);
public void Add(string message, object data, ELogLevel logLevel = ELogLevel.None);
public void Add(ILogEntry logEntry);
```

Adds a new log entry with a simple message.

- `string message`: The message that describes the log entry.
- `object data`: The data bound to the log entry.
- `ELogLevel logLevel = ELogLevel.None`: The level of this log entry.
- `ILogEntry logEntry`: The log entry to add.

#### `Message()`

```cs
public void Message(string message, object data = null);
```

Adds a new log entry that contains a simple message.

- `string message`: The message that describes the log entry.
- `ELogLevel logLevel = ELogLevel.None`: The level of this log entry.

#### `Warning()`

```cs
public void Warning(string message, object data = null);
```

Adds a new log entry that contains a warning message.

- `string message`: The message that describes the log entry.
- `ELogLevel logLevel = ELogLevel.None`: The level of this log entry.

#### `Error()`

```cs
public void Error(string message, object data = null);
```

Adds a new log entry that contains an error message.

- `string message`: The message that describes the log entry.
- `ELogLevel logLevel = ELogLevel.None`: The level of this log entry.

#### `Fatal()`

```cs
public void Fatal(string message, object data = null);
```

Adds a new log entry that contains the message of an error that caused a process to fail and exit.

- `string message`: The message that describes the log entry.
- `ELogLevel logLevel = ELogLevel.None`: The level of this log entry.

#### `Exception()`

```cs
public void Exception(Exception exception, ELogLevel logLevel = ELogLevel.Fatal);
public void Exception(Exception exception, string message, ELogLevel logLevel = ELogLevel.Fatal);
public void Exception(Exception exception, object data, ELogLevel logLevel = ELogLevel.Fatal);
public void Exception(Exception exception, string message, object data, ELogLevel logLevel = ELogLevel.Fatal);
```

Adds a new log entry bound to a handled exception.

- `Exception exception`: The exception that have been handled
- `string message`: The message that describes the log entry.
- `object data`: The data bound to the log entry.
- `ELogLevel logLevel = ELogLevel.None`: The level of this log entry.

#### `Filter()`

```cs
public ILogEntry[] Filter(ELogLevel logLevel);
```

Gets an array of log entries that have the given log level or higher.

- `ELogLevel logLevel`: The minimum log level of the entries.

Returns the filtered log entries list.

#### `ToFile()`

```cs
public void ToFile(string path, string title = null);
```

Writes the log entries of this *Log* into a file.

- `string path`: The absolute path to the file.
- `string title = null`: Eventual title of the log file.

### Properties

#### `LogEntries`

```cs
public ILogEntry[] LogEntries { get; }
```

The list of all entries in this log.

#### `HighestLogLevel`

```cs
public ELogLevel HighestLogLevel { get; }
```

Gets the highest log level among the entries in the list.

#### `ContainsFatalErrors`

```cs
public bool ContainsFatalErrors { get; }
```

Checks if this Log contains fatal errors.

#### `ContainsErrors`

```cs
public bool ContainsErrors { get; }
```

Checks if this Log contains errors or fatal errors.

---

[<= Back to *Log* summary](./README.md)