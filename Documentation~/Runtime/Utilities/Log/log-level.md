# Core - Documentation - `LogLevel`

Represents the level of a log entry.

## Public API

```cs
public enum ELogLevel
{
  None = 0,
  Log = 1,
  Warning = 2,
  Error = 3,
  Fatal = 4
}
```

### Values

- **`None`**: Undefined log level.
- **`Log`**: Basic information.
- **`Warning`**: Warning, information about something that may cause a process to produce unexpected results, or invalid configuration.
- **`Error`**: Error that may cause the process to fail, but not necessariliy cancel it.
- **`Fatal`**: Fatal error, that cause a process to fail and exit.

---

[<= Back to *Log* summary](./README.md)