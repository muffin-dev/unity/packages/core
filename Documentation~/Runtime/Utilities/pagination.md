# Core - Runtime - `Pagination`

Utility class for pagination system.

![Pagination inspector](../../Images/pagination.png)

## Public API

```cs
public struct Pagination { }
```

### Constants

#### `DEFAULT_NB_ELEMENTS_PER_PAGE`

```cs
public const int DEFAULT_NB_ELEMENTS_PER_PAGE = 25;
```

### Constructors

```cs
public Pagination(int page, int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE);
public Pagination(int nbElements, int page, int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE);
```

- `int page`: The current page index.
- `int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE`: The number of elements per page.
- `The number of elements per page.`: The number of elements in your paginated ensemble.

### Functions

#### `Paginate()`

```cs
public T[] Paginate<T>(IList<T> list);
public static T[] Paginate<T>(IList<T> list, int page, int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE);
public static T[] Paginate<T>(IList<T> list, out Pagination pagination, int page, int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE)
```

Creates a sub-list of the given one that contains only the elements in the paginated range.

- `<T>`: The type of elements in the given list.
- `IList<T> list`: The list that is paginated
- `int page`: The current page index.
- `int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE`: The number of elements per page.
- `out Pagination pagination`: Outputs the `Pagination` infos of the operation.

Returns the sub-list of the elements to display.

#### `CountPages()`

```cs
public static int CountPages<T>(IList<T> list, int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE);
public static int CountPages(int elementsCount, int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE);
```

Computes the number of pages, given the total number of elements and the number of elements per page.

- `<T>`: The type of elements in the given list.
- `IList<T> list`: The list that is paginated
- `int nbElementsPerPage = DEFAULT_NB_ELEMENTS_PER_PAGE`: The number of elements per page.
- `int elementsCount`: The total number of elements.

Returns the computed number of pages.

### Properties

#### `Page`

```cs
public int Page { get; set; }
```

Gets/sets the current page index (starting at 0).

#### `PagesCount`

```cs
public int PagesCount { get; }
```

Gets the number of pages.

#### `ElementsCount`

```cs
public int ElementsCount { get; set; }
```

Gets/sets the total number of elements.

#### `NbElementsPerPage`

```cs
public int NbElementsPerPage { get; set; }
```

Gets/sets the number of elements per page.

#### `FirstIndex`

```cs
public int FirstIndex { get; }
```

Gets the first index (inclusive) in the paginated range.

#### `LastIndex`

```cs
public int LastIndex { get; }
```

Gets the last index (exclusive) in the paginated range.

### Operators

#### `Equals()`

```cs
public override bool Equals(object other)
```

Checks if the given object is equal to this `Pagination` object.

#### `GetHashCode()`

```cs
public override int GetHashCode()
```

Gets the hash code of this `Pagination` object.

#### `ToString()`

```cs
public override string ToString()
```

Converts this `Pagination` object into a string.

#### `Pagination == Pagination`

```cs
public static bool operator ==(Pagination a, Pagination b)
```

Checks if the given `Pagination` values are equal.

#### `Pagination != Pagination`

```cs
public static bool operator !=(Pagination a, Pagination b)
```

Checks if the given `Pagination` values are different.

#### `Pagination++`

```cs
public static Pagination operator ++(Pagination pagination)
```

Increment the current page index.

#### `Pagination--`

```cs
public static Pagination operator --(Pagination pagination)
```

Decrement the current page index.

#### `Pagination + int`

```cs
public static Pagination operator +(Pagination pagination, int nbPages)
```

Increment the current page index by the given value.

#### `Pagination - int`

```cs
public static Pagination operator -(Pagination pagination, int nbPages)
```

Decrement the current page index by the given value.

---

[<= Back to summary](./README.md)