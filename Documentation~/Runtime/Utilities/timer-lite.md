# Core - Runtime - `TimerLite`

Utility class for creating custom timer, updated manually.

![TimerLite inspector preview](../../Images/timer-lite.png)

## Public API

```cs
[System.Serializable]
public class TimerLite { }
```

### Constructors

```cs
public TimerLite();
public TimerLite(float duration);
public TimerLite(float duration, ElapsedDelegate onElapsed);
public TimerLite(float duration, ElapsedPayloadDelegate onElapsedPayload, object payload = null)
```

- `float duration`: The duration of this timer.
- `ElapsedDelegate onElapsed`: The function to call when this timer is elapsed.
- `ElapsedPayloadDelegate onElapsedPayload`: The function to call when this timer is elapsed, with payload.
- `object payload = null`: The data to send when this timer ends.

### Delegates

#### `ElapsedDelegate()`

```cs
public delegate void ElapsedDelegate();
```

Called when this timer ends.

#### `ElapsedPayloadDelegate()`

```cs
public delegate void ElapsedPayloadDelegate(object paylod);
```

Called when this timer ends, using the input payload.

- `object paylod`: The current payload.

### Events

#### `OnElapsed`

```cs
public event ElapsedDelegate OnElapsed;
```

Called when this timer ends.

#### `OnElapsedPayload`

```cs
public event ElapsedPayloadDelegate OnElapsedPayload;
```

Called when this timer ends, using the input payload.

### Functions

#### `Start()`

```cs
public void Start();
public void Start(float duration);
public void Start(object payload);
public void Start(float duration, object payload);
```

Starts this timer, If the timer is already running, this function just sets the new data.

- `float duration`: The new duration of this timer.
- `object payload`: The data to send when this timer ends.

#### `Restart()`

```cs
public void Restart();
public void Restart(float duration);
public void Restart(object payload);
public void Restart(float duration, object payload);
```

Reset the timer to 0 and starts it.

- `float duration`: The new duration of this timer.
- `object payload`: The data to send when this timer ends.

#### `Update()`

```cs
public bool Update(float deltaTime)
```

Updates this timer.

- `float deltaTime`: The time elapsed since the last update.

Returns `true` if the timer has been updated successfully, otherwise `false`.

#### `Stop()`

```cs
public void Stop()
```

Stops this timer (so it can't be updated anymore).

### Properties

#### `IsRunning`

```cs
public bool IsRunning { get; }
```

Checks if this timer is running.

#### `Duration`

```cs
public float Duration { get; set; }
```

The duration of the timer, in seconds. This value can't be negative.

#### `Payload`

```cs
public object Payload { get; set; }
```

The current payload for this timer.

#### `ElapsedTime`

```cs
public float ElapsedTime { get; }
```

Gets the elapsed time, in seconds.

#### `ElapsedTimeRatio`

```cs
public float ElapsedTimeRatio { get; }
```

Gets the elapsed time, reported between 0 and 1, where 1 means the timer is elapsed, and 0 means the timer has just started.

#### `RemainingTime`

```cs
public float RemainingTime { get; }
```

Gets the remaining time, in seconds.

#### `RemainingTimeRatio`

```cs
public float RemainingTimeRatio { get; }
```

Gets the elapsed time, reported between 0 and 1, where 1 means the timer has just started, and 0 means the timer is elapsed.

---

[<= Back to summary](./README.md)