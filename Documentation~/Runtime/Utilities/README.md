# Core - Documentation - Utilities

## Summary

- [`ColorUtility`](./color-utility.md): Miscellaneous functions for working with `Color` and `Color32` values.
- [`FieldOrPropertyInfo`](./color-utility.md): Groups informations about a field or a property being processed through C# reflection.
- [`IOUtility`](./io-utility.md): Miscellaneous functions for working with IO features.
- [*Log*](./Log/README.md): Utility for tracking informations about a running process.
- [`Math`](./math.md): Miscellaneous functions for mathematics.
- [`Nullable<T>`](./nullable.md): Implements NullObject pattern.
- [`Pagination`](./pagination.md): Utility class for pagination system.
- [`PathsCollection`](./paths-collection.md): Represents a list of paths, identified by a key.
- [`PathsHistory`](./paths-history.md): Keeps a history of selected paths to files and directories.
- [`PathUtility`](./path-utility.md): Miscellaneous functions for working with path strings.
- [`ReflectionUtility`](./reflection-utility.md): Miscellaneous functions for working with C# reflection.
- [`RuntimeObjectUtility`](./runtime-object-utility.md): Miscellaneous functions for working with `Object`s and assets at runtime.
- [`SaveUtility`](./save-utility.md): Utility class for saving data to files or prefs.
- [Scopes](./Scopes/README.md): Set the state of a code block temporarily.
- [`SteppedOperationComponent`](./stepped-operation.md): Utility component for running an operation step by step.
- [`TimerLite`](./timer-lite.md): MUtility class for creating custom timer, updated manually.
- [`VectorUtility`](./vector-utility.md): Miscellaneous functions for working with vectors and vector calculations.

---

[<= Back to *Runtime* summary](../README.md)