# Core - Runtime - `PathUtility`

Miscellaneous functions for working with `Object`s and assets at runtime.

## Public API

```cs
public static class RuntimeObjectUtility { }
```

### Functions

#### `GetTransform()`

```cs
public static Transform GetTransform(Object obj)
```

Gets the `Transform` component of an object that can be placed in a scene.

- `Object obj`: The object from which you want to get the `Transform` component.

Returns the found `Transform` component.

#### `TryGetTransform()`

```cs
public static bool TryGetTransform(Object obj, out Transform transform)
```

Tries to get the `Transform` component of an object that can be placed in a scene.

- `Object obj`: The object from which you want to get the `Transform` component.
- `out Transform transform`: Outputs the found `Transform` component.

Returns `true` if the given object has a `Transform` component, otherwise `false`.

#### `GetGameObject()`

```cs
public static GameObject GetGameObject(Object obj)
```

Gets the `GameObject` to which the given object is attached.

- `Object obj`: The object of which you want to get the attached `GameObject`.

Returns the found attcahed `GameObject`.

#### `TryGetGameObject()`

```cs
public static bool TryGetGameObject(Object obj, out GameObject gameObject)
```

Tries to get `GameObject` to which the given object is attached.

- `Object obj`: The object of which you want to get the attached `GameObject`.
- ` out GameObject gameObject`: Outputs the found attached `GameObject`.

Returns `true` if the given object is attached to a `GameObject`, otherwise `false`.

---

[<= Back to summary](./README.md)