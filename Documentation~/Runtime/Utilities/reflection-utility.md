# Core - Runtime - `ReflectionUtility`

Miscellaneous functions for working with C# reflection.

## Public API

```cs
public static class ReflectionUtility { }
```

### Constants

#### `INSTANCE_FLAGS`

```cs
public const BindingFlags INSTANCE_FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
```

Targets elements declared on the instance, public and non-public.

#### `STATIC_FLAGS`

```cs
public const BindingFlags STATIC_FLAGS = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
```

Targets elements declared static, public and non-public.

#### `UNITY_CSHARP_ASSEMBLY_NAME`

```cs
public const string UNITY_CSHARP_ASSEMBLY_NAME = "Assembly-CSharp";
```

The name of the main Unity C# scripts assembly.

#### `UNITY_EDITOR_CSHARP_ASSEMBLY_NAME`

```cs
public const string UNITY_EDITOR_CSHARP_ASSEMBLY_NAME = "Assembly-CSharp-Editor";
```

The name of the main Unity C# scripts for editor assembly.

### Fields

#### `NotProjectAssemblies`

```cs
public static readonly string[] NotProjectAssemblies =
{
  "mscorlib",
  "UnityEngine",
  "UnityEditor",
  "Unity.",
  "System",
  "Mono.",
  "netstandard",
  "Microsoft"
};
```

The name of the assemblies that are not part of the project.

### Functions

#### `GetAllAssemblies()`

```cs
public static Assembly[] GetAllAssemblies(IList<string> excludedAssembliesPrefixes = null)
```

Gets all assemblies in the current app domain.

- `IList<string> excludedAssembliesPrefixes = null`: The prefixes of the assembly names to exclude from this query.

Returns the found assemblies.

#### `GetProjectAssemblies()`

```cs
public static Assembly[] GetProjectAssemblies()
```

Get all assemblies related to the current Unity project.

Returns the found assemblies.

#### `GetAllTypesAssignableFrom()`

```cs
public static Type[] GetAllTypesInProjectAssignableFrom<T>();
public static Type[] GetAllTypesAssignableFrom<T>(Assembly assembly);
public static Type[] GetAllTypesAssignableFrom<T>(IList<Assembly> assemblies);
public static Type[] GetAllTypesInProjectAssignableFrom(Type parentType);
public static Type[] GetAllTypesAssignableFrom(Type parentType, Assembly assembly);
public static Type[] GetAllTypesAssignableFrom(Type parentType, IList<Assembly> assemblies);
```

Gets all the types that implement the given type in the given assembly.

*NOTE: This function is inspired by this answer by Nick VanderPyle:* https://stackoverflow.com/a/8645519/6699339

- `<T>`: The type of the class you want to find inheritors.
- `Assembly assembly`: The assembly where you want to find the given type implementations.
- `IList<Assembly> assemblies`: The assemblies where you want to find the given type implementations.
- `Type parentType`: The type of the class you want to find inheritors. You can pass a generic type in this paramater, by using the "open generic" syntax. As an example, if the parent class is `MyGenericClass<T>`, use `typeof(MyGenericClass<>)`, without any value inside the less-than/greater-than characters.

Returns an enumerable that contains all the found types.

#### `GetFieldAndProperty()`

```cs
public static FieldOrPropertyInfo GetFieldOrProperty<T>(string name, BindingFlags bindingFlags = INSTANCE_FLAGS);
public static FieldOrPropertyInfo GetFieldOrProperty(Type type, string name, BindingFlags bindingFlags = INSTANCE_FLAGS);
public static FieldOrPropertyInfo GetFieldOrProperty(object target, string name, BindingFlags bindingFlags = INSTANCE_FLAGS);
```

Gets the named field or property info.

- `<T>`: The type from which you want to get the reflection data.
- `BindingFlags bindingFlags = INSTANCE_FLAGS`: The binding flags to use for querying the reflection data.
- `Type type`: The type from which you want to get the reflection data.
- `object target`: The object from which you want to get the type's reflection data.

Returns the found field or property info.

#### `GetFieldsAndProperties()`

```cs
public static FieldOrPropertyInfo[] GetFieldsAndProperties<T>(BindingFlags bindingFlags = INSTANCE_FLAGS);
public static FieldOrPropertyInfo[] GetFieldsAndProperties(Type type, BindingFlags bindingFlags = INSTANCE_FLAGS);
public static FieldOrPropertyInfo[] GetFieldsAndProperties(object target, BindingFlags bindingFlags = INSTANCE_FLAGS);
```

Extracts all the fields and properties of a given type.

- `<T>`: The type from which you want to get the reflection data.
- `BindingFlags bindingFlags = INSTANCE_FLAGS`: The binding flags to use for querying the reflection data.
- `Type type`: The type from which you want to get the reflection data.
- `object target`: The object from which you want to get the type's reflection data.

Returns the informations about all the fields and properties on the target type.

#### `GetFieldsAndProperties()`

```cs
public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute<TType, TAttribute>(BindingFlags bindingFlags = INSTANCE_FLAGS)
  where TAttribute : Attribute;
public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute<TAttribute>(object target, BindingFlags bindingFlags = INSTANCE_FLAGS)
  where TAttribute : Attribute;
public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute(Type type, Type attributeType, BindingFlags bindingFlags = INSTANCE_FLAGS);
public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute(object target, Type attributeType, BindingFlags bindingFlags = INSTANCE_FLAGS);
```

Extracts all the fields and properties of a given type that have a specific attribute.

- `<TType>`: The type from which you want to get the reflection data.
- `<TAttribute>`: The type of the attribute the filtered fields and properties must have.
- `BindingFlags bindingFlags = INSTANCE_FLAGS`: 
- `object target`: The object from which you want to get the type's reflection data.
- `Type type`: The type from which you want to get the reflection data.
- `Type attributeType`: The type of the attribute the filtered fields and properties must have.

Returns the informations about all the fields and properties on the target type.

#### `GetFieldsAndProperties()`

```cs
public static object GetNestedObject(object source, string propertyPath);
public static T GetNestedObject<T>(object source, string propertyPath);
```

Gets a reference or value of an object given a property path (like `myObject.myContainer[3].myProperty`).

This function is inspired by the SpacePuppy Unity Framework: https://github.com/lordofduct/spacepuppy-unity-framework-4.0/blob/master/Framework/com.spacepuppy.core/Editor/src/EditorHelper.cs

- `<T>`: The type of the expected target object.
- `object source`: The object in which you want to get the nested object.
- `string propertyPath`: The property path for navigating to the expected object (like `myObject.myContainer[3].myProperty`).

Returns the found nested object, or `null` if the path didn't lead to a valid object.

---

[<= Back to summary](./README.md)