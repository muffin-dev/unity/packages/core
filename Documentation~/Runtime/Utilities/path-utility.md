# Core - Runtime - `PathUtility`

Miscellaneous functions for working with path strings.

## Public API

```cs
public static class PathUtility { }
```

### Constants

#### `DEFAULT_DIRECTORY_SEPARATOR`

```cs
public const char DEFAULT_DIRECTORY_SEPARATOR = '/';
```

The character used by default as directory separator for formatted path strings.

#### `ProjectPath`

```cs
public static readonly string ProjectPath;
```

The absolute path to this project.

In the editor, this is the path to the project's `/Assets` folder. In build, it's the path to the persistent data directory. This value is meant to be used by any editor or in-game feature that needs to store persistent data.

#### `DataPath`

```cs
public static readonly string DataPath;
```

The asbolute path to the game content directory.

In the editor, this is the path to the project's /Assets folder. In build, it's the path to the game executable. Note that in build, the game executable directory may be readonly, so you should use `PersistentDataPath` to store persistent custom data.

See also: https://docs.unity3d.com/ScriptReference/Application-dataPath.html

#### `PeristentDataPath`

```cs
public static readonly string PersistentDataPath;
```

The absolute path to the persistent data directory.

This path varies from platforms, but is guaranteed to be writable, so you can use it to store persistent custom data.

See also: https://docs.unity3d.com/ScriptReference/Application-peristentDataPath.html

### Functions

#### `ToPath()`

```cs
public static string ToPath(string str);
public static void ToPath(ref string str);
public static string ToPath(string str, char separator);
public static void ToPath(ref string str, char separator);
```

Ensures given string represents a path, so it doesn't contain forbidden characters, and uses only slash as drectory separator.

- `string str`: The string to format.
- `char separator`: The character to use as directory separator.

Returns the formatted string.

#### `ToPaths()`

```cs
public static void ToPaths(IList<string> strs);
public static void ToPaths(IList<string> strs, char separator);
```

Ensures given strings represent paths, so they don't contain forbidden characters, and use only slash as directory separator.

- `IList<string> strs`: The string to format.
- `char separator`: The character to use as directory separator.

Returns the formatted string.

#### `ToAbsolutePath()`

```cs
public static string ToAbsolutePath(string path)
```

If the given string represents a relative path, it's combined with `ProjectPath` (which is the path to the current project in the editor, and `PeristentDataPath` in build) to make it absolute.

- `string path`: The path string to convert.

Returns the absolute path, or an empty string if the input path is not valid.

#### `RelativePath()`

```cs
public static string ToRelativePath(string path)
```

If the given string represents an absolute path to a project file or directory, this function makes it relative to `ProjectPath` (which is the path to the current project in the editor, and `PeristentDataPath` in build). If the given string is already relative or doesn't target the project, the input string is returned as is.

- `string path`: The path string to convert.

Returns the relative path, or an empty string if the input path is not valid.

#### `IsProjectPath()`

```cs
public static bool IsProjectPath(string path)
```

Checks if the given path is relative to the current Unity project's root directory.

- `string path`: The path you want to check. If it's a relative path, this function always returns `true`.

Returns `true` if the given path is relative to the current Unity project's root directory, otherwise `false`.

#### `CombinePaths()`

```cs
public static string CombinePaths(string path, params string[] relPaths)
```

Combines and resolves a path with relative ones.

- `string path`: The path from which you want to resolve the relative paths. Note that if this path is relative, the path is resolved from the directory where the code is executed. Also, if the path targets a file, relative paths are resolved from the last directory.
- `params string[] relPaths`: The relative paths to resolve.

Returns the processed path.

#### `GetDirectoryPath()`

```cs
public static string GetDirectoryPath(string path)
```

Gets the path to the parent directory of this one. If this path already targets a directory, this function returns the input path as is.

- `string path`: The path of which you want to get the directory path.

Returns the processed path.

---

[<= Back to summary](./README.md)