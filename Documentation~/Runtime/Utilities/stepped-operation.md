# Core - Runtime - `SteppedOperation`

Utility component for running an operation step by step.

This is initially designed for debug purposes, but can still be used at runtime.

![Stepped Operation component inspector](../../Images/stepped-operation-demo.png)

Stepped operation is basically a corourine, and makes use of [`IEnumerator`](https://docs.microsoft.com/dotnet/api/system.collections.ienumerator). By using enumerators, you can create "pausable" functions, using the `yield` statement.

This component acts like a coroutine, but is an abstraction for dealing with timers, and optionally use manual stepping.

## Usage

You can use this component in two ways:

- Using the static function `Init()` from anywhere, which will instantiate a new `GameObject` and add this component to run the operation
- Inherit from this component to add parameters and setup the stepped operation as you want

### Options

- **Continuous**: If enabled, the component will iterate through the stepped operation automatically using the interval.
- **Interval**: The time (in seconds) between each step. This is used if continuous mode is enabled, but acts as a cooldown for `Step()` otherwise.

### Example

The following example illustrates how to use the inheritance method:

```cs
using System.Collections;
using UnityEngine;
public class SteppedOperationDemo : SteppedOperationComponent
{
  [SerializeField]
  private int _nbInstances = 10;

  [SerializeField]
  private Vector3 _origin = Vector3.zero;

  [SerializeField]
  private Vector3 _target = new Vector3(10, 5, 0);

  // Initialize the operation to run step by step
  private void Start()
  {
    Init(InstantiatePoints());
  }

  // Use mouse click to step over the operation (using built-in Input Manager)
  protected override void Update()
  {
    if (Input.GetMouseButtonDown(0))
      Step();

    base.Update();
  }

  // Instantiate spheres along the segment from origin to target
  private IEnumerator InstantiatePoints()
  {
    for (int i = 0; i < _nbInstances; i++)
    {
      GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
      obj.name = $"Primitive [{i}]";
      obj.transform.position = Vector3.Lerp(_origin, _target, i / (_nbInstances - 1f));
      // Pause the method, waiting for the next step
      yield return null;
    }
  }

  // Draw origin and target points in the scene
  private void OnDrawGizmos()
  {
    Gizmos.color = Color.red;
    Gizmos.DrawSphere(_origin, .05f);

    Gizmos.color = Color.green;
    Gizmos.DrawSphere(_target, .05f);
  }
}
```

![Stepped Operation demo animation](../../Images/stepped-operation-demo.gif)

## Public API

```cs
public class SteppedOperationComponent : MonoBehaviour { }
```

### Functions

#### `Init()`

```cs
public static SteppedOperationComponent Init(IEnumerator operation, string gameObjectName);
```

Creates an empty `GameObject` in the scene with a `SteppedOperationComponent` that will run the given peration step by step.

- `IEnumerator operation`: The operation you want to run step by step.
- `string gameObjectName`: The name of the game object created to run the stepped operation.

Returns the created `SteppedOperationComponent` instance.

---

```cs
public void Init(IEnumerator operation, bool force = false);
```

Initializes the stepped operation. Note that if an operation is already running, 

- `IEnumerator operation`: The operation you want to run step by step.
- `bool force = false`: If an operation has already been initialized and this parameter is enabled, the previous operation is simply replaced.

#### `Step()`

```cs
public bool Step();
```

Steps manually through the operation. You must call `Init()` to setup the operation before calling this method. Also, this function won't do anything if continuous mode is enabled.

### Properties

#### `Continuous`

```cs
public bool Continuous { get; set; }
```

If enabled, the component will iterate through the stepped operation automatically using the interval.

#### `Interval`

```cs
public float Interval { get; set; }
```

The time (in seconds) between each step. This is used if continuous mode is enabled, but acts as a cooldown for `Step()` otherwise.

#### `HasNextStep`

```cs
public bool HasNextStep { get; }
```

Checks if the stepped operation has a next step available.

---

[<= Back to summary](./README.md)