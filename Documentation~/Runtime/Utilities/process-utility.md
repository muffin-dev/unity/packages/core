# Core - Runtime - `ProcessUtility`

Miscellaneous utility methods for working with external processed.

## Public API

```cs
public static class PathUtility { }
```

### Functions

#### `ExecCommand()`

```cs
public static bool ExecCommand(string command, out string output, string workingDirectory = null, bool showWindow = false)
```

Executes a command using the shell application synchronously.

- `string command`: The command you want to run.
- `out string output`: Outputs the standard shell output if the command has been successfully executed, otherwise the standard error output.
- `string workingDirectory = null`: The working directory where the command will be executed.
- `bool showWindow = false`: If enabled, the shell application is displayed.

Returns `true` if the command has been executed successfully, otherwise `false`.

---

[<= Back to summary](./README.md)