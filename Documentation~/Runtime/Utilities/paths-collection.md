# Core - Runtime - `PathsCollection`

Represents a list of paths, identified by a key. You can use this class through [`PathsHistory`](./paths-history.md) to create histories for "recent items" menus or just keep track of a previously selected item.

## Public API

```cs
[System.Serializable]
public class PathsCollection : IList<string> { }
```

### Constructors

```cs
public PathsCollection();
public PathsCollection(string key, int limit = 0, EPathOrigin origin = EPathOrigin.Source);
public PathsCollection(string key, IEnumerable<string> paths, int limit = 0, EPathOrigin origin = EPathOrigin.Source);
```

- `string key`: The key used to identify this collection.
- `int limit = 0`: The maximum number of paths stored in this collection. `0` or negative means unlimited.
- `EPathOrigin origin = EPathOrigin.Source`: Defines where the path strings in this collection should start.
- `IEnumerable<string> paths`: The paths to add to this collection.

### Functions

#### `Add()`

```cs
public void Add(string path);
public void Add(IEnumerable<string> paths);
```

Adds given path(s) to this collection. If a path string is already in this collection, the "add date" of the existing entry is updated.

- `string path | IEnumerable<string>`: The path(s) to add.

#### `Clear()`

```cs
public void Clear()
```

Clears this paths collection.

#### `Contains()`

```cs
public bool Contains(string path)
```

Checks if this collection contains the given path string.

- `string path`: The path to check.

Returns `true` if this collection contains the given path string, otherwise `false`.

#### `CopyTo()`

```cs
public void CopyTo(string[] array, int arrayIndex)
```

Copies the elements of this collection to an array.

- `string[] array`: The array where paths are copied.
- `int arrayIndex`: The index at which copy begins.

#### `IndexOf()`

```cs
public int IndexOf(string path)
```

Gets the index of the given path string in this collection.

- `string path`: The path of which to get the index.

Returns the found index, or `-1` if the given path string is not in this collection.

#### `Insert()`

```cs
public void Insert(int index, string path)
```

Inserts a path at the given index in this collection.

- `int index`: The index at which you want to insert the given path string. This operation does nothing if the given index exceeds the defined `Limit`.
- `string path`: The path string to insert.

#### `GetPaths()`

```cs
public string[] GetPaths(EPathOrigin origin = EPathOrigin.Source)
```

Gets the paths in this collection.

- `EPathOrigin origin = EPathOrigin.Source`: Defines where the output path strings should start.

Returns the path strings in this collection.

#### `Remove()`

```cs
public bool Remove(string path);
public bool Remove(IEnumerable<string> paths);
```

Removes the given path string(s) from this collection.

- `string | IEnumerable<string> path`: The path(s) to remove from this collection.

Returns `true` if an entry has been removed successfully, or `false` if the path string was not registered.

#### `RemoveAt()`

```cs
public void RemoveAt(int index);
```

Removes the path at the given index.

- `int index`: The index at which to remove the entry.

### Properties

#### `this[int]`

```cs
public string this[int index] { get; set; }
```

Gets/sets the path string at the given index in this collection.

- `int index`: The index of the path string to get or set.

Returns the found path string.

#### `Key`

```cs
public string Key { get; }
```

The identifier of this paths collection.

#### `Paths`

```cs
public string[] Paths { get; }
```

The paths in this collection.

#### `PathInfos`

```cs
public PathInfo[] PathInfos { get; }
```

Gets the complete informations about the paths in this collection.

#### `Limit`

```cs
public int Limit { get; set; }
```

The maximum number of paths stored in this collection. `0` or negative means unlimited.

#### `Origin`

```cs
public EPathOrigin Origin { get; set; }
```

Defines where a path string should start.

#### `Count`

```cs
public int Count { get; }
```

Gets the number of paths in this collection.

#### `IsReadOnly`

```cs
public bool IsReadOnly { get; }
```

Defines if this list is readonly.

---

[<= Back to summary](./README.md)