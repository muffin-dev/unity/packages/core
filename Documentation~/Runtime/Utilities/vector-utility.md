# Core - Runtime - `VectorUtility`

Miscellaeous functions for working with vectors.

## Public API

```cs
public static class VectorUtility { }
```

### Functions

#### `Barycentre()`

```cs
public static Vector3 Barycentre(IList<Vector3> points);
public static Vector2 Barycentre(IList<Vector2> points);
public static Vector3 Barycentre(IList<(Vector3, float)> weightedPoints);
public static Vector2 Barycentre(IList<(Vector2, float)> weightedPoints);
```

Computes the barycentre of the given points.

Barycentre formula:

```txt
                ->                ->
->    weightA * OA + weightN... * ON...
OG = ___________________________________
            weightA + weightN...
```

- `IList<Vector3> points`: The points from which you want to compute the barycentre.
- `IList<(Vector3, float)> weightedPoints`: The points from which you want to compute the barycentre, and their associated weight. The more the weight value, the closer the barycentre to this point.

#### `ToMesh()`

```cs
public static Mesh ToMesh(IList<Vector3> points)
```

Converts the given points list into a [`Mesh`](https://docs.unity3d.com/ScriptReference/Mesh.html), using that path as a contour.

***WARNING**: This function uses a very naive solution, which could lead to bad results. It takes the barycentre of all the sampled points of the given path, and creates the mesh triangles from that barycentre to every sampled points. It works fine with simple use cases though.*

- `IList<Vector3> points`: The points that represent the contour of the mesh to create.

Returns the created [`Mesh`](https://docs.unity3d.com/ScriptReference/Mesh.html).

---

[<= Back to summary](./README.md)