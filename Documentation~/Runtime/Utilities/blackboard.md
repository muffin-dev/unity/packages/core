# Core - Runtime - `Blackboard`

Utility to store data using keys.

## Public API

```cs
public class Blackboard { }
```

### Functions

####  `Get()`

```cs
public T Get<T>(string key);
public T Get<T>(string key, T defaultValue);
```

Gets an item by its key.

- `<T>`: The expected type of the item.
- `string key`: The key of the item to get.
- `T defaultValue`: The value to output if the key doesn't exist or the item doesn't have the expected type.

Returns the found item value if the key exists and the item has the expected type, otherwise return the given default value.

---

```cs
public bool Get<T>(string key, out T value);
public bool Get<T>(string key, out T value, T defaultValue);
```

Gets an item by its key.

- `<T>`: The expected type of the item.
- `string key`: The key of the item to get.
- `T defaultValue`: The value to output if the key doesn't exist or the item doesn't have the expected type.
- `out T value`: Outputs the item value, or the default value if the key doesn't exist or the item doesn't have the expected type.

Returns `true` if the key exists and the item has the expected type, otherwise `false`.

---

```cs
public object Get(string key);
public object Get(string key, object defaultValue);
```

Gets an item by its key.

- `string key`: The key of the item to get.
- `object defaultValue`: The value to output if the key doesn't exist.

Returns the found item value if the key exists, or the given default value.

---

```cs
public bool Get(string key, out object value);
public bool Get(string key, out object value, object defaultValue);
```

Gets an item by its key.

- `string key`: The key of the item to get.
- `object defaultValue`: The value to output if the key doesn't exist.
- `out object value`: Outputs the item value, or the default value if the key doesn't exist.

Returns `true` if the key exists, otherwise `false`.

#### `Set()`

```cs
public void Set<T>(string key, T value);
public void Set(string key, object value);
```

Adds or replaces an item with the given key.

- `<T>`: The type of the item value.
- `string key`: The key of the item to add or replace.
- `object value`: The new value of the item.

#### `ContainsKey()`

```cs
public bool ContainsKey(string key)
```

Checks if this blackboard contains an item with the iven key.

- `string key`: The key of the item to check.

Returns `true` if this blackboard contains an item with the given key, otherwise `false`.

#### `Remove()`

```cs
public bool Remove(string key)
```

Removes an item from this blackboard.

- `string key`: The key of the item to remove.

Returns `true` if the key exists, otherwise `false`.

#### `Remove()`

```cs
public void Clear()
```

Removes all items in this blackboard.

### Properties

#### `this[string]`

```cs
public object this[string key] { get; set; }
```

Gets/sets the item with the given key.

- `string key`: The key of the item to get or set.

Returns the found item value if the key exists, otherwise `null`.

#### `this[string, object]`

```cs
public object this[string key, object defaultValue] { get; set; }
```

Gets the item with the given key.

- `string key`: The key of the item to get.
- `object defaultValue`: The value to output if the key doesn't exist.

Returns the found item value if the key exists, or the given default value.

---

[<= Back to summary](./README.md)