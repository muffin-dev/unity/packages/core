# Core - Runtime - `ColorScope`

Custom GUI Scope for temporarily changing the GUI color.

## Usage

```cs
using UnityEngine;
using UnityEditor;
using MuffinDev.Core;
public class DemoColorScopeWindow : EditorWindow
{
    [MenuItem("Demos/Color Scope")]
    private static void Open()
    {
        GetWindow<DemoColorScopeWindow>("Color Scope Demo");
    }

    private void OnGUI()
    {
        EditorGUILayout.TextField("First field", "...");
        using (new ColorScope(Color.yellow))
        {
            EditorGUILayout.TextField("Second field", "...");
        }
    }
}
```

![ColorScope window preview](../../Images/color-scope.png)

## Public API

```cs
public class ColorScope : GUI.Scope { }
```

### Constructor

```cs
public ColorScope(Color color);
public ColorScope(EColor color, bool ignoreAlpha = false);
```

Begins a scope with custom GUI color.

- `Color color`: The new GUI color.
- `bool ignoreAlpha = false`: If enabled, the alpha component of the given color is ignored.

---

[<= Back to summary](./README.md)