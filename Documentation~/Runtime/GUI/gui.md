# Core - Runtime - `MoreGUI`

Miscellaneous functions and values for drawing user interfaces.

## Public API

```cs
public static class MoreGUI { }
```

### Constants

#### `HEIGHT_?`

```cs
public const float HEIGHT_XL = 48f;
public const float HEIGHT_L = 36f;
public const float HEIGHT_M = 28f;
public const float HEIGHT_S = 20f;
public const float HEIGHT_XS = 16f;
```

Height values for drawing wider fields or buttons.

#### `WIDTH_?`

```cs
public const float WIDTH_XL = 180f;
public const float WIDTH_L = 140f;
public const float WIDTH_M = 112f;
public const float WIDTH_S = 80f;
public const float WIDTH_XS = 40f;
```

Width values for drawing wider fields or buttons.

### `H_MARGIN`

```cs
public const float H_MARGIN = 2f;
```

Size of an horizontal margin.

### `V_MARGIN`

```cs
public const float V_MARGIN = 2f;
```

Size of an vertical margin.

#### `Height?`

```cs
public static readonly GUILayoutOption HeightXL = GUILayout.Height(HEIGHT_XL);
public static readonly GUILayoutOption HeightL = GUILayout.Height(HEIGHT_L);
public static readonly GUILayoutOption HeightM = GUILayout.Height(HEIGHT_M);
public static readonly GUILayoutOption HeightS = GUILayout.Height(HEIGHT_S);
public static readonly GUILayoutOption HeightXS = GUILayout.Height(HEIGHT_XS);
```

#### `Width?`

```cs
public static readonly GUILayoutOption WidthXL = GUILayout.Width(WIDTH_XL);
public static readonly GUILayoutOption WidthL = GUILayout.Width(WIDTH_L);
public static readonly GUILayoutOption WidthM = GUILayout.Width(WIDTH_M);
public static readonly GUILayoutOption WidthS = GUILayout.Width(WIDTH_S);
public static readonly GUILayoutOption WidthXS = GUILayout.Width(WIDTH_XS);
```

---

[<= Back to summary](./README.md)