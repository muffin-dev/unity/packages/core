# Core - Runtime - `EnabledScope`

Custom GUI Scope for drawing disabled fields.

## Usage

```cs
using UnityEditor;
using MuffinDev.Core;
public class DemoEnabledScopeWindow : EditorWindow
{
    [MenuItem("Demos/Enabled Scope")]
    private static void Open()
    {
        GetWindow<DemoEnabledScopeWindow>("Enabled Scope Demo");
    }

    private void OnGUI()
    {
        EditorGUILayout.TextField("Enabled Field", "...");
        using (new EnabledScope(false))
        {
            EditorGUILayout.TextField("Disabled Field", "...");
        }
    }
}
```

![EnabledScope window preview](../../Images/enabled-scope.png)

## Public API

```cs
public class EnabledScope : GUI.Scope { }
```

### Constructor

```cs
public EnabledScope(bool enabled = true)
```

Begins a scope with disabled fields.

- `bool enabled = true`: If true, make the fields in this block are enabled.

---

[<= Back to summary](./README.md)