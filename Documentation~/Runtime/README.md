# Core - Documentation - Runtime

## Summary

- [Custom Attributes](./Attributes/README.md): Custom property attributes.
- [Enums](./Enums/README.md): Miscellaneous enumerations.
- [Events](./Events/README.md): Typed `UnityEvent` implementations
- [Extensions](./Extensions/README.md): Extensions for native, Unity and custom types.
- [GUI](./GUI/README.md): Functions for drawing GUI.
- [Interfaces](./Interfaces/README.md): Miscellaneous interfaces.
- [Patterns](./Patterns/README.md): Design patterns implementations, adapted for Unity.
- [Utilities](./Utilities/README.md): Miscellaneous utilities and helpers.

---

[<= Back to package's summary](../README.md)