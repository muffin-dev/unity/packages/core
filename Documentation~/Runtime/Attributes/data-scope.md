# Core - Runtime - `IndentAttribute`

Defines a scope for a data, in order to filter or sort it when displaying it in an inspector or saving it into files or prefs.

## Public API

```cs
public class DataScopeAttribute : Attribute { }
```

### Constructors

```cs
public DataScopeAttribute(EDataScope scope);
```

- `EDataScope scope`: The scope of the data.

### Properties

#### `Scope`

```cs
public EDataScope Scope { get; }
```

The scope of the data.

---

[<= Back to summary](./README.md)