# Core - Runtime - `FilePathFieldAttribute`


Draws a field to select a file path, with a "Browse..." button next to it. 

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class FilePathFieldAttributeDemo : MonoBehaviour
{
    [FolderPathField]
    public string folderPathField = "";

    [FilePathField]
    public string filePathField = "";
}
```

![File Path Field attribute demo preview](../../Images/path-fields-attribute.png)

## Public API

```cs
public class FilePathFieldAttribute : PropertyAttribute { }
```

### Constructors

```cs
public FilePathFieldAttribute();
public FilePathFieldAttribute(string extension);
public FilePathFieldAttribute(string[] filters);
public FilePathFieldAttribute(string extension, string panelTitle);
public FilePathFieldAttribute(string[] filters, string panelTitle);
public FilePathFieldAttribute(string extension, string panelTitle, string historyKey);
public FilePathFieldAttribute(string[] filters, string panelTitle, string historyKey);
```

- `string panelTitle`: The title of the "open file" panel.
- `string extension`: The expected extension of the selected file.
- `string[] filters`: The accepted extensions of the file. Must follow this pattern: `{ "Image files", "png,jpg,jpeg", "All files", "*" }`
- `string historyKey`: The key to use to save the path in paths history.

### Properties

#### `PanelTitle`

```cs
public string PanelTitle { get; }
```

The title of the "open file" panel.

#### `Extension`

```cs
public string Extension { get; }
```

The expected extension of the selected file.

#### `Filters`

```cs
public string[] Filters { get; }
```

The accepted extensions of the file. Must follow this pattern: `{ "Image files", "png,jpg,jpeg", "All files", "*" }`

#### `HistoryKey`

```cs
public string HistoryKey { get; }
```

The key to use to save the path in paths history.

---

[<= Back to summary](./README.md)