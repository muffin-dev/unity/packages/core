# Core - Runtime - `SeparatorAttribute`

Draws a separator line before the next property.

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class SeparatorAttributeDemo : MonoBehaviour
{
    public string regularField = "First field";

    [Separator]

    public string separatedField = "Second field";
}
```

![Separated attribute demo preview](../../Images/separator-attribute.png)

## Public API

```cs
public class SeparatorAttribute : PropertyAttribute { }
```

### Constructors

```cs
public SeparatorAttribute(bool wide = false)
```

- `bool wide = false`: If enabled, the separator will have the exact current view width.

### Properties

#### `Wide`

```cs
public bool Wide { get; }
```

If enabled, the separator will have the exact current view width.

---

[<= Back to summary](./README.md)