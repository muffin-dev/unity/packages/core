# Core - Runtime - `IndentAttribute`

Indents a property in the inspector.

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class IndentAttributeDemo : MonoBehaviour
{
    public string regularField = "Not indented";

    [Indent]
    public string indentedField1 = "Indent level = 1";

    [Indent(2)]
    public string indentedField2 = "Indent level = 1";
}
```

![Indent attribute demo preview](../../Images/indent-attribute.png)

## Public API

```cs
public class IndentAttribute : PropertyAttribute { }
```

### Constructors

```cs
public IndentAttribute(int levels = 1);
```

Indents this property in the inspector.

- `int levels = 1`: The number of levels to add to the current indent level

### Properties

#### `Levels`

```cs
public int Levels { get; }
```

The number of levels to add to the current indent level.

---

[<= Back to summary](./README.md)