# Core - Runtime - `FolderPathFieldAttribute`

Draws a field to select a folder path, with a "Browse..." button next to it. 

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class FolderPathFieldAttributeDemo : MonoBehaviour
{
    [FolderPathField]
    public string folderPathField = "";

    [FilePathField]
    public string filePathField = "";
}
```

![Folder Path Field attribute demo preview](../../Images/path-fields-attribute.png)

## Public API

```cs
public class FolderPathFieldAttribute : PropertyAttribute { }
```

### Constructors

```cs
public FolderPathFieldAttribute();
public FolderPathFieldAttribute(string panelTitle);
public FolderPathFieldAttribute(string panelTitle, string historyKey, int historyLimit = 1);
```

- `string panelTitle`: The title of the "open folder" panel.
- `string historyKey`: The key to use to save the path in paths history.
- `int historyLimit = 1`: The number of paths that can be stored in the paths history using the given history key.

### Properties

#### `PanelTitle`

```cs
public string PanelTitle { get; }
```

The title of the "open folder" panel.

#### `HistoryKey`

```cs
public string HistoryKey { get; }
```

The key to use to save the path in paths history.

---

[<= Back to summary](./README.md)