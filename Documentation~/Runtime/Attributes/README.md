# Core - Runtime - Attributes

## Summary

- [`AnimCurveAttribute`](./anim-curve.md): Bounded animation curve editor.
- [`DataScopeAttribute`](./data-scope.md): Defines a scope for a data, in order to filter or sort it when displaying it in an inspector or saving it into files or prefs.
- [`FilePathFieldAttribute`](./file-path-field.md): Draws a field to select a file path, with a "Browse..." button next to it. 
- [`FolderPathFieldAttribute`](./folder-path-field.md): Draws a field to select a folder path, with a "Browse..." button next to it. 
- [`IndentAttribute`](./indent.md): Indents a property in the inspector.
- [`ProgressBarAttribute`](./progress-bar.md): Draws a progress bar next to a numeric field in the inspector.
- [`ReadonlyAttribute`](./readonly.md): Makes a property readonly.
- [`SaveAttribute`](./save.md): Marks a class, field or a property that it can be saved to files or prefs by following given rules.
- [`SeparatorAttribute`](./separator.md): Draws a separator line before the next property.

---

[<= Back to *Runtime* summary](../README.md)