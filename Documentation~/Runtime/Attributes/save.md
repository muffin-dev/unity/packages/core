# Core - Runtime - `SaveAttribute`

Marks a class, field or a property that it can be saved to files or prefs by following given rules. It can work in combination with [`DataScopeAttribute`](../Attributes/data-scope.md) in ordder to define where the data should be saved.

## Public API

```cs
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
public class SaveAttribute : Attribute { }
```

### Constructors

```cs
public SaveAttribute(string filePath);
public SaveAttribute(string filePath, EDataScope scope);
```

Makes the data savable in a file at the given path, using the [`SaveUtility`](../Utilities/save-utility.md) helper class. You can also define a scope for saving the data using the [`DataScopeAttribute`](../Attributes/data-scope.md).

If relative path given:

- it's resolved from the /ProjectSettings directory of this project if a scope is defined and set to [`EDataScope.Project`](../Enums/data-scope.md)
- it's resolved from the /UserSettings directory of this project if a scope is defined and set to [`EDataScope.User`](../Enums/data-scope.md)
- it's resolved from [`PathUtility.PersistentDataPath`](../Utilities/path-utility.md) directory in any other cases

- `string filePath`: The path to the file where data is saved.
- `EDataScope scope`: The scope for saving the data.

---

```cs
public SaveAttribute(EDataScope scope, bool preferPrefs = false);
```

Makes the data savable in a file or prefs, using the [`SaveUtility`](../Utilities/save-utility.md) helper class. By default, the file will have the class' name.

- `EDataScope scope`: The scope for saving the data.
- `bool preferPrefs = false`: If enabled, prefer saving the data in prefs instead of a file, using [`UnityEngine.PlayerPrefs`](https://docs.unity3d.com/ScriptReference/PlayerPrefs.html) if the scope is [`EDataScope.Player`](../Enums/data-scope.md), or [`UnityEngine.PlayerPrefs`](https://docs.unity3d.com/ScriptReference/EditorPrefs.html) otherwise.

### Properties

#### `FilePath`

```cs
public string FilePath { get; }
```

The path to the file where the data should be stored.

#### `Scope`

```cs
public EDataScope? Scope { get; }
```

The scope for saving the data. Note that the data scope can also be defined using [`DataScopeAttribute`](../Attributes/data-scope.md), and will always be overriden by that attribute.

#### `PreferPrefs`

```cs
public bool PreferPrefs { get; }
```

If enabled, prefer saving the data in prefs instead of a file, using [`UnityEngine.PlayerPrefs`](https://docs.unity3d.com/ScriptReference/PlayerPrefs.html) if the scope is [`EDataScope.Player`](../Enums/data-scope.md), or [`UnityEngine.PlayerPrefs`](https://docs.unity3d.com/ScriptReference/EditorPrefs.html) otherwise.

---

[<= Back to summary](./README.md)