# Core - Runtime - `AnimCurveAttribute`

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class AttributeTester : MonoBehaviour
{
  [AnimCurve(EColor.Cyan)]
  [Tooltip("Simple curve with time and value between 0 and 1.")]
  public AnimationCurve curve0To1 = AnimationCurve.EaseInOut(0, 0, 1, 1);

  [AnimCurve(5, 10, EColor.Orange)]
  [Tooltip("Curve with time between 0 and 5, and value between 0 and 10.")]
  public AnimationCurve curveTime5Value10 = AnimationCurve.EaseInOut(0, 0, 5, 10);

  [AnimCurve(0, 1, -5, 5, EColor.Purple)]
  [Tooltip("Curve with time between 0 and 1, and value between -5 and 5.")]
  public AnimationCurve curveNegative5To5 = AnimationCurve.EaseInOut(0, -5, 1, 5);
}
```

## Public API

```cs
public class AnimCurveAttribute : PropertyAttribute { }
```

### Constructors

```cs
public AnimCurveAttribute();
```

Creates an `AnimCurveAttribute` instance with time and value clamped between 0 and 1.

```cs
public AnimCurveAttribute(EColor curveColor);
```

Creates an `AnimCurveAttribute` instance with no bounds.

- `EColor curveColor`: The color of the curve in the editor.

```cs
public AnimCurveAttribute(float maxTime, float maxValue, EColor curveColor = EColor.Green);
```

Creates an `AnimCurveAttribute` instance with time clamped between 0 and the given max time, and value clamped between 0 and given max value.

- `float maxTime`: The maximum time (along the X axis).
- `float maxValue`: The maximum value (along the Y axis).
- `EColor curveColor = EColor.Green`: The color of the curve in the editor.

```cs
public AnimCurveAttribute(float minTime, float maxTime, float minValue, float maxValue, EColor curveColor = EColor.Green);
```

Creates an `AnimCurveAttribute`instance with time clamped between the given min and max time, and value clamped between the min and max value.

- `float minTime`: The minimum time (along the X axis).
- `float maxTime`: The maximum time (along the X axis).
- `float minValue`: The minimum value (along the Y axis).
- `float maxValue`: The maximum value (along the Y axis).
- `EColor curveColor = EColor.Green`: The color of the curve in the editor.

### Properties

#### `MinTime`

```cs
public float MinTime { get; }
```

The minimum time value (along the X axis).

#### `MaxTime`

```cs
public float MaxTime { get; }
```

The maximum time value (along the X axis).

#### `MinValue`

```cs
public float MinValue { get; }
```

The minimum curve value (along the Y axis).

#### `MaxValue`

```cs
public float MaxValue { get; }
```

The maximum curve value (along the Y axis).

#### `CurveColor`

```cs
public Color CurveColor { get; }
```

The color of the curve in the editor.

#### `Ranges`

```cs
public Rect Ranges { get; }
```

Creates a `Rect` representing the curve's ranges:

- x is min time
- y is min value
- width is (abs(min time) + abs(max time)
- height is (abs(min value) + abs(max value)

---

[<= Back to summary](./README.md)