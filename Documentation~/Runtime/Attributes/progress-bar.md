# Core - Runtime - `ProgressBarAttribute`

Draws a progress bar next to a numeric field in the inspector.

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class ProgressBarAttributeDemo : MonoBehaviour
{
    [Tooltip("Simple float field between 0 and 1, unclamped, displayed as a progress bar.")]
    [ProgressBar]
    public float ProgressBarRatio = .8f;

    [Tooltip("Float field representing a percentage, unclamped, displayed as a progress bar.")]
    [ProgressBar(100, "%", EColor.Green)]
    public float ProgressBarPercents = 25f;

    [Tooltip("Int field from -100 to 100, clamped, displayed as a progress bar.")]
    [ProgressBar(-100, 100, EColor.Red, true, true)]
    public int ProgressBarNegative100ToPositive100 = 50;
}
```

![ProgressBar attribute demo preview](../../Images/progress-bar-attribute.png)

## Public API

```cs
public class ProgressBarAttribute : PropertyAttribute { }
```

### Constructors

```cs
public ProgressBarAttribute(EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
public ProgressBarAttribute(string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
public ProgressBarAttribute(string prefix, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
```

Displays a progress bar that represents a value from 0 to 1.

```cs
public ProgressBarAttribute(float max, string prefix, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
public ProgressBarAttribute(float max, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
public ProgressBarAttribute(float max, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
```

Displays a progress bar that represents a value between 0 and maximum.

```cs
public ProgressBarAttribute(float min, float max, string prefix, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
public ProgressBarAttribute(float min, float max, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
public ProgressBarAttribute(float min, float max, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false);
```

Displays a progress bar that represents a value between minimum and maximum.

- `float min`: The minimum value of the progress bar.
- `float max`: The maximum value of the progress bar.
- `EColor color = DEFAULT_COLOR`: The color of the progress bar.
- `string prefix`: The prefix to write before the value on the progress bar.
- `string suffix`: The suffix to write after the value on the progress bar.
- `bool clamp = false`: If enabled, the field's value will be clamped between min and max.
- `bool replaceField = false`: If enabled, the field is completely hidden by the progress bar.
- `bool readonlyField = false`: Should the field be readonly?

### Functions

#### `GetLabel()`

```cs
public string GetLabel(float value) { }
```

Gets the label of the progress bar, using the defined prefix and suffix.

- `float value`: The current value of the field.

Returns the computed label.

### Properties

#### `Min`

```cs
public float Min { get; }
```

The minimum value of the progress bar.

#### `Max`

```cs
public float Max { get; }
```

The maximum value of the progress bar.

#### `Prefix`

```cs
public string Prefix { get; }
```

The prefix to write before the value on the progress bar.

#### `Suffix`

```cs
public string Suffix { get; }
```

The suffix to write after the value on the progress bar.

#### `Color`

```cs
public Color Color { get; }
```

The color of the progress bar.

#### `Clamp`

```cs
public bool Clamp { get; }
```

If enabled, the field's value will be clamped between min and max.

#### `ReplaceField`

```cs
public bool ReplaceField { get; }
```

If enabled, the field is completely hidden by the progress bar.

#### `Readonly`

```cs
public bool Readonly { get; }
```

Should the field be readonly?

---

[<= Back to summary](./README.md)