# Core - Runtime - `ReadonlyAttribute`

Makes a property readonly: visible but not editable in the inspector.

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class ReadonlyAttributeDemo : MonoBehaviour
{
    public string regularField = "Editable";

    [Readonly]
    public string readonlyField = "Not editable";
}
```

![Readonly attribute demo preview](../../Images/readonly-attribute.png)

## Public API

```cs
public class ReadonlyAttribute : PropertyAttribute { }
```

---

[<= Back to summary](./README.md)