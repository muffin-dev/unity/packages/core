# Core - Runtime - `EPathOrigin`

Defines where a path string should start. Used by [`PathsHistory`](../Utilities/paths-history.md) and [`PathsCollection`](../Utilities/paths-collection.md) to process paths.

## Public API

```cs
public enum EPathOrigin
{
  Source = 0,
  Absolute = 1,
  Relative = 2,
}
```

- `Source`: Keep the path string as is.
- `Absolute`: If a string represents a relative path, resolve it from the current project's absolute path.
- `Relative`: If a string represents an absolute path that leads to the current project, make it relative from the current project's root directory.

---

[<= Back to summary](./README.md)