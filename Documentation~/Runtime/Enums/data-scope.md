# Core - Runtime - `EDataScope`

Defines a scope for a data, in order to filter or sort it when displaying it in an inspector or saving it into files or prefs.

## Public API

```cs
public enum EDataScope
{
  Player = 0,
  Project = 1,
  User = 2,
  Temp = 3
}
```

- `Player`: The data is meant to be displayed in game and saved in game files.
- `Project`: Only for editor context. The data should be displayed in Project Settings window, and shared with other members of the development team.
- `User`: Only for editor context. The data should be displayed in Preferences window, and available only for the current user.
- `Temp`: The data is meant to be hidden, and saved into temporary folders.

---

[<= Back to summary](./README.md)