# Core - Runtime - `EColor`

Defines a color using flags, so you can blend simple color values. Use the [`EColorExtensions`](../Extensions/ecolor.md) class to resolve the values.

## Usage

```cs
using UnityEngine;
using MuffinDev.Core;
public class EColorDemo : MonoBehaviour
{
    public EColor colorEnum = EColor.Orange | EColor.Alpha100;
    public Color color;

    private void OnValidate()
    {
        color = colorEnum.ToColor();
    }

    private void Reset()
    {
        color = colorEnum.ToColor();
    }
}
```

By using `OnValidate()` and `Reset()` messages, everytime the `colorEnum` value changes in the editor will update the `color` property, using the `ToColor()` extension to convert the `EColor` value into a Unity `Color` instance.

## Public API

```cs
[System.Flags]
public enum EColor
{
  // RVB components
  Red     = 1 << 0,
  Green   = 1 << 2,
  Blue    = 1 << 4,
  // Half RVB components
  Maroon  = 1 << 1,
  Lime    = 1 << 3,
  Navy    = 1 << 5,

  // Alpha
  Alpha100 = 1 << 6,
  Alpha87 = 1 << 7,
  Alpha75 = 1 << 8,
  Alpha50 = 1 << 9,
  Alpha25 = 1 << 10,
  Alpha12 = 1 << 11,
  Alpha0 = 1 << 12,

  // Tints
  Clear   = 0,
  Black   = Alpha100,
  Grey    = Maroon | Green | Navy | Alpha100,
  White   = Red | Green | Blue | Alpha100,

  // Other colors
  Yellow  = Red | Green | Alpha100,
  Orange  = Red | Lime | Alpha100,
  Olive   = Maroon | Green | Alpha100,
  Purple  = Maroon | Navy | Alpha100,
  Magenta = Red | Blue | Alpha100,
  Pink    = Red | Green | Blue | Alpha100,
  Teal    = Green | Navy | Alpha100,
  Cyan    = Green | Blue | Alpha100,
  Azure   = Lime | Blue | Alpha100,

  // Aliases
  Fuchsia = Magenta,
  Aqua = Cyan,
}
```

---

[<= Back to summary](./README.md)