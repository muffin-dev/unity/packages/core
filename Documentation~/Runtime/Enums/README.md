# Core - Documentation - Enums

## Summary

- [`EColor`](./ecolor.md): Defines a color using flags, so you can blend simple color values.
- [`EDataScope`](./data-scope.md): Defines a scope for a data, in order to filter or sort it when displaying it in an inspector or saving it into files or prefs.
- [`EPathOrigin`](./path-origin.md): Defines where a path string should start. Used by [`PathsHistory`](../Utilities/paths-history.md) and [`PathsCollection`](../Utilities/paths-collection.md) to process paths.

---

[<= Back to *Runtime* summary](../README.md)