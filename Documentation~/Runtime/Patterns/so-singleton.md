# Core - Runtime - `SOSingleton<T>`

Implements the Singleton design pattern, adapted for `ScriptableObject`s.

Note that the `ScriptableSingleton<TT>` already exists since Unity 2020, but has several limitations:

- Its inspector is disabled by default, so you must create a custom inspector if you want the properties to be edited
- You must save changes on disk manually
- Available only in the editor

`SOSingleton<T>` is designed to resolve these limitations, which means that it can be use to create unique configs for both runtime and editor contexts, and even saved as a config file on disk.

Note that the `OnEnable()` and `OnDisable()` callbacks are private. You should override `Set()` and `Unset()` instead when using `SOSingleton<T>`, since those functions are called only if an instance is the actual singleton instance.

## Usage

The following code creates a `SOSingleton<T>` meant to be saved on file. When saved, it logs the path to the file in the console. Note the use of the [`SaveAttribute`](../Attributes/save.md), which tells both the name and scope of the file where the data should be saved.

```cs
using UnityEngine;
using MuffinDev.Core;

[Save("save.json", EDataScope.Player)]
public class SavableAsset : SOSingleton<SaveAsset>, ISavable
{
  public string playerName = "Chipset";
  public int score = 2000;

  private void BeforeSave()
  {
    Debug.Log("Savable Asset is about to be saved to file at " + SaveUtility.GetFilePath(this));
  }

  private void AfterLoad()
  {
    Debug.Log("Savable Asset has been loaded from file from " + SaveUtility.GetFilePath(this));
  }
}
```

The following code creates a component that uses the `SaveAsset` singleton. Switch to play mode to initialize the component with the values loaded from the singleton's file, and switch to edit mode to save the changes.

```cs
using UnityEngine;
public class SaveAssetDemo : MonoBehaviour
{
  public int score = 0;

  private void OnEnable()
  {
    score = SaveAsset.Instance.score;
    score += 100;
  }

  private void OnDisable()
  {
    SaveAsset.Instance.score = score;
  }
}
```

## Public API

```cs
public abstract class SOSingleton<T> : SOSingletonBase
  where T : SOSingleton<T> { }
```

### Properties

#### `Instance`

```cs
public static T Instance { get; }
```

Gets the unique instance of this singleton.

#### `I`

```cs
public static T I { get; }
```

Alias of `Instance` property.

## Protected API

### Properties

#### `Set()`

```cs
protected virtual void Set()
```

Called when this asset is set as the singleton instance.

#### `Unet()`

```cs
protected virtual void Unet()
```

Called when this asset is unloaded but was the singleton instance.

---

[<= Back to summary](./README.md)