# Core - Documentation - Patterns

Design patterns implementations, adapted for Unity.

## Summary

- [`Singleton<T>`](./singleton.md): Implements a thread-safe version of the Singleton pattern.
- [`SOSingleton<T>`](./so-singleton.md): Implements the Singleton design pattern, adapted for `ScriptableObject`s.

---

[<= Back to *Runtime* summary](../README.md)