# Core - Runtime - `Singleton<T>`

Implements a thread-safe version of the Singleton pattern. The goal is to make an instance of a class unique and accessible from anywhere.

Note that Singletons are considered as anti-patterns, and may be used only for prototyping quickly or if it's the only solution in your software architecture.

The implementation is inspired from: https://jlambert.developpez.com/tutoriels/dotnet/implementation-pattern-singleton-csharp

## Usage

```cs
using MuffinDev.Core;
public class SingletonDemo : Singleton<SingletonDemo>
{
  protected override void Init()
  {
    Debug.Log("Initialize singleton instance");
  }

  public void Test()
  {
    Debug.Log("Test method called");
  }
}
```

```cs
using UnityEngine;
public class SingletonDemoComponent : MonoBehaviour
{
  private void Start()
  {
    SingletonDemo.Instance.Test();
  }
}
```

## Public API

```cs
public abstract class Singleton<T>
  where T : Singleton<T>, new() { }
```

### Constructors

```cs
protected Singleton()
```

### Properties

#### `Instance`

```cs
public static T Instance { get; }
```

Gets the singleton instance of this class.

#### `Instance`

```cs
public static T I { get; }
```

Alias of `Instance` property.

## Protected API

### Functions

#### `Init()`

```cs
protected virtual void Init()
```

Called when this object is set as the singleton instance.

---

[<= Back to summary](./README.md)