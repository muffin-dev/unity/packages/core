# Core - Documentation - Patterns

Design patterns implementations, adapted for Unity.

## Summary

- [`ISavable`](./savable.md): Qualifies a class as being savable, so it can receive callbacks before being saved or after being loaded.

---

[<= Back to *Runtime* summary](../README.md)