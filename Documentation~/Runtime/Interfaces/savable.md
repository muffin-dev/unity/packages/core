# Core - Runtime - `ISavable`

Qualifies a class as being savable, so it can receive callbacks before being saved or after being loaded using [`SaveUtility`](../Utilities/save-utility.md).

## Public API

```cs
public interface ISavable { }
```

### Functions

#### `BeforeSave()`

```cs
void BeforeSave()
```

Called before this object is saved, using [`SaveUtility`](../Utilities/save-utility.md).

#### `AfterLoad()`

```cs
void AfterLoad()
```

Called after this object is loaded, using [`SaveUtility`](../Utilities/save-utility.md).

---

[<= Back to summary](./README.md)