# Core - Runtime - `QuaternionEvent`

Custom `UnityEvent`s for `Quaternion` values.

## Public API

```cs
[System.Serializable]
  public class QuaternionEvent : UnityEvent<Quaternion> { }
```

---

[<= Back to summary](./README.md)