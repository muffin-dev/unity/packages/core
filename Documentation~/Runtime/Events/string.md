# Core - Runtime - `StringEvent`

Custom `UnityEvent`s for `string` values.

## Public API

```cs
[System.Serializable]
  public class StringEvent : UnityEvent<string> { }
```

---

[<= Back to summary](./README.md)