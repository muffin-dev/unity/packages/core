# Core - Runtime - `VoidEvent`

Custom `UnityEvent`s with no parameters.

## Public API

```cs
[System.Serializable]
  public class VoidEvent : UnityEvent { }
```

---

[<= Back to summary](./README.md)