# Core - Runtime - `ObjectEvent`

Custom `UnityEvent`s for `Object` instances.

## Public API

```cs
[System.Serializable]
  public class ObjectEvent : UnityEvent<Object> { }
```

---

[<= Back to summary](./README.md)