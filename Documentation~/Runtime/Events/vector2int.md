# Core - Runtime - `Vector2IntEvent`

Custom `UnityEvent`s for `Vector2Int` values.

## Public API

```cs
[System.Serializable]
  public class Vector2IntEvent : UnityEvent<Vector2Int> { }
```

---

[<= Back to summary](./README.md)