# Core - Runtime - `Vector2Event`

Custom `UnityEvent`s for `Vector2` values.

## Public API

```cs
[System.Serializable]
  public class Vector2Event : UnityEvent<Vector2> { }
```

---

[<= Back to summary](./README.md)