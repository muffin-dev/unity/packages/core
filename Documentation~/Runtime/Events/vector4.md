# Core - Runtime - `Vector4Event`

Custom `UnityEvent`s for `Vector4` values.

## Public API

```cs
[System.Serializable]
  public class Vector4Event : UnityEvent<Vector4> { }
```

---

[<= Back to summary](./README.md)