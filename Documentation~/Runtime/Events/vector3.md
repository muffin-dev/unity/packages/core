# Core - Runtime - `Vector3Event`

Custom `UnityEvent`s for `Vector3` values.

## Public API

```cs
[System.Serializable]
  public class Vector3Event : UnityEvent<Vector3> { }
```

---

[<= Back to summary](./README.md)