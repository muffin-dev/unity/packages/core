# Core - Runtime - `ColorEvent`

Custom `UnityEvent`s for `Color` values.

## Public API

```cs
[System.Serializable]
  public class ColorEvent : UnityEvent<Color> { }
```

---

[<= Back to summary](./README.md)