# Core - Runtime - `IntEvent`

Custom `UnityEvent`s for `int` values.

## Public API

```cs
[System.Serializable]
  public class IntEvent : UnityEvent<int> { }
```

---

[<= Back to summary](./README.md)