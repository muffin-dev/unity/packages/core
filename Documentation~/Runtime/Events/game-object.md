# Core - Runtime - `GameObjectEvent`

Custom `UnityEvent`s for `GameObject` instances.

## Public API

```cs
[System.Serializable]
  public class GameObjectEvent : UnityEvent<GameObject> { }
```

---

[<= Back to summary](./README.md)