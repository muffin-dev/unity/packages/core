# Core - Runtime - `FloatEvent`

Custom `UnityEvent`s for `float` values.

## Public API

```cs
[System.Serializable]
  public class FloatEvent : UnityEvent<float> { }
```

---

[<= Back to summary](./README.md)