# Core - Runtime - `Vector3IntEvent`

Custom `UnityEvent`s for `Vector3Int` values.

## Public API

```cs
[System.Serializable]
  public class Vector3IntEvent : UnityEvent<Vector3Int> { }
```

---

[<= Back to summary](./README.md)