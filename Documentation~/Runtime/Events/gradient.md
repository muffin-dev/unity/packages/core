# Core - Runtime - `GradientEvent`

Custom `UnityEvent`s for `Gradient` values.

## Public API

```cs
[System.Serializable]
  public class GradientEvent : UnityEvent<Gradient> { }
```

---

[<= Back to summary](./README.md)