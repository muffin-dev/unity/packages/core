# Core - Runtime - `TransformEvent`

Custom `UnityEvent`s for `Transform` instances.

## Public API

```cs
[System.Serializable]
  public class TransformEvent : UnityEvent<Transform> { }
```

---

[<= Back to summary](./README.md)