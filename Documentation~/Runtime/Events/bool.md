# Core - Runtime - `BoolEvent`

Custom `UnityEvent`s for `bool` values.

## Public API

```cs
[System.Serializable]
  public class BoolEvent : UnityEvent<bool> { }
```

---

[<= Back to summary](./README.md)