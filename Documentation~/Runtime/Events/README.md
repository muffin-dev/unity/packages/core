# Core - Runtime - Events

## Summary

- [`BoolEvent`](./bool.md)
- [`ColorEvent`](./color.md)
- [`FloatEvent`](./float.md)
- [`GameObjectEvent`](./game-object.md)
- [`GradientEvent`](./gradient.md)
- [`IntEvent`](./int.md)
- [`ObjectEvent`](./object.md)
- [`QuaternionEvent`](./quaternion.md)
- [`StringEvent`](./string.md)
- [`TransformEvent`](./transform.md)
- [`Vector2Event`](./vector2.md)
- [`Vector2IntEvent`](./vector2int.md)
- [`Vector3Event`](./vector3.md)
- [`Vector3IntEvent`](./vector3int.md)
- [`Vector4Event`](./vector4.md)
- [`VoidEvent`](./void.md)

---

[<= Back to *Runtime* summary](../README.md)