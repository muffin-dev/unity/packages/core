# Core - Documentation - Constant values

Some constant values are used widely in the project, mostly to unify the content of menus and shortcuts.

## Public API

### Runtime constants

Runtime constants are accessible from the `Constants` class in the runtime directory.

#### `COMPANY_NAME`

```cs
public const string COMPANY_NAME = "Muffin Dev";
```

The name of the company. This value is used as base path for menus and shortcuts.

#### `BASE_HELP_URL`

```cs
public const string BASE_HELP_URL = "http://unity.muffindev.com/packages";
```

Base URL for online documentation of the framework. You must concatenate your own path starting with "/".

#### `DEMOS_SUBMENU`

```cs
public const string DEMOS_SUBMENU = "/Demos";
```

The submenu for demo features (from *Add Component* button, `Asset > Create` and the main toolbar menus. Starts with "/". You must concatenate your own path starting with "/".

#### `DEMOS_DEFINE`

```cs
public const string DEMOS_DEFINE = "MUFFINDEV_DEMOS";
```

The define symbol used to enable demos of the framework.

#### `ADD_COMPONENT_MENU`

```cs
public const string ADD_COMPONENT_MENU = COMPANY_NAME;
```

Base path used for `CreateAssetMenuAttribute`. You must concatenate your own path starting with "/".

#### `ADD_COMPONENT_MENU_DEMOS`

```cs
public const string ADD_COMPONENT_MENU_DEMOS = COMPANY_NAME + DEMOS_SUBMENU;
```

Base path used for `CreateAssetMenuAttribute` with demo components. You must concatenate your own path starting with "/".

#### `CREATE_ASSET_MENU`

```cs
public const string CREATE_ASSET_MENU = COMPANY_NAME;
```

Base path used for `CreateAssetMenuAttribute`. You must concatenate your own path starting with "/".

#### `CREATE_ASSET_MENU_DEMOS`

```cs
public const string CREATE_ASSET_MENU_DEMOS = COMPANY_NAME + DEMOS_SUBMENU;
```

Base path used for `CreateAssetMenuAttribute` with demo assets. You must concatenate your own path starting with "/".

### Editor constants

Runtime constants are accessible from the `EditorConstants` class in the runtime directory.

#### `TOOLBAR_MENU_PREFIX`

```cs
public const string TOOLBAR_MENU_PREFIX = "Tools/";
```

Prefix used for main toolbar menus. Ends with "/".

#### `TOOLBAR_MENU`

```cs
public const string TOOLBAR_MENU = TOOLBAR_MENU_PREFIX + Constants.COMPANY_NAME;
```

Base path used for custom menus in the main toolbar. You must concatenate your own path starting with "/".

#### `EDITOR_WINDOW_MENU`

```cs
public const string EDITOR_WINDOW_MENU = TOOLBAR_MENU;
```

Base path used for custom menus in the main toolbar for opening custom editor windows. You must concatenate your own path starting with "/".

#### `EDITOR_WINDOW_MENU_DEMOS`

```cs
public const string EDITOR_WINDOW_MENU_DEMOS = EDITOR_WINDOW_MENU + Constants.DEMOS_SUBMENU;
```

Base path used for custom menus in the main toolbar for opening demo custom editor windows. You must concatenate your own path starting with "/".

#### `PREFERENCES`

```cs
public const string PREFERENCES = Constants.COMPANY_NAME;
```

Base path used for custom Preferences menus. You must concatenate your own path starting with "/".

#### `PROJECT_SETTINGS`

```cs
public const string PROJECT_SETTINGS = Constants.COMPANY_NAME;
```

Base path used for custom Project Settings menus. You must concatenate your own path starting with "/".