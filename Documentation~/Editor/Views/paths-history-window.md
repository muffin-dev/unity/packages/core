# Core - Editor - `PathsHistoryWindow`

Utility window to see and manage the content of [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

## Public API

```cs
public class PathsHistoryWindow : EditorWindow { }
```

@todo

---

[<= Back to summary](./README.md)