# Core - Editor - Windows

## Summary

- [`PathsHistoryWindow`](./paths-history-window.md): Utility window to see and manage the content of [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

---

[<= Back to *Editor* summary](../README.md)