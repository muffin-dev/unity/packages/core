# Core - Editor - `ScriptableObjectExtensions`

Extensions for `ScriptableObject` instances.

## Public API

```cs
public static class ScriptableObjectExtensions { }
```

### Functions

#### `GetScriptPath()`

```cs
public static string GetScriptPath(this ScriptableObject obj);
```

Gets the path to the source script used by the given `ScriptableObject`.

- `this ScriptableObject obj`: The `ScriptableObject` you want to get the script path.

Returns the found script path, or `null` if the path to the script file can't be found.

---

[<= Back to summary](./README.md)