# Core - Editor - `ObjectExtensions`

Extensions for `Object` instances.

## Public API

```cs
public static class ObjectExtensions { }
```

### Functions

#### `GetGUID()`

```cs
public static string GetGUID(this Object obj)
```

Gets the GUID string of the given `Object`.

Note that GUIDs are assigned only to assets, not to scene objects. So if this `Object` is a scene object, this function will return `null`.

- `this Object obj`: The `Object` whose GUID you want to get.

Returns the GUID string of this `Object`, or `null` if this `Object` is not an asset (meaning it's a scene object).

#### `IsAsset()`

```cs
public static bool IsAsset(this Object obj)
```

Checks if the given `Object` is an asset (and not a scene object).

- `this Object obj`: The `Object` you want to check.

Returns `true` if the given `Object` is an asset, otherwise `false`.

#### `GetUniqueAssetName()`

```cs
public static string GetUniqueAssetName(this Object asset, string expectedName)
```

Gets the unique name of an asset to create in the same directory than the given one.

- `this Object asset`: The asset you want to process.
- `string expectedName`: The name you want to set on the asset.

Returns the processed unique asset name.

#### `GetAssetPath()`

```cs
public static string GetAssetPath(this Object asset)
```

Gets the relative path to the given asset from the root directory of the current Unity project.

- `this Object asset`: The asset of which you want to get the path.

Returns the found relative asset path, or `null` if the given asset is not an asset.

#### `GetAbsoluteAssetPath()`

```cs
public static string GetAbsoluteAssetPath(this Object asset)
```

Gets the absolute path to the given asset.

- `this Object asset`: The asset of which you want to get the path.

Returns the found absolute asset path, or `null` if the given asset is not an asset.

#### `FindSubassetOfType()`

```cs
public static Object FindSubassetOfType(this Object asset, Type type, bool includeMainAsset = false);
public static T FindSubassetOfType<T>(this Object asset, bool includeMainAsset = false)
  where T : Object;
```

Gets the first subasset of the given type in the given main asset.

- `<T>`: The expected type of the subasset.
- `this Object asset`: The main asset of which you want to get the subasset.
- `Type type`: The expected type of the subasset.
- `bool includeMainAsset = false`: If enabled, if the main asset has the expected type, it's the one being returned.

Returns the found subasset with the expected type.

#### `FindAllSubassetsOfType()`

```cs
public static Object[] FindAllSubassetsOfType(this Object asset, Type type, bool includeMainAsset = false);
public static T[] FindAllSubassetsOfType<T>(this Object asset, bool includeMainAsset = false)
  where T : Object
```

Gets the subassets of the given type in the given main asset.

- `<T>`: The expected type of the subassets.
- `this Object asset`: The main asset of which you want to get the subassets.
- `Type type`: The expected type of the subassets.
- `bool includeMainAsset = false`: If enabled, if the main asset has the expected type, it's the one being returned.

Returns the found subassets with the expected type.

---

[<= Back to summary](./README.md)