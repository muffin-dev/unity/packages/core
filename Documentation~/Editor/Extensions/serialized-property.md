# Core - Editor - `SerializedPropertyExtensions`

Extension functions for `SerializedPropertyExtensions` instances.

## Public API

```cs
public static class SerializedPropertyExtensions { }
```

### Functions

#### `GetTarget()`

```cs
public static object GetTarget(this SerializedProperty property);
public static T GetTarget<T>(this SerializedProperty property);
```

Gets the target object of this `SerializedProperty`.

This function is inspired by the [SpacePuppy Unity Framework](https://github.com/lordofduct/spacepuppy-unity-framework-4.0/blob/master/Framework/com.spacepuppy.core/Editor/src/EditorHelper.cs).

- `<T>`: The expected target type.
- `this SerializedProperty property`: The property of which you want to get the target object.

Returns the found target object.

---

[<= Back to summary](./README.md)