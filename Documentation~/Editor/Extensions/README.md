# Core - Editor - Extensions

## Summary

- [`ObjectExtensions`](./object.md): Extensions for `Object` instances.
- [`ScriptableObjectExtensions`](./scriptable-object.md): Extensions for `ScriptableObject` instances.
- [`SerializedPropertyExtensions`](./serialized-property.md): Extensions for `SerializedPropertyExtensions` instances.

---

[<= Back to *Editor* summary](../README.md)