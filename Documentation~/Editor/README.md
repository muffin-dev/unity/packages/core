# Core - Documentation - Editor

## Summary

- [GUI](./GUI/README.md): Functions for drawing GUI.
- [Extensions](./Extensions/README.md): Extensions for editor features.
- [Utilities](./Utilities/README.md): Miscellaneous utilities for working in the editor context.
- [Views](./Views/README.md): Viewers & GUI for package features.

---

[<= Back to package's summary](../README.md)