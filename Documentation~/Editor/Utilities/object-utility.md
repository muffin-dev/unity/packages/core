# Core - Editor - `ObjectUtility`

Miscellaneous utility functions for working with `Object` instances and assets.

## Public API

```cs
public static class ObjectUtility { }
```

### Functions

#### `GetGUID()`

```cs
public static string GetGUID(Object obj)
```

Gets the GUID string of the given `Object`.

Note that GUIDs are assigned only to assets, not to scene objects. So if this `Object` is a scene object, this function will return `null`.

- `Object obj`: The `Object` whose GUID you want to get.

Returns the GUID string of this `Object`, or `null` if this `Object` is not an asset (meaning it's a scene object).

#### `IsAsset()`

```cs
public static bool IsAsset(Object obj)
```

Checks if the given `Object` is an asset (and not a scene object).

- `Object obj`: The `Object` you want to check.

Returns `true` if the given `Object` is an asset, otherwise `false`.

#### `GetUniqueAssetName()`

```cs
public static string GetUniqueAssetName(Object asset, string expectedName)
```

Gets the unique name of an asset to create in the same directory than the given one.

- `Object asset`: The asset you want to process.
- `string expectedName`: The name you want to set on the asset.

Returns the processed unique asset name.

#### `GetAssetPath()`

```cs
public static string GetAssetPath(Object asset)
```

Gets the relative path to the given asset from the root directory of the current Unity project.

- `Object asset`: The asset of which you want to get the path.

Returns the found relative asset path, or `null` if the given asset is not an asset.

#### `GetAbsoluteAssetPath()`

```cs
public static string GetAbsoluteAssetPath(Object asset)
```

Gets the absolute path to the given asset.

- `Object asset`: The asset of which you want to get the path.

Returns the found absolute asset path, or `null` if the given asset is not an asset.

#### `FindSubassetOfType()`

```cs
public static Object FindSubassetOfType(Object asset, Type type, bool includeMainAsset = false);
public static T FindSubassetOfType<T>(Object asset, bool includeMainAsset = false)
  where T : Object;
```

Gets the first subasset of the given type in the given main asset.

- `<T>`: The expected type of the subasset.
- `Object asset`: The main asset of which you want to get the subasset.
- `Type type`: The expected type of the subasset.
- `bool includeMainAsset = false`: If enabled, if the main asset has the expected type, it's the one being returned.

Returns the found subasset with the expected type.

#### `FindAllSubassetsOfType()`

```cs
public static Object[] FindAllSubassetsOfType(Object asset, Type type, bool includeMainAsset = false);
public static T[] FindAllSubassetsOfType<T>(Object asset, bool includeMainAsset = false)
  where T : Object
```

Gets the subassets of the given type in the given main asset.

- `<T>`: The expected type of the subassets.
- `Object asset`: The main asset of which you want to get the subassets.
- `Type type`: The expected type of the subassets.
- `bool includeMainAsset = false`: If enabled, if the main asset has the expected type, it's the one being returned.

Returns the found subassets with the expected type.

#### `FindAllSubassetsOfType()`

```cs
public static Object FindAssetOfType(Type type, bool querySubassets = false);
public static T FindAssetOfType<T>(bool querySubassets = false)
  where T : Object
```

Find an asset of the given type in the project.

- `<T>`: The type of the asset you want to find.
- `Type type`: The type of the asset you want to find.
- `bool querySubassets = false`: If enabled, this function will also search in the assets attached to other ones.

Returns the found asset.

#### `FindAllSubassetsOfType()`

```cs
public static Object[] FindAllAssetsOfType(Type type, bool querySubassets = false);
public static T[] FindAllAssetsOfType<T>(bool querySubassets = false)
  where T : Object
```

Find all assets of the given type in the project.

- `<T>`: The type of assets you want to find.
- `Type type`: The type of the assets you want to find.
- `bool querySubassets = false`: If enabled, this function will also search in the assets attached to other ones.

Returns the found assets.

---

[<= Back to summary](./README.md)