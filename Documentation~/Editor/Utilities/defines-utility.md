# Core - Editor - `DefinesUtility`

Utility class for managing scripting define symbols through code.

## Public API

```cs
public static class DefinesUtility { }
```

### Functions

#### `GetDefines()`

```cs
public static string[] GetDefines();
public static string[] GetDefines(BuildTargetGroup platform);
```

Gets the scripting define symbols from Player Settings for the given target platform.

- `BuildTargetGroup platform`: The platform of which you want to get the scripting define symbols. If not provided, uses the current target platform.

Returns the defines from Player Settings.

#### `IsDefined()`

```cs
public static bool IsDefined(string define);
public static bool IsDefined(string define, BuildTargetGroup platform);
```

Checks if the named scripting define symbol is set for the given target platform.

- `string define`: The define you want to check.
- `BuildTargetGroup platform`: The platform for which you want to check the define.

Returns `true` if the define is set, otherwise `false`.

#### `AddDefine()`

```cs
public static bool AddDefine(string define);
public static bool AddDefine(string define, BuildTargetGroup platform);
```

Adds the named scripting define symbol for the given target platform.

- `string define`: The define you want to add.
- `BuildTargetGroup platform`: The platform to which the define is added.

Returns `true` if the define has been added successfully, or `false` if it was already registered.

#### `RemoveDefine()`

```cs
public static bool RemoveDefine(string define);
public static bool RemoveDefine(string define, BuildTargetGroup platform);
```

Removes the named scripting define symbol for the given target platform.

- `string define`: The define you want to remove.
- `BuildTargetGroup platform`: The platform from which the define is removed.

Returns `true` if the define has been removed successfully, or `false` if it wasn't used.

---

[<= Back to summary](./README.md)