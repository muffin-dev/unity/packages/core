# Core - Editor - `Progress`

Utility class to store the progress of background tasks.

This custom implementation mimics the common usages of the [*Progress API*](https://docs.unity3d.com/Documentation/ScriptReference/Progress.html) available since *Unity 2020.1*, so you can get an equivalent behavior to display tasks progress in a similar way in previous versions.

[=> See Unity's Progress API docs](https://docs.unity3d.com/Documentation/ScriptReference/Progress.html)

---

[<= Back to summary](./README.md)