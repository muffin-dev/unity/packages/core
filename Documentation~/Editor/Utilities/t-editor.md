# Core - Editor - `TEditor<T>`

Helper for making a custom editor with "typed" targets.

## Public API

```cs
public abstract class TEditor<T> : Editor
  where T : Object { }
```

- `<T>`: The type of this custom editor's targets.

### Properties

#### `target`

```cs
public new T target { get; }
```

The object being inspected.

#### `targets`

```cs
public new T[] targets { get; }
```

An array of all the object being inspected.

---

[<= Back to summary](./README.md)