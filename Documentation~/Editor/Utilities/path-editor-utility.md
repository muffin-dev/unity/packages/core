# Core - Editor - `PathEditorUtility`

Miscellaneous functions for working with paths in the editor.

## Public API

```cs
[InitializeOnLoad]
public static class PathEditorUtility { }
```

### Constants

#### `ASSETS_DIR`

```cs
public const string ASSETS_DIR = "Assets";
```

Name of the `/Assets` directory.

#### `DEFAULT_PANEL_DIRECTORY`

```cs
private const string DEFAULT_PANEL_DIRECTORY = ASSETS_DIR;
```

The default path used for opening *save/open file/folder* panels.

#### `AssetsPath`

```cs
public static readonly string AssetsPath;
```

The absolute path to this project's `/Assets` directory.

#### `ProjectPath`

```cs
public static readonly string ProjectPath;
```

The absolute path to this project.

#### `TemporaryPath`

```cs
public static readonly string TemporaryPath;
```

The absolute path to the temporary data directory.

### Functions

#### `OpenFolderPanel()`

```cs
public static string OpenFolderPanel(bool forceInProject = false);
public static string OpenFolderPanel(string panelTitle, bool forceInProject = false);
public static string OpenFolderPanel(string key, string panelTitle, bool forceInProject = false);
```

Opens the *open folder* dialog window to select a path to a folder, and save it in [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

- `string key`: The key used to retrieve the previously selected path from [`PathsHistory`](../../Runtime/Utilities/paths-history.md).
- `string panelTitle`: The title of the dialog window.
- `bool forceInProject = false`: If enabled and the selected path leads outside of this project, the selection is canceled.

Returns the selected path, or an empty path if the selected is not valid (which means that the selection was canceled).

#### `OpenFilePanel()`

```cs
public static string OpenFilePanel(bool forceInProject = false);
public static string OpenFilePanel(string extension, bool forceInProject = false);
public static string OpenFilePanel(string panelTitle, string extension, bool forceInProject = false);
public static string OpenFilePanel(string key, string panelTitle, string extension, bool forceInProject = false);
public static string OpenFilePanel(string[] filters, bool forceInProject = false);
public static string OpenFilePanel(string panelTitle, string[] filters, bool forceInProject = false);
public static string OpenFilePanel(string key, string panelTitle, string[] filters, bool forceInProject = false);
```

Opens the *open file* dialog window to select a path to a folder, and save it in [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

- `string key`: The key used to retrieve the previously selected path from [`PathsHistory`](../../Runtime/Utilities/paths-history.md).
- `string panelTitle`: The title of the dialog window.
- `string extension`: The expected extension of the selected file.
- `string[] filters`: The expected file extensions. This array must follow this format: `{ "Image files", "png,jpg,jpeg", "All files", "*" }`.
- `bool forceInProject = false`: If enabled and the selected path leads outside of this project, the selection is canceled.

Returns the selected path, or an empty path if the selected is not valid (which means that the selection was canceled).

#### `SaveFolderPanel()`

```cs
public static string SaveFolderPanel(bool forceInProject = false);
public static string SaveFolderPanel(string panelTitle, bool forceInProject = false);
public static string SaveFolderPanel(string key, string panelTitle, bool forceInProject = false);
public static string SaveFolderPanel(string key, string panelTitle, string defaultName, bool forceInProject = false);
```

Opens the *save folder* dialog window to select a path to a folder, and save it in [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

- `string key`: The key used to retrieve the previously selected path from [`PathsHistory`](../../Runtime/Utilities/paths-history.md).
- `string panelTitle`: The title of the dialog window.
- `bool forceInProject = false`: If enabled and the selected path leads outside of this project, the selection is canceled.

Returns the selected path, or an empty path if the selected is not valid (which means that the selection was canceled).

#### `SaveFilePanel()`

```cs
public static string SaveFilePanel(bool forceInProject = false);
public static string SaveFilePanel(string extension, bool forceInProject = false);
public static string SaveFilePanel(string panelTitle, string extension, bool forceInProject = false);
public static string SaveFilePanel(string key, string panelTitle, string extension, bool forceInProject = false);
public static string SaveFilePanel(string key, string panelTitle, string extension, string defaultName, bool forceInProject = false);
```

Opens the *save file* dialog window to select a path to a folder, and save it in [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

- `string key`: The key used to retrieve the previously selected path from [`PathsHistory`](../../Runtime/Utilities/paths-history.md).
- `string panelTitle`: The title of the dialog window.
- `string extension`: The expected extension of the selected file.
- `bool forceInProject = false`: If enabled and the selected path leads outside of this project, the selection is canceled.

Returns the selected path, or an empty path if the selected is not valid (which means that the selection was canceled).

---

[<= Back to summary](./README.md)