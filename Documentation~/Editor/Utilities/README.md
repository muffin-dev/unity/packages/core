# Core - Editor - Utilities

## Summary

- [`EditorHelpers`](./editor-helpers.md): Miscellaneous functions for working in the editor context.
- [`ObjectUtility`](./object-utility.md): Miscellaneous utility functions for working with `Object` instances and assets.
- [`PathEditorUtility`](./path-editor-utility.md): Miscellaneous functions to display path selection panels using [`PathsHistory`](../../Runtime/Utilities/paths-history.md).
- [`Progress`](./progress.md): Utility class to store the progress of background tasks.
- [`ScriptableObjectUtility`](./scriptable-object-utility.md): Miscellaneous utility functions for working with `ScriptableObject` instances and assets.
- [`TEditor<T>`](./t-editor.md): Helper for making a custom editor with "typed" targets.

---

[<= Back to *Editor* summary](../README.md)