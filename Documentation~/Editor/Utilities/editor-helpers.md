# Core - Editor - `EditorHelpers`

Miscellaneous functions for working in the editor context.

## Public API

```cs
public static class EditorHelpers { }
```

### Functions

#### `FocusObject()`

```cs
public static void FocusObject(Object obj, bool pingObject = true, bool selectObject = true)
```

Highlights and select an object. If the object is an asset, focus the project window and highlight it from project view. If it's an object in a scene, highlight it from the hierarchy.

- `Object obj`: The object you want to focus.
- `bool pingObject = true`: Should the object be highlighted in the Project view?
- `bool selectObject = true`: Should the object be selected?

#### `FindEditorWindow()`

```cs
public static EditorWindow FindEditorWindow(string windowTitle)
```

Gets the `EditorWindow` instance of the named window.

Note that this function will only find opened (or loaded) windows.

- `string windowTitle`: The title of the window you want to find.

Returns the found window.

#### `GetLabel()`

```cs
public static GUIContent GetLabel(object obj, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
public static GUIContent GetLabel(object obj, string fieldOrPropertyName, string customLabel, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
public static GUIContent GetLabel<T>(string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
public static GUIContent GetLabel<T>(string fieldOrPropertyName, string customLabel, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
public static GUIContent GetLabel(Type type, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
public static GUIContent GetLabel(Type type, string fieldOrPropertyName, string customLabel, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
```

Creates a [`GUIContent`](https://docs.unity3d.com/ScriptReference/GUIContent.html) value with the expected property's label and tooltip.

- `Type type | <T>`: The type of the object from which you want to get the data.
- `object obj`: The object from which you want to get the data.
- `string fieldOrPropertyName`: The name of the field or the property you want to display.
- `string customLabel`: If defined, this text is used as is for the [`GUIContent`](https://docs.unity3d.com/ScriptReference/GUIContent.html) label.
- `BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS`: Filters to use for finding the field or property. By default, targets only instance data, public and non-public.

Returns the processed [`GUIContent`](https://docs.unity3d.com/ScriptReference/GUIContent.html) value.

##### Example

```cs
// Consider this class Player
public class Player {
  [Tooltip("The speed (in units/s) of this player.")]
  public float speed = 10f;
}

// You could use the following line in a custom inspector
myPlayer.speed = EditorGUILayout.FloatField(EditorHelpers.GetLabel<Player>(nameof(Player.speed)), myPlayer.speed);
```

#### `GetTooltip()`

```cs
public static string GetTooltip(object obj, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
public static string GetTooltip<T>(string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
public static string GetTooltip(Type type, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS);
```

Gets the Unity tooltip of the named field or property.

- `Type type | <T>`: The type of the object from which you want to get the data.
- `object obj`: The object from which you want to get the data.
- `string fieldOrPropertyName`: The name of the field or the property you want to get the tooltip. Note that this method will search for the named field first, then search for the property.
- `BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS`: Filters to use for finding the field or property. By default, targets only instance data, public and non-public.

Returs the found tooltip value, otherwise `string.Empty`.

#### `TryGetActiveFolderPath()`

```cs
public static bool TryGetActiveFolderPath(out string path)
```

Tries to get the active folder path from the Project view if it's open.

- `out string path`: Outputs the relative path to the fuond active folder.

Returns `true` if a path has been found, otherwise `false`.

---

[<= Back to summary](./README.md)