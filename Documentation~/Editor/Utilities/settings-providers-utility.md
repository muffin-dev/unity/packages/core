# Core - Editor - `SettingsProvidersUtility`

Utility for creating inspectors from [settings providers](https://docs.unity3d.com/ScriptReference/SettingsProvider.html).

## Usage

This utility is meant to provide a standard way to create menus from settings windows.

As an example, the following script defines a config asset that could be used in a project. It uses [`SOSingleton`](../../Runtime/Patterns/so-singleton.md) in order to make it "savable" on disk, and accessible from everywhere.

```cs
using MuffinDev.Core;

namespace MuffinDev.Demos.Core
{
  public class SettingsProviderDemo : SOSingleton<SettingsProviderDemo>
  {
    public string mainSceneName;
    public float defaultPlayerSpeed;
    public float defaultGameDuration;

    public void Reset()
    {
      mainSceneName = "GameScene";
      defaultPlayerSpeed = 6f;
      defaultGameDuration = 120f;
    }

    // This will save these settings in EditorPrefs
    protected override SOSingletonOptions SingletonOptions => SOSingletonOptions.SaveInEditor("Temp/Settings Provider Demo.json", true);
  }
}
```

And here is an editor script that registers the [`SettingsProvider`](https://docs.unity3d.com/ScriptReference/SettingsProvider.html) in both `Edit > Preferences` and `Edit > Project Settings` windows.

```cs
using UnityEngine;
using UnityEditor;
using MuffinDev.Demos.Core;
using MuffinDev.Core.EditorOnly;

namespace MuffinDev.Demos.Core.EditorOnly
{
  public class SettingsProviderDemoEditor
  {
    // This will create a "Muffin Dev/Demos/Game Default Values" menu from Edit > Preferences, displaying the default inspector of the asset.
    [SettingsProvider]
    private static SettingsProvider RegisterPreferencesMenu()
    {
      return SettingsProvidersUtility.GetSettingsMenu(SettingsProviderDemo.I, "Muffin Dev/Demos/Game Default Values", SettingsScope.User);
    }

    // This will create a "Muffin Dev/Demos/Game Default Values" menu from Edit > Project Settings, displaying the default inspector of the asset, and an additional control button.
    [SettingsProvider]
    private static SettingsProvider RegisterProjectSettingsMenu()
    {
      return SettingsProvidersUtility.GetSettingsMenu(SettingsProviderDemo.I, "Muffin Dev/Demos/Game Default Values", SettingsScope.Project, (drawDefaultInspector, obj, search) =>
      {
        drawDefaultInspector();

        EditorGUILayout.Space();
        if (GUILayout.Button("Reset", GUILayout.Height(36f)))
        {
          SettingsProviderDemo.I.Reset();
        }
      });
    }
  }
}
```

![View from Preferences window](../../Images/preferences-settings-provider.png)
![View from Project Settings window](../../Images/project-settings-provider.png)

## Public API

```cs
public class SettingsProvidersUtility : SOSingleton<SettingsProvidersUtility> { }
```

### Delegates

#### `DrawDefaultInspectorDelegate`

```cs
public delegate void DrawDefaultInspectorDelegate();
```

Function call to draw the default inspector of an object.

#### `GUIHandlerDelegate`

```cs
public delegate void GUIHandlerDelegate(DrawDefaultInspectorDelegate drawDefaultInspector, Object obj, string search);
```

Custom implementation of the GUI Handler for a [`SettingsProvider`](https://docs.unity3d.com/ScriptReference/SettingsProvider.html).

- `DrawDefaultInspectorDelegate drawDefaultInspector`: The original call, which draws the default inspector of the object.
- `Object obj`: The object being edited in the [`SettingsProvider`](https://docs.unity3d.com/ScriptReference/SettingsProvider.html).
- `string search`: The current search text.

### Functions

#### `GetSettingsMenu()`

```cs
public static SettingsProvider GetSettingsMenu(Object obj, SettingsScope scope);
public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, SettingsScope scope);
public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, string[] keywords, SettingsScope scope);
public static SettingsProvider GetSettingsMenu(Object obj, SettingsScope scope, GUIHandlerDelegate customGUIHandler);
public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, SettingsScope scope, GUIHandlerDelegate customGUIHandler);
public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, string[] keywords, SettingsScope scope, GUIHandlerDelegate customGUIHandler)
```

Gets the [`SettingsProvider`](https://docs.unity3d.com/ScriptReference/SettingsProvider.html) for the given object, displaying its default inspector.

- `Object obj`: The object for which you want to get a settings provider.
- `SettingsScope scope`: The scope of the settings inspector.
- `string menuPath`: The menu path to reach the settings inspector from Preferences or Project Settings tabs.
- `string[] keywords`: The keywords used to find the settings from the search bar.
- `GUIHandlerDelegate customGUIHandler`: Custom implementation for drawing GUI in the editor.

Returns the created [`SettingsProvider`](https://docs.unity3d.com/ScriptReference/SettingsProvider.html).

---

[<= Back to summary](./README.md)