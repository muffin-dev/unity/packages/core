# Core - Editor - `ScriptableObjectUtility`

Miscellaneous utility functions for working with `ScriptableObject` instances and assets.

## Public API

```cs
public static class ScriptableObjectUtility { }
```

### Functions

#### `GetScriptPath()`

```cs
public static string GetScriptPath(ScriptableObject obj);
public static string GetScriptPath(Type scriptableObjectType);
public static string GetScriptPath<TScriptableObject>()
  where TScriptableObject : ScriptableObject;
```

Gets the path to the source script used by the given `ScriptableObject`.

- `<TScriptableObject>`: The type of the `ScriptableObject` you want to get the script file path.
- `ScriptableObject obj`: The `ScriptableObject` you want to get the script path.
- `Type scriptableObjectType`: The type of the `ScriptableObject` you want to get the script file path.

Returns the found script path, or `null` if the path to the script file can't be found.

---

[<= Back to summary](./README.md)