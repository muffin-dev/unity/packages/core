# Core - Editor - `HorizontalSplitView`

Draws an horizontal split view that can be resized on GUI.

## Usage

```cs
using UnityEditor;
using UnityEngine;
using MuffinDev.Core.EditorOnly;
public class SplitViewDemo : EditorWindow
{
    [SerializeField]
    private HorizontalSplitView _splitView = new HorizontalSplitView();

    [MenuItem("Demos/Split View")]
    private static void Open()
    {
        GetWindow<SplitViewDemo>("Split View Demo");
    }

    private void OnGUI()
    {
        _splitView.DoLayout
        (
            () => EditorGUILayout.HelpBox("Left side", MessageType.Info),
            () => EditorGUILayout.HelpBox("Right side", MessageType.Info),
            Repaint
        );
    }
}
```

![Horizontal Split View demo](../../Images/horizontal-split-view.png)

## Public API

```cs
[System.Serializable]
public class HorizontalSplitView : SplitViewBase { }
```

### Constructors

```cs
public HorizontalSplitView(bool resizable = true);
public HorizontalSplitView(float leftSideWidth, bool resizable = true);
```

- `bool resizable = true`: If enabled, this split view can be resized.
- `float leftSideWidth`: The width of the left side of this split view.

### Delegates

#### `ResizeDelegate()`

```cs
public delegate void ResizeDelegate(float leftSize)
```

Called when this split view is resized.

- `float leftSize`: The new size of the left side of this split view.

### Events

#### `OnResize`

```cs
public event ResizeDelegate OnResize
```

Called when this split view is resized.

### Functions

#### `DoLayout()`

```cs
public void DoLayout(Action onDrawLeftSide, Action onDrawRightSide, Action onResize, bool noScrollView = false);
public void DoLayout(Action onDrawLeftSide, Action onDrawRightSide, bool noScrollView = false);
```

Draws this split view on GUI.

Note that it must be drawn in a vertical scope to avoid unexpected layout.

- `Action onDrawLeftSide`: Called when the left side of this split view is drawn.
- `Action onDrawRightSide`: Called when the right side of this split view is drawn.
- `Action onResize`: Called when this split view is resized.
- `bool noScrollView = false)`: By default, each side is wrapped in a scroll view. If this option is enabled, the scroll views are not drawn, so you must draw it and manage the scroll positions yourself if needed.

### Properties

#### `LeftSize`

```cs
public float LeftSize { get; set; }
```

The current size of the left side of this split view.

#### `Resizable`

```cs
public bool Resizable { get; set; }
```

Defines if this split view can be resized.

#### `CursorColor`

```cs
public Color CursorColor { get; set; }
```

The color of the vertical separator bewteen this split view sides.

#### `LeftSideScrollPosition`

```cs
public Vector2 LeftSideScrollPosition { get; set; }
```

The current scroll position of the left size of this split view.

#### `RightSizeScrollPosition`

```cs
public Vector2 RightSizeScrollPosition { get; set; }
```

The current scroll position of the right size of this split view.

---

[<= Back to summary](./README.md)