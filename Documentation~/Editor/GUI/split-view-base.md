# Core - Editor - `SplitViewBase`

Base class for drawing resizable split views on GUI.

## Public API

```cs
[System.Serializable]
public abstract class SplitViewBase { }
```

### Constructors

```cs
public SplitViewBase(bool resizable = true);
public SplitViewBase(float firstSideWidth, bool resizable = true);
```

- `bool resizable = true`: If enabled, the first side of this split view can be resized.
- `float leftSideWidth`: The width of the first side of this split view.

### Delegates

#### `ResizeDelegate()`

```cs
public delegate void ResizeDelegate(float size)
```

Called when this split view is resized.

- `float size`: The new size of the first side of this split view.

### Events

#### `OnResize`

```cs
public event ResizeDelegate OnResize
```

Called when this split view is resized.

### Properties

#### `Size`

```cs
public float Size { get; set; }
```

The current size of the first side of this split view.

#### `Resizable`

```cs
public bool Resizable { get; set; }
```

Defines if this split view can be resized.

#### `CursorColor`

```cs
public Color CursorColor { get; set; }
```

The color of the vertical separator bewteen this split view sides.

#### `FirstSideScrollPosition`

```cs
public Vector2 FirstSideScrollPosition { get; set; }
```

The current scroll position of the first side of this split view.

#### `SecondSideScrollPosition`

```cs
public Vector2 SecondSideScrollPosition { get; set; }
```

The current scroll position of the second side of this split view.

## Protected API

### Functions

#### `HandleResizeEvents()`

```cs
protected bool HandleResizeEvents(Rect cursorRect, float availableSpace)
```

Handle mouse events for the resize cursor.

- `Rect cursorRect`: The rect used for drawing the cursor.
- `float availableSpace`: The available space for resizing the split view.

Returns `true` if the split view has been resized, otherwise `false`.

#### `BeginResize()`

```cs
protected virtual void BeginResize()
```

Called when the user starts dragging the cursor.

#### `ApplyResize()`

```cs
protected abstract void ApplyResize(float availableSpace)
```

Called each frame while the user is dragging the cursor. It updates the first side's size value.

- `float availableSpace`: The available space for resizing the split view.

#### `EndResize()`

```cs
protected virtual void EndResize()
```

Called when the user stops dragging the cursor.

---

[<= Back to summary](./README.md)