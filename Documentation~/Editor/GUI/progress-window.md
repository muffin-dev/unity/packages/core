# Core - Editor - `ProgressWindow`

Displays progress indicators from [`Progress`](../Utilities/progress.md) utility.

You can display this window from `Tools > Muffin Dev > Progress`, or using [`Progress.ShowDetails()`](../Utilities/progress.md).

## Public API

```cs
public class ProgressWindow : EditorWindow { }
```

---

[<= Back to summary](./README.md)