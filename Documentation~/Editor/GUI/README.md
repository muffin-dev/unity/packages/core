# Core - Editor - GUI

## Summary

- [`DropZone`](./drop-zone.md): Utility for drawing *drop zones* used to handle drag and drop for given types of objects.
- [`HorizontalSplitView`](./horizontal-split-view.md): Draws an horizontal split view that can be resized on GUI.
- [`IndentedScope`](./indented-scope.md): Custom GUI Scope for drawing indented fields.
- [`LabelWidthScope`](./label-width-scope.md): Custom GUI scope for customizing the default label width in a block.
- [`MoreEdiorGUI`](./editor-gui-layout.md): Miscellaneous functions for drawing user interfaces in the editor.
- [`ProgressWindow`](./progress-window.md): Displays progress indicators from [`Progress`](../Utilities/progress.md) utility.
- [`SplitViewBase`](./split-view-base.md): Base class for drawing resizable split views on GUI.
- [`VerticalSplitView`](./vertical-split-view.md): Draws a vertical split view that can be resized on GUI.

---

[<= Back to *Editor* summary](../README.md)