# Core - Editor - `LabelWidthScope`

Custom GUI scope for customizing the default label width in a block.

## Usage

```cs
using UnityEditor;
using MuffinDev.Core;
public class DemoLabelWidthScopeWindow : EditorWindow
{
    [MenuItem("Demos/Label Width Scope")]
    private static void Open()
    {
        GetWindow<DemoLabelWidthScopeWindow>("Label Width Scope Demo");
    }

    private void OnGUI()
    {
        EditorGUILayout.TextField("Regular Label", "...");

        using (new LabelWidthScope(MoreGUI.WIDTH_S))
        {
            EditorGUILayout.TextField("Small Label", "...");
        }

        using (new LabelWidthScope(MoreGUI.WIDTH_XL))
        {
            EditorGUILayout.TextField("Large Label", "...");
        }
    }
}
```

![IndentedScope window preview](../../Images/label-width-scope.png)

## Public API

```cs
public class LabelWidthScope : GUI.Scope { }
```

### Constructor

```cs
public LabelWidthScope(float labelWidth)
```

Defines the editor label width inside this block.

- `float labelWidth`: The expected label width.

---

[<= Back to summary](./README.md)