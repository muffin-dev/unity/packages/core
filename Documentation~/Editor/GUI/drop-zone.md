# Core - Editor - `DropZone`

Utility for drawing *drop zones* used to handle drag and drop for given types of objects.

## Public API

```cs
public static class DropZone { }
```

### Delegates

#### `DropDelegate()`

```cs
public delegate bool DropDelegate<T>(T[] objectReferences);
public delegate bool DropDelegate(Object[] objectReferences);
```

Called when objects are dropped in a drop zone using `Draw()`.

- `<T>`: The type of objects allowed to be dropped in the drop zone.
- `T[]|Object[] objectReferences`: The filtered list of dragged objects.

Returns `true` if the objects have been dropped successfully, otherwise `false`.

#### `CanDropDelegate()`

```cs
public delegate bool CanDropDelegate<T>(T obj);
public delegate bool CanDropDelegate(Object obj);
```

Checks if an object can be dropped in a drop zone.

Called when objects are dragged over the drop zone, as an additional check after ensuring the item has the expected type. So you don't need to check the type of the object by yourself at this point.

- `<T>`: The type of objects allowed to be dropped in the drop zone.
- `T|Object obj`: The object being checked.

Returns `true` if the object can be dropped in the drop zone, otherwise `false`.

### Functions

#### `Draw()`

```cs
public static void Draw(Rect rect, Type[] draggableItems, DropDelegate onDrop, CanDropDelegate canDrop = null);
public static void Draw<T>(Rect rect, DropDelegate<T> onDrop, CanDropDelegate canDrop = null)
    where T : Object;

public static void Draw<T1, T2>(Rect rect, DropDelegate onDrop, CanDropDelegate canDrop = null)
    where T1 : Object
    where T2 : Object;
public static void Draw<T1, T2, T3>(Rect rect, DropDelegate onDrop, CanDropDelegate canDrop = null)
    where T1 : Object
    where T2 : Object
    where T3 : Object;
public static void Draw<T1, T2, T3, T4>(Rect rect, DropDelegate onDrop, CanDropDelegate canDrop = null)
    where T1 : Object
    where T2 : Object
    where T3 : Object
    where T4 : Object;
```

Draws a *drop zone* on the GUI, where items of the given types can be dragged.

Note that this method just draws a "zone", an invisible area, so you need to draw the rect that represent it by yourself. As an example, you can use [`GUI.Box()`](https://docs.unity3d.com/ScriptReference/GUI.Box.html) to draw a simple box where your drop zone is.

- `<T>|<T1>`: The first type of objects allowed to be dropped in this drop zone.
- `<T2>`: The second type of objects allowed to be dropped in this drop zone.
- `<T3>`: The third type of objects allowed to be dropped in this drop zone.
- `<T4>`: The fourth type of objects allowed to be dropped in this drop zone.
- `Rect rect`: The position and size of the drop zone on screen
- `Type[] draggableItems`: The allowed object types for this drop zone.
- `DropDelegate|DropDelegate<T> onDrop`: Called when items are dropped in this drop zone.
- `CanDropDelegate|CanDropDelegate<T> canDrop = null`: Additional function to check if items can be dropped. Note that this is called after checking if the items have the appropriate types.

---

[<= Back to summary](./README.md)