# Core - Editor - `IndentedScope`

Custom GUI Scope for drawing indented fields.

## Usage

```cs
using UnityEditor;
using UnityEngine;
using MuffinDev.Core;
public class DemoIndentedScopeWindow : EditorWindow
{
    [MenuItem("Demos/Indented Scope")]
    private static void Open()
    {
        GetWindow<DemoIndentedScopeWindow>("Indented Scope Demo");
    }

    private void OnGUI()
    {
        EditorGUILayout.TextField("Simple Text Field", "...");
        using (new IndentedScope())
        {
            EditorGUILayout.TextField("Indented Text Field", "...");
        }
    }
}
```

![IndentedScope window preview](../../Images/indented-scope.png)

## Public API

```cs
public class IndentedScope : GUI.Scope { }
```

### Constructor

```cs
public IndentedScope(int levels = 1)
```

Begins a scope with the given indentation level.

- `int levels = 1`: The number of indentation levels to apply. Can't be less than 1.

---

[<= Back to summary](./README.md)