# Core - Editor - `MoreEditorGUI`

Miscellaneous functions for drawing user interfaces in the editor.

## Public API

```cs
public static class MoreEditorGUI { }
```

### Delegates

#### `AddItemDelegate<T>()`

```cs
public delegate void AddItemDelegate<T>(IList<T> list);
```

Called when an item is added to a list.

- `<T>`: The type of the list.
- `IList<T> list`: The list drawn using `DrawList<T>()`.

#### `DrawItemDelegate<T>()`

```cs
public delegate bool DrawItemDelegate<T>(IList<T> list, int index);
```

Called when an item of the list is drawn on the GUI.

- `<T>`: The type of the list.
- `IList<T> list`: The list drawn using `DrawList<T>()`.
- `int index`: The index of the item being drawn on the GUI.

Returns `true` if the item should be removed, otherwise `false`.

### Constants

#### `DEFAULT_SEPARATOR_SIZE`

```cs
public const float DEFAULT_SEPARATOR_SIZE = 2f;
```0

### Functions

#### `DrawList<T>()`

```cs
public static void DrawList<T>(IList<T> list, GUIContent label, DrawItemDelegate<T> onDrawItem);
public static void DrawList<T>(IList<T> list, GUIContent label, AddItemDelegate<T> onAdd, DrawItemDelegate<T> onDrawItem)
```

Draws a non-reorderable list of items, with "Add" control in the header.

- `<T>`: The type of the items.
- `IList<T> list`: The list itself.
- `GUIContent label`: The label of the list field.
- `AddItemDelegate<T> onAdd`: Called when the "Add" button is clicked.
- `DrawItemDelegate<T> onDrawItem`: Called when an item is drawn on the GUI. Note that you must draw the "Remove" control yourself as needed in your GUI.

##### Example

```cs
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MuffinDev.Core.EditorOnly;
public class DrawListDemoWindow : EditorWindow
{
    [SerializeField]
    private List<string> _items = new List<string>(new string[]
    {
        "Potion",
        "Sword",
        "Shield"
    });

    [MenuItem("Demos/Draw List")]
    private static void Open()
    {
        GetWindow<DrawListDemoWindow>("Draw List Demo");
    }

    private void OnGUI()
    {
        MoreEditorGUI.DrawList(_items, new GUIContent("Items"), (list) =>
        {
            list.Add("");
        }, (list, i) =>
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                list[i] = EditorGUILayout.TextField($"Item {i}", list[i]);
                if (GUILayout.Button("X"))
                    return true;
            }
            return false;
        });
    }
}
```

![DrawList preview](../../Images/draw-list-editor-gui.png)

#### `MinMaxFloatFields()`

```cs
public static Vector2 MinMaxFloatFields(GUIContent label, Vector2 value, float minLimit = Mathf.NegativeInfinity, float maxLimit = Mathf.Infinity);
public static Vector2 MinMaxFloatFields(GUIContent label, Vector2 value, float minLimit = Mathf.NegativeInfinity, float maxLimit = Mathf.Infinity);
public static Vector2 MinMaxFloatFields(Vector2 value, float minLimit = Mathf.NegativeInfinity, float maxLimit = Mathf.Infinity);
```

Draws composite float fields that represents a minimum and a maximum.

- `GUIContent|string label`: The label of the field to draw.
- `Vector2 value`: The current value of the field to draw.
- `float minLimit = Mathf.NegativeInfinity`: The minimum available value.
- `float maxLimit = Mathf.Infinity`: The maximum available value.

Returns the new value of the field.

##### Example

```cs
MoreEditorGUI.MinMaxFloatFields("Random Range", new Vector2(0, 10));
```

![MinMaxFloatFields preview](../../Images/min-max-editor-gui.png)

#### `MinMaxSlider()`

```cs
public static void MinMaxSlider(GUIContent label, ref float min, ref float max, float minLimit, float maxLimit, bool multilines = false);
public static void MinMaxSlider(string label, ref float min, ref float max, float minLimit, float maxLimit, bool multilines = false);
```

Displays a `MinMaxSlider` field, but with minimum and maximum values clearly displayed in float fields at the bounds of the slider.

- `GUIContent|string label`: The label of the field to draw.
- `ref float min`: The current minimum value.
- `ref float max`: The current maximum value.
- `float minLimit`: The minimum limit of the slider to the left.
- `float maxLimit`: The maximum limit of the slider to the right.
- `bool multilines = false`: If enabled, displays the min and max float fields under the slider.

##### Example

```cs
Vector2 jumpHeight = new Vector2(2, 6);
MoreEditorGUI.MinMaxSlider("Jump Height", ref jumpHeight.x, ref jumpHeight.y, 0f, 10f);
```

![MinMaxFloatFields preview](../../Images/min-max-slider-editor-gui.png)

#### `FolderPathField()`

```cs
public static string FolderPathField(string path, string panelTitle);
public static string FolderPathField(string path, string panelTitle, string historyKey);
public static string FolderPathField(string label, string path, string panelTitle, string historyKey);
public static string FolderPathField(GUIContent label, string path, string panelTitle);
public static string FolderPathField(GUIContent label, string path, string panelTitle, string historyKey);
public static string FolderPathField(Rect position, string path, string panelTitle);
public static string FolderPathField(Rect position, string path, string panelTitle, string historyKey);
public static string FolderPathField(Rect position, string label, string path, string panelTitle, string historyKey);
public static string FolderPathField(Rect position, GUIContent label, string path, string panelTitle);
public static string FolderPathField(Rect position, GUIContent label, string path, string panelTitle, string historyKey);
```

Draws a text field with a "Browse..." button to select a path to a folder. Also draws a "drop zone" that allow you to drag and drop a folder from the project view to extract its path.

- `GUIContent|string label`: The label of the field to draw.
- `string path`: The current path to use in the text field.
- `string panelTitle`: The title of the "open folder" dialog.
- `string historyKey = ""`: The key string used to get/set the default path from the [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

Returns the path to the selected folder.

#### `FilePathField()`

```cs
public static string FilePathField(string path, string panelTitle);
public static string FilePathField(string path, string panelTitle, string extension);
public static string FilePathField(string path, string panelTitle, string extension, string historyKey);
public static string FilePathField(string label, string path, string panelTitle, string extension, string historyKey);
public static string FilePathField(GUIContent label, string path, string panelTitle);
public static string FilePathField(GUIContent label, string path, string panelTitle, string extension);
public static string FilePathField(GUIContent label, string path, string panelTitle, string extension, string historyKey);
public static string FilePathField(Rect position, string path, string panelTitle);
public static string FilePathField(Rect position, string path, string panelTitle, string extension);
public static string FilePathField(Rect position, string path, string panelTitle, string extension, string historyKey);
public static string FilePathField(Rect position, string label, string path, string panelTitle, string extension, string historyKey);
public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle);
public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string extension);
public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string extension, string historyKey);

// Overloads with complex filters
public static string FilePathField(string path, string panelTitle, string[] filters);
public static string FilePathField(string path, string panelTitle, string[] filters, string historyKey);
public static string FilePathField(string label, string path, string panelTitle, string[] filters, string historyKey);
public static string FilePathField(GUIContent label, string path, string panelTitle, string[] filters);
public static string FilePathField(GUIContent label, string path, string panelTitle, string[] filters, string historyKey);
public static string FilePathField(Rect position, string path, string panelTitle, string[] filters);
public static string FilePathField(Rect position, string path, string panelTitle, string[] filters, string historyKey);
public static string FilePathField(Rect position, string label, string path, string panelTitle, string[] filters, string historyKey);
public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string[] filters);
public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string[] filters, string historyKey);
```

Draws a text field with a "Browse..." button to select a path to a file. Also draws a "drop zone" that allow you to drag and drop a file from the project view to extract its path.

- `GUIContent|string label`: The label of the field to draw.
- `string path`: The current path to use in the text field.
- `string panelTitle`: The title of the "open file" dialog.
- `string extension = null`: The expected extension of the file to select.
- `string[] filters`: The accepted extensions of the file to select. Must follow this pattern: `{ "Image files", "png,jpg,jpeg", "All files", "*" }`.
- `string historyKey = ""`: The key string used to get/set the default path from the [`PathsHistory`](../../Runtime/Utilities/paths-history.md).

Returns the path to the selected file.

##### Example

```cs
MoreEditorGUI.FolderPathField("Selected Folder", null, "Select a Folder", "Utility");
// Restricts file selection to *.asset extensions
MoreEditorGUI.FilePathField("Selected Asset", null, "Select an Asset", "asset", "Utility");
// Restricts file selection to image files with *.png, *.jpg or *.gif extensions
MoreEditorGUI.FilePathField("Selected Image", null, "Select an Image", new string[] { "Image files", "png,jpg,gif" }, "Utility");
```

![Foldeer and File path fields preview](../../Images/browse-fields-editor-gui.png)

#### `HorizontalSeparator()`

```cs
public static void HorizontalSeparator(bool wide = false);
public static void HorizontalSeparator(float size, bool wide = false);
public static void HorizontalSeparator(float size, Color color, bool wide = false);
```

Draws an horizontal line.

- `bool wide = false`: If enabled, the separator will use the full view width. This is designed to draw a separator that doesn't use the margins in the inspector window.
- `float size`: The height of the separator.
- `Color color`: The color of the separator.

#### `VerticalSeparator()`

```cs
public static void HorizontalSeparator();
public static void HorizontalSeparator(float size);
public static void HorizontalSeparator(float size, Color color);
```

Draws a vertical line.

- `float size`: The width of the separator.
- `Color color`: The color of the separator.

#### `PaginationField()`

```cs
public static void PaginationField(ref Pagination pagination);
public static void PaginationField(string label, ref Pagination pagination);
public static void PaginationField(GUIContent label, ref Pagination pagination);
public static void PaginationField(Rect position, ref Pagination pagination);
public static void PaginationField(Rect position, string label, ref Pagination pagination);
public static void PaginationField(Rect position, GUIContent label, ref Pagination pagination);
```

Draws a field to edit the given pagination value.

- `ref Pagination pagination`: The pagination value to edit.
- `string|GUIContent label`: The label of the field.
- `Rect position`: The position and size of the field.

### Properties

#### `SeparatorColor`

```cs
public static Color SeparatorColor { get; }
```

---

[<= Back to summary](./README.md)