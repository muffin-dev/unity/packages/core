# Muffin Dev for Unity - Core - Documentation

## Summary

- [Constants](./constants.md): Constant values used widely in the framework
- [Editor](./Editor/README.md): Editor features, excluded from builds
- [Runtime](./Runtime/README.md): Runtime features, included in builds

## Core Library Settings

You can edit the core library settings from `Edit > Preferences > Muffin Dev > General`.

- **Enable Demos**: Enables/disables demos. When enabled, new menus are added in main toolbar, *Add Component* or *Assets > Create* menus. Refer to the documentation of the corresponding package for more informations about those demos.

---

[<= Back to package's homepage](../README.md)