/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for boolean values.
    /// </summary>
    public static class BoolExtensions
    {

        public const string TRUE_LABEL = "true";
        public const string FALSE_LABEL = "false";

        /// <summary>
        /// Converts this boolean into a label.
        /// </summary>
        /// <param name="value">The boolean value to convert.</param>
        /// <param name="labelIfTrue">The output label if the boolean is true.</param>
        /// <param name="labelIfFalse">The output label if the boolean is false.</param>
        /// <returns>Returns the appropriate label.</returns>
        public static string ToLabel(this bool value, string labelIfTrue = TRUE_LABEL, string labelIfFalse = FALSE_LABEL)
        {
            return value ? labelIfTrue : labelIfFalse;
        }

    }

}