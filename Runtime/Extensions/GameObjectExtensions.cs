/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="GameObject"/> instances.
    /// </summary>
    public static class GameObjectExtensions
    {

        /// <summary>
        /// Computes the local bounds of an object, before it's transformed in the scene.<br/>
        /// This function will attempt to use the <see cref="MeshFilter"/> component in order to get the original mesh bounds (in local
        /// space), then multiply it by the object's scale if required.
        /// </summary>
        /// <param name="obj">The object you want to get the bounds.</param>
        /// <param name="applyScale">If enabled, multiply the local bounds by the object's scale.</param>
        /// <returns>Returns the local bounds of the object.</returns>
        public static Vector3 ComputeSize(this GameObject obj, bool applyScale = false)
        {
            Vector3 localSize = Vector3.zero;
            if (obj.TryGetComponent(out MeshFilter filter))
            {
                Mesh mesh = Application.isPlaying ? filter.mesh : filter.sharedMesh;
                localSize = mesh.bounds.size;
            }
            else if (obj.TryGetComponent(out SpriteRenderer spriteRenderer))
            {
                localSize = spriteRenderer.sprite != null ? spriteRenderer.sprite.bounds.size : Vector3.zero;
            }
            else if (obj.TryGetComponent(out Renderer renderer))
            {
                localSize = renderer.bounds.size;
            }
            else if (obj.TryGetComponent(out Collider collider))
            {
                localSize = collider.bounds.size;
            }

            if (applyScale)
            {
                localSize.x *= obj.transform.localScale.x;
                localSize.y *= obj.transform.localScale.y;
                localSize.z *= obj.transform.localScale.z;
            }

            return localSize;
        }

    }

}