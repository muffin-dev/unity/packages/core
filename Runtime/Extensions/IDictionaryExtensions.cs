/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="IDictionary{T,U}"/> instances.
    /// </summary>
    public static class IDictionaryExtensions
    {

        /// <summary>
        /// Adds a new entry in the dictionary if the key doesn't exist yet, or replace the previous value otherwise.
        /// </summary>
        /// <typeparam name="TKey">The key type of this dictionary.</typeparam>
        /// <typeparam name="TValue">The value type of this dictionary.</typeparam>
        /// <param name="dictionary">The dictionary in which you want to add or set the value.</param>
        /// <param name="key">The target key.</param>
        /// <param name="value">The new value.</param>
        public static void AddOrSet<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.TryGetValue(key, out _))
                dictionary[key] = value;
            else
                dictionary.Add(key, value);
        }

        /// <summary>
        /// Adds a new entry in the dictionary if the key doesn't yet, or adds the given value to the list otherwise.
        /// </summary>
        /// <typeparam name="TKey">The key type of this dictionary.</typeparam>
        /// <typeparam name="TItem">The type of a single item in the list.</typeparam>
        /// <param name="dictionary">The dictionary in which you want to add or push the value.</param>
        /// <param name="key">The target key.</param>
        /// <param name="item">The item to add.</param>
        public static void AddOrPush<TKey, TItem>(this IDictionary<TKey, List<TItem>> dictionary, TKey key, TItem item)
        {
            if (dictionary.TryGetValue(key, out List<TItem> list))
                list.Add(item);
            else
                dictionary.Add(key, new List<TItem>(new TItem[1] { item }));
        }

    }

}