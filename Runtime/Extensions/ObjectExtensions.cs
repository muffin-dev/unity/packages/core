/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="Object"/> instances.
    /// </summary>
    public static class ObjectExtension
    {

        /// <inheritdoc cref="RuntimeObjectUtility.GetTransform(Object)"/>
        public static Transform GetTransform(this Object obj)
        {
            return RuntimeObjectUtility.GetTransform(obj);
        }

        /// <inheritdoc cref="RuntimeObjectUtility.TryGetTransform(Object, out Transform)"/>
        public static bool TryGetTransform(this Object obj, out Transform transform)
        {
            return RuntimeObjectUtility.TryGetTransform(obj, out transform);
        }

        /// <inheritdoc cref="RuntimeObjectUtility.GetGameObject(Object)"/>
        public static GameObject GetGameObject(this Object obj)
        {
            return RuntimeObjectUtility.GetGameObject(obj);
        }

        /// <inheritdoc cref="RuntimeObjectUtility.TryGetGameObject(Object, out GameObject)"/>
        public static bool TryGetGameObject(this Object obj, out GameObject gameObject)
        {
            return RuntimeObjectUtility.TryGetGameObject(obj, out gameObject);
        }

    }

}