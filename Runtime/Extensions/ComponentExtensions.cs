/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Reflection;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="Component"/> instances.
    /// </summary>
    public static class ComponentExtensions
    {

        #region Public API

        /// <summary>
        /// Copy the values of the original component onto another one. Note that if the types doesn't match, only the matching properties
        /// will be copied.
        /// </summary>
        /// <param name="original">The original component.</param>
        /// <param name="target">The target component, on which to set the original values.</param>
        /// <param name="ignoreFilterProperties">If true, all properties and fields will be copied BUT the target properties list.</param>
        /// <param name="filterProperties">Defines the names of the properties or fields you want to copy.</param>
        public static void CopyTo(this Component original, Component target, string[] filterProperties = null, bool ignoreFilterProperties = false)
        {
#if UNITY_EDITOR
            if (UnityEditorInternal.ComponentUtility.CopyComponent(original))
            {
                UnityEditorInternal.ComponentUtility.PasteComponentValues(target);
            }
#else
            Type originalType = original.GetType();
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;

            // Write properties (use setters)
            PropertyInfo[] properties = originalType.GetProperties(flags);
            foreach (PropertyInfo property in properties)
            {
                // Skip if the property can't be written
                if (!property.CanWrite)
                    continue;

                // Skip if the property is obsolete
                bool obsolete = false;
                foreach (CustomAttributeData attribute in property.CustomAttributes)
                {
                    if (attribute.AttributeType == typeof(ObsoleteAttribute))
                    {
                        obsolete = true;
                        break;
                    }
                }
                if (obsolete)
                    continue;

                // Skip if the property should be ignored
                if (!ShouldCopyProperty(property.Name, filterProperties, ignoreFilterProperties))
                    continue;

                try { property.SetValue(target, property.GetValue(original, null), null); }
                catch { }
            }

            // Write fields
            FieldInfo[] fields = originalType.GetFields(flags);
            foreach(FieldInfo field in fields)
            {
                if (!ShouldCopyProperty(field.Name, filterProperties, ignoreFilterProperties))
                    continue;

                try { field.SetValue(target, field.GetValue(original)); }
                catch { }
            }
#endif
        }

        /// <summary>
        /// Copy the values of this component onto another one.
        /// </summary>
        /// <inheritdoc cref="CopyTo(Component, Component, string[], bool)"/>
        /// <typeparam name="T">The type of the component to copy.</typeparam>
        public static void CopyTo<T>(this T original, T target, bool ignoreFilterProperties, params string[] filterProperties)
            where T : Component
        {
            CopyTo(original as Component, target as Component, filterProperties, ignoreFilterProperties);
        }

        #endregion


        #region Private API

#if !UNITY_EDITOR
        /// <summary>
        /// Checks if the named property should be copied when using <see cref="CopyTo(Component, Component, string[], bool)"/> function,
        /// depending on the given targets and settings.
        /// </summary>
        /// <param name="propertyName">The name of the current property to copy.</param>
        /// <param name="filterProperties">The list of the properties that should be copied.</param>
        /// <param name="ignoreTargetProperties">If true, the filter properties list becomes the "ignored properties" list.</param>
        /// <returns>Returns true if the named property should be copied, otherwise false.</returns>
        private static bool ShouldCopyProperty(string propertyName, string[] filterProperties, bool ignoreTargetProperties)
        {
            // Returns true if the target properties list is not valid
            if (filterProperties == null || filterProperties.Length == 0)
                return true;

            foreach (string targetProperty in filterProperties)
            {
                // If the property is in the list of targets
                if (targetProperty == propertyName)
                    // Returns true if the property is a target and the targets are not ignored
                    return !ignoreTargetProperties;
            }

            // If the property is not in the list of targets, returns true if targets are ignored, otherwise false
            return ignoreTargetProperties;
        }
#endif

        #endregion

    }

}