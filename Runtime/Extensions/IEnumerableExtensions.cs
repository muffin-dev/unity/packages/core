/**
 * Muffin Dev (c) 2023
 * Author: contact@muffindev.com
 */

using System.Collections;
using System.Collections.Generic;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="IEnumerable{T}"/> instances.
    /// </summary>
    public static class IEnumerableExtensions
    {

        #region Delegates

        /// <summary>
        /// Called for each item when using the <see cref="Map{TInput, TOutput}(IList{TInput}, MapPredicateDelegate{TInput, TOutput})"/> function.
        /// </summary>
        /// <typeparam name="TInput">The type of the elements in the list.</typeparam>
        /// <typeparam name="TOutput">The type of the output data for each item.</typeparam>
        /// <param name="input">The original list item.</param>
        /// <returns>Returns the "mapped" value.</returns>
        public delegate TOutput MapPredicateDelegate<TInput, TOutput>(TInput input);

        #endregion


        #region Public API

        /// <summary>
        /// Joins the items in the given collection into a single string using a separator.
        /// </summary>
        /// <param name="enumerable">The collection to pack into a single string.</param>
        /// <param name="separator">The character(s) that separates each elements in the output text.</param>
        /// <returns>Returns the processed string.</returns>
        public static string Join(this IEnumerable enumerable, string separator)
        {
            return string.Join(separator, enumerable);
        }

        /// <param name="mapFunc">The function called for every item to convert it into a string.</param>
        /// <inheritdoc cref="Join{T}(IEnumerable{T}, string)"/>
        public static string Join<T>(this IEnumerable<T> enumerable, string separator, MapPredicateDelegate<T, string> mapFunc)
        {
            return string.Join(separator, Map(enumerable, mapFunc));
        }

        /// <summary>
        /// Creates a new array populated with the results of calling a given function every item in the given collection.
        /// </summary>
        /// <typeparam name="TInput">The type of an item in the input collection.</typeparam>
        /// <typeparam name="TOutput">The type of the output data when calling the action function on each item.</typeparam>
        /// <param name="enumerable">The collection you want to "map".</param>
        /// <param name="mapFunc">The function to call to provide a data for each item.</param>
        /// <returns>Returns the mapped items array.</returns>
        public static TOutput[] Map<TInput, TOutput>(this IEnumerable<TInput> enumerable, MapPredicateDelegate<TInput, TOutput> mapFunc)
        {
            List<TOutput> outputList = new List<TOutput>();

            foreach(TInput item in enumerable)
                outputList.Add(mapFunc(item));

            return outputList.ToArray();
        }

        #endregion

    }

}