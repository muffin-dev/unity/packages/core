/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="Transform"/> instances.
    /// </summary>
    public static class TransformExtensions
    {

        /// <summary>
        /// Destroys all child <see cref="GameObject"/> of this <see cref="Transform"/>.
        /// </summary>
        public static void ClearHierarchy(this Transform transform)
        {
            foreach (Transform child in transform)
            {
                if (child == transform)
                    continue;

                if (Application.isPlaying)
                {
                    Object.Destroy(child.gameObject);
                }
                else
                {
                    Object.DestroyImmediate(child.gameObject, false);
                }
            }
        }

    }

}