/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="Vector3Int"/> values.
    /// </summary>
    public static class Vector3IntExtensions
    {

        /// <summary>
        /// Returns a new <see cref="Vector3Int"/> with its values superior or equal to the given minimum value.
        /// </summary>
        /// <param name="vector">The input vector to compute.</param>
        /// <param name="min">The minimum value of the given vector.</param>
        /// <returns>Returns the computed vector.</returns>
        public static Vector3Int Min(this Vector3Int vector, int min)
        {
            return new Vector3Int
            (
                Mathf.Max(vector.x, min),
                Mathf.Max(vector.y, min),
                Mathf.Max(vector.z, min)
            );
        }

        /// <summary>
        /// Returns a new <see cref="Vector3Int"/> instance with its values inferior or equal to the given maximum value.
        /// </summary>
        /// <inheritdoc cref="Min(Vector3Int, int)"/>
        /// <param name="max">The maximum value of the given vector.</param>
        public static Vector3Int Max(this Vector3Int vector, int max)
        {
            return new Vector3Int
            (
                Mathf.Min(vector.x, max),
                Mathf.Min(vector.y, max),
                Mathf.Min(vector.z, max)
            );
        }

    }

}