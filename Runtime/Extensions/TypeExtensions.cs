/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="Type"/> instances.
    /// </summary>
    public static class TypeExtension
    {

        /// <summary>
        /// Gets the full <see cref="Type"/> name string, including the assmebly name. The output string looks like:
        /// "TypeNamespace.TypeName, AssemblyName, Version=0.0.0.0, Culture=neutral, PublicKeyKoken=null"
        /// </summary>
        /// <param name="type">The <see cref="Type"/> of which you want to get the full name string.</param>
        /// <returns>Returns the computed full name string, or null if the given <see cref="Type"/> is null.</returns>
        public static string GetFullNameWithAssembly(this Type type)
        {
            return type != null ? $"{type.FullName}, {type.Assembly}" : null;
        }

        /// <summary>
        /// Checks if the given <see cref="Type"/> is "really" a primitive. This function is meant to replace the
        /// <see cref="Type.IsPrimitive"/> property, which will return false even if the type is a string, decimal, long, ulong, short or
        /// ushort.
        /// </summary>
        /// <param name="type">The <see cref="Type"/> you want to check as primitive.</param>
        /// <returns>Returns true if the <see cref="Type"/> is "really" a primitive, otherwise false.</returns>
        public static bool IsReallyPrimitive(this Type type)
        {
            return
                type.IsPrimitive ||
                type == typeof(string) ||
                type == typeof(decimal) ||
                type == typeof(long) ||
                type == typeof(ulong) ||
                type == typeof(short) ||
                type == typeof(ushort);
        }

        /// <summary>
        /// Tries to get a class attribute on this type.
        /// </summary>
        /// <param name="type">The type from which you want to get the class attribute.</param>
        /// <param name="attributeType">The type of the attribute you want to get.</param>
        /// <param name="attribute">Outputs the found attribute.</param>
        /// <returns>Returns true if this type has the expected attribute, otherwise false.</returns>
        public static bool TryGetAttribute(this Type type, Type attributeType, out Attribute attribute)
        {
            attribute = Attribute.GetCustomAttribute(type, attributeType);
            return attribute != null;
        }

        /// <inheritdoc cref="TryGetAttribute(Type, Type, out Attribute)"/>
        /// <typeparam name="T">The type of the attribute you want to get.</typeparam>
        public static bool TryGetAttribute<T>(this Type type, out T attribute)
            where T : Attribute
        {
            attribute = Attribute.GetCustomAttribute(type, typeof(T)) as T;
            return attribute != null;
        }

        /// <summary>
        /// Checks if this type has a given class attribute.
        /// </summary>
        /// <param name="type">The type from which you want to check for the class attribute.</param>
        /// <param name="attributeType">The type of the attribute you want to check.</param>
        /// <returns>Returns true if this type has the expected attribute, otherwise false.</returns>
        public static bool HasAttribute(this Type type, Type attributeType)
        {
            return Attribute.IsDefined(type, attributeType);
        }

        /// <inheritdoc cref="HasAttribute(Type, Type)"/>
        /// <typeparam name="T">The type of the attribute you want to check.</typeparam>
        public static bool HasAttribute<T>(this Type type)
            where T : Attribute
        {
            return Attribute.IsDefined(type, typeof(T));
        }

    }

}