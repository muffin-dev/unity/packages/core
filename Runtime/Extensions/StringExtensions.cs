/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Collections.Generic;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for string values.
    /// </summary>
    public static class StringExtensions
    {

        /// <summary>
        /// The ASCII code for "0".
        /// </summary>
        public const byte ASCII_0 = (byte)'0';

        /// <summary>
        /// The ASCII code for "9".
        /// </summary>
        public const byte ASCII_9 = (byte)'9';

        /// <summary>
        /// The ASCII code for "A".
        /// </summary>
        public const byte ASCII_A = (byte)'A';

        /// <summary>
        /// The ASCII code for "Z".
        /// </summary>
        public const byte ASCII_Z = (byte)'Z';

        /// <summary>
        /// The ASCII code for "a".
        /// </summary>
        public const byte ASCII_a = (byte)'a';

        /// <summary>
        /// The ASCII code for "z".
        /// </summary>
        public const byte ASCII_z = (byte)'z';

        /// <summary>
        /// Shortcut for using <see cref="string.Split(string[], StringSplitOptions)"/> with one string as a separator.
        /// </summary>
        /// <param name="str">The string to split.</param>
        /// <param name="separator">The separator to use for splitting the string.</param>
        /// <param name="splitOptions">Eventual split options.</param>
        /// <returns>Returns the splitted string.</returns>
        public static string[] Split(this string str, string separator, StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries)
        {
            return str.Split(new string[1] { separator }, splitOptions);
        }

        /// <summary>
        /// Splits a string by newline characters.
        /// </summary>
        /// <param name="str">The string to split.</param>
        /// <param name="splitOptions">Eventual split options.</param>
        /// <returns>Returns the splitted string.</returns>
        public static string[] SplitLines(this string str, StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries)
        {
            return str.Split(new string[] { "\n", "\r\n" }, splitOptions);
        }

        /// <summary>
		/// Creates a string that contains several iterations of the given input string.
		/// </summary>
		/// <param name="str">The string to repeat.</param>
		/// <param name="iterations">The number of times the input string is repeated in the output.</param>
		/// <returns>Returns the input string repeated the given number of times.</returns>
		public static string Repeat(this string str, int iterations)
        {
            string output = "";
            for (int i = 0; i < System.Math.Max(0, iterations); i++)
            {
                output += str;
            }
            return output;
        }

        /// <summary>
        /// Counts the number of occurrences of a given string into another.
        /// </summary>
        /// <param name="str">The string in which you want to count the occurrences.</param>
        /// <param name="pattern">The pattern you want to count the occurrences.</param>
        /// <returns>Returns the number of occurrences found.</returns>
        public static int Occurrences(this string str, string pattern)
        {
            return Regex.Matches(str, pattern).Count;
        }

        /// <inheritdoc cref="Slice(string, int, int)"/>
        /// <returns>Returns the substring, from the given start index to the end of the string.</returns>
        public static string Slice(this string str, int start)
        {
            return Slice(str, start, str.Length);
        }

        /// <summary>
        /// Extracts a substring, from an index to another.
        /// </summary>
        /// <param name="str">The string you want to slice.</param>
        /// <param name="start">The start index of the string to extract. If negative, it's used as (str.Length - start).</param>
        /// <param name="end">The end index of the string to extract (exclusive). If negative, it's used as (str.Length - end). If the end
        /// index is lower than the start index, start and end are inverted.</param>
        /// <returns>Returns the substring, from the given start index to the given end one.</returns>
        public static string Slice(this string str, int start, int end)
        {
            if (start < 0)
                start = str.Length + start;
            if (start < 0)
                start = 0;

            if (end < 0)
                end = str.Length + end;
            if (end < 0)
                end = 0;

            if (start > end)
            {
                int tmp = start;
                start = end;
                end = tmp;
            }

            if (end > str.Length)
                end = str.Length;

            return str.Substring(start, end - start);
        }

        /// <summary>
        /// Remove all diacritic characters, keeping their base character in the output string. Examples: "�BCD�?" -> "aBCDE?".
        /// </summary>
        /// <param name="str">The string from which you want to remove the diacritic characters.</param>
        /// <returns>Returns the processed string.</returns>
        public static string RemoveDiacritics(this string str)
        {
            str = str.Normalize(NormalizationForm.FormD);
            string output = string.Empty;

            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];
                UnicodeCategory category = CharUnicodeInfo.GetUnicodeCategory(c);
                if (category != UnicodeCategory.NonSpacingMark && category != UnicodeCategory.Surrogate)
                    output += c;
            }
            return output;
        }

        /// <summary>
        /// Removes all characters that are not letters or digits in the given string.
        /// </summary>
        /// <param name="str">The string from which you want to remove the characters.</param>
        /// <param name="allowedChars">The allowed characters in the output string that won't be removed.</param>
        /// <returns>Returns the processed string.</returns>
        public static string RemoveSpecialChars(this string str, string allowedChars)
        {
            string output = string.Empty;
            for (int i = 0; i < str.Length; i++)
            {
                if (!IsLetterOrDigit(str[i]))
                {
                    bool allowed = false;
                    for (int j = 0; j < allowedChars.Length; j++)
                    {
                        if(str[i] == allowedChars[j])
                        {
                            allowed = true;
                            break;
                        }
                    }

                    if (!allowed)
                        continue;
                }

                output += str[i];
            }

            return output;
        }

        #region Paths

        /// <inheritdoc cref="PathUtility.ToPath(string)"/>
        public static string Path(this string str)
        {
            return PathUtility.ToPath(str);
        }

        /// <inheritdoc cref="PathUtility.ToPath(string, char)"/>
        public static string Path(this string str, char separator)
        {
            return PathUtility.ToPath(str, separator);
        }

        /// <inheritdoc cref="PathUtility.ToPaths(IList{string})"/>
        public static void Paths(this IList<string> strs)
        {
            PathUtility.ToPaths(strs);
        }

        /// <inheritdoc cref="PathUtility.ToPaths(IList{string}, char)"/>
        public static void Paths(this IList<string> strs, char separator)
        {
            PathUtility.ToPaths(strs, separator);
        }

        /// <inheritdoc cref="PathUtility.ToAbsolutePath(string)"/>
        public static string AbsolutePath(this string path)
        {
            return PathUtility.ToAbsolutePath(path);
        }

        /// <inheritdoc cref="PathUtility.ToRelativePath(string)"/>
        public static string RelativePath(this string path)
        {
            return PathUtility.ToRelativePath(path);
        }

        /// <inheritdoc cref="PathUtility.IsProjectPath(string)"/>
        public static bool IsProjectPath(this string path)
        {
            return PathUtility.IsProjectPath(path);
        }

        /// <inheritdoc cref="PathUtility.CombinePaths(string, string[])"/>
        public static string CombinePaths(this string absPath, params string[] relPaths)
        {
            return PathUtility.CombinePaths(absPath, relPaths);
        }
        
        /// <inheritdoc cref="PathUtility.GetDirectoryPath(string)"/>
        public static string GetDirectoryPath(this string path)
        {
            return PathUtility.GetDirectoryPath(path);
        }

        /// <summary>
        /// Converts this string into a <see cref="GUIContent"/> instance.
        /// </summary>
        /// <param name="text">The text of the <see cref="GUIContent"/>.</param>
        /// <param name="icon">The icon of the <see cref="GUIContent"/>.</param>
        /// <param name="tooltip">The tooltip of the <see cref="GUIContent"/>.</param>
        /// <returns>Returns the converted <see cref="GUIContent"/>.</returns>
        public static GUIContent ToGUIContent(this string text, Texture2D icon, string tooltip)
        {
            return new GUIContent(text, icon, tooltip);
        }

        /// <inheritdoc cref="ToGUIContent(string, Texture2D, string)"/>
        public static GUIContent ToGUIContent(this string text, Texture2D icon)
        {
            return new GUIContent(text, icon);
        }

        /// <inheritdoc cref="ToGUIContent(string, Texture2D, string)"/>
        public static GUIContent ToGUIContent(this string text, string tooltip)
        {
            return new GUIContent(text, tooltip);
        }

        /// <inheritdoc cref="ToGUIContent(string, Texture2D, string)"/>
        public static GUIContent ToGUIContent(Texture2D icon, string tooltip)
        {
            return new GUIContent(icon, tooltip);
        }

        /// <inheritdoc cref="ToGUIContent(string, Texture2D, string)"/>
        public static GUIContent ToGUIContent(this string text)
        {
            return new GUIContent(text);
        }

        #endregion


        #region Private API

        /// <summary>
        /// Checks if the given character is a letter (capital or not) or a digit.
        /// </summary>
        /// <param name="c">The character you want to check.</param>
        /// <returns>Returns true if the given character is a letter or a digit, otherwise false.</returns>
        private static bool IsLetterOrDigit(this char c)
        {
            byte asByte = (byte)c;
            return
                (asByte >= ASCII_0 && asByte <= ASCII_9) ||
                (asByte >= ASCII_A && asByte <= ASCII_Z) ||
                (asByte >= ASCII_a && asByte <= ASCII_z);
        }

        #endregion

    }

}