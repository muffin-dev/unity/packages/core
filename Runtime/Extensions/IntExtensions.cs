/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for interger values.
    /// </summary>
    public static class IntExtensions
    {

        /// <summary>
        /// "Pads" this value by adding leading 0s.
        /// </summary>
        /// <param name="number">The number you want to pad.</param>
        /// <param name="length">The expected length of the output string, after adding leading 0 if applicable.</param>
        /// <returns>Returns the padded number as string.</returns>
        public static string Pad(this int number, int length)
        {
            return number.ToString().PadLeft(length, '0');
        }

        /// <summary>
        /// Apply proportions to this value, between a given min and max, and report it to the given ratio.
        /// </summary>
        /// <inheritdoc cref="Math.Ratio(float, float, float, float)"/>
        public static float Ratio(this int value, float min, float max, float ratio = 1f)
        {
            return Math.Ratio(value, min, max, ratio);
        }

        /// <summary>
        /// Apply proportions to this value, between 0 and a given max, and report it to the given ratio.
        /// </summary>
        /// <inheritdoc cref="Ratio(int, float, float, float)"/>
        public static float Ratio(this int value, float max, float ratio = 1f)
        {
            return Math.Ratio(value, max, ratio);
        }

        /// <summary>
        /// Apply proportions to this value, between a given min and max, and report it to a percentage.
        /// </summary>
        /// <inheritdoc cref="Ratio(int, float, float, float)"/>
        public static float Percents(this int value, float min, float max)
        {
            return Math.Percents(value, min, max);
        }

        /// <summary>
        /// Apply proportions to this value, between 0 and a given max, and report it to a percentage.
        /// </summary>
        /// <inheritdoc cref="Percents(int, float, float)"/>
        public static float Percents(this int value, float max)
        {
            return Math.Percents(value, max);
        }

        /// <summary>
        /// Clamps this value between given min and max.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="min">The minimum possible value.</param>
        /// <param name="max">The maximum possible value.</param>
        /// <returns>Returns the clamped value.</returns>
        public static int Clamp(this int value, int min, int max)
        {
            return Mathf.Clamp(value, min, max);
        }

    }

}