/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.IO;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for working with IO features.
    /// </summary>
    public static class IOExtensions
    {

        /// <summary>
        /// Copies all the files of the source directory to the output directory.
        /// </summary>
        /// <param name="source">The source directory.</param>
        /// <param name="outputDirectory">The output directory. If relative path given, it's resolved from this project's path.</param>
        /// <returns>Returns true if at least one file has been copied to the output directory, otherwise false.</returns>
        public static bool CopyTo(this DirectoryInfo source, string outputDirectory)
        {
            outputDirectory = outputDirectory.AbsolutePath();
            Directory.CreateDirectory(outputDirectory);

            foreach (DirectoryInfo dir in source.GetDirectories("*", SearchOption.AllDirectories))
                Directory.CreateDirectory($"{outputDirectory}{dir.FullName.Substring(source.FullName.Length)}");

            FileInfo[] files = source.GetFiles("*", SearchOption.AllDirectories);
            if (files.Length <= 0)
                return false;

            foreach (FileInfo f in files)
                File.Copy(f.FullName, $"{outputDirectory}{f.FullName.Substring(source.FullName.Length)}", true);

            return true;
        }

    }

}