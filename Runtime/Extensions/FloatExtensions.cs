/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for float values.
    /// </summary>
    public static class FloatExtensions
    {

        /// <inheritdoc cref="Math.Approximately(float, float)"/>
        public static bool Approximately(this float a, float b)
        {
            return Mathf.Approximately(a, b);
        }

        /// <inheritdoc cref="Math.Approximately(float, float, float)"/>
        public static bool Approximately(this float a, float b, float epsilon)
        {
            return a > b - epsilon && a < b + epsilon;
        }

        /// <summary>
        /// Apply proportions to this value, between a given min and max, and report it to the given ratio.
        /// </summary>
        /// <inheritdoc cref="Math.Ratio(float, float, float, float)"/>
        public static float Ratio(this float value, float min, float max, float ratio = 1f)
        {
            return Math.Ratio(value, min, max, ratio);
        }

        /// <summary>
        /// Apply proportions to this value, between a given min and max, and report it to a percentage.
        /// </summary>
        /// <inheritdoc cref="Ratio(float, float, float, float)"/>
        public static float Percents(this float value, float min, float max)
        {
            return Math.Percents(value, min, max);
        }

        /// <summary>
        /// Apply proportions to this value, between 0 and a given max, and report it to a percentage.
        /// </summary>
        /// <inheritdoc cref="Percents(float, float, float)"/>
        public static float Percents(this float value, float max)
        {
            return Math.Percents(value, max);
        }

        /// <summary>
        /// Clamps this value between given min and max.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="min">The minimum possible value.</param>
        /// <param name="max">The maximum possible value.</param>
        /// <returns>Returns the clamped value.</returns>
        public static float Clamp(this float value, float min, float max)
        {
            return Mathf.Clamp(value, min, max);
        }

    }

}