/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="AnimationCurve"/> instances.
    /// </summary>
    public static class AnimationCurveExtension
    {

        /// <summary>
        /// Default <see cref="Keyframe"/> value.
        /// </summary>
        public static Keyframe DefaultKeyframe => new Keyframe(0f, 0f);

        /// <summary>
        /// Calculates the duration of an <see cref="AnimationCurve"/>, using its first and last keyframes on X axis.
        /// </summary>
        /// <param name="curve">The curve of which you want to compute the duration.</param>
        /// <returns>Returns the calculated duration of the <see cref="AnimationCurve"/>. Note that it will return 0 if the
        /// <see cref="AnimationCurve"/> counts less than 2 keyframes.</returns>
        public static float ComputeDuration(this AnimationCurve curve)
        {
            float minTime = GetFirstKeyframe(curve).time;
            float maxTime = GetLastKeyframe(curve).time;
            return maxTime - minTime;
        }

        /// <summary>
        /// Calculates the value range of an <see cref="AnimationCurve"/>, using its lowest and highest keyframes on Y axis.
        /// </summary>
        /// <param name="curve">The curve of which you want to compute the range.</param>
        /// <returns>Returns the calculated value range of the <see cref="AnimationCurve"/>. Note that it will return 0 if the
        /// <see cref="AnimationCurve"/> counts less than 2 keyframes.</returns>
        public static float ComputeRange(this AnimationCurve curve)
        {
            float minRange = GetMinKeyframe(curve).value;
            float maxRange = GetMaxKeyframe(curve).value;
            return maxRange - minRange;
        }

        /// <summary>
        /// Gets the first keyframe on X axis of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the first keyrame.</param>
        /// <returns>Returns the found keyframe, or the default one if there's no keyframe on the curve.</returns>
        public static Keyframe GetFirstKeyframe(this AnimationCurve curve)
        {
            int count = curve.keys.Length;
            if (count > 0)
            {
                int minTimeIndex = 0;
                for (int i = 1; i < count; i++)
                {
                    if (curve.keys[i].time < curve.keys[minTimeIndex].time)
                    {
                        minTimeIndex = i;
                    }
                }
                return curve.keys[minTimeIndex];
            }
            return DefaultKeyframe;
        }

        /// <summary>
        /// Gets the last keyframe on X axis of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the last keyrame.</param>
        /// <returns>Returns the found keyframe, or the default one if there's no keyframe on the curve.</returns>
        public static Keyframe GetLastKeyframe(this AnimationCurve curve)
        {
            int count = curve.keys.Length;
            if (count > 0)
            {
                int maxTimeIndex = 0;
                for (int i = 1; i < count; i++)
                {
                    if (curve.keys[i].time > curve.keys[maxTimeIndex].time)
                    {
                        maxTimeIndex = i;
                    }
                }
                return curve.keys[maxTimeIndex];
            }
            return DefaultKeyframe;
        }

        /// <summary>
        /// Gets the lowest keyframe on Y axis of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the minimum keyrame.</param>
        /// <returns>Returns the found keyframe, or the default one if there's no keyframe on the curve.</returns>
        public static Keyframe GetMinKeyframe(this AnimationCurve curve)
        {
            int count = curve.keys.Length;
            if (count > 0)
            {
                int minValueIndex = 0;
                for (int i = 1; i < count; i++)
                {
                    if (curve.keys[i].value < curve.keys[minValueIndex].value)
                    {
                        minValueIndex = i;
                    }
                }
                return curve.keys[minValueIndex];
            }
            return DefaultKeyframe;
        }

        /// <summary>
        /// Gets the highest keyframe on Y axis of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the maximum keyrame.</param>
        /// <returns>Returns the found keyframe, or the default one if there's no keyframe on the curve.</returns>
        public static Keyframe GetMaxKeyframe(this AnimationCurve curve)
        {
            int count = curve.keys.Length;
            if (count > 0)
            {
                int maxValueIndex = 0;
                for (int i = 1; i < count; i++)
                {
                    if (curve.keys[i].value > curve.keys[maxValueIndex].value)
                    {
                        maxValueIndex = i;
                    }
                }
                return curve.keys[maxValueIndex];
            }
            return DefaultKeyframe;
        }

        /// <summary>
        /// Gets the minimum time of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the min time value.</param>
        /// <returns>Returns the found minimum time, or default keyframe's if there's no keyframe on the curve.</returns>
        public static float GetMinTime(this AnimationCurve curve)
        {
            return curve.GetFirstKeyframe().time;
        }

        /// <summary>
        /// Gets the maximum time of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the max time value.</param>
        /// <returns>Returns the found maximum time, or default keyframe's if there's no keyframe on the curve.</returns>
        public static float GetMaxTime(this AnimationCurve curve)
        {
            return curve.GetLastKeyframe().time;
        }

        /// <summary>
        /// Gets the minimum value of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the min value.</param>
        /// <returns>Returns the found minimum value, or default keyframe's if there's no keyframe on the curve.</returns>
        public static float GetMinValue(this AnimationCurve curve)
        {
            return curve.GetMinKeyframe().value;
        }

        /// <summary>
        /// Gets the maximum value of this <see cref="AnimationCurve"/>.
        /// </summary>
        /// <param name="curve">The curve of which you want to get the max value.</param>
        /// <returns>Returns the found maximum value, or default keyframe's if there's no keyframe on the curve.</returns>
        public static float GetMaxValue(this AnimationCurve curve)
        {
            return curve.GetMaxKeyframe().value;
        }

        /// <summary>
        /// Makes this <see cref="AnimationCurve"/> repeat (loop) before its first frame, and after its last frame.
        /// </summary>
        /// <param name="curve">The curve you want to set the wrap mode.</param>
        /// <returns>Returns the updated input curve.</returns>
        public static AnimationCurve Loop(this AnimationCurve curve)
        {
            curve.preWrapMode = WrapMode.Loop;
            curve.postWrapMode = WrapMode.Loop;
            return curve;
        }

        /// <summary>
        /// Makes this <see cref="AnimationCurve"/> ping-pong before its first frame, and after its last frame.
        /// </summary>
        /// <param name="curve">The curve you want to set the wrap mode.</param>
        /// <returns>Returns the updated input curve.</returns>
        public static AnimationCurve PingPong(this AnimationCurve curve)
        {
            curve.preWrapMode = WrapMode.PingPong;
            curve.postWrapMode = WrapMode.PingPong;
            return curve;
        }

    }

}