/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="Exception"/> instances.
    /// </summary>
    public static class ExceptionExtensions
    {

        /// <inheritdoc cref="ToLogEntry(Exception, string, object, ELogLevel)"/>
        public static ExceptionLogEntry ToLogEntry(this Exception exception, ELogLevel logLevel = ELogLevel.Fatal)
        {
            return ToLogEntry(exception, null, null, logLevel);
        }

        /// <inheritdoc cref="ToLogEntry(Exception, string, object, ELogLevel)"/>
        public static ExceptionLogEntry ToLogEntry(this Exception exception, string message, ELogLevel logLevel = ELogLevel.Fatal)
        {
            return ToLogEntry(exception, message, null, logLevel);
        }

        /// <inheritdoc cref="ToLogEntry(Exception, string, object, ELogLevel)"/>
        public static ExceptionLogEntry ToLogEntry(this Exception exception, object data, ELogLevel logLevel = ELogLevel.Fatal)
        {
            return ToLogEntry(exception, null, data, logLevel);
        }

        /// <summary>
        /// Creates a log entry that contains this exception.
        /// </summary>
        /// <inheritdoc cref="ExceptionLogEntry(Exception, string, object, ELogLevel)"/>
        /// <returns>Returns the created log entry.</returns>
        public static ExceptionLogEntry ToLogEntry(this Exception exception, string message, object data, ELogLevel logLevel = ELogLevel.Fatal)
        {
            return new ExceptionLogEntry(exception, message, data, logLevel);
        }

    }

}