/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Reflection;
using System.Collections;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Extension functions for <see cref="MonoBehaviour"/> instances.
    /// </summary>
    public static class MonoBehaviourExtensions
    {

        private const BindingFlags FIND_METHOD_FLAGS = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Invokes the named function on the given object after a delay.
        /// Does the same as MonoBehaviour.Invoke(), but allow you to pass parameters to the invoked function.
        /// </summary>
        /// <param name="obj">The component on which you want to invoke the function.</param>
        /// <param name="methodName">The name of the function to call. Note that you can use the <see cref="nameof"/>() keyword in order to avoid naming
        /// errors.</param>
        /// <param name="delay">The delay after which you want the function to be invoked. If this value is less or equal to 0, calls the
        /// function at the end of the frame. In this case, you should call the function directly.</param>
        /// <param name="params">The parameters you want to pass to the named function.</param>
        public static void Invoke(this MonoBehaviour obj, string methodName, float delay, params object[] @params)
        {
            obj.StartCoroutine(InvokeCoroutine(obj, methodName, delay, @params));
        }

        /// <summary>
        /// Invokes the named function on the given object after a delay.
        /// </summary>
        /// <inheritdoc cref="Invoke(MonoBehaviour, string, float, object[])"/>
        private static IEnumerator InvokeCoroutine(this MonoBehaviour obj, string methodName, float delay, params object[] @params)
        {
            MethodInfo function = obj.GetType().GetMethod(methodName, FIND_METHOD_FLAGS);

            if (function == null)
            {
                Debug.LogError($"No function {methodName}() found on component {obj.GetType().Name}");
                yield return null;
            }

            if (delay <= 0f)
                yield return new WaitForEndOfFrame();
            else
                yield return new WaitForSeconds(delay);

            function.Invoke(obj, @params);
        }

    }

}