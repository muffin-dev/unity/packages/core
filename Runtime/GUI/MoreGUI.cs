/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaneous functions and values for drawing user interfaces.
    /// </summary>
    public static class MoreGUI
    {

        #region Fields

        public const float HEIGHT_XL = 48f;
        public const float HEIGHT_L = 36f;
        public const float HEIGHT_M = 28f;
        public const float HEIGHT_S = 20f;
        public const float HEIGHT_XS = 16f;

        public const float WIDTH_XL = 180f;
        public const float WIDTH_L = 140f;
        public const float WIDTH_M = 112f;
        public const float WIDTH_S = 80f;
        public const float WIDTH_XS = 40f;

        /// <summary>
        /// Size of an horizontal margin.
        /// </summary>
        public const float H_MARGIN = 2f;

        /// <summary>
        /// Size of a vertical margin.
        /// </summary>
        public const float V_MARGIN = 2f;

        public static readonly GUILayoutOption HeightXL = GUILayout.Height(HEIGHT_XL);
        public static readonly GUILayoutOption HeightL = GUILayout.Height(HEIGHT_L);
        public static readonly GUILayoutOption HeightM = GUILayout.Height(HEIGHT_M);
        public static readonly GUILayoutOption HeightS = GUILayout.Height(HEIGHT_S);
        public static readonly GUILayoutOption HeightXS = GUILayout.Height(HEIGHT_XS);

        public static readonly GUILayoutOption WidthXL = GUILayout.Width(WIDTH_XL);
        public static readonly GUILayoutOption WidthL = GUILayout.Width(WIDTH_L);
        public static readonly GUILayoutOption WidthM = GUILayout.Width(WIDTH_M);
        public static readonly GUILayoutOption WidthS = GUILayout.Width(WIDTH_S);
        public static readonly GUILayoutOption WidthXS = GUILayout.Width(WIDTH_XS);

        #endregion

    }

}