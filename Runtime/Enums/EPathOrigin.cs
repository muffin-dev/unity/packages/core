/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Core
{

    /// <summary>
    /// Defines where a path string should start. Used by <see cref="PathsHistory"/> and <see cref="PathsCollection"/> to process paths.
    /// </summary>
    public enum EPathOrigin
    {

        /// <summary>
        /// Keep the path string as is.
        /// </summary>
        Source = 0,

        /// <summary>
        /// If a string represents a relative path, resolve it from the current project's absolute path.
        /// </summary>
        Absolute = 1,

        /// <summary>
        /// If a string represents an absolute path that leads to the current project, make it relative from the current project's root
        /// directory.
        /// </summary>
        Relative = 2,

    }

}