/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Utility class for creating custom timer, updated manually.
    /// </summary>
    [System.Serializable]
    public class TimerLite
    {

        #region Delegates

        /// <summary>
        /// Called when this timer ends.
        /// </summary>
        public delegate void ElapsedDelegate();

        /// <summary>
        /// Called when this timer ends, using the input payload.
        /// </summary>
        /// <param name="paylod">The current payload.</param>
        public delegate void ElapsedPayloadDelegate(object paylod);

        #endregion


        #region Fields

        private const float STOPPED_TIMER_VALUE = -1f;

        /// <inheritdoc cref="ElapsedDelegate"/>
        public event ElapsedDelegate OnElapsed;

        /// <inheritdoc cref="ElapsedPayloadDelegate"/>
        public event ElapsedPayloadDelegate OnElapsedPayload;

        /// <summary>
        /// The duration of the timer, in seconds. This value can't be negative.
        /// </summary>
        [SerializeField, Min(0)]
        private float _duration = 0f;

        /// <summary>
        /// The current timer value, from duration to 0, in seconds. If negative, this timer is stopped.
        /// </summary>
        private float _timer = STOPPED_TIMER_VALUE;

        /// <summary>
        /// The current payload for this timer.
        /// </summary>
        private object _payload = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TimerLite() { }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <inheritdoc cref="Init(float, ElapsedDelegate, ElapsedPayloadDelegate, object)"/>
        public TimerLite(float duration)
        {
            Init(duration, null, null, null);
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <inheritdoc cref="Init(float, ElapsedDelegate, ElapsedPayloadDelegate object)"/>
        public TimerLite(float duration, ElapsedDelegate onElapsed)
        {
            Init(duration, onElapsed, null, null);
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <inheritdoc cref="Init(float, ElapsedDelegate, ElapsedPayloadDelegate, object)"/>
        public TimerLite(float duration, ElapsedPayloadDelegate onElapsedPayload, object payload = null)
        {
            Init(duration, null, onElapsedPayload, payload);
        }

        /// <summary>
        /// Initializes this timer.
        /// </summary>
        /// <param name="duration">The duration of this timer.</param>
        /// <param name="onElapsed">The function to call when this timer is elapsed.</param>
        /// <param name="onElapsedPayload">The function to call when this timer is elapsed, with payload.</param>
        /// <param name="payload">The data to send when this timer ends.</param>
        private void Init(float duration, ElapsedDelegate onElapsed, ElapsedPayloadDelegate onElapsedPayload, object payload)
        {
            Duration = duration;
            Payload = payload;

            if (onElapsed != null)
                OnElapsed += onElapsed;
            if (onElapsedPayload != null)
                OnElapsedPayload += onElapsedPayload;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Checks if this timer is running.
        /// </summary>
        public bool IsRunning => _timer >= 0f;

        /// <inheritdoc cref="_duration"/>
        public float Duration
        {
            get => _duration;
            set => _duration = Mathf.Max(0, value);
        }

        /// <inheritdoc cref="_payload"/>
        public object Payload
        {
            get => _payload;
            set => _payload = value;
        }

        /// <summary>
        /// Gets the elapsed time, in seconds.
        /// </summary>
        public float ElapsedTime => _duration - Mathf.Max(0, _timer);

        /// <summary>
        /// Gets the elapsed time, reported between 0 and 1, where 1 means the timer is elapsed, and 0 means the timer has just started.
        /// </summary>
        public float ElapsedTimeRatio => ElapsedTime.Ratio(0, _duration);

        /// <summary>
        /// Gets the remaining time, in seconds.
        /// </summary>
        public float RemainingTime => Mathf.Max(0, _timer);

        /// <summary>
        /// Gets the elapsed time, reported between 0 and 1, where 1 means the timer has just started, and 0 means the timer is elapsed.
        /// </summary>
        public float RemainingTimeRatio => RemainingTime.Ratio(0, _duration);

        /// <summary>
        /// Starts this timer, If the timer is already running, this function just sets the new data.
        /// </summary>
        /// <param name="duration">The new duration of this timer.</param>
        /// <param name="payload">The data to send when this timer ends.</param>
        public void Start(float duration, object payload)
        {
            Duration = duration;
            Payload = payload;
            Start();
        }

        /// <inheritdoc cref="Start(float, object)"/>
        public void Start(float duration)
        {
            Duration = duration;
            Start();
        }

        /// <inheritdoc cref="Start(float, object)"/>
        public void Start(object payload)
        {
            Payload = payload;
            Start();
        }

        /// <summary>
        /// Starts this timer. If the timer is already running, this function does nothing.
        /// </summary>
        public void Start()
        {
            if (!IsRunning)
                _timer = _duration;
        }

        /// <summary>
        /// Reset the timer to 0 and starts it.
        /// </summary>
        /// <param name="duration">The new duration of this timer.</param>
        /// <param name="payload">The data to send when this timer ends.</param>
        public void Restart(float duration, object payload)
        {
            Duration = duration;
            Payload = payload;
            Restart();
        }

        /// <inheritdoc cref="Restart(float, object)"/>
        public void Restart(float duration)
        {
            Duration = duration;
            Restart();
        }

        /// <inheritdoc cref="Restart(float, object)"/>
        public void Restart(object payload)
        {
            Payload = payload;
            Restart();
        }

        /// <inheritdoc cref="Restart(float, object)"/>
        public void Restart()
        {
            _timer = _duration;
        }

        /// <summary>
        /// Updates this timer.
        /// </summary>
        /// <param name="deltaTime">The time elapsed since the last update.</param>
        /// <returns>Returns true if the timer has been updated successfully, otherwise false.</returns>
        public bool Update(float deltaTime)
        {
            if (!IsRunning)
                return false;

            _timer -= deltaTime;
            if (_timer <= 0f)
                Trigger();

            return true;
        }

        /// <summary>
        /// Stops this timer (so it can't be updated anymore).
        /// </summary>
        public void Stop()
        {
            _timer = STOPPED_TIMER_VALUE;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Ends this timer and invoke the callbacks.
        /// </summary>
        private void Trigger()
        {
            Stop();

            OnElapsed?.Invoke();
            OnElapsedPayload?.Invoke(_payload);
        }

        #endregion

    }

}