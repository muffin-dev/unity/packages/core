/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;

namespace MuffinDev.Core
{

    /// <summary>
    /// Utility to store data using keys.
    /// </summary>
    public class Blackboard
    {

        #region Fields

        /// <summary>
        /// The data stored in this blackboard by keys.
        /// </summary>
        private Dictionary<string, object> _items = new Dictionary<string, object>();

        #endregion


        #region Public API

        /// <summary>
        /// Gets/sets the item with the given key.
        /// </summary>
        /// <param name="key">The key of the item to get or set.</param>
        /// <inheritdoc cref="Get(string)"/>
        public object this[string key]
        {
            get => Get(key);
            set => Set(key, value);
        }

        /// <inheritdoc cref="Get(string, object)"/>
        public object this[string key, object defaultValue] => Get(key, defaultValue);

        /// <returns>Returns the found item value if the key exists, otherwise null.</returns>
        /// <inheritdoc cref="Get(string, out object, object)"/>
        public object Get(string key)
        {
            return _items.TryGetValue(key, out object value) ? value : null;
        }

        /// <param name="defaultValue">The value to output if the key doesn't exist.</param>
        /// <returns>Returns the found item value if the key exists, or the given default value.</returns>
        /// <inheritdoc cref="Get(string, out object, object)"/>
        public object Get(string key, object defaultValue)
        {
            Get(key, out object value, defaultValue);
            return value;
        }

        /// <inheritdoc cref="Get(string, out object, object)"/>
        public bool Get(string key, out object value)
        {
            return Get(key, out value, null);
        }

        /// <param name="value">Outputs the item value, or the default value if the key doesn't exist.</param>
        /// <returns>Returns true if the key exists, otherwise false.</returns>
        /// <inheritdoc cref="Get{T}(string, out T, T)"/>
        public bool Get(string key, out object value, object defaultValue)
        {
            if (_items.TryGetValue(key, out value))
                return true;

            value = defaultValue;
            return false;
        }

        /// <inheritdoc cref="Get{T}(string, T)"/>
        public T Get<T>(string key)
        {
            return Get(key, default(T));
        }

        /// <returns>Returns the found item value if the key exists and the item has the expected type, otherwise return the given default
        /// value.</returns>
        /// <inheritdoc cref="Get{T}(string, out T, T)"/>
        public T Get<T>(string key, T defaultValue)
        {
            Get(key, out T value, defaultValue);
            return value;
        }

        /// <inheritdoc cref="Get{T}(string, out T, T)"/>
        public bool Get<T>(string key, out T value)
        {
            return Get(key, out value, default);
        }

        /// <summary>
        /// Gets an item by its key.
        /// </summary>
        /// <typeparam name="T">The expected type of the item.</typeparam>
        /// <param name="key">The key of the item to get.</param>
        /// <param name="value">Outputs the item value, or the default value if the key doesn't exist or the item doesn't have the expected
        /// type.</param>
        /// <param name="defaultValue">The value to output if the key doesn't exist or the item doesn't have the expected type.</param>
        /// <returns>Returns true if the key exists and the item has the expected type, otherwise false.</returns>
        public bool Get<T>(string key, out T value, T defaultValue)
        {
            if (_items.TryGetValue(key, out object objValue))
            {
                try
                {
                    value = (T)objValue;
                    return true;
                }
                catch (System.InvalidCastException) { }
            }

            value = defaultValue;
            return false;
        }
        
        /// <inheritdoc cref="Set{T}(string, T)"/>
        public void Set(string key, object value)
        {
            if (!_items.ContainsKey(key))
                _items.Add(key, value);
            else
                _items[key] = value;
        }

        /// <summary>
        /// Adds or replaces an item with the given key.
        /// </summary>
        /// <typeparam name="T">The type of the item value.</typeparam>
        /// <param name="key">The key of the item to add or replace.</param>
        /// <param name="value">The new value of the item.</param>
        public void Set<T>(string key, T value)
        {
            Set(key, (object)value);
        }

        /// <summary>
        /// Checks if this blackboard contains an item with the iven key.
        /// </summary>
        /// <param name="key">The key of the item to check.</param>
        /// <returns>Returns true if this blackboard contains an item with the given key, otherwise false.</returns>
        public bool ContainsKey(string key)
        {
            return _items.ContainsKey(key);
        }

        /// <summary>
        /// Removes an item from this blackboard.
        /// </summary>
        /// <param name="key">The key of the item to remove.</param>
        /// <returns>Returns true if the key exists, otherwise false.</returns>
        public bool Remove(string key)
        {
            return _items.Remove(key);
        }

        /// <summary>
        /// Removes all items in this blackboard.
        /// </summary>
        public void Clear()
        {
            _items.Clear();
        }

        #endregion

    }

}