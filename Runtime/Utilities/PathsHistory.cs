/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Keeps a history of selected paths to files and directories.
    /// </summary>
    [Save(EDataScope.User, true)]
    [System.Serializable]
    public class PathsHistory : Singleton<PathsHistory>, IEnumerable<PathsCollection>
    {

        #region Fields

        /// <summary>
        /// The default paths collection identifier.
        /// </summary>
        private static readonly string DefaultKey = string.Empty;

        /// <summary>
        /// The paths collections in this history.
        /// </summary>
        [SerializeField]
        private List<PathsCollection> _pathsCollections = new List<PathsCollection>();

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="Singleton{T}.Init"/>
        protected override void Init()
        {
            // Adds the default paths collection if it's not already registered
            CreateHistory(DefaultKey, 1, EPathOrigin.Relative);
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets the paths collection at the given index in this history.
        /// </summary>
        /// <param name="index">The index of the paths collection to get.</param>
        /// <returns>Returns the paths collection at the given index in this history.</returns>
        public PathsCollection this[int index] => _pathsCollections[index];

        /// <inheritdoc cref="GetHistory(string)"/>
        public PathsCollection this[string key] => GetHistory(key);

        /// <inheritdoc cref="GetHistory(string, int, EPathOrigin)"/>
        public PathsCollection this[string key, int limit, EPathOrigin origin = EPathOrigin.Source] => GetHistory(key, limit, origin);

        /// <inheritdoc cref="GetPath(string, string, EPathOrigin)"/>
        public string this[string key, EPathOrigin origin] => GetPath(key, null, origin);

        /// <inheritdoc cref="GetPath(string, string, EPathOrigin)"/>
        public string this[string key, string defaultPath, EPathOrigin origin] => GetPath(key, defaultPath, origin);

        /// <summary>
        /// The path collections in this history.
        /// </summary>
        public static PathsCollection[] PathCollections => Instance._pathsCollections.ToArray();

        /// <summary>
        /// Gets the number of paths collections in this history.
        /// </summary>
        public static int Count => Instance._pathsCollections.Count;

        /// <returns>Returns the created paths collection, or null if the key is already used.</returns>
        /// <inheritdoc cref="CreateHistory(string, out PathsCollection, int, EPathOrigin)"/>
        public static PathsCollection CreateHistory(string key, int limit = 0, EPathOrigin origin = EPathOrigin.Source)
        {
            return CreateHistory(key, out PathsCollection history, limit, origin) ? history : null;
        }

        /// <summary>
        /// Creates a new paths collection in this history, identified by the given key.
        /// </summary>
        /// <param name="key">The identifier of the paths collection to create.</param>
        /// <param name="history">Outputs the create paths collection.</param>
        /// <param name="limit">The maximum number of paths that can be registered in the collection to create. 0 or negative means
        /// unlimited.</param>
        /// <param name="origin">Defines where the path strings of the collection should start in the collection to create.</param>
        /// <returns>Returns true if the paths collection has been created successfully, or false if the given key is already
        /// used.</returns>
        public static bool CreateHistory(string key, out PathsCollection history, int limit = 0, EPathOrigin origin = EPathOrigin.Source)
        {
            if (string.IsNullOrEmpty(key))
                key = DefaultKey;

            if (IndexOf(key) >= 0)
            {
                history = null;
                return false;
            }

            history = new PathsCollection(key, limit, origin);
            Instance._pathsCollections.Add(history);
            return true;
        }

        /// <summary>
        /// Gets the default paths collection.
        /// </summary>
        /// <returns>Returns the default paths collection.</returns>
        /// <inheritdoc cref="GetHistory(string)"/>
        public static PathsCollection GetHistory()
        {
            return GetHistory(DefaultKey, 1, EPathOrigin.Source);
        }

        /// <returns>Returns the found paths collection, or null if no paths collection identified by the given key has been
        /// found.</returns>
        /// <inheritdoc cref="GetHistory(string, out PathsCollection)"/>
        public static PathsCollection GetHistory(string key)
        {
            return GetHistory(key, out PathsCollection history) ? history : null;
        }

        /// <summary>
        /// Gets the paths collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the paths collection to get.</param>
        /// <param name="history">Outputs the found paths collection.</param>
        /// <returns>Returns true if a paths collection has been found successfully, otherwise false.</returns>
        public static bool GetHistory(string key, out PathsCollection history)
        {
            if (string.IsNullOrEmpty(key))
                key = DefaultKey;

            history = Instance._pathsCollections.Find(pc => pc.Key == key);
            return history != null;
        }

        /// <returns>Returns the found or created paths collection.</returns>
        /// <inheritdoc cref="GetHistory(string, out PathsCollection, int, EPathOrigin)"/>
        public static PathsCollection GetHistory(string key, int limit, EPathOrigin origin = EPathOrigin.Source)
        {
            GetHistory(key, out PathsCollection history, limit, origin);
            return history;
        }

        /// <summary>
        /// Gets the paths collection identified by the given key in this history, or create it if it doesn't exist yet.
        /// </summary>
        /// <param name="key">The identifier of the paths collections to get or create.</param>
        /// <param name="history">Outputs the found or created paths collection.</param>
        /// <inheritdoc cref="GetHistory(string, out PathsCollection)"/>
        /// <inheritdoc cref="CreateHistory(string, out PathsCollection, int, EPathOrigin)"/>
        public static void GetHistory(string key, out PathsCollection history, int limit, EPathOrigin origin = EPathOrigin.Source)
        {
            if(!GetHistory(key, out history))
                CreateHistory(key, out history, limit, origin);
        }

        /// <summary>
        /// Clears the default paths collection.
        /// </summary>
        /// <inheritdoc cref="ClearHistory(string)"/>
        public static bool ClearHistory()
        {
            return ClearHistory(DefaultKey);
        }

        /// <summary>
        /// Clears the paths collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the paths collection to clear.</param>
        /// <returns>Returns true if a paths collection has been cleared successfully, otherwise false.</returns>
        public static bool ClearHistory(string key)
        {
            if (string.IsNullOrEmpty(key))
                key = DefaultKey;

            if (GetHistory(key, out PathsCollection history))
            {
                history.Clear();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes the paths collection identified by the given key from this history.
        /// </summary>
        /// <param name="key">The identifier of the paths collection to remove.</param>
        /// <returns>Returns true if a paths collection has been removed successfully, otherwise false.</returns>
        public static bool RemoveHistory(string key)
        {
            if (string.IsNullOrEmpty(key))
                key = DefaultKey;

            int index = IndexOf(key);
            if (index >= 0)
            {
                Instance._pathsCollections.RemoveAt(index);
                // Re-init if the default history has been deleted
                if (key == DefaultKey)
                    I.Init();

                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the index of the paths collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the paths collection to find.</param>
        /// <returns>Returns the found paths collection index in this history, or -1 if no paths collection identified by the given key has
        /// been found.</returns>
        public static int IndexOf(string key)
        {
            if (string.IsNullOrEmpty(key))
                key = DefaultKey;

            return Instance._pathsCollections.FindIndex(pc => pc.Key == key);
        }

        /// <summary>
        /// Adds a path to the default collection.
        /// </summary>
        /// <returns>Returns true if the path has been added successfully, otherwise false.</returns>
        /// <inheritdoc cref="AddPath(string, string)"/>
        public static bool AddPath(string path)
        {
            return AddPath(DefaultKey, path);
        }

        /// <summary>
        /// Adds a path to the collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the paths collection in which to add the path.</param>
        /// <param name="path">The path to add.</param>
        /// <returns>Returns true if the path has been added successfully, or false if no paths collection identified by the given key has
        /// been found.</returns>
        public static bool AddPath(string key, string path)
        {
            if (GetHistory(key, out PathsCollection history))
            {
                history.Add(path);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds given paths to the default collection.
        /// </summary>
        /// <returns>Returns true if the path have been added successfully, otherwise false.</returns>
        /// <inheritdoc cref="AddPaths(string, IEnumerable{string})"/>
        public static bool AddPaths(IEnumerable<string> paths)
        {
            return AddPaths(DefaultKey, paths);
        }

        /// <summary>
        /// Adds given paths to the collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the paths collection in which to add the paths.</param>
        /// <param name="paths">The paths to add.</param>
        /// <returns>Returns true if the paths have been added successfully, or false if no paths collection identified by the given key
        /// has been found.</returns>
        /// <inheritdoc cref="AddPath(string, string)"/>
        public static bool AddPaths(string key, IEnumerable<string> paths)
        {
            if (GetHistory(key, out PathsCollection history))
            {
                history.Add(paths);
                return true;
            }
            return false;
        }

        /// <returns>Returns the first path from the default paths collection, or the default path if the default collection is
        /// empty.</returns>
        /// <inheritdoc cref="GetPath(out string, string, EPathOrigin)"/>
        public static string GetPath(EPathOrigin origin = EPathOrigin.Source, string defaultPath = null)
        {
            return GetPath(DefaultKey, defaultPath, origin);
        }

        /// <returns>Returns the first path from the found paths collection, or the default path if the found collection is empty or if no
        /// collection identified with the given key has been found.</returns>
        /// <inheritdoc cref="GetPath(string, out string, string, EPathOrigin)"/>
        public static string GetPath(string key, string defaultPath = null, EPathOrigin origin = EPathOrigin.Source)
        {
            GetPath(key, out string path, defaultPath, origin);
            return path;
        }

        /// <summary>
        /// Gets the first path from the default collection.
        /// </summary>
        /// <returns>Returns true if a path has been found successfully, or false if the found default paths collection is empty.</returns>
        /// <inheritdoc cref="GetPath(string, out string, string, EPathOrigin)"/>
        public static bool GetPath(out string path, string defaultPath = null, EPathOrigin origin = EPathOrigin.Source)
        {
            return GetPath(DefaultKey, out path, defaultPath, origin);
        }

        /// <summary>
        /// Gets the first path from the collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the collection of which to get the first path.</param>
        /// <param name="path">Outputs the first path from the found collection.</param>
        /// <param name="defaultPath">The value to return if the found paths collection is empty or if no paths collection identified by
        /// the given key has been found in this history.</param>
        /// <param name="origin">Defines where the output path string should start.</param>
        /// <returns>Returns true if a path has been found successfully, or false if the found paths collection is empty or if no
        /// collection identified with the given key has been found.</returns>
        /// <inheritdoc cref="GetPaths(string, out string[], EPathOrigin)"/>
        public static bool GetPath(string key, out string path, string defaultPath = null, EPathOrigin origin = EPathOrigin.Source)
        {
            if (GetPaths(key, out string[] paths, origin))
            {
                if (paths.Length > 0)
                {
                    path = paths[0];
                    return true;
                }
            }

            path = defaultPath;
            return false;
        }

        /// <summary>
        /// Gets the paths from the default collection.
        /// </summary>
        /// <returns>Returns the default collection's paths.</returns>
        /// <inheritdoc cref="GetPaths(string, EPathOrigin)"/>
        public static string[] GetPaths(EPathOrigin origin = EPathOrigin.Source)
        {
            return GetPaths(DefaultKey, origin);
        }

        /// <returns>Returns the found collection's paths, or null if no paths collection identified by the given key has been
        /// found.</returns>
        /// <inheritdoc cref="GetPaths(string, out string[], EPathOrigin)"/>
        public static string[] GetPaths(string key, EPathOrigin origin = EPathOrigin.Source)
        {
            return GetPaths(key, out string[] paths, origin) ? paths : null;
        }

        /// <summary>
        /// Gets the paths from the collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the collection of which to get the paths.</param>
        /// <param name="paths">Outputs the paths from the found collection.</param>
        /// <param name="origin">Defines where the output path strings should start.</param>
        /// <returns>Returns true if the paths have been found successfully, or false if no paths collection identified by the given key
        /// has been found.</returns>
        public static bool GetPaths(string key, out string[] paths, EPathOrigin origin = EPathOrigin.Source)
        {
            paths = GetHistory(key, out PathsCollection history)
                ? history.GetPaths(origin)
                : null;
            return paths != null;
        }

        /// <summary>
        /// Gets the complete paths informations in the default collection.
        /// </summary>
        /// <returns>Returns the default collection's paths informations.</returns>
        /// <inheritdoc cref="GetPathInfos(string)"/>
        public static PathsCollection.PathInfo[] GetPathInfos()
        {
            return GetPathInfos(DefaultKey);
        }

        /// <returns>Returns the found collection's paths informations, or null if no paths collection identified by the given key has
        /// been found.</returns>
        /// <inheritdoc cref="GetPathInfos(string, out PathsCollection.PathInfo[])"/>
        public static PathsCollection.PathInfo[] GetPathInfos(string key)
        {
            return GetPathInfos(key, out PathsCollection.PathInfo[] pathInfos) ? pathInfos : null;
        }

        /// <summary>
        /// Gets the complete paths informations in the collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the collection of which to get the paths informations.</param>
        /// <param name="pathInfos">Outputs the paths informations from the found collection.</param>
        /// <returns>Returns true if the paths informations have been found successfully, or false if no paths collection identified by the
        /// given key has been found.</returns>
        public static bool GetPathInfos(string key, out PathsCollection.PathInfo[] pathInfos)
        {
            pathInfos = GetHistory(key, out PathsCollection history)
                ? history.PathInfos
                : null;
            return pathInfos != null;
        }

        /// <summary>
        /// Sets the maximum number of paths accepted by the default collection.
        /// </summary>
        /// <returns>Returns true if the default paths collection's limit has been set successfully, otherwise false.</returns>
        /// <inheritdoc cref="SetLimit(string, int)"/>
        public static void SetLimit(int limit)
        {
            SetLimit(DefaultKey, limit);
        }

        /// <summary>
        /// Sets the maximum number of paths accepted by the collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the collection of which to set the limit.</param>
        /// <param name="limit">The maximum number of paths accepted by the collection. 0 or negative means unlimited.</param>
        /// <returns>Returns true if the paths collection's limit has been set successfully, or false if no collection identified by the
        /// given key has been found.</returns>
        public static bool SetLimit(string key, int limit)
        {
            if (GetHistory(key, out PathsCollection history))
            {
                history.Limit = limit;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes the given path from the default collection.
        /// </summary>
        /// <returns>Returns true if a path has been removed successfully, or false if the path was not registered.</returns>
        /// <inheritdoc cref="RemovePath(string, string)"/>
        public static bool RemovePath(string path)
        {
            return RemovePath(DefaultKey, path);
        }

        /// <summary>
        /// Removes the given path from the collection identified by the given key in this history.
        /// </summary>
        /// <param name="key">The identifier of the collection of which to remove the path.</param>
        /// <param name="path">The path string to remove.</param>
        /// <returns>Returns true if a path has been removed successfully, or false if the path was not registered or no collection
        /// identified by the given key has been found.</returns>
        public static bool RemovePath(string key, string path)
        {
            return GetHistory(key, out PathsCollection history) && history.Remove(path);
        }

        /// <summary>
        /// Removes the given paths from the default collection.
        /// </summary>
        /// <returns>Returns true if at least one path has been removed successfully, or false if no path was registered.</returns>
        /// <inheritdoc cref="RemovePaths(string, IEnumerable{string})"/>
        public static bool RemovePaths(IEnumerable<string> paths)
        {
            return RemovePaths(DefaultKey, paths);
        }

        /// <summary>
        /// Removes the given paths from the collection identified by the given key in this history.
        /// </summary>
        /// <param name="paths">The path strings to remove.</param>
        /// <returns>Returns true if at least one path has been removed successfully, or false if no path was registered or no collection
        /// identified by the given key has been found.</returns>
        /// <inheritdoc cref="RemovePath(string, string)"/>
        public static bool RemovePaths(string key, IEnumerable<string> paths)
        {
            return GetHistory(key, out PathsCollection history) && history.Remove(paths);
        }

        /// <summary>
        /// Iterates through the paths collections in this history.
        /// </summary>
        public IEnumerator<PathsCollection> GetEnumerator()
        {
            return ((IEnumerable<PathsCollection>)_pathsCollections).GetEnumerator();
        }

        /// <inheritdoc cref="GetEnumerator"/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_pathsCollections).GetEnumerator();
        }

        #endregion

    }

}