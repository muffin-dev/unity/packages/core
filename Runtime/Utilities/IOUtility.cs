/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.IO;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaneous functions for working with IO features.
    /// </summary>
    public static class IOUtility
    {

        /// <summary>
        /// Copies the content of a directory to another recursively.
        /// </summary>
        /// <param name="sourceDirectory">The path of the directory you want to copy. If relative path given, it's resolved from the
        /// project's absolute path.</param>
        /// <param name="targetDirectory">The path of the directory where you want to copy the contents. If relative path given, it's
        /// resolved from the project's absolute path.</param>
        public static void CopyDirectory(string sourceDirectory, string targetDirectory)
        {
            sourceDirectory = sourceDirectory.AbsolutePath();
            targetDirectory = targetDirectory.AbsolutePath();

            // Create all directories in the target directory, so it mirrors the source's structure
            foreach (string dirPath in Directory.GetDirectories(sourceDirectory, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourceDirectory, targetDirectory));

            // Copy all files from the source directory to the target one
            foreach (string templateFilePath in Directory.GetFiles(sourceDirectory, "*.*", SearchOption.AllDirectories))
                File.Copy(templateFilePath, templateFilePath.Replace(sourceDirectory, targetDirectory));
        }

    }

}