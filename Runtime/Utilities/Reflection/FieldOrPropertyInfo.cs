/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Reflection;

namespace MuffinDev.Core.Reflection
{

    /// <summary>
    /// Groups informations about a field or a property being processed through C# reflection.<br/>
    /// This is used by <see cref="ReflectionUtility.GetFieldsAndProperties(Type, BindingFlags)"/>, and is useful to unifies operations
    /// over both fields and properties, avoiding code duplications.
    /// </summary>
    public class FieldOrPropertyInfo
    {

        #region Fields

        /// <summary>
        /// Informations about the field. Null if the element is a property (see <see cref="Property"/>).
        /// </summary>
        public FieldInfo Field;

        /// <summary>
        /// Informations about the property. Null if the element is a field (see <see cref="Field"/>).
        /// </summary>
        public PropertyInfo Property;

        #endregion


        #region Public API

        /// <summary>
        /// Gets the name of the element.
        /// </summary>
        public string Name => Field != null ? Field.Name : Property.Name;

        /// <summary>
        /// Gets the value of this field or property.
        /// </summary>
        /// <param name="target">The object that owns the field or property to read.</param>
        /// <returns>Returns the value of the field or property.</returns>
        public object GetValue(object target)
        {
            return Field != null ? Field.GetValue(target) : Property.GetValue(target);
        }

        /// <summary>
        /// Sets the value of this field or property.
        /// </summary>
        /// <param name="target">The object that owns the field or property to set.</param>
        /// <param name="value">The value you want to set for the field or property.</param>
        public void SetValue(object target, object value)
        {
            if (Field != null)
            {
                Field.SetValue(target, value);
            }
            else if (Property != null)
            {
                Property.SetValue(target, value);
            }
        }

        /// <summary>
        /// Tries to get a custom attribute on the current field or property.
        /// </summary>
        /// <typeparam name="T">The type of the attribute.</typeparam>
        /// <param name="attribute">Outputs the found attribute.</param>
        /// <param name="inherit">If enabled, checks if the attribute exists on the parent values.</param>
        /// <returns>Returns true if the expected attribute has been found on the element, otherwise false.</returns>
        public bool TryGetCustomAttribute<T>(out T attribute, bool inherit = false)
            where T : Attribute
        {
            if (Field != null)
            {
                attribute = Field.GetCustomAttribute<T>(inherit);
                return attribute != null;
            }
            else if (Property != null)
            {
                attribute = Property.GetCustomAttribute<T>(inherit);
                return attribute != null;
            }

            attribute = null;
            return false;
        }

        #endregion

    }

}