/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace MuffinDev.Core.Reflection
{

    /// <summary>
    /// Miscellaneous functions for working with C# reflection.
    /// </summary>
    public static class ReflectionUtility
    {

        #region Fields

        /// <summary>
        /// Targets elements declared on the instance, public and non-public.
        /// </summary>
        public const BindingFlags INSTANCE_FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Targets elements declared static, public and non-public.
        /// </summary>
        public const BindingFlags STATIC_FLAGS = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// The name of the main Unity C# scripts assembly.
        /// </summary>
        public const string UNITY_CSHARP_ASSEMBLY_NAME = "Assembly-CSharp";

        /// <summary>
        /// The name of the main Unity C# scripts for editor assembly.
        /// </summary>
        public const string UNITY_EDITOR_CSHARP_ASSEMBLY_NAME = "Assembly-CSharp-Editor";

        /// <summary>
        /// The name of the assemblies that are not part of the project.
        /// </summary>
        public static readonly string[] NotProjectAssemblies =
        {
            "mscorlib",
            "UnityEngine",
            "UnityEditor",
            "Unity.",
            "System",
            "Mono.",
            "netstandard",
            "Microsoft"
        };

        #endregion


        #region Type Searching

        /// <summary>
        /// Gets all assemblies in the current app domain.
        /// </summary>
        /// <param name="excludedAssembliesPrefixes">The prefixes of the assembly names to exclude from this query.</param>
        /// <returns>Returns the found assemblies.</returns>
        public static Assembly[] GetAllAssemblies(IList<string> excludedAssembliesPrefixes = null)
        {
            List<Assembly> assemblies = new List<Assembly>();
            // For each assembly in the current domain
            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                string assemblyName = a.GetName().Name;

                // If assmeblies are filtered
                if (excludedAssembliesPrefixes != null)
                {
                    bool isExcluded = false;
                    // For each excluded assembly prefix
                    foreach (string assemblyPrefix in excludedAssembliesPrefixes)
                    {
                        // Mark the current assembly as excluded if the prefix matches
                        if (assemblyName.StartsWith(assemblyPrefix))
                        {
                            isExcluded = true;
                            break;
                        }
                    }

                    // Skip if the current assembly is excluded
                    if (isExcluded)
                    {
                        continue;
                    }
                }

                assemblies.Add(a);
            }

            return assemblies.ToArray();
        }

        /// <summary>
        /// Get all assemblies related to the current Unity project.
        /// </summary>
        /// <returns>Returns the found assemblies.</returns>
        public static Assembly[] GetProjectAssemblies()
        {
            return GetAllAssemblies(NotProjectAssemblies);
        }

        /// <summary>
        /// Gets all the types that implement the given type in the given assembly.<br/>
        /// NOTE: This function is inspired by this answer by Nick VanderPyle: https://stackoverflow.com/a/8645519/6699339
        /// </summary>
        /// <param name="parentType">The type of the class you want to find inheritors. You can pass a generic type in this paramater, by
        /// using the "open generic" syntax. As an example, if the parent class is MyGenericClass<T>, use typeof(MyGenericClass<>), without
        /// any value inside the less-than/greater-than characters.</param>
        /// <param name="assembly">The assembly where you want to find the given type implementations.</param>
        /// <returns>Returns an enumerable that contains all the found types.</returns>
        public static Type[] GetAllTypesAssignableFrom(Type parentType, Assembly assembly)
        {
            IEnumerable<Type> foundTypes =
                // type = The current type among the assembly's
                from type in assembly.GetTypes()
                    // baseType = The type of which the current type inherits
                let baseType = type.BaseType
                where
                (
                    // If the query type is an interface
                    parentType.IsInterface &&
                    (
                        // Include the current type if it implements the interface
                        !type.IsGenericType && type.GetInterfaces().Contains(parentType)
                    ) ||
                    // Or, if the current type inherits from another type
                    baseType != null &&
                    (
                        // Include the current type only if the type of which the current type inherits is generic, the current type itself is
                        // not generic, and the inherited type is assignable from the queried one
                        (baseType.IsGenericType && !type.IsGenericType && parentType.IsAssignableFrom(baseType.GetGenericTypeDefinition())) ||
                        // Include the current type only if the type of which the current type inherits is not generic, the current type itself
                        // is not generic too, and if the inherited type is assignable from the queried one
                        (!baseType.IsGenericType && !type.IsGenericType && parentType.IsAssignableFrom(baseType))
                    )
                )
                select type;
            return foundTypes.ToArray();
        }

        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, Assembly)"/>
        /// <typeparam name="T">The type of the class you want to find inheritors.</typeparam>
        public static Type[] GetAllTypesAssignableFrom<T>(Assembly assembly)
        {
            return GetAllTypesAssignableFrom(typeof(T), assembly);
        }

        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, Assembly)"/>
        /// <param name="assemblies">The assemblies where you want to find the given type implementations.</param>
        public static Type[] GetAllTypesAssignableFrom(Type parentType, IList<Assembly> assemblies)
        {
            List<Type> types = new List<Type>();
            foreach (Assembly a in assemblies)
            {
                types.AddRange(GetAllTypesAssignableFrom(parentType, a));
            }
            return types.ToArray();
        }

        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, IList{Assembly})"/>
        /// <typeparam name="T">The type of the class you want to find inheritors.</typeparam>
        public static Type[] GetAllTypesAssignableFrom<T>(IList<Assembly> assemblies)
        {
            return GetAllTypesAssignableFrom(typeof(T), assemblies);
        }

        /// <summary>
        /// Gets all the types that implement the given type in the project assemblies.
        /// </summary>
        /// <inheritdoc cref="GetAllTypesAssignableFrom(Type, IList{Assembly})"/>
        public static Type[] GetAllTypesInProjectAssignableFrom(Type parentType)
        {
            return GetAllTypesAssignableFrom(parentType, GetProjectAssemblies());
        }

        /// <summary>
        /// Gets all the types that implement the given type in the project assemblies.
        /// </summary>
        /// <inheritdoc cref="GetAllTypesAssignableFrom{T}(IList{Assembly})"/>
        public static Type[] GetAllTypesInProjectAssignableFrom<T>()
        {
            return GetAllTypesAssignableFrom(typeof(T), GetProjectAssemblies());
        }

        #endregion


        #region Fields & Properties

        /// <summary>
        /// Gets the named field or property info.
        /// </summary>
        /// <param name="type">The type from which you want to get the reflection data.</param>
        /// <param name="name">The name of the field or property you want to get.</param>
        /// <param name="bindingFlags">The binding flages use for querying the reflection data.</param>
        /// <returns>Returns the found field or property info.</returns>
        public static FieldOrPropertyInfo GetFieldOrProperty(Type type, string name, BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            foreach (FieldInfo field in type.GetFields(bindingFlags))
            {
                if (field.Name == name)
                {
                    return new FieldOrPropertyInfo { Field = field };
                }
            }

            foreach (PropertyInfo property in type.GetProperties(bindingFlags))
            {
                if (property.Name == name)
                {
                    return new FieldOrPropertyInfo { Property = property };
                }
            }

            return null;
        }

        /// <inheritdoc cref="GetFieldOrProperty(Type, string, BindingFlags)"/>
        /// <typeparam name="T">The type from which you want to get the reflection data.</typeparam>
        public static FieldOrPropertyInfo GetFieldOrProperty<T>(string name, BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            return GetFieldOrProperty(typeof(T), name, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldOrProperty(Type, string, BindingFlags)"/>
        /// <param name="target">The object from which you want to get the type's reflection data.</param>
        public static FieldOrPropertyInfo GetFieldOrProperty(object target, string name, BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            return GetFieldOrProperty(target.GetType(), name, bindingFlags);
        }

        /// <summary>
        /// Extracts all the fields and properties of a given type.
        /// </summary>
        /// <param name="type">The type from which you want to get the reflection data.</param>
        /// <param name="bindingFlags">The binding flags to use for querying the reflection data.</param>
        /// <returns>Returns the informations about all the fields and properties on the target type.</returns>
        public static FieldOrPropertyInfo[] GetFieldsAndProperties(Type type, BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            List<FieldOrPropertyInfo> valueInfos = new List<FieldOrPropertyInfo>();
            foreach (FieldInfo field in type.GetFields(bindingFlags))
            {
                valueInfos.Add(new FieldOrPropertyInfo { Field = field });
            }

            foreach (PropertyInfo property in type.GetProperties(bindingFlags))
            {
                valueInfos.Add(new FieldOrPropertyInfo { Property = property });
            }

            return valueInfos.ToArray();
        }

        /// <inheritdoc cref="GetFieldsAndProperties(Type, BindingFlags)"/>
        /// <typeparam name="T">The type from which you want to get the reflection data.</typeparam>
        public static FieldOrPropertyInfo[] GetFieldsAndProperties<T>(BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            return GetFieldsAndProperties(typeof(T), bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndProperties(Type, BindingFlags)"/>
        /// <param name="target">The object from which you want to get the type's reflection data.</param>
        public static FieldOrPropertyInfo[] GetFieldsAndProperties(object target, BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            return GetFieldsAndProperties(target.GetType(), bindingFlags);
        }

        /// <summary>
        /// Extracts all the fields and properties of a given type that have a specific attribute.
        /// </summary>
        /// <inheritdoc cref="GetFieldsAndProperties(Type, BindingFlags)"/>
        /// <param name="attributeType">The type of the attribute the filtered fields and properties must have.</param>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute(Type type, Type attributeType, BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            List<FieldOrPropertyInfo> valueInfos = new List<FieldOrPropertyInfo>();
            foreach (FieldInfo field in type.GetFields(bindingFlags))
            {
                if (field.GetCustomAttribute(attributeType) != null)
                {
                    valueInfos.Add(new FieldOrPropertyInfo { Field = field });
                }
            }

            foreach (PropertyInfo property in type.GetProperties(bindingFlags))
            {
                if (property.GetCustomAttribute(attributeType) != null)
                {
                    valueInfos.Add(new FieldOrPropertyInfo { Property = property });
                }
            }

            return valueInfos.ToArray();
        }

        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute(Type, Type, BindingFlags)"/>
        /// <param name="target">The object from which you want to get the type's reflection data.</param>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute(object target, Type attributeType, BindingFlags bindingFlags = INSTANCE_FLAGS)
        {
            return GetFieldsAndPropertiesWithAttribute(target.GetType(), attributeType, bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute(Type, Type, BindingFlags)"/>
        /// <typeparam name="TType">The type from which you want to get the reflection data.</typeparam>
        /// <typeparam name="TAttribute">The type of the attribute the filtered fields and properties must have.</typeparam>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute<TType, TAttribute>(BindingFlags bindingFlags = INSTANCE_FLAGS)
            where TAttribute : Attribute
        {
            return GetFieldsAndPropertiesWithAttribute(typeof(TType), typeof(TAttribute), bindingFlags);
        }

        /// <inheritdoc cref="GetFieldsAndPropertiesWithAttribute(Type, Type, BindingFlags)"/>
        /// <param name="target">The object from which you want to get the type's reflection data.</param>
        /// <typeparam name="TAttribute">The type of the attribute the filtered fields and properties must have.</typeparam>
        public static FieldOrPropertyInfo[] GetFieldsAndPropertiesWithAttribute<TAttribute>(object target, BindingFlags bindingFlags = INSTANCE_FLAGS)
            where TAttribute : Attribute
        {
            return GetFieldsAndPropertiesWithAttribute(target.GetType(), typeof(TAttribute), bindingFlags);
        }

        /// <summary>
        /// Gets a reference or value of an object given a property path (like myObject.myContainer[3].myProperty).<br/>
        /// This function is inspired by the SpacePuppy Unity Framework:
        /// https://github.com/lordofduct/spacepuppy-unity-framework-4.0/blob/master/Framework/com.spacepuppy.core/Editor/src/EditorHelper.cs
        /// </summary>
        /// <param name="source">The object in which you want to get the nested object.</param>
        /// <param name="propertyPath">The property path for navigating to the expected object (like
        /// myObject.myContainer[3].myProperty).</param>
        /// <returns>Returns the found nested object, or null if the path didn't lead to a valid object.</returns>
        public static object GetNestedObject(object source, string propertyPath)
        {
            // Deal specific array representation from a Unity property path, as you can get it from SerializedProperty
            propertyPath = propertyPath.Replace(".Array.data[", "[");

            // Split the property path
            string[] propertyPathSplit = propertyPath.Split('.');
            // For each part in the property path
            foreach (string part in propertyPathSplit)
            {
                // If the part targets an item in an array
                if (part.Contains("["))
                {
                    // Extract the name of the property that contains the array
                    string elementName = part.Substring(0, part.IndexOf("["));
                    // Extract the index value
                    int index = Convert.ToInt32(part.Substring(part.IndexOf("[")).Replace("[", "").Replace("]", ""));
                    // Get the item at the expected position in the array
                    source = GetSubObject(source, elementName, index);
                }
                // Else, if the part targets an object
                else
                {
                    // Get that part's target
                    source = GetSubObject(source, part);
                }
            }
            return source;
        }

        /// <inheritdoc cref="GetNestedObject(object, string)"/>
        /// <typeparam name="T">The type of the expected target object.</typeparam>
        public static T GetNestedObject<T>(object source, string propertyPath)
        {
            return (T)GetNestedObject(source, propertyPath);
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets a container from the given source object by just using its field or property name.
        /// </summary>
        /// <param name="source">The source object from which you want to get the sub object.</param>
        /// <param name="fieldOrPropertyName">The name of the field or property for the container you want to get.</param>
        /// <returns>Returns the found sub-object.</returns>
        private static object GetSubObject(object source, string fieldOrPropertyName)
        {
            if (source == null)
                return null;

            Type type = source.GetType();
            while (type != null)
            {
                FieldOrPropertyInfo fieldOrProperty = GetFieldOrProperty(source.GetType(), fieldOrPropertyName);
                if (fieldOrProperty != null)
                    return fieldOrProperty.GetValue(source);
                type = type.BaseType;
            }
            return null;
        }

        /// <summary>
        /// Gets a container from the given source object by just using its field or property name, and the index of its source array.
        /// </summary>
        /// <inheritdoc cref="GetSubObject(object, string)"/>
        /// <param name="index">The index of the property for the container you want to find.</param>
        private static object GetSubObject(object source, string fieldOrPropertyName, int index)
        {
            IEnumerable enumerable = GetNestedObject(source, fieldOrPropertyName) as IEnumerable;
            if (enumerable == null)
                return null;

            // Iterate through the container to find the expected index
            IEnumerator enm = enumerable.GetEnumerator();
            for (int i = 0; i <= index; i++)
            {
                if (!enm.MoveNext())
                    return null;
            }
            return enm.Current;
        }

        #endregion

    }

}