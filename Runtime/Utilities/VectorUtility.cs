/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaeous functions for working with vectors.
    /// </summary>
    public static class VectorUtility
    {

        /// <summary>
        /// Computes the barycentre of the given points.<br/>
        /// Barycentre formula:<br/>
        /// <code>
        /// .               ->                ->
        /// ->    weightA * OA + weightN... * ON...
        /// OG = ___________________________________
        /// .            weightA + weightN...
        /// </code>
        /// </summary>
        /// <param name="points">The points from which you want to compute the barycentre.</param>
        /// <returns>Returns the computed barycentre.</returns>
        public static Vector3 Barycentre(IList<Vector3> points)
        {
            Vector3 sum = Vector3.zero;
            foreach (Vector3 p in points)
                sum += p;
            return points.Count > 0 ? sum / points.Count : Vector3.zero;
        }

        /// <inheritdoc cref="Barycentre(IList{Vector2})"/>
        public static Vector2 Barycentre(IList<Vector2> points)
        {
            Vector2 sum = Vector2.zero;
            foreach (Vector2 p in points)
                sum += p;
            return points.Count > 0 ? sum / points.Count : Vector2.zero;
        }

        /// <inheritdoc cref="Barycentre(IList{Vector3})"/>
        /// <param name="weightedPoints">The points from which you want to compute the barycentre, and their associated weight. The more
        /// the weight value, the closer the barycentre to this point.</param>
        public static Vector3 Barycentre(IList<(Vector3, float)> weightedPoints)
        {
            Vector3 sum = Vector3.zero;
            float weightSum = 0f;
            foreach ((Vector3, float) p in weightedPoints)
            {
                sum += p.Item1 * p.Item2;
                weightSum += p.Item2;
            }
            return weightSum != 0f ? sum / weightSum : Vector3.zero;
        }

        /// <inheritdoc cref="Barycentre(IList{(Vector3, float)})"/>
        public static Vector2 Barycentre(IList<(Vector2, float)> weightedPoints)
        {
            Vector2 sum = Vector2.zero;
            float weightSum = 0f;
            foreach ((Vector2, float) p in weightedPoints)
            {
                sum += p.Item1 * p.Item2;
                weightSum += p.Item2;
            }
            return weightSum != 0f ? sum / weightSum : Vector2.zero;
        }

        /// <summary>
        /// Converts the given points list into a <see cref="Mesh"/>, using that path as a contour.
        /// </summary>
        /// <remarks>
        /// WARNING: This function uses a very naive solution, which could lead to bad results. It takes the barycentre of all the sampled
        /// points of the given path, and creates the mesh triangles from that barycentre to every sampled points. It works fine with
        /// simple use cases though.<br/>
        /// @todo
        /// </remarks>
        /// <param name="points">The points that represent the contour of the mesh to create.</param>
        /// <returns>Returns the created <see cref="Mesh"/>.</returns>
        public static Mesh ToMesh(IList<Vector3> points)
        {
            Vector3 barycentre = points.Barycentre();

            List<Vector3> vertices = new List<Vector3>();
            List<int> triangles = new List<int>();

            vertices.Add(barycentre);

            for (int i = 0; i < points.Count; i++)
            {
                vertices.Add(points[i]);
                // Skip if the current point is the first one (not enough points for a triangle)
                if (i == 0)
                    continue;

                // Create triangle using points: barycentre, previous point and current point, in that order
                triangles.Add(0);
                triangles.Add(i);
                triangles.Add(i + 1);

                // If the current point is the last one, create the last triangle without adding a vertice
                if (i == points.Count - 1)
                {
                    triangles.Add(0);
                    triangles.Add(i + 1);
                    triangles.Add(1);
                }
            }

            // Build the mesh
            Mesh mesh = new Mesh
            {
                vertices = vertices.ToArray(),
                triangles = triangles.ToArray()
            };
            mesh.RecalculateNormals();

            return mesh;
        }

    }

}