/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Utility component for running an operation step by step.<br/>
    /// This is initially designed for debug purposes, but can still be used at runtime.
    /// </summary>
    [AddComponentMenu(Constants.ADD_COMPONENT_MENU + "/Core/Stepped Operation Component")]
    public class SteppedOperationComponent : MonoBehaviour
    {

        #region Fields

        [Header("Stepped Operation Settings")]

        [SerializeField]
        [Tooltip("If enabled, the stepped operation is automatically ran using the interval.")]
        private bool _continuous = true;

        [SerializeField]
        [Tooltip("The time (in seconds) between each step. This is used if continuous mode is enabled, and will also be used as a cooldown otherwise.")]
        private float _interval = 0.1f;

        private IEnumerator _operation = null;
        private float _timer = 0f;
        private bool _hasNextStep = false;

        #endregion


        #region Lifecycle

        protected virtual void Update()
        {
            // In any case, 
            _timer += Time.deltaTime;

            // If continuous mode is enabled
            if (_continuous)
            {
                bool canIterate = false;

                if (_interval <= 0f)
                {
                    canIterate = true;
                }
                else
                {
                    // Update timer
                    while (_timer > _interval)
                    {
                        canIterate = true;
                        _timer -= _interval;
                    }
                }

                if (canIterate)
                    MoveNext();
            }
            // Else, if continuous mode is disabled, lock the timer to the interval value in order to use it as a cooldown for Step()
            else if (_timer > _interval)
                _timer = _interval;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_continuous"/>
        public bool Continuous
        {
            get => _continuous;
            set => _continuous = value;
        }

        /// <inheritdoc cref="_interval"/>
        public float Interval
        {
            get => _interval;
            set => _interval = value;
        }

        /// <summary>
        /// Checks if the stepped operation has a next step available.
        /// </summary>
        public bool HasNextStep => _operation != null && _hasNextStep;

        /// <summary>
        /// Creates an empty <see cref="GameObject"/> in the scene with a <see cref="SteppedOperationComponent"/> that will run the given
        /// operation step by step.
        /// </summary>
        /// <param name="operation">The operation you want to run step by step.</param>
        /// <param name="gameObjectName">The name of the game object created to run the stepped operation.</param>
        /// <returns>Returns the created <see cref="SteppedOperationComponent"/> instance.</returns>
        public static SteppedOperationComponent Init(IEnumerator operation, string gameObjectName)
        {
            GameObject obj = new GameObject(!string.IsNullOrEmpty(gameObjectName) ? gameObjectName : "Stepped Operation");
            obj.transform.position = Vector3.zero;
            SteppedOperationComponent comp = obj.AddComponent<SteppedOperationComponent>();
            comp.Init(operation);
            return comp;
        }

        /// <summary>
        /// Initializes the stepped operation. Note that if an operation is already running, 
        /// </summary>
        /// <param name="operation">The operation you want to run step by step.</param>
        /// <param name="force">If an operation has already been initialized and this parameter is enabled, the previous operation is
        /// simply replaced.</param>
        public void Init(IEnumerator operation, bool force = false)
        {
            if (force || _operation == null)
                _operation = operation;
            else
                Debug.LogWarning("There's already a stepped operation running on this component.", this);
        }

        /// <summary>
        /// Steps manually through the operation. You must call <see cref="Init(IEnumerator, bool)"/> to setup the operation before calling
        /// this method. Also, this function won't do anything if continuous mode is enabled.
        /// </summary>
        /// <returns>Returns true if the operation has been steppe through successfully, otherwise false.</returns>
        public bool Step()
        {
            if (_operation == null)
            {
                Debug.LogWarning("There's no stepped operation running on this component.", this);
                return false;
            }

            // Cancel if continuous mode is enabled or 
            if (Continuous || _timer < _interval)
                return false;

            return MoveNext();
        }

        #endregion


        #region Private API

        /// <summary>
        /// Iterates through the stepped operation.
        /// </summary>
        private bool MoveNext()
        {
            if (_operation != null)
            {
                _hasNextStep = _operation.MoveNext();
                return _hasNextStep;
            }
            return false;
        }

        #endregion

    }

}