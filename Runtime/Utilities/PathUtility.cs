/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;
using System.IO;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaneous functions for working with path strings.
    /// </summary>
    public static class PathUtility
    {

        #region Fields

        /// <summary>
        /// The character used by default as directory separator for formatted path strings.
        /// </summary>
        public const char DEFAULT_DIRECTORY_SEPARATOR = '/';

        /// <summary>
        /// The absolute path to this project. In the editor, this is the path to the project's /Assets folder. In build, it's the path to
        /// the persistent data directory. This value is meant to be used by any editor or in-game feature that needs to store persistent
        /// data.
        /// </summary>
        public static readonly string ProjectPath = null;

        /// <summary>
        /// The asbolute path to the game content directory. In the editor, this is the path to the project's /Assets folder. In build,
        /// it's the path to the game executable. Note that in build, the game executable directory may be readonly, so you should use
        /// <see cref="PersistentDataPath"/> to store persistent custom data.<br/>
        /// <seealso href="https://docs.unity3d.com/ScriptReference/Application-dataPath.html"/>
        /// </summary>
        public static readonly string DataPath = null;

        /// <summary>
        /// The absolute path to the persistent data directory. This path varies from platforms, but is guaranteed to be writable, so you
        /// can use it to store persistent custom data.<br/>
        /// <seealso href="https://docs.unity3d.com/ScriptReference/Application-peristentDataPath.html"/>
        /// </summary>
        public static readonly string PersistentDataPath = null;

        /// <summary>
        /// The directory separators used by the current platform.
        /// </summary>
        private static readonly char[] s_platformDirectorySeparators = new char[]
        {
            Path.DirectorySeparatorChar,
            Path.AltDirectorySeparatorChar
        };

        #endregion


        #region Lifecycle

        static PathUtility()
        {
            DataPath = Application.dataPath;
            PersistentDataPath = Application.persistentDataPath;
#if UNITY_EDITOR
            ProjectPath = DataPath.Substring(0, DataPath.Length - $"/Assets".Length);
#else
            ProjectPath = PersistentDataPath;
#endif
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="ToPath(ref string)"/>
        public static string ToPath(string str)
        {
            return ToPath(str, DEFAULT_DIRECTORY_SEPARATOR);
        }

        /// <summary>
        /// Ensures given string represents a path, so it doesn't contain forbidden characters, and uses only slash as drectory separator.
        /// </summary>
        /// <inheritdoc cref="ToPath(string, char)"/>
        public static void ToPath(ref string str)
        {
            ToPath(ref str, DEFAULT_DIRECTORY_SEPARATOR);
        }

        /// <summary>
        /// Ensures given string represents a path, so it doesn't contain forbidden characters, and uses only the given character as
        /// directory separator.
        /// </summary>
        /// <param name="str">The string to format.</param>
        /// <param name="separator">The character to use as directory separator.</param>
        /// <returns>Returns the formatted string.</returns>
        public static string ToPath(string str, char separator)
        {
            ToPath(ref str, separator);
            return str;
        }

        /// <inheritdoc cref="ToPath(string, char)"/>
        public static void ToPath(ref string str, char separator)
        {
            // Cancel if the input string is not valid
            if (string.IsNullOrEmpty(str))
                return;

            foreach (char sep in s_platformDirectorySeparators)
                str = str.Replace(sep, separator);
        }

        /// <summary>
        /// Ensures given strings represent paths, so they don't contain forbidden characters, and use only slash as directory separator.
        /// </summary>
        /// <inheritdoc cref="ToPaths(IList{string}, char)"/>
        public static void ToPaths(IList<string> strs)
        {
            ToPaths(strs, DEFAULT_DIRECTORY_SEPARATOR);
        }

        /// <summary>
        /// Ensures given strings represent paths, so they don't contain forbidden characters, and use only the given character as
        /// directory separator.
        /// </summary>
        /// <param name="strs">The strings to format.</param>
        /// <inheritdoc cref="ToPath(string, char)"/>
        public static void ToPaths(IList<string> strs, char separator)
        {
            for (int i = 0; i < strs.Count; i++)
                strs[i] = ToPath(strs[i], separator);
        }

        /// <summary>
        /// If the given string represents a relative path, it's combined with <see cref="ProjectPath"/> (which is the path to the current
        /// project in the editor, and <see cref="PersistentDataPath"/> in build) to make it absolute.
        /// </summary>
        /// <param name="path">The path string to convert.</param>
        /// <returns>Returns the absolute path, or an empty string if the input path is not valid.</returns>
        public static string ToAbsolutePath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return ProjectPath;

            ToPath(ref path);
            if (!Path.IsPathRooted(path))
                path = Path.GetFullPath(Path.Combine(ProjectPath, path));
            return path;
        }

        /// <summary>
        /// If the given string represents an absolute path to a project file or directory, this function makes it relative to
        /// <see cref="ProjectPath"/> (which is the path to the current project in the editor, and <see cref="PersistentDataPath"/> in
        /// build). If the given string is already relative or doesn't target the project, the input string is returned as is.
        /// </summary>
        /// <param name="path">The path string to convert.</param>
        /// <returns>Returns the relative path, or an empty string if the input path is not valid.</returns>
        public static string ToRelativePath(string path)
        {
            // Cancel if the path is not valid
            if (string.IsNullOrEmpty(path))
                return string.Empty;

            ToPath(ref path);
            // Process the path if it's absolute and leads to the current Unity project
            if (Path.IsPathRooted(path) && path.StartsWith(ProjectPath))
            {
                int length = ProjectPath.Length;
                // If the input path is longer than the project path, discard the first separator
                if (path.Length > length)
                {
                    length++;
                }
                path = path.Substring(length);
            }

            return path;
        }

        /// <summary>
        /// Checks if the given path is relative to the current Unity project's root directory.
        /// </summary>
        /// <param name="path">The path you want to check. If it's a relative path, this function always returns true.</param>
        /// <returns>Returns true if the given path is relative to the current Unity project's root directory, otherwise false.</returns>
        public static bool IsProjectPath(string path)
        {
            // Return true if the path is empty or if it's relative
            if (string.IsNullOrEmpty(path) || !Path.IsPathRooted(path))
                return true;

            ToPath(ref path);
            return path.StartsWith(ProjectPath);
        }

        /// <summary>
        /// Combines and resolves a path with relative ones.
        /// </summary>
        /// <example>
        /// <code>
        /// "C:\MyProject\Assets\Models".Combine("Prefabs") // Outputs "C:\MyProject\Assets\Models\Prefabs
        /// "C:\MyProject\Assets\Models".Combine("./Prefabs") // Outputs "C:\MyProject\Assets\Models\Prefabs
        /// "C:\MyProject\Assets\Models".Combine("../Prefabs") // Outputs "C:\MyProject\Assets\Prefabs
        /// "C:\MyProject\Assets\Models".Combine("../../Prefabs") // Outputs "C:\MyProject\Prefabs
        /// "C:\MyProject\Assets\Models".Combine("../Prefabs/MyPrefab.prefab") // Outputs "C:\MyProject\Assets\Prefabs\MyPrefab.prefab
        /// "C:\MyProject\Assets\Models\MyModel.fbx".Combine("../Prefabs/MyPrefab.prefab") // Outputs "C:\MyProject\Assets\Prefabs\MyPrefab.prefab
        /// "C:\MyProject\Assets\Models".Combine("C:\MyProject\Assets\Prefabs") // Outputs "C:\MyProject\Assets\Prefabs
        /// </code>
        /// </example>
        /// <param name="path">The path from which you want to resolve the relative paths. Note that if this path is relative, the path is
        /// resolved from the directory where the code is executed. Also, if the path targets a file, relative paths are resolved from the
        /// last directory.</param>
        /// <param name="relPaths">The relative paths to resolve.</param>
        /// <returns>Returns the processed path.</returns>
        public static string CombinePaths(string path, params string[] relPaths)
        {
            path = !string.IsNullOrEmpty(path) ? Path.GetFullPath(path) : string.Empty;
            if (string.IsNullOrEmpty(path))
                path = string.Empty;

            // Cancel if no parameters defined
            if (relPaths.Length == 0)
            {
                return path;
            }

            // Ensures the origin path targets a directory
            path = GetDirectoryPath(path);

            // For each relative path param
            foreach (string relPath in relPaths)
            {
                // If the current rel path is rooted, replace the input path
                path = Path.IsPathRooted(relPath)
                    ? Path.GetFullPath(relPath)
                    : Path.GetFullPath(Path.Combine(path, relPath));
            }

            return path;
        }

        /// <summary>
        /// Gets the path to the parent directory of this one. If this path already targets a directory, this function returns the input
        /// path as is.
        /// </summary>
        /// <param name="path">The path of which you want to get the directory path.</param>
        /// <returns>Returns the processed path.</returns>
        public static string GetDirectoryPath(string path)
        {
            // If the path targets a directory
            if (Directory.Exists(path))
                return path;

            return Path.HasExtension(path)
                ? Path.GetDirectoryName(path)
                : path;
        }

        #endregion

    }

}