/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Represents a list of paths, identified by a key. You can use this class through <see cref="PathsHistory"/> to create histories for
    /// "recent items" menus or just keep track of a previously selected item.
    /// </summary>
    [System.Serializable]
    public class PathsCollection : IList<string>
    {

        #region Subclasses

        [System.Serializable]
        public struct PathInfo : IComparable<PathInfo>
        {
            /// <summary>
            /// The path string itself.
            /// </summary>
            public string path;

            /// <summary>
            /// The date when the path was added to a collection, as string.
            /// </summary>
            public string dateStr;

            /// <inheritdoc cref="PathInfo(string, DateTime)"/>
            public PathInfo(string path)
            {
                this.path = path;
                dateStr = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            }

            /// <summary>
            /// Class constructor.
            /// </summary>
            /// <param name="path">The path string itself.</param>
            /// <param name="date">The date at which the path is added to a collection.</param>
            internal PathInfo(string path, DateTime date)
            {
                this.path = path;
                dateStr = date.ToString(CultureInfo.InvariantCulture);
            }

            /// <summary>
            /// Gets the date when the path was added to a collection.
            /// </summary>
            public DateTime Date => DateTime.Parse(dateStr, CultureInfo.InvariantCulture);

            /// <summary>
            /// Compares the dates of this value and the given one.
            /// </summary>
            /// <returns>Returns 0 if the dates are equal, -1 if this date is more recent than the other or 1 otherwise.</returns>
            public int CompareTo(PathInfo other)
            {
                return other.Date.CompareTo(Date);
            }
        }

        #endregion


        #region Fields

        /// <summary>
        /// The key used to identify this collection.
        /// </summary>
        [SerializeField]
        private string _key = null;

        /// <summary>
        /// The paths in this collection.
        /// </summary>
        [SerializeField]
        private List<PathInfo> _paths = new List<PathInfo>();

        /// <summary>
        /// Defines where a path string should start.
        /// </summary>
        [SerializeField]
        private EPathOrigin _origin = EPathOrigin.Source;

        /// <summary>
        /// The maximum number of paths stored in this collection. 0 or negative means unlimited.
        /// </summary>
        [SerializeField]
        private int _limit = 0;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PathsCollection() { }

        /// <inheritdoc cref="PathsCollection(string, IEnumerable{string}, int, EPathOrigin)"/>
        public PathsCollection(string key, int limit = 0, EPathOrigin origin = EPathOrigin.Source)
        {
            _key = key;
            _limit = limit;
            _origin = origin;
        }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="key">The key used to identify this collection.</param>
        /// <param name="paths">The paths to add to this collection.</param>
        /// <param name="limit">The maximum number of paths stored in this collection. 0 or negative means unlimited.</param>
        /// <param name="origin">Defines where the path strings in this collection should start.</param>
        public PathsCollection(string key, IEnumerable<string> paths, int limit = 0, EPathOrigin origin = EPathOrigin.Source)
        {
            _key = key;
            _limit = limit;
            _origin = origin;

            // For each path in the list
            foreach (string p in paths)
            {
                // If this collection can handle more paths, add the current one
                if (limit <= 0 || _paths.Count < limit)
                {
                    Add(p);
                    // Stop if the maximum number of paths has bene reached
                    if (_paths.Count >= limit)
                        break;
                }
            }
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets/sets the path string at the given index in this collection.
        /// </summary>
        /// <param name="index">The index of the path string to get or set.</param>
        /// <returns>Returns the found path string.</returns>
        public string this[int index]
        {
            get => _paths[index].path;
            set
            {
                if (ProcessPath(ref value))
                    _paths[index] = new PathInfo(value, _paths[index].Date);
            }
        }

        /// <summary>
        /// The identifier of this paths collection.
        /// </summary>
        public string Key => _key;

        /// <inheritdoc cref="_paths"/>
        public string[] Paths => _paths.Map(pi => pi.path);

        /// <summary>
        /// Gets the complete informations about the paths in this collection.
        /// </summary>
        public PathInfo[] PathInfos => _paths.ToArray();

        /// <inheritdoc cref="_limit"/>
        public int Limit
        {
            get => _limit;
            set
            {
                _limit = value;
                ApplyLimit(_limit);
            }
        }

        /// <inheritdoc cref="_origin"/>
        public EPathOrigin Origin
        {
            get => _origin;
            set
            {
                // Cancel if the origin doesn't change
                if (_origin == value)
                    return;

                _origin = value;
                // Cancel if the existing path strings should not change
                if (_origin == EPathOrigin.Source)
                    return;

                // For each path in this collection
                for (int i = 0; i < _paths.Count; i++)
                {
                    // Apply the appropriate path origin
                    switch (_origin)
                    {
                        case EPathOrigin.Absolute:
                            this[i] = _paths[i].path.AbsolutePath();
                            break;

                        default:
                            this[i] = _paths[i].path.RelativePath();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the number of paths in this collection.
        /// </summary>
        public int Count => _paths.Count;

        /// <summary>
        /// Defines if this list is readonly.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Adds a path to this collection. If a path string is already in this collection, the "add date" of the existing entry is
        /// updated.
        /// </summary>
        /// <param name="path">The path to add.</param>
        public void Add(string path)
        {
            // Cancel if the path is not valid.
            if (!ProcessPath(ref path))
                return;

            int index = IndexOf(path);
            // If the path is already registered, remove it, so it can be added with an updated "add date" value
            if (index >= 0)
                RemoveAt(index);

            _paths.Add(new PathInfo(path));
            _paths.Sort();
            ApplyLimit(_limit);
        }

        /// <summary>
        /// Adds given paths to this collection. If a path string is already in this collection, the "add date" of the existing entry is
        /// updated.
        /// </summary>
        /// <param name="paths">The paths to add.</param>
        /// <inheritdoc cref="Add(string)"/>
        public void Add(IEnumerable<string> paths)
        {
            foreach (string path in paths)
                Add(path);
        }

        /// <summary>
        /// Clears this paths collection.
        /// </summary>
        public void Clear()
        {
            _paths.Clear();
        }

        /// <summary>
        /// Checks if this collection contains the given path string.
        /// </summary>
        /// <param name="path">The path to check.</param>
        /// <returns>Returns true if this collection contains the given path string, otherwise false.</returns>
        public bool Contains(string path)
        {
            return ProcessPath(ref path) && _paths.Exists(pi => pi.path == path);
        }

        /// <summary>
        /// Copies the elements of this collection to an array.
        /// </summary>
        /// <param name="array">The array where paths are copied.</param>
        /// <param name="arrayIndex">The index at which copy begins.</param>
        public void CopyTo(string[] array, int arrayIndex)
        {
            for (int i = 0; i < _paths.Count; i++)
                array[i + arrayIndex] = _paths[i].path;
        }

        /// <summary>
        /// Iterates through the paths strings in this collection.
        /// </summary>
        public IEnumerator<string> GetEnumerator()
        {
            foreach (PathInfo pi in _paths)
                yield return pi.path;
        }

        /// <inheritdoc cref="GetEnumerator"/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_paths).GetEnumerator();
        }

        /// <summary>
        /// Gets the index of the given path string in this collection.
        /// </summary>
        /// <param name="path">The path of which to get the index.</param>
        /// <returns>Returns the found index, or -1 if the given path string is not in this collection.</returns>
        public int IndexOf(string path)
        {
            return ProcessPath(ref path)
                ? _paths.FindIndex(pi => pi.path == path)
                : -1;
        }

        /// <summary>
        /// Inserts a path at the given index in this collection.
        /// </summary>
        /// <param name="index">The index at which you want to insert the given path string. This operation does nothing if the given index
        /// exceeds the defined <see cref="Limit"/>.</param>
        /// <param name="path">The path string to insert.</param>
        public void Insert(int index, string path)
        {
            // Cancel if the expected index exceeds this collection's limit
            if (_limit > 0 && index >= _limit)
                return;

            // Cancel if the path is not valid
            if (!ProcessPath(ref path))
                return;

            _paths.Insert(index, new PathInfo(path));
            _paths.Sort();
            ApplyLimit(_limit);
        }

        /// <summary>
        /// Gets the paths in this collection.
        /// </summary>
        /// <param name="origin">Defines where the output path strings should start.</param>
        /// <returns>Returns the path strings in this collection.</returns>
        public string[] GetPaths(EPathOrigin origin = EPathOrigin.Source)
        {
            // Stop the origin doesn't change
            if (origin == _origin || origin == EPathOrigin.Source)
                return Paths;

            string[] paths = new string[_paths.Count];
            // For each path in this collection
            for (int i = 0; i < _paths.Count; i++)
            {
                // Apply the appropriate path origin
                switch (origin)
                {
                    case EPathOrigin.Absolute:
                        paths[i] = _paths[i].path.AbsolutePath();
                        break;

                    default:
                        paths[i] = _paths[i].path.RelativePath();
                        break;
                }
            }

            return paths;
        }

        /// <summary>
        /// Removes the given path string from this collection.
        /// </summary>
        /// <param name="path">The path to remove from this collection.</param>
        /// <returns>Returns true if an entry has been removed successfully, or false if the path string was not registered.</returns>
        public bool Remove(string path)
        {
            int index = IndexOf(path);
            if (index >= 0)
            {
                _paths.RemoveAt(index);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes the given path strings from this collection
        /// </summary>
        /// <param name="paths">The paths to remove from this collection.</param>
        /// <returns>Returns true if at least one path has been removed successfully, otherwise false.</returns>
        /// <inheritdoc cref="Remove(string)"/>
        public bool Remove(IEnumerable<string> paths)
        {
            bool success = false;
            foreach (string p in paths)
            {
                if (Remove(p))
                    success = true;
            }
            return success;
        }

        /// <summary>
        /// Removes the path at the given index.
        /// </summary>
        /// <param name="index">The index at which to remove the entry.</param>
        public void RemoveAt(int index)
        {
            _paths.RemoveAt(index);
        }

        #endregion


        #region Private API

        /// <summary>
        /// If the number of paths in this collection exceeds the given limit, removes the oldest paths until that limit is reached
        /// exactly.
        /// </summary>
        /// <param name="limit">The maximum number of paths accepted in this collection. 0 or negative means unlimited, so this function
        /// does nothing.</param>
        private void ApplyLimit(int limit)
        {
            // Cancel if the limit is not reached
            if (limit <= 0 || _paths.Count <= limit)
                return;

            while (_paths.Count > limit)
                _paths.RemoveAt(_paths.Count - 1);
        }

        /// <summary>
        /// Processes the given path so it matches the settings of this collection.
        /// </summary>
        /// <param name="path">The path to process.</param>
        /// <returns>Returns true if the path has been processed successfully, or false if it's invalid.</returns>
        private bool ProcessPath(ref string path)
        {
            // Cancel if the path is not valid 
            if (path == null)
                return false;

            switch (_origin)
            {
                case EPathOrigin.Relative:
                    path = path.RelativePath();
                    break;

                case EPathOrigin.Absolute:
                    path = path.AbsolutePath();
                    break;

                default:
                    PathUtility.ToPath(ref path);
                    break;
            }
            return true;
        }

        #endregion

    }

}