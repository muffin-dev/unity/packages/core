/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaneous functions for mathematics.
    /// </summary>
    public static class Math
    {

        public const float APPROXIMATION_5 = 0.00001f;
        public const float APPROXIMATION_8 = 0.00000001f;

        public const float PERCENTS = 100f;

        /// <summary>
        /// Checks the two float values are close enough to be considered equals. This is meant to prevent float imprecisions.
        /// </summary>
        /// <returns>Returns true if b is equals (or very close) to a.</returns>
        public static bool Approximately(float a, float b)
        {
            return Mathf.Approximately(a, b);
        }

        /// <inheritdoc cref="Approximately(float, float)"/>
        /// <param name="epsilon">The approximation value. Basically, this function returns true if a is > to (b - epsilon) and a < to (b +
        /// epsilon).</param>
        public static bool Approximately(float a, float b, float epsilon)
        {
            return a > b - epsilon && a < b + epsilon;
        }

        /// <summary>
        /// Apply proportions to a given value, between a given min and max, and report it to the given ratio.
        /// </summary>
        /// <example>
        /// Ratio(50, 0, 100, 1); // Outputs 0.5
        /// Ratio(50, 0, 100, 20); // Outputs 10
        /// Ratio(50, -100, 100, 1); // Outputs 0.75
        /// Ratio(-10, 0, 10, 1); // Outputs -1
        /// </example>
        /// <param name="value">The value of you want to compute proportions.</param>
        /// <param name="min">The minimum value.</param>
        /// <param name="max">The maximum value.</param>
        /// <param name="ratio">The proportion operand.</param>
        /// <returns>Returns the computed ratio.</returns>
        public static float Ratio(float value, float min, float max, float ratio = 1f)
        {
            float range = Mathf.Abs(min) + Mathf.Abs(max);
            value += Mathf.Abs(min);
            return range > 0f ? value * ratio / range : 0f;
        }

        /// <summary>
        /// Apply proportions to a given value, between a given min and max, and report it to a percentage.
        /// </summary>
        /// <inheritdoc cref="Ratio(float, float, float, float)"/>
        public static float Percents(float value, float min, float max)
        {
            return Ratio(value, min, max, PERCENTS);
        }

        /// <summary>
        /// Apply proportions to a given value, between 0 and a given max, and report it to a percentage.
        /// </summary>
        /// <inheritdoc cref="Percents(float, float, float)"/>
        public static float Percents(float value, float max)
        {
            return Ratio(value, 0, max, PERCENTS);
        }

    }

}