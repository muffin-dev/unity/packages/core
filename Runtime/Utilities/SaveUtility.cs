/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.IO;
using System.Collections.Generic;

using UnityEngine;

using MuffinDev.Core.Reflection;

namespace MuffinDev.Core
{

    /// <summary>
    /// Utility class for saving data to files or prefs.
    /// </summary>
    public static class SaveUtility
    {

        #region Fields

        /// <summary>
        /// The name of the project settings directory.
        /// </summary>
        private const string PROJECT_SETTINGS_DIR = "ProjectSettings";

        /// <summary>
        /// The name of the user settings directory.
        /// </summary>
        private const string USER_SETTINGS_DIR = "UserSettings";

        /// <summary>
        /// The name of the temporary directory.
        /// </summary>
        private const string TEMP_DIR = "Temp";

        /// <summary>
        /// The list of all savable objects.
        /// </summary>
        private static List<object> _savableObjects = new List<object>();

        #endregion


        #region Public API

        /// <summary>
        /// Saves the given data following the rules defines from <see cref="SaveAttribute"/> and <see cref="DataScopeAttribute"/> on the
        /// object.
        /// </summary>
        /// <inheritdoc cref="Save(object, string, EDataScope)"/>
        public static bool Save(object data)
        {
            // Cancel if the data is not valid
            if (data == null)
                return false;

            GetSaveOptions(data, out string filePath, out EDataScope scope);
            return Save(data, filePath, scope);
        }

        /// <summary>
        /// Saves the given data into a file at the given path, and try to get the scope defined by <see cref="DataScopeAttribute"/>.
        /// If relative path given:<br/>
        ///     - it's resolved from the /ProjectSettings directory of this project if the scope is defined and set to
        /// <see cref="EDataScope.Project"/><br/>
        ///     - it's resolved from the /UserSettings directory of this project if the scope is defined and set to
        /// <see cref="EDataScope.User"/><br/>
        ///     - it's resolved from <see cref="PathUtility.PersistentDataPath"/> directory in any other cases
        /// </summary>
        /// <inheritdoc cref="Save(object, string, EDataScope)"/>
        public static bool Save(object data, string filePath)
        {
            // Cancel if the data is not valid
            if (data == null)
                return false;

            GetSaveOptions(data, out _, out EDataScope scope);
            return Save(data, filePath, scope);
        }

        /// <summary>
        /// Saves the given data into <see cref="PlayerPrefs"/> if the scope is <see cref="EDataScope.Player"/>, or
        /// <see cref="UnityEditor.EditorPrefs"/> otherwise if available.<br/>
        /// This function will also try to get file path from <see cref="SaveAttribute"/>. If a valid file path is found, prefer save the
        /// data to a file. If that file path is relative:<br/>
        ///     - it's resolved from the /ProjectSettings directory of this project if the scope is <see cref="EDataScope.Project"/><br/>
        ///     - it's resolved from the /UserSettings directory of this project if the scope is <see cref="EDataScope.User"/><br/>
        ///     - it's resolved from <see cref="PathUtility.PersistentDataPath"/> directory in any other cases
        /// </summary>
        /// <inheritdoc cref="Save(object, string, EDataScope)"/>
        public static bool Save(object data, EDataScope scope)
        {
            // Cancel if the data is not valid
            if (data == null)
                return false;

            GetSaveOptions(data, out string filePath, out _);
            return Save(data, filePath, scope);
        }

        /// <summary>
        /// Saves the given data into a file at the given path.<br/>
        /// If relative path given:<br/>
        ///     - it's resolved from the /ProjectSettings directory of this project if the scope is <see cref="EDataScope.Project"/><br/>
        ///     - it's resolved from the /UserSettings directory of this project if the scope is <see cref="EDataScope.User"/><br/>
        ///     - it's resolved from <see cref="PathUtility.PersistentDataPath"/> directory in any other cases
        /// </summary>
        /// <param name="data">The data to save.</param>
        /// <param name="filePath">The path to the file where the data will be saved. If invalid path given, saves the data using
        /// <see cref="PlayerPrefs"/> if the scope is <see cref="EDataScope.Player"/>, or <see cref="UnityEditor.EditorPrefs"/>
        /// otherwise if available.</param>
        /// <param name="scope">The scope of the data to save.</param>
        /// <returns>Returns true if the data has been saved successfully, otherwise false.</returns>
        public static bool Save(object data, string filePath, EDataScope scope)
        {
            // Cancel if the given data is not valid
            if (data == null)
                return false;

            // Call savable callback if applicable.
            if (data is ISavable savable)
                savable.BeforeSave();

#if UNITY_EDITOR
            string json = UnityEditor.EditorJsonUtility.ToJson(data, true);
#else
            string json = JsonUtility.ToJson(data, false);
#endif

            bool success = true;

            // If data should be saved in file
            if (!string.IsNullOrEmpty(filePath))
            {
                filePath = filePath.AbsolutePath();
                try
                {
                    Directory.CreateDirectory(filePath.GetDirectoryPath());
                    File.WriteAllText(filePath, json);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    success = false;
                }
            }

#if UNITY_EDITOR
            // If data should be saved in editor prefs
            if (scope != EDataScope.Player)
            {
                UnityEditor.EditorPrefs.SetString(data.GetType().AssemblyQualifiedName, json);
                return true;
            }
#endif

            PlayerPrefs.SetString(data.GetType().AssemblyQualifiedName, json);
            return success;
        }

        /// <inheritdoc cref="Load(out ScriptableObject, Type)"/>
        /// <inheritdoc cref="Load{T}(out T, string)"/>
        public static bool Load<T>(out T obj)
            where T : ScriptableObject
        {
            obj = ScriptableObject.CreateInstance<T>();
            GetSaveOptions(obj, out string filePath, out _);
            return Load(obj, filePath);
        }

        /// <inheritdoc cref="Load(out ScriptableObject, Type, string)"/>
        public static bool Load(out ScriptableObject obj, Type objType)
        {
            obj = ScriptableObject.CreateInstance(objType);
            // Cancel if the given type is not assignable from ScriptableObject
            if (obj == null)
                return false;

            GetSaveOptions(obj, out string filePath, out _);
            return Load(obj, filePath);
        }

        /// <summary>
        /// <inheritdoc cref="Load(object, string)"/><br/>
        /// This function will try to get the path to the file that contain the serialized content, using <see cref="SaveAttribute"/> and
        /// <see cref="DataScopeAttribute"/> attributes defined in the data class.
        /// </summary>
        /// <inheritdoc cref="Load(object, string)"/>
        public static bool Load(object data)
        {
            // Cancel if the data is not valid
            if (data == null)
                return false;

            GetSaveOptions(data, out string filePath, out _);
            return Load(data, filePath);
        }

        /// <typeparam name="T">The type of the object to create and load.</typeparam>
        /// <inheritdoc cref="Load(out ScriptableObject, Type, string)"/>
        public static bool Load<T>(out T obj, string filePath)
            where T : ScriptableObject
        {
            obj = ScriptableObject.CreateInstance<T>();
            return Load(obj, filePath);
        }

        /// <summary>
        /// Loads data by overriding a created asset, using serialized content from file or prefs.
        /// </summary>
        /// <param name="obj">Outputs the created object.</param>
        /// <param name="objType">The type of the object to create and load.</param>
        /// <inheritdoc cref="Load(object, string)"/>
        public static bool Load(out ScriptableObject obj, Type objType, string filePath)
        {
            obj = ScriptableObject.CreateInstance(objType);
            // Cancel if the given type is not assignable from ScriptableObject
            if (obj == null)
                return false;

            return Load(obj, filePath);
        }

        /// <summary>
        /// Loads data by overriding the given object, using serialized content from file or prefs.
        /// </summary>
        /// <param name="data">The data object to overwrite.</param>
        /// <param name="filePath">The path to the file that contains the serialized content to load.</param>
        /// <returns>Returns true if serialized content has been loaded successfully, otherwise false.</returns>
        public static bool Load(object data, string filePath)
        {
            // Cancel if the input data to overwrite is not valid
            if (data == null)
                return false;

            string json = null;

            // If data is saved in file
            if (!string.IsNullOrEmpty(filePath))
            {
                PathUtility.ToPath(ref filePath);
                try
                {
                    if (File.Exists(filePath))
                        json = File.ReadAllText(filePath);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

#if UNITY_EDITOR
            // If no data has been found from file, try to load it from editor prefs
            if (json == null)
                json = UnityEditor.EditorPrefs.GetString(data.GetType().AssemblyQualifiedName, null);
#endif

            // If no data has been found from file, try to load it from player prefs
            if (json == null)
                json = PlayerPrefs.GetString(data.GetType().AssemblyQualifiedName, null);

            // If no data has been found from any source
            if (json == null)
                return false;

#if UNITY_EDITOR
            UnityEditor.EditorJsonUtility.FromJsonOverwrite(json, data);
#else
            JsonUtility.FromJsonOverwrite(json, data);
#endif

            if (data is ISavable savable)
                savable.AfterLoad();

            return true;
        }

        /// <summary>
        /// Registers an object that can be saved in batch by calling <see cref="SaveAll"/>.
        /// </summary>
        /// <param name="savableObject">The object to register.</param>
        /// <returns>Returns true if the object has been registered successfully, or false if it was already registered.</returns>
        public static bool Register(object savableObject)
        {
            if (_savableObjects.Contains(savableObject))
                return false;

            _savableObjects.Add(savableObject);
            return true;
        }

        /// <summary>
        /// Unregisters an object from the batch save list.
        /// </summary>
        /// <param name="savableObject">The object to unregister.</param>
        /// <returns>Returns true if the object has been unregistered successfully, otherwise false.</returns>
        public static bool Unregister(object savableObject)
        {
            return _savableObjects.Remove(savableObject);
        }

        /// <summary>
        /// Saves all registered objects.
        /// </summary>
        public static void SaveAll()
        {
            foreach (object savableObject in _savableObjects)
                Save(savableObject);
        }

        /// <inheritdoc cref="GetFilePath(object, EDataScope)"/>
        public static string GetFilePath(object data)
        {
            GetSaveOptions(data, out string filePath, out _);
            return filePath;
        }

        /// <summary>
        /// Gets the path to the file where the data should be saved. This function uses <see cref="SaveAttribute"/> and
        /// <see cref="DataScopeAttribute"/> to get that file path.
        /// </summary>
        /// <param name="data">The object of which to get the save file path.</param>
        /// <param name="scope">The scope of the data to save. This value overrides the ones from <see cref="SaveAttribute"/> or
        /// <see cref="DataScopeAttribute"/> if defined on the given data object.</param>
        /// <returns>Returns the found save file path.</returns>
        public static string GetFilePath(object data, EDataScope scope)
        {
            GetSaveOptions(data, out string filePath, out _);
            return GetAbsoluteFilePath(filePath, scope);
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets the options defined by <see cref="SaveAttribute"/> and <see cref="DataScopeAttribute"/>.
        /// </summary>
        /// <param name="data">The data from which to get the options.</param>
        /// <param name="filePath">Outputs the first file path defined from a <see cref="SaveAttribute"/>, starting from class declaration,
        /// then fields, then properties;.</param>
        /// <param name="scope">Outputs the "highest" scope defined from a <see cref="DataScopeAttribute"/>, with from the highest to
        /// lowest: <see cref="EDataScope.Player"/>, <see cref="EDataScope.Project"/> and <see cref="EDataScope.User"/>.</param>
        private static void GetSaveOptions(object data, out string filePath, out EDataScope scope)
        {
            scope = EDataScope.Temp;
            bool scopeDefinedFromDataScopeAttribute = false;
            filePath = null;

            if (data.GetType().TryGetAttribute(out DataScopeAttribute dataScopeAttribute))
            {
                scope = dataScopeAttribute.Scope;
                scopeDefinedFromDataScopeAttribute = true;
            }

            if (data.GetType().TryGetAttribute(out SaveAttribute saveAttribute))
            {
                if (!scopeDefinedFromDataScopeAttribute && saveAttribute.Scope != null)
                    scope = (EDataScope)saveAttribute.Scope;

                filePath = GetAbsoluteFilePath(saveAttribute.FilePath, scope);
            }

            // If scope if not the "highest" possible
            if (scope != EDataScope.Player)
            {
                foreach (FieldOrPropertyInfo fieldOrProperty in ReflectionUtility.GetFieldsAndPropertiesWithAttribute<DataScopeAttribute>(data))
                {
                    // If the defined scope has a higher level
                    if (fieldOrProperty.TryGetCustomAttribute(out DataScopeAttribute attr) && (int)attr.Scope < (int)scope)
                    {
                        scope = attr.Scope;
                        // Stop if the scope is now the highest possible
                        if (scope == EDataScope.Player)
                            break;
                    }
                }
            }

            // If the file path is not defined
            if (string.IsNullOrEmpty(filePath))
            {
                foreach (FieldOrPropertyInfo fieldOrProperty in ReflectionUtility.GetFieldsAndPropertiesWithAttribute<SaveAttribute>(data))
                {
                    if (fieldOrProperty.TryGetCustomAttribute(out SaveAttribute attr))
                    {
                        if (string.IsNullOrEmpty(filePath))
                            filePath = GetAbsoluteFilePath(attr.FilePath, scope);

                        if (!scopeDefinedFromDataScopeAttribute && attr.Scope != null && (int)attr.Scope < (int)scope)
                            scope = (EDataScope)attr.Scope;
                    }
                }
            }

            // If no file path is defined but user still wants to use a file
            if (string.IsNullOrEmpty(filePath) && !saveAttribute.PreferPrefs)
                filePath = GetAbsoluteFilePath($"{data.GetType().Name}.json", scope);
        }

        /// <summary>
        /// Gets the absolute path to the file where the data should be saved.
        /// </summary>
        /// <param name="filePath">The path to the file where data should be saved.</param>
        /// <param name="scope">The scope of the data to save. Ignored if the defined file path is absolute.</param>
        /// <returns>Returns the absolute path to the file where the data should be saved.</returns>
        private static string GetAbsoluteFilePath(string filePath, EDataScope scope)
        {
            // Cancel if the path is not valid
            if (string.IsNullOrEmpty(filePath))
                return string.Empty;

            PathUtility.ToPath(ref filePath);

            // Returns the path as is if it's absolute
            if (Path.IsPathRooted(filePath))
                return filePath;

            switch (scope)
            {
                case EDataScope.Project:
                    return $"{PROJECT_SETTINGS_DIR}/{filePath}".AbsolutePath();

                case EDataScope.User:
                    return $"{USER_SETTINGS_DIR}/{filePath}".AbsolutePath();

                case EDataScope.Temp:
                    return $"{TEMP_DIR}/{filePath}".AbsolutePath();

                default:
                    return $"{PathUtility.PersistentDataPath}/{filePath}";
            }
        }

        #endregion

    }

}