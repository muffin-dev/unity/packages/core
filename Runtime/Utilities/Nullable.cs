/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

namespace MuffinDev.Core
{

    /// <summary>
    /// Implements NullObject pattern.<br/>
    /// This is mostly meant to use "null" keys for Dictionaries, which is useful to create default or fallback values.<br/>
    /// The implementation is inspired from here: https://stackoverflow.com/a/22261282
    /// </summary>
    public struct Nullable<T>
    {

        #region Fields

        /// <summary>
        /// The nullable item.
        /// </summary>
        private T _item;

        /// <summary>
        /// Defines if the item is null.
        /// This is useful to consider non-nullable values (like structs) null.
        /// </summary>
        private bool _isNull;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="item">The nullable item.</param>
        public Nullable(T item) : this(item, item == null) { }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="item">The nullable item.</param>
        /// <param name="isNull">Defines if the item is null or not.</param>
        private Nullable(T item, bool isNull) : this()
        {
            _item = item;
            _isNull = isNull;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets a default <see cref="Nullable{T}"/> item.
        /// </summary>
        public static Nullable<T> Null => new Nullable<T>();

        /// <summary>
        /// Gets the nullable item.
        /// </summary>
        public T Item => _item;

        /// <summary>
        /// Checks if the item is null.
        /// </summary>
        public bool IsNull => _isNull;

        #endregion


        #region Operators

        /// <summary>
        /// Implicit conversion into the generic type.
        /// </summary>
        /// <param name="nullable">The nullable item you want to convert.</param>
        public static implicit operator T(Nullable<T> nullable)
        {
            return nullable.Item;
        }

        /// <summary>
        /// Implicit conversion into a nullable item.
        /// </summary>
        /// <param name="item">The item you want to make nullable.</param>
        public static implicit operator Nullable<T>(T item)
        {
            return new Nullable<T>(item);
        }

        /// <summary>
        /// Checks if the nullable item is equal to the other.
        /// </summary>
        public static bool operator ==(Nullable<T> item, object other)
        {
            return item.Equals(other);
        }

        /// <summary>
        /// Checks if the nullable item is different from the other.
        /// </summary>
        public static bool operator !=(Nullable<T> item, object other)
        {
            return !(item == other);
        }

        /// <summary>
        /// Checks if the given object is equals to this one.
        /// </summary>
        public override bool Equals(object obj)
        {
            // Return true if the given object is null and so is this one
            if (obj == null)
                return IsNull;

            try
            {
                // Try casting the given object
                Nullable<T> other = (Nullable<T>)obj;
                // Check if both the objects are null
                if (IsNull)
                    return other.IsNull;
                // Check if the other object is null but not this one
                else if (other.IsNull)
                    return false;

                // Compare items directly to check for equality
                return Item.Equals(other.Item);
            }
            catch (InvalidCastException) { }

            return false;
        }

        /// <summary>
        /// Gets this nullablle item's hash code.
        /// </summary>
        /// <returns>Returns 0 if this nullable is null, or get the item's hash code otherwise.</returns>
        public override int GetHashCode()
        {
            if (IsNull)
                return 0;

            int hash = Item.GetHashCode();
            if (hash >= 0)
                hash++;
            return hash;
        }

        #endregion

    }

}