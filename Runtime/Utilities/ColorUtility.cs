/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

using UnityColorUtility = UnityEngine.ColorUtility;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellanous functions for working with <see cref="Color"/> and <see cref="Color32"/> values.
    /// </summary>
    public static class ColorUtility
    {

        #region Fields

        public const int MAX_COLOR_VALUE = 255;
        public const float MAX_COLOR_VALUE_F = 255f;

        #endregion


        #region Public API

        /// <summary>
        /// Converts the given color into an hexadecimal string value.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        /// <returns>Returns the "color string" with format "RRGGBB".</returns>
        public static string ToHexRGB(Color color)
        {
            return UnityColorUtility.ToHtmlStringRGB(color);
        }

        /// <inheritdoc cref="ToHexRGBA(Color)"/>
        public static string ToHexRGB(Color32 color)
        {
            return ToHexRGB(color);
        }

        /// <inheritdoc cref="ToHexRGB(Color)"/>
        /// <returns>Returns the "color string" with format "RRGGBBAA".</returns>
        public static string ToHexRGBA(Color color)
        {
            return UnityColorUtility.ToHtmlStringRGBA(color);
        }

        /// <inheritdoc cref="ToHexRGBA(Color)"/>
        public static string ToHexRGBA(Color32 color)
        {
            return ToHexRGBA(color);
        }

        /// <summary>
        /// Sets the given color values from the given hexadecimal string.
        /// </summary>
        /// <param name="color">The color to override.</param>
        /// <param name="hexString">The hexadecimal string (like F9F9F9, F9F9F9F9, #F9F9F9, #F9F9F9F9).</param>
        /// <returns>Returns true if the string has been parsed successfully, otherwise false.</returns>
        public static bool FromHex(ref Color color, string hexString)
        {
            string[] split = ParseHexColorString(hexString);
            if (split == null)
                return false;

            color.r = int.Parse(split[0], System.Globalization.NumberStyles.HexNumber) / MAX_COLOR_VALUE_F;
            color.g = int.Parse(split[1], System.Globalization.NumberStyles.HexNumber) / MAX_COLOR_VALUE_F;
            color.b = int.Parse(split[2], System.Globalization.NumberStyles.HexNumber) / MAX_COLOR_VALUE_F;
            color.a = int.Parse(split[3], System.Globalization.NumberStyles.HexNumber) / MAX_COLOR_VALUE_F;
            return true;
        }

        /// <summary>
        /// Creates a color value from the given hexadecimal string.
        /// </summary>
        /// <inheritdoc cref="FromHex(ref Color, string)"/>
        /// <returns>Returns the created color value.</returns>
        public static Color FromHex(string hexString)
        {
            Color output = Color.black;
            FromHex(ref output, hexString);
            return output;
        }

        /// <inheritdoc cref="FromHex(ref Color string)"/>
        public static bool FromHex32(ref Color32 color, string hexString)
        {
            string[] split = ParseHexColorString(hexString);
            if (split == null)
                return false;

            color.r = byte.Parse(split[0], System.Globalization.NumberStyles.HexNumber);
            color.g = byte.Parse(split[1], System.Globalization.NumberStyles.HexNumber);
            color.b = byte.Parse(split[2], System.Globalization.NumberStyles.HexNumber);
            color.a = byte.Parse(split[3], System.Globalization.NumberStyles.HexNumber);
            return true;
        }

        /// <inheritdoc cref="FromHex32(ref Color32, string)"/>
        public static Color32 FromHex32(string hexString)
        {
            Color32 output = new Color32(0, 0, 0, MAX_COLOR_VALUE);
            FromHex32(ref output, hexString);
            return output;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Splits an hexadecimal color string in 4 hex values.
        /// </summary>
        /// <param name="hexString">The hexadecimal string (like F9F9F9, F9F9F9F9, #F9F9F9, #F9F9F9F9).</param>
        /// <returns>Returns an array with 4 values, or null if the given hexadecimal string can't be parsed.</returns>
        private static string[] ParseHexColorString(string hexString)
        {
            hexString = hexString.Replace("#", "");
            if (hexString.Length < 8)
            {
                hexString += "ff";
            }

            string[] split = new string[4];
            try
            {
                split[0] = hexString.Substring(0, 2);
                split[1] = hexString.Substring(2, 2);
                split[2] = hexString.Substring(4, 2);
                split[3] = hexString.Substring(6, 2);
            }
            catch (System.Exception)
            {
                return null;
            }

            return split;
        }

        #endregion

    }

}