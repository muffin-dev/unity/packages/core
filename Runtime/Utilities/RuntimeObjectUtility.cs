/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaneous functions for working with <see cref="Object"/>s and assets at runtime.<br/>
    /// WARNING: Most of these methods are meant to be used in the editor context, mainly for debug purposes. Using them at runtime may
    /// have unexpected results.
    /// </summary>
    public static class RuntimeObjectUtility
    {

        /// <summary>
        /// Gets the <see cref="Transform"/> component of an object that can be placed in a scene.
        /// </summary>
        /// <param name="obj">The object from which you want to get the <see cref="Transform"/> component.</param>
        /// <returns>Returns the found <see cref="Transform"/> component.</returns>
        public static Transform GetTransform(Object obj)
        {
            if (obj is GameObject go)
                return go.transform;
            else if (obj is Component comp)
                return comp.transform;
            else
                return null;
        }

        /// <inheritdoc cref="GetTransform(Object)"/>
        /// <param name="transform">Outputs the found <see cref="Transform"/> component.</param>
        /// <returns>Returns true if the given object has a <see cref="Transform"/> component, otherwise false.</returns>
        public static bool TryGetTransform(Object obj, out Transform transform)
        {
            transform = GetTransform(obj);
            return transform != null;
        }

        /// <summary>
        /// Gets the <see cref="GameObject"/> to which the given object is attached.
        /// </summary>
        /// <param name="obj">The object of which you want to get the attached <see cref="GameObject"/>.</param>
        /// <returns>Returns the found attached <see cref="GameObject"/>.</returns>
        public static GameObject GetGameObject(Object obj)
        {
            if (obj is GameObject go)
                return go;
            else if (obj is Component comp)
                return comp.gameObject;
            else
                return null;
        }

        /// <inheritdoc cref="GetGameObject(Object)"/>
        /// <param name="gameObject">Outputs the found attached <see cref="GameObject"/>.</param>
        /// <returns>Returns true if the given object is attached to a <see cref="GameObject"/>, otherwise false.</returns>
        public static bool TryGetGameObject(Object obj, out GameObject gameObject)
        {
            gameObject = GetGameObject(obj);
            return gameObject != null;
        }

    }

}