/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Diagnostics;

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaneous utility methods for working with external processed.
    /// </summary>
    public class ProcessUtility
    {

        #region Public API

        /// <summary>
        /// Executes a command using the shell application synchronously.
        /// </summary>
        /// <param name="command">The command you want to run.</param>
        /// <param name="output">Outputs the standard shell output if the command has been successfully executed, otherwise the standard
        /// error output.</param>
        /// <param name="workingDirectory">The working directory where the command will be executed.</param>
        /// <param name="showWindow">If enabled, the shell application is displayed.</param>
        /// <returns>Returns true if the command has been executed successfully, otherwise false.</returns>
        public static bool ExecCommand(string command, out string output, string workingDirectory = null, bool showWindow = false)
        {
            using (Process cmd = new Process())
            {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                cmd.StartInfo.FileName = "cmd.exe";
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
                cmd.StartInfo.FileName = "/bin/bash";
#endif
                if (!string.IsNullOrEmpty(workingDirectory))
                    cmd.StartInfo.WorkingDirectory = workingDirectory;
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.RedirectStandardError = true;
                cmd.StartInfo.CreateNoWindow = !showWindow;
                cmd.StartInfo.UseShellExecute = false;
                cmd.Start();

                cmd.StandardInput.WriteLine(command);
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();

                string errorOutput = cmd.StandardError.ReadToEnd();
                if (!string.IsNullOrEmpty(errorOutput))
                {
                    output = errorOutput;
                    return false;
                }

                output = cmd.StandardOutput.ReadToEnd();
                return true;
            }
        }

        #endregion

    }

}