/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Represents a basic log entry in a <see cref="Log"/> instance.
    /// </summary>
    public class LogEntry : ILogEntry
    {

        #region Fields

        /// <summary>
        /// The level of this log entry.
        /// </summary>
        private ELogLevel _logLevel = ELogLevel.None;

        /// <summary>
        /// The message that describes this log entry.
        /// </summary>
        private string _message = string.Empty;

        /// <summary>
        /// Additional data.
        /// </summary>
        private object _data = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="LogEntry(string, object, ELogLevel)"/>
        public LogEntry(string message, ELogLevel level = ELogLevel.None)
        {
            _message = message;
            _logLevel = level;
        }

        /// <inheritdoc cref="LogEntry"/>
        /// <param name="message">The message that describes this log entry.</param>
        /// <param name="data">Additional data for the log entry.</param>
        /// <param name="logLevel">The level of this log entry.</param>
        public LogEntry(string message, object data, ELogLevel logLevel = ELogLevel.None)
        {
            _message = message;
            _data = data;
            _logLevel = logLevel;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="ILogEntry.LogLevel"/>
        public ELogLevel LogLevel => _logLevel;

        /// <inheritdoc cref="ILogEntry.Message"/>
        public string Message => _message;

        /// <inheritdoc cref="_data"/>
        public object Data => _data;

        /// <inheritdoc cref="ILogEntry.MessageWithDetails"/>
        public string MessageWithDetails
        {
            get
            {
                string output = _message;

                if (_data != null)
                {
                    if (!string.IsNullOrEmpty(output))
                        output += "\n";
                    output += _data.ToString();
                }

                return output;
            }
        }

        /// <inheritdoc cref="ILogEntry.ShowInConsole"/>
        public void ShowInConsole()
        {
            switch (_logLevel)
            {
                case ELogLevel.Warning:
                    Debug.LogWarning(MessageWithDetails);
                    break;

                case ELogLevel.Error:
                case ELogLevel.Fatal:
                    Debug.LogError(MessageWithDetails);
                    break;

                default:
                    Debug.Log(MessageWithDetails);
                    break;
            }
        }

        #endregion

    }

}