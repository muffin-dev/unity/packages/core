/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Represents a log entry that contains an exception.
    /// </summary>
    public class ExceptionLogEntry : ILogEntry
    {

        #region Fields

        /// <summary>
        /// The log level of this entry.
        /// </summary>
        private ELogLevel _logLevel = ELogLevel.None;

        /// <summary>
        /// Additional message to the exception. If not given, this log entry uses the original exception's message.
        /// </summary>
        private string _message = null;

        /// <summary>
        /// The exception of this log entry.
        /// </summary>
        private Exception _exception = null;

        /// <summary>
        /// Additional data.
        /// </summary>
        private object _data = null;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="ExceptionLogEntry(Exception, string, object, ELogLevel)"/>
        public ExceptionLogEntry(Exception exception, ELogLevel logLevel = ELogLevel.Fatal)
        {
            _logLevel = logLevel;
            _exception = exception;
        }

        /// <inheritdoc cref="ExceptionLogEntry(Exception, string, object, ELogLevel)"/>
        public ExceptionLogEntry(Exception exception, string message, ELogLevel logLevel = ELogLevel.Fatal)
        {
            _logLevel = logLevel;
            _message = message;
            _exception = exception;
        }

        /// <inheritdoc cref="ExceptionLogEntry(Exception, string, object, ELogLevel)"/>
        public ExceptionLogEntry(Exception exception, object data, ELogLevel logLevel = ELogLevel.Fatal)
        {
            _logLevel = logLevel;
            _data = data;
            _exception = exception;
        }

        /// <inheritdoc cref="ExceptionLogEntry"/>
        /// <param name="exception">The handled exception.</param>
        /// <param name="message">The additional message to the exception.</param>
        /// <param name="data">Additional data that complete exception information.</param>
        /// <param name="logLevel">The level of the log entry.</param>
        public ExceptionLogEntry(Exception exception, string message, object data, ELogLevel logLevel = ELogLevel.Fatal)
        {
            _logLevel = logLevel;
            _message = message;
            _exception = exception;
            _data = data;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_logLevel"/>
        public ELogLevel LogLevel => _logLevel;

        /// <inheritdoc cref="_data"/>
        public object Data => _data;

        /// <inheritdoc cref="_exception"/>
        public Exception Exception => _exception;

        /// <summary>
        /// Gets the custom message, or the exception message.
        /// </summary>
        public string Message
        {
            get
            {
                return string.IsNullOrEmpty(_message)
                    ? _exception != null ? _exception.Message : string.Empty
                    : _message;
            }
        }

        /// <summary>
        /// Gets the full content of the exception, with eventual additional message.
        /// </summary>
        public string MessageWithDetails
        {
            get
            {
                string output = "Exception: " + string.Empty;

                if (!string.IsNullOrEmpty(_message))
                    output = _message;

                if (_exception != null)
                {
                    if (!string.IsNullOrEmpty(_message))
                        output += "\n";

                    output += _exception.Message;
                    output += "\n" + _exception.StackTrace;
                }

                if (_data != null)
                {
                    if (!string.IsNullOrEmpty(output))
                        output += "\n";
                    output += _data.ToString();
                }

                return output;
            }
        }

        /// <inheritdoc cref="ILogEntry.ShowInConsole"/>
        public void ShowInConsole()
        {
            switch (_logLevel)
            {
                case ELogLevel.Warning:
                    Debug.LogWarning(MessageWithDetails);
                    break;

                case ELogLevel.Error:
                case ELogLevel.Fatal:
                    Debug.LogError(MessageWithDetails);
                    break;

                default:
                    Debug.Log(MessageWithDetails);
                    break;
            }
        }

        #endregion

    }

}