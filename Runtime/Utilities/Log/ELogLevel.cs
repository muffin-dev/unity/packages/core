/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Core
{

    /// <summary>
    /// Represents the level of a log entry.
    /// </summary>
    public enum ELogLevel
    {
        /// <summary>
        /// Undefined log level.
        /// </summary>
        None = 0,

        /// <summary>
        /// Basic information.
        /// </summary>
        Log = 1,

        /// <summary>
        /// Warning, information about something that may cause a process to produce unexpected results, or invalid configuration.
        /// </summary>
        Warning = 2,

        /// <summary>
        /// Error that may cause the process to fail, but not necessariliy cancel it.
        /// </summary>
        Error = 3,

        /// <summary>
        /// Fatal error, that cause a process to fail and exit.
        /// </summary>
        Fatal = 4
    }

}