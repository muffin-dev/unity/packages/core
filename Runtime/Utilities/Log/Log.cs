/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace MuffinDev.Core
{

    /// <summary>
    /// Utility for tracking informations about a running process.<br/>
    /// Its purpose is to handle messages and context objects, so you can use and display these informations as you want.
    /// </summary>
    public class Log : IEnumerable<ILogEntry>
    {

        #region Fields

        public const string DEFAULT_LOG_FILE_EXTENSION = ".txt";

        /// <summary>
        /// The list of all entries in this log.
        /// </summary>
        private List<ILogEntry> _logEntries = new List<ILogEntry>();

        #endregion


        #region Public API

        /// <summary>
        /// Gets the log entries count.
        /// </summary>
        public int Count => _logEntries.Count;

        /// <inheritdoc cref="_logEntries"/>
        public ILogEntry[] LogEntries => _logEntries.FindAll(entry => entry != null).ToArray();

        /// <summary>
        /// Gets the highest log level among the entries in the list.
        /// </summary>
        public ELogLevel HighestLogLevel
        {
            get
            {
                int highestLevelInt = (int)ELogLevel.None;

                foreach (ILogEntry entry in _logEntries)
                {
                    if ((int)entry.LogLevel > highestLevelInt)
                    {
                        if (entry.LogLevel == ELogLevel.Fatal)
                            return ELogLevel.Fatal;
                        else
                            highestLevelInt = (int)entry.LogLevel;
                    }
                }

                return (ELogLevel)highestLevelInt;
            }
        }

        /// <summary>
        /// Checks if this Log contains fatal errors.
        /// </summary>
        public bool ContainsFatalErrors => HighestLogLevel == ELogLevel.Fatal;

        /// <summary>
        /// Checks if this Log contains errors or fatal errors.
        /// </summary>
        public bool ContainsErrors => (int)HighestLogLevel >= (int)ELogLevel.Error;

        /// <inheritdoc cref="Add(string, object, ELogLevel)"/>
        public void Add(string message, ELogLevel logLevel = ELogLevel.None)
        {
            Add(message, null, logLevel);
        }

        /// <summary>
        /// Adds a new log entry with a simple message.
        /// </summary>
        /// <param name="message">The message that describes the log entry.</param>
        /// <param name="data">The data bound to the log entry.</param>
        /// <param name="logLevel">The level of this log entry.</param>
        public void Add(string message, object data, ELogLevel logLevel = ELogLevel.None)
        {
            _logEntries.Add(new LogEntry(message, data, logLevel));
        }

        /// <summary>
        /// Adds the given log entry to the list.
        /// </summary>
        /// <param name="logEntry">The log entry to add.</param>
        public void Add(ILogEntry logEntry)
        {
            _logEntries.Add(logEntry);
        }

        /// <summary>
        /// Adds a new log entry that contains a simple message.
        /// </summary>
        /// <inheritdoc cref="Add(string, object, ELogLevel)"/>
        public void Message(string message, object data = null)
        {
            Add(message, data, ELogLevel.Log);
        }

        /// <summary>
        /// Adds a new log entry that contains a warning message.
        /// </summary>
        /// <inheritdoc cref="Add(string, object, ELogLevel)"/>
        public void Warning(string message, object data = null)
        {
            Add(message, data, ELogLevel.Warning);
        }

        /// <summary>
        /// Adds a new log entry that contains an error message.
        /// </summary>
        /// <inheritdoc cref="Add(string, object, ELogLevel)"/>
        public void Error(string message, object data = null)
        {
            Add(message, data, ELogLevel.Error);
        }

        /// <summary>
        /// Adds a new log entry that contains the message of an error that caused a process to fail and exit.
        /// </summary>
        /// <inheritdoc cref="Add(string, object, ELogLevel)"/>
        public void Fatal(string message, object data = null)
        {
            Add(message, data, ELogLevel.Fatal);
        }

        /// <inheritdoc cref="Exception(System.Exception, string, object, ELogLevel)"/>
        public void Exception(Exception exception, ELogLevel logLevel = ELogLevel.Fatal)
        {
            Exception(exception, null, null, logLevel);
        }

        /// <inheritdoc cref="Exception(System.Exception, string, object, ELogLevel)"/>
        public void Exception(Exception exception, string message, ELogLevel logLevel = ELogLevel.Fatal)
        {
            Exception(exception, message, null, logLevel);
        }

        /// <inheritdoc cref="Exception(System.Exception, string, object, ELogLevel)"/>
        public void Exception(Exception exception, object data, ELogLevel logLevel = ELogLevel.Fatal)
        {
            Exception(exception, null, data, logLevel);
        }

        /// <summary>
        /// Adds a new log entry bound to a handled exception.
        /// </summary>
        /// <inheritdoc cref="Add(string, object, ELogLevel)"/>
        /// <param name="exception">The exception that have been handled.</param>
        public void Exception(Exception exception, string message, object data, ELogLevel logLevel = ELogLevel.Fatal)
        {
            _logEntries.Add(new ExceptionLogEntry(exception, message, data, logLevel));
        }

        /// <summary>
        /// Gets an array of log entries that have the given log level or higher.
        /// </summary>
        /// <param name="logLevel">The minimum log level of the entries.</param>
        /// <returns>Returns the filtered log entries list.</returns>
        public ILogEntry[] Filter(ELogLevel logLevel)
        {
            // Get all entries if the required log level is the minimum
            if (logLevel == ELogLevel.None || logLevel == ELogLevel.Log)
                return _logEntries.ToArray();

            int logLevelInt = (int)logLevel;
            return _logEntries.FindAll(e => e != null && (int)e.LogLevel >= logLevelInt).ToArray();
        }

        /// <summary>
        /// Writes the log entries of this Log into a file.
        /// </summary>
        /// <param name="path">The absolute path to the file.</param>
        /// <param name="title">Eventual title of the log file.</param>
        public void ToFile(string path, string title = null)
        {
            string content = !string.IsNullOrEmpty(title) ? title : string.Empty;
            if (!string.IsNullOrEmpty(content))
                content += " - ";

            content += DateTime.Now.ToString("G");
            content += "\n";

            foreach (ILogEntry entry in _logEntries)
            {
                content += "\n";
                if (entry == null)
                    continue;

                content += $"[{(entry.LogLevel != ELogLevel.None ? entry.LogLevel : ELogLevel.Log)}] {entry.MessageWithDetails}";
            }

            if (string.IsNullOrEmpty(Path.GetExtension(path)))
                path += DEFAULT_LOG_FILE_EXTENSION;

            if (!Directory.Exists(Path.GetDirectoryName(path)))
                Directory.CreateDirectory(Path.GetDirectoryName(path));

            File.WriteAllText(path, content);
        }

        /// <summary>
        /// Write all log entries as logs in the Unity console.
        /// </summary>
        public void ShowInConsole()
        {
            foreach (ILogEntry entry in _logEntries)
            {
                entry.ShowInConsole();
            }
        }

        /// <summary>
        /// Iterates through all log entries.
        /// </summary>
        public IEnumerator<ILogEntry> GetEnumerator()
        {
            return ((IEnumerable<ILogEntry>)_logEntries).GetEnumerator();
        }

        /// <inheritdoc cref="GetEnumerator"/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_logEntries).GetEnumerator();
        }

        #endregion

    }

}