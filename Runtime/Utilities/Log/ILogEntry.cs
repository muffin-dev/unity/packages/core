/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Core
{

    /// <summary>
    /// Represents an entry in a <see cref="Log"/> instance.
    /// </summary>
    public interface ILogEntry
    {

        /// <summary>
        /// The level of this log entry.
        /// </summary>
        ELogLevel LogLevel { get; }

        /// <summary>
        /// The message that describes the log entry.
        /// </summary>
        string Message { get; }

        /// <summary>
        /// The message that describes the log entry, including eventual additional informations. This is mainly used to write full log
        /// informations into a file or display call stacks.
        /// </summary>
        string MessageWithDetails { get; }

        /// <summary>
        /// Write this log entry as a message in the Unity console.
        /// </summary>
        void ShowInConsole();

    }

}