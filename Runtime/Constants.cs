/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Core
{

    /// <summary>
    /// Miscellaneous constant values.
    /// </summary>
    public static class Constants
    {

        /// <summary>
        /// Name of the company.
        /// </summary>
        public const string COMPANY_NAME = "Muffin Dev";

        /// <summary>
        /// Base URL for online documentation of the framework. You must concatenate your own path starting with "/".
        /// </summary>
        public const string BASE_HELP_URL = "http://unity.muffindev.com/packages";

        /// <summary>
        /// The submenu for demo features (from "Add Component" button, "Asset > Create" and the main toolbar menus. Starts with "/". You
        /// must concatenate your own path starting with "/".
        /// </summary>
        public const string DEMOS_SUBMENU = "/Demos";

        /// <summary>
        /// The define symbol used to enable demos of the framework.
        /// </summary>
        public const string DEMOS_DEFINE = "MUFFINDEV_DEMOS";

        /// <summary>
        /// Base path used for <see cref="UnityEditor.CreateAssetMenuAttribute"/>. You must concatenate your own path starting with "/".
        /// </summary>
        public const string ADD_COMPONENT_MENU = COMPANY_NAME;

        /// <summary>
        /// Base path used for <see cref="UnityEditor.CreateAssetMenuAttribute"/> with demo components. You must concatenate your own path
        /// starting with "/".
        /// </summary>
        public const string ADD_COMPONENT_MENU_DEMOS = COMPANY_NAME + DEMOS_SUBMENU;

        /// <summary>
        /// Base path used for <see cref="UnityEditor.CreateAssetMenuAttribute"/>. You must concatenate your own path starting with "/".
        /// </summary>
        public const string CREATE_ASSET_MENU = COMPANY_NAME;

        /// <summary>
        /// Base path used for <see cref="UnityEditor.CreateAssetMenuAttribute"/> with demo assets. You must concatenate your own path
        /// starting with "/".
        /// </summary>
        public const string CREATE_ASSET_MENU_DEMOS = COMPANY_NAME + DEMOS_SUBMENU;

    }

}