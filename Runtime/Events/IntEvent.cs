/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s for int values.
    /// </summary>
    [System.Serializable]
    public class IntEvent : UnityEvent<int> { }

}