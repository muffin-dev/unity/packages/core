/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s for <see cref="Vector3Int"/> values.
    /// </summary>
    [System.Serializable]
    public class Vector3IntEvent : UnityEvent<Vector3Int> { }

}