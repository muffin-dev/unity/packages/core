/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s for <see cref="Quaternion"/> values.
    /// </summary>
    [System.Serializable]
    public class QuaternionEvent : UnityEvent<Quaternion> { }

}