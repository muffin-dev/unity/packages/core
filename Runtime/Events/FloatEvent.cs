/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s for float values.
    /// </summary>
    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }

}