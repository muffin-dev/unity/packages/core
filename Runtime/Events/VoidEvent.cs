/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s with no parameters.
    /// </summary>
    [System.Serializable]
    public class VoidEvent : UnityEvent { }

}