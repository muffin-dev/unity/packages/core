/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s for <see cref="GameObject"/> instances.
    /// </summary>
    [System.Serializable]
    public class GameObjectEvent : UnityEvent<GameObject> { }

}