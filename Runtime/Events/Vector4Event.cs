/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s for <see cref="Vector4"/> values.
    /// </summary>
    [System.Serializable]
    public class Vector4Event : UnityEvent<Vector4> { }

}