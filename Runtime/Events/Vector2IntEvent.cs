/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEngine.Events;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom <see cref="UnityEvent"/>s for <see cref="Vector2Int"/> values.
    /// </summary>
    [System.Serializable]
    public class Vector2IntEvent : UnityEvent<Vector2Int> { }

}