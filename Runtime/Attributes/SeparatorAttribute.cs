/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Draws a separator line before the next property.
    /// </summary>
    public class SeparatorAttribute : PropertyAttribute
    {

        /// <summary>
        /// If enabled, the separator will have the exact current view width.
        /// </summary>
        private bool _wide = false;

        /// <summary>
        /// Draws a separator line before the next property.
        /// </summary>
        /// <param name="wide">If enabled, the separator will have the exact current view width.</param>
        public SeparatorAttribute(bool wide = false)
        {
            _wide = wide;
        }

        /// <inheritdoc cref="_wide"/>
        public bool Wide => _wide;

    }

}