/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Indents a property in the inspector.
    /// </summary>
    public class IndentAttribute : PropertyAttribute
    {

        /// <summary>
        /// The number of levels to add to the current indent level.
        /// </summary>
        private int _levels = 1;

        /// <summary>
        /// Indents this property in the inspector.
        /// </summary>
        /// <param name="levels">The number of levels to add to the current indent level.</param>
        public IndentAttribute(int levels = 1)
        {
            _levels = levels;
        }

        /// <inheritdoc cref="_levels"/>
        public int Levels => _levels;

    }

}