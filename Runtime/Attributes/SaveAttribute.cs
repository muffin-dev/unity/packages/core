/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

namespace MuffinDev.Core
{

    /// <summary>
    /// Marks a class, field or a property that it can be saved to files or prefs by following given rules. It can work in combination with
    /// <see cref="DataScopeAttribute"/> in ordder to define where the data should be saved.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class SaveAttribute : Attribute
    {

        #region Fields

        /// <summary>
        /// The path to the file where the data should be stored.
        /// </summary>
        private string _filePath = string.Empty;

        /// <summary>
        /// The scope for saving the data. Note that the data scope can also be defined using <see cref="DataScopeAttribute"/>, and will
        /// always be overriden by that attribute.
        /// </summary>
        private EDataScope? _scope = null;

        /// <summary>
        /// If enabled, prefer saving the data in prefs instead of a file, using <see cref="UnityEngine.PlayerPrefs"/> if the scope is
        /// <see cref="EDataScope.Player"/>, or <see cref="UnityEditor.EditorPrefs"/> otherwise. This value is only used if no file path is
        /// provided.
        /// </summary>
        private bool _preferPrefs = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Makes the data savable in a file at the given path, using the <see cref="SaveUtility"/> helper class. You can also define a
        /// scope for saving the data using the <see cref="DataScopeAttribute"/>.
        /// 
        /// If relative path given:<br/>
        ///     - it's resolved from the /ProjectSettings directory of this project if a scope is defined and set to
        /// <see cref="EDataScope.Project"/><br/>
        ///     - it's resolved from the /UserSettings directory of this project if a scope is defined and set to
        /// <see cref="EDataScope.User"/><br/>
        ///     - it's resolved from <see cref="PathUtility.PersistentDataPath"/> directory in any other cases
        /// </summary>
        /// <inheritdoc cref="SaveAttribute(string, EDataScope)"/>
        public SaveAttribute(string filePath)
        {
            _filePath = filePath;
        }

        /// <summary>
        /// Makes the data savable in a file at the given path, using the <see cref="SaveUtility"/> helper class.<br/>
        /// If relative path given:<br/>
        ///     - it's resolved from the /ProjectSettings directory of this project if the scope is <see cref="EDataScope.Project"/><br/>
        ///     - it's resolved from the /UserSettings directory of this project if the scope is <see cref="EDataScope.User"/><br/>
        ///     - it's resolved from <see cref="PathUtility.PersistentDataPath"/> directory in any other cases
        /// </summary>
        /// <param name="filePath">The path to the file where data is saved.</param>
        /// <param name="scope">The scope for saving the data.</param>
        public SaveAttribute(string filePath, EDataScope scope)
        {
            _filePath = filePath;
            _scope = scope;
        }

        /// <summary>
        /// Makes the data savable in a file or prefs, using the <see cref="SaveUtility"/> helper class. By default, the file will have
        /// the class' name.
        /// </summary>
        /// <param name="preferPrefs">If enabled, prefer saving the data in prefs instead of a file, using
        /// <see cref="UnityEngine.PlayerPrefs"/> if the scope is <see cref="EDataScope.Player"/>, or <see cref="UnityEditor.EditorPrefs"/>
        /// otherwise.</param>
        /// <inheritdoc cref="SaveAttribute(string, EDataScope)"/>
        public SaveAttribute(EDataScope scope, bool preferPrefs = false)
        {
            _scope = scope;
            _preferPrefs = preferPrefs;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_filePath"/>
        public string FilePath => _filePath;

        /// <inheritdoc cref="_scope"/>
        public EDataScope? Scope => _scope;

        /// <inheritdoc cref="_preferPrefs"/>
        public bool PreferPrefs => _preferPrefs;

        #endregion

    }

}