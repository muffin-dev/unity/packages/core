/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Draws a field to select a file path, with a "Browse..." button next to it. 
    /// </summary>
    public class FilePathFieldAttribute : PropertyAttribute
    {

        #region Fields

        /// <summary>
        /// The title of the "open file" panel.
        /// </summary>
        private string _panelTitle = string.Empty;

        /// <summary>
        /// The expected extension of the selected file.
        /// </summary>
        private string _extension = string.Empty;

        /// <summary>
        /// The accepted extensions of the file. Must follow this pattern: { "Image files", "png,jpg,jpeg", "All files", "*" }
        /// </summary>
        private string[] _filters = null;

        /// <summary>
        /// The key to use to save the path in paths history.
        /// </summary>
        private string _historyKey = string.Empty;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="FilePathFieldAttribute(string, string, string"/>
        public FilePathFieldAttribute() { }

        /// <inheritdoc cref="FilePathFieldAttribute(string, string, string"/>
        public FilePathFieldAttribute(string extension)
        {
            _extension = extension;
        }

        /// <inheritdoc cref="FilePathFieldAttribute(string[], string, string"/>
        public FilePathFieldAttribute(string[] filters)
        {
            _filters = filters;
        }

        /// <inheritdoc cref="FilePathFieldAttribute(string, string, string"/>
        public FilePathFieldAttribute(string extension, string panelTitle)
        {
            _panelTitle = panelTitle;
            _extension = extension;
        }

        /// <inheritdoc cref="FilePathFieldAttribute(string[], string, string"/>
        public FilePathFieldAttribute(string[] filters, string panelTitle)
        {
            _panelTitle = panelTitle;
            _filters = filters;
        }

        /// <param name="extension">The expected extension of the selected file.</param>
        /// <inheritdoc cref="FilePathFieldAttribute(string[], string, string"/>
        public FilePathFieldAttribute(string extension, string panelTitle, string historyKey)
        {
            _panelTitle = panelTitle;
            _extension = extension;
            _historyKey = historyKey;
        }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="panelTitle">The title of the "open file" panel.</param>
        /// <param name="filters">The accepted extensions of the file. Must follow this pattern: { "Image files", "png,jpg,jpeg", "All
        /// files", "*" }</param>
        /// <param name="historyKey">The key to use to save the path in paths history.</param>
        public FilePathFieldAttribute(string[] filters, string panelTitle, string historyKey)
        {
            _panelTitle = panelTitle;
            _filters = filters;
            _historyKey = historyKey;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_panelTitle"/>
        public string PanelTitle => _panelTitle;

        /// <inheritdoc cref="_extension"/>
        public string Extension => _extension;

        /// <inheritdoc cref="_filters"/>
        public string[] Filters => _filters;

        /// <inheritdoc cref="_historyKey"/>
        public string HistoryKey => _historyKey;

        #endregion

    }

}