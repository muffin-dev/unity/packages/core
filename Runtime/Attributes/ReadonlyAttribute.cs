/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Makes a property readonly: visible but not editable in the inspector.
    /// </summary>
    public class ReadonlyAttribute : PropertyAttribute { }

}