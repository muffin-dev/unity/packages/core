/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Custom attribute for <see cref="AnimationCurve"/> properties.<br/>
    /// It provides a custom curve editor in the inspector view, with min and max time or value, and a custom color.
    /// </summary>
    public class AnimCurveAttribute : PropertyAttribute
    {

        #region Fields

        /// <summary>
        /// The default offset added to a value if given min and max are equal.
        /// </summary>
        private const float DEFAULT_OFFSET = 1f;

        /// <summary>
        /// The minimum time value (along the X axis).
        /// </summary>
        private readonly float _minTime = 0f;

        /// <summary>
        /// The maximum time value (along the X axis).
        /// </summary>
        private readonly float _maxTime = 0f;

        /// <summary>
        /// The minimum curve value (along the Y axis).
        /// </summary>
        private readonly float _minValue = 0f;

        /// <summary>
        /// The maximum curve value (along the Y axis).
        /// </summary>
        private readonly float _maxValue = 0f;

        /// <summary>
        /// The color of the curve in the editor.
        /// </summary>
        private Color _curveColor = Color.green;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Creates an <see cref="AnimCurveAttribute"/> instance with time and value clamped between 0 and 1.
        /// </summary>
        public AnimCurveAttribute()
        {
            _maxTime = 1f;
            _maxValue = 1f;
        }

        /// <summary>
        /// Creates an <see cref="AnimCurveAttribute"/> instance with no bounds.
        /// </summary>
        /// <inheritdoc cref="AnimCurveAttribute(float, float, float, float, EColor)"/>
        public AnimCurveAttribute(EColor curveColor)
        {
            _maxTime = 0;
            _maxValue = 0;
            _curveColor = curveColor.ToColor();
        }

        /// <summary>
        /// Creates an <see cref="AnimCurveAttribute"/> instance with time clamped between 0 and the given max time, and value clamped
        /// between 0 and given max value.
        /// </summary>
        /// <inheritdoc cref="AnimCurveAttribute(float, float, float, float, EColor)"/>
        public AnimCurveAttribute(float maxTime, float maxValue, EColor curveColor = EColor.Green)
        {
            // Ensure given values are positive
            _maxTime = maxTime > 0 ? maxTime : 1f;
            _maxValue = maxValue > 0 ? maxValue : 1f;
            _curveColor = curveColor.ToColor();
        }

        /// <summary>
        /// Creates an <see cref="AnimCurveAttribute"/> instance with time clamped between the given min and max time, and value clamped
        /// between the min and max value.
        /// </summary>
        /// <param name="minTime">The minimum time (along the X axis).</param>
        /// <param name="maxTime">The maximum time (along the X axis).</param>
        /// <param name="minValue">The minimum value (along the Y axis).</param>
        /// <param name="maxValue">The maximum value (along the Y axis).</param>
        /// <param name="curveColor">The color of the curve in the editor.</param>
        public AnimCurveAttribute(float minTime, float maxTime, float minValue, float maxValue, EColor curveColor = EColor.Green)
        {
            _minTime = minTime;
            _maxTime = maxTime > _minTime ? maxTime : _minTime + DEFAULT_OFFSET;

            _minValue = minValue;
            _maxValue = maxValue > _minValue ? maxValue : _minValue + DEFAULT_OFFSET;

            _curveColor = curveColor.ToColor();
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_minTime"/>
        public float MinTime => _minTime;

        /// <inheritdoc cref="_maxTime"/>
        public float MaxTime => _maxTime;

        /// <inheritdoc cref="_minValue"/>
        public float MinValue => _minValue;

        /// <inheritdoc cref="_maxValue"/>
        public float MaxValue => _maxValue;

        /// <inheritdoc cref="_curveColor"/>
        public Color CurveColor => _curveColor;

        /// <summary>
        /// Creates a <see cref="Rect"/> representing the curve's ranges:
        ///     - x is min time
        ///     - y is min value
        ///     - width is (abs(min time) + abs(max time)
        ///     - height is (abs(min value) + abs(max value)
        /// </summary>
        public Rect Ranges
        {
            get
            {
                return new Rect
                (
                    _minTime,
                    _minValue,
                    Mathf.Abs(_minTime) + Mathf.Abs(_maxTime),
                    Mathf.Abs(_minValue) + Mathf.Abs(_maxValue)
                );
            }
        }

        #endregion

    }

}