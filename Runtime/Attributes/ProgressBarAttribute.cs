/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Draws a progress bar next to a numeric field in the inspector.
    /// </summary>
    public class ProgressBarAttribute : PropertyAttribute
    {

        #region Fields

        public const EColor DEFAULT_COLOR = EColor.Cyan | EColor.Alpha100;

        /// <summary>
        /// The minimum value of the progress bar.
        /// </summary>
        private float _min = 0f;

        /// <summary>
        /// The maximum value of the progress bar.
        /// </summary>
        private float _max = 100f;

        /// <summary>
        /// The prefix to write before the value on the progress bar.
        /// </summary>
        private string _prefix = string.Empty;

        /// <summary>
        /// The suffix to write after the value on the progress bar.
        /// </summary>
        private string _suffix = string.Empty;

        /// <summary>
        /// The color of the progress bar.
        /// </summary>
        private Color _color = Color.blue;

        /// <summary>
        /// If enabled, the field's value will be clamped between min and max.
        /// </summary>
        private bool _clamp = false;

        /// <summary>
        /// If enabled, the field is completely hidden by the progress bar.
        /// </summary>
        private bool _replaceField = false;

        /// <summary>
        /// Should the field be readonly?
        /// </summary>
        private bool _readonly = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Displays a progress bar that represents a value from 0 to 1.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, 1, null, null, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value from 0 to 1.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, 1, null, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value from 0 to 1.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(string prefix, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, 1, prefix, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between minimum and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float min, float max, string prefix, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(min, max, prefix, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between minimum and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float min, float max, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(min, max, null, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between minimum and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float min, float max, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(min, max, null, null, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between 0 and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float max, string prefix, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, max, prefix, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between 0 and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float max, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, max, null, null, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Displays a progress bar that represents a value between 0 and maximum.
        /// </summary>
        /// <inheritdoc cref="Init(float, float, string, string, EColor, bool, bool, bool)"/>
        public ProgressBarAttribute(float max, string suffix, EColor color = DEFAULT_COLOR, bool clamp = false, bool replaceField = false, bool readonlyField = false)
        {
            Init(0, max, null, suffix, color, clamp, replaceField, readonlyField);
        }

        /// <summary>
        /// Initializes the values of this attribute.
        /// </summary>
        /// <param name="min">The minimum value of the progress bar.</param>
        /// <param name="max">The maximum value of the progress bar.</param>
        /// <param name="prefix">The prefix to write before the value on the progress bar.</param>
        /// <param name="suffix">The suffix to write after the value on the progress bar.</param>
        /// <param name="color">The color of the progress bar.</param>
        /// <param name="clamp">If enabled, the field's value will be clamped between min and max.</param>
        /// <param name="replaceField">If enabled, the field is completely hidden by the progress bar.</param>
        /// <param name="readonlyField">Should the field be readonly?</param>
        private void Init(float min, float max, string prefix, string suffix, EColor color, bool clamp, bool replaceField, bool readonlyField)
        {
            _min = Mathf.Min(min, max);
            _max = Mathf.Max(min, max);
            _prefix = !string.IsNullOrEmpty(prefix) ? prefix : string.Empty;
            _suffix = !string.IsNullOrEmpty(suffix) ? suffix : string.Empty;
            _color = color.ToColor(true);
            _clamp = clamp;
            _replaceField = replaceField;
            _readonly = readonlyField;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_min"/>
        public float Min => _min;

        /// <inheritdoc cref="_max"/>
        public float Max => _max;

        /// <inheritdoc cref="_prefix"/>
        public string Prefix => _prefix;

        /// <inheritdoc cref="_suffix"/>
        public string Suffix => _suffix;

        /// <inheritdoc cref="_color"/>
        public Color Color => _color;

        /// <inheritdoc cref="_clamp"/>
        public bool Clamp => _clamp;

        /// <inheritdoc cref="_replaceField"/>
        public bool ReplaceField => _replaceField;

        /// <inheritdoc cref="_readonly"/>
        public bool Readonly => _readonly;

        /// <summary>
        /// Gets the label of the progress bar, using the defined prefix and suffix.
        /// </summary>
        /// <param name="value">The current value of the field.</param>
        /// <returns>Returns the computed label.</returns>
        public string GetLabel(float value)
        {
            return $"{_prefix}{value}{_suffix}";
        }

        #endregion

    }

}