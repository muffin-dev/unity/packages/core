/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

namespace MuffinDev.Core
{

    /// <inheritdoc cref="EDataScope"/>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property)]
    public class DataScopeAttribute : Attribute
    {

        /// <summary>
        /// The scope of the data.
        /// </summary>
        private EDataScope _scope = EDataScope.Player;

        /// <param name="scope">The scope of the data.</param>
        /// <inheritdoc cref="DataScopeAttribute"/>
        public DataScopeAttribute(EDataScope scope)
        {
            _scope = scope;
        }

        public EDataScope Scope => _scope;

    }

}