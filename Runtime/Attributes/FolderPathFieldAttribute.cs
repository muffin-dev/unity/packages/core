/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    /// <summary>
    /// Draws a field to select a folder path, with a "Browse..." button next to it. 
    /// </summary>
    public class FolderPathFieldAttribute : PropertyAttribute
    {

        #region Fields

        /// <summary>
        /// The title of the "open folder" panel.
        /// </summary>
        private string _panelTitle = string.Empty;

        /// <summary>
        /// The key to use to save the path in paths history.
        /// </summary>
        private string _historyKey = string.Empty;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="FolderPathFieldAttribute(string, string)"/>
        public FolderPathFieldAttribute() { }

        /// <inheritdoc cref="FolderPathFieldAttribute(string, string)"/>
        public FolderPathFieldAttribute(string panelTitle)
        {
            _panelTitle = panelTitle;
        }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="panelTitle">The title of the "open folder" panel.</param>
        /// <param name="historyKey">The key to use to save the path in paths history.</param>
        public FolderPathFieldAttribute(string panelTitle, string historyKey)
        {
            _panelTitle = panelTitle;
            _historyKey = historyKey;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_panelTitle"/>
        public string PanelTitle => _panelTitle;

        /// <inheritdoc cref="_historyKey"/>
        public string HistoryKey => _historyKey;

        #endregion

    }

}