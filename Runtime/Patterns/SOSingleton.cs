/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core
{

    ///<summary>
    /// Implements the Singleton design pattern, adapted for <see cref="ScriptableObject"/>s.<br/>
    /// Note that the <see cref="UnityEditor.ScriptableSingleton{T}"/> already exists since Unity 2020, but has several limitations:<br/>
    ///     - Its inspector is disabled by default, so you must create a custom inspector if you want the properties to be edited<br/>
    ///     - You must save changes on disk manually<br/>
    ///     - Available only in the editor<br/>
    /// <see cref="SOSingleton{T}"/> is designed to resolve these limitations, which means that it can be use to create unique configs for
    /// both runtime and editor contexts, and even saved as a config file on disk.
    ///</summary>
    public abstract class SOSingleton<T> : ScriptableObject
        where T : SOSingleton<T>
    {

        #region Fields

        /// <summary>
        /// The singleton instance.
        /// </summary>
        private static T s_instance = null;

        /// <summary>
        /// Flag enabled the first time this asset is set as the singleton instance.
        /// </summary>
        private bool _initialized = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this asset is loaded.
        /// </summary>
        private void OnEnable()
        {
            if (s_instance == null)
                InternalSet();
        }

        /// <summary>
        /// Called when this asset is unloaded.
        /// </summary>
        private void OnDisable()
        {
            if (s_instance == this)
                InternalUnset();
        }

        #endregion


        #region Public API

        /// <summary>
        /// Gets the unique instance of this singleton.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (s_instance == null)
                {
#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        string[] guids = UnityEditor.AssetDatabase.FindAssets($"t:{typeof(T).Name}");
                        if (guids.Length > 0)
                        {
                            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]);
                            s_instance = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);
                        }
                    }
#endif

                    // Try to load asset from resources
                    if (s_instance == null)
                    {

                        T[] existingAssets = Resources.FindObjectsOfTypeAll<T>();
                        if (existingAssets.Length > 0)
                            s_instance = existingAssets[0];
                    }

                    // If no aset of this singleton type has been loaded yet, create one
                    if (s_instance == null)
                        s_instance = CreateInstance<T>();

                    // Initializes the singleton instance if it's not yet
                    if (!s_instance._initialized)
                    {
                        s_instance._initialized = true;
                        s_instance.Set();
                    }
                }

                return s_instance;
            }
        }

        /// <inheritdoc cref="Instance"/>
        public static T I => Instance;

        #endregion


        #region Protected API

        /// <inheritdoc cref="InternalSet"/>
        protected virtual void Set()
        {

        }

        /// <inheritdoc cref="InternalUnset"/>
        protected virtual void Unset()
        {

        }

        #endregion


        #region Private API

        /// <summary>
        /// Defines if this singleton asset is loaded and saved automatically. Note that <see cref="SaveAttribute"/> can be used on this
        /// class to define a file path or a scope.
        /// </summary>
        private bool AutoSave => GetType().HasAttribute<SaveAttribute>();

        /// <summary>
        /// Called when this asset is set as the singleton instance.
        /// </summary>
        private void InternalSet()
        {
            s_instance = this as T;
            if (!_initialized)
            {
                _initialized = true;
                
                if (AutoSave)
                {
                    SaveUtility.Load(this);
                    SaveUtility.Register(this);
                }

                Set();
            }
        }

        /// <summary>
        /// Called when this asset is unloaded but was the singleton instance.
        /// </summary>
        private void InternalUnset()
        {
            Unset();
            s_instance = null;

            if (AutoSave)
            {
                SaveUtility.Save(this);
                SaveUtility.Unregister(this);
            }
        }

        #endregion

    }

}