/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous functions for drawing user interfaces in the editor.
    /// </summary>
    public static class MoreEditorGUI
    {

        #region Delegates

        /// <summary>
        /// Called when an item is added to a list.
        /// </summary>
        /// <typeparam name="T">The type of the list.</typeparam>
        /// <param name="list">The list drawn using
        /// <see cref="DrawList{T}(IList{T}, GUIContent, AddItemDelegate{T}, DrawItemDelegate{T})"/>.</param>
        public delegate void AddItemDelegate<T>(IList<T> list);

        /// <summary>
        /// Called when an item of the list is drawn on the GUI.
        /// </summary>
        /// <typeparam name="T">The type of the list.</typeparam>
        /// <param name="list">The list itself.</param>
        /// <param name="index">The index of the item being drawn on the GUI.</param>
        /// <returns>Returns true if the item should be removed, otherwise false.</returns>
        public delegate bool DrawItemDelegate<T>(IList<T> list, int index);

        /// <summary>
        /// Defines a behavior when the user has to select a path.
        /// </summary>
        /// <returns>Returns the selected path.</returns>
        private delegate string SelectPathDelegate();

        #endregion


        #region Fields

        public const float MIN_MAX_SLIDER_FLOAT_FIELD_WIDTH_RATIO = .2f;
        public const float MIN_MAX_SLIDER_FLOAT_FIELD_RESPONSIVE_WIDTH = 300f;
        public const float MIN_MAX_SLIDER_FLOAT_FIELD_MAX_WIDTH = 48f;

        public const float DEFAULT_SEPARATOR_SIZE = 2f;
        public static Color SeparatorColor => new Color(1, 1, 1, .4f);

        // Pagination

        /// <summary>
        /// Defines the width ratio for drawing pagination field's page index and total number of pages.
        /// </summary>
        private const float PAGINATION_MAIN_FIELD_WIDTH_RATIO = .5f;
        private const float PAGINATION_MAIN_FIELD_SEPARATOR_WIDTH = 6f;

        /// <summary>
        /// Defines the width ratio of the single-page change button, relative to the remaining space after drawing the main page field.
        /// </summary>
        private const float PAGINATION_BUTTON_WIDTH_RATIO = .6f;

        #endregion


        #region Min Max Fields

        /// <summary>
        /// Draws composite float fields that represents a minimum and a maximum.
        /// </summary>
        /// <param name="label">The label of the field to draw.</param>
        /// <param name="value">The current value of the field to draw.</param>
        /// <param name="minLimit">The minimum available value.</param>
        /// <param name="maxLimit">The maximum available value.</param>
        /// <returns>Returns the new value of the field.</returns>
        public static Vector2 MinMaxFloatFields(GUIContent label, Vector2 value, float minLimit = Mathf.NegativeInfinity, float maxLimit = Mathf.Infinity)
        {
            EditorGUILayout.LabelField(label);
            return MinMaxFloatFields(value, minLimit, maxLimit);
        }

        /// <inheritdoc cref="MinMaxFloatFields(GUIContent, Vector2, float, float)"/>
        public static Vector2 MinMaxFloatFields(string label, Vector2 value, float minLimit = Mathf.NegativeInfinity, float maxLimit = Mathf.Infinity)
        {
            return MinMaxFloatFields(new GUIContent(label), value, minLimit, maxLimit);
        }

        /// <inheritdoc cref="MinMaxFloatFields(GUIContent, Vector2, float, float)"/>
        public static Vector2 MinMaxFloatFields(Vector2 value, float minLimit = Mathf.NegativeInfinity, float maxLimit = Mathf.Infinity)
        {
            Vector2 valueCopy = value;
            using (new IndentedScope())
            {
                valueCopy.x = Mathf.Clamp(EditorGUILayout.FloatField("Min", valueCopy.x), minLimit, maxLimit);
                // If the minimum value changed
                if (valueCopy.x != value.x)
                {
                    // If the minimum value is more than the maximum, increase the maximum
                    if (valueCopy.x > valueCopy.y)
                    {
                        valueCopy.y = valueCopy.x;
                    }
                }

                valueCopy.y = Mathf.Clamp(EditorGUILayout.FloatField("Max", valueCopy.y), minLimit, maxLimit);
                // If the minimum value changed
                if (valueCopy.y != value.y)
                {
                    // If the maximum value is less than the minimum, decrease the minimum
                    if (valueCopy.y < valueCopy.x)
                    {
                        valueCopy.x = valueCopy.y;
                    }
                }
            }

            return valueCopy;
        }

        /// <summary>
        /// Displays a MinMaxSlider field, but with minimum and maximum values clearly displayed in float fields at the bounds of the
        /// slider.
        /// </summary>
        /// <param name="label">The label of the field to draw.</param>
        /// <param name="min">The current minimum value.</param>
        /// <param name="max">The current maximum value.</param>
        /// <param name="minLimit">The minimum limit of the slider to the left.</param>
        /// <param name="maxLimit">The maximum limit of the slider to the right.</param>
        /// <param name="multilines">If enabled, displays the min and max float fields under the slider.</param>
        public static void MinMaxSlider(GUIContent label, ref float min, ref float max, float minLimit, float maxLimit, bool multilines = false)
        {
            // If the user allow displaying the field on several lines, draw the min-max slider, then draw min and max values as float
            // field right below
            if (multilines)
            {
                EditorGUILayout.MinMaxSlider(label, ref min, ref max, minLimit, maxLimit);
                Vector2 value = MinMaxFloatFields(new Vector2(min, max), minLimit, maxLimit);
                min = value.x;
                max = value.y;
            }

            // Else, display the minimum value as a float field, the min-max slider, and then the maximum value as a float field on the
            // same line
            else
            {
                Rect controlRect = EditorGUILayout.GetControlRect(true);
                Rect rect = new Rect(controlRect);
                rect.width = EditorGUIUtility.labelWidth;
                EditorGUI.LabelField(rect, label);

                float floatFieldWidth = controlRect.width >= MIN_MAX_SLIDER_FLOAT_FIELD_RESPONSIVE_WIDTH
                    ? Mathf.Min(MIN_MAX_SLIDER_FLOAT_FIELD_MAX_WIDTH, (controlRect.width - EditorGUIUtility.labelWidth - MoreGUI.H_MARGIN * 3) * MIN_MAX_SLIDER_FLOAT_FIELD_WIDTH_RATIO)
                    : 0f;

                if (floatFieldWidth > 0f)
                {
                    rect.x += rect.width + MoreGUI.H_MARGIN;
                    rect.width = floatFieldWidth;
                    min = Mathf.Max(minLimit, EditorGUI.FloatField(rect, min, EditorStyles.textField.FontSize(9)));
                }

                rect.x += rect.width + MoreGUI.H_MARGIN;
                rect.width = controlRect.width - floatFieldWidth * 2 - EditorGUIUtility.labelWidth - MoreGUI.H_MARGIN * 3;
                EditorGUI.MinMaxSlider(rect, ref min, ref max, minLimit, maxLimit);

                if (floatFieldWidth > 0f)
                {
                    rect.x += rect.width + MoreGUI.H_MARGIN;
                    rect.width = floatFieldWidth;
                    max = Mathf.Min(maxLimit, EditorGUI.FloatField(rect, max, EditorStyles.textField.FontSize(9)));
                }
            }
        }

        /// <inheritdoc cref="MinMaxSlider(GUIContent, ref float, ref float, float, float, bool)"/>
        public static void MinMaxSlider(string label, ref float min, ref float max, float minLimit, float maxLimit, bool multilines = false)
        {
            MinMaxSlider(new GUIContent(label), ref min, ref max, minLimit, maxLimit, multilines);
        }

        #endregion


        #region Paths Fields

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(string path, string panelTitle)
        {
            return FolderPathField(new GUIContent(), path, panelTitle, null);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(string path, string panelTitle, string historyKey)
        {
            return FolderPathField(new GUIContent(), path, panelTitle, historyKey);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(string label, string path, string panelTitle, string historyKey)
        {
            return FolderPathField(new GUIContent(label), path, panelTitle, historyKey);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(GUIContent label, string path, string panelTitle)
        {
            return FolderPathField(label, path, panelTitle, null);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(GUIContent label, string path, string panelTitle, string historyKey)
        {
            Rect position = EditorGUILayout.GetControlRect();
            return FolderPathField(position, label, path, panelTitle, historyKey);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(Rect position, string path, string panelTitle)
        {
            return FolderPathField(position, new GUIContent(), path, panelTitle, null);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(Rect position, string path, string panelTitle, string historyKey)
        {
            return FolderPathField(position, new GUIContent(), path, panelTitle, historyKey);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(Rect position, string label, string path, string panelTitle, string historyKey)
        {
            return FolderPathField(position, new GUIContent(label), path, panelTitle, historyKey);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)"/>
        public static string FolderPathField(Rect position, GUIContent label, string path, string panelTitle)
        {
            return FolderPathField(position, label, path, panelTitle, null);
        }

        /// <summary>
        /// Draws a text field with a "Browse..." button to select a path to a folder. Also draws a "drop zone" that allow you to drag and
        /// drop a folder from the project view to extract its path.
        /// </summary>
        /// <inheritdoc cref="PathField(Rect, GUIContent, string, string, SelectPathDelegate)"/>
        /// <param name="panelTitle">The title of the open folder panel.</param>
        public static string FolderPathField(Rect position, GUIContent label, string path, string panelTitle, string historyKey)
        {
            string previousPath = path;
            path = PathField(position, label, path, () => PathEditorUtility.OpenFolderPanel(historyKey, panelTitle, false));

            // Draw a drop zone that accept only folders
            DropZone.Draw<Object>(position, obj =>
            {
                path = AssetDatabase.GetAssetPath(obj[0]);
                return true;
            }, obj =>
            {
                string tmpPath = AssetDatabase.GetAssetPath(obj);
                return AssetDatabase.IsValidFolder(tmpPath);
            });

            if (path != previousPath)
                PathsHistory.AddPath(historyKey, path);

            return path;
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(string path, string panelTitle)
        {
            return FilePathField(new GUIContent(), path, panelTitle, string.Empty, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(string path, string panelTitle, string extension)
        {
            return FilePathField(new GUIContent(), path, panelTitle, extension, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(string path, string panelTitle, string extension, string historyKey)
        {
            return FilePathField(new GUIContent(), path, panelTitle, extension, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(string label, string path, string panelTitle, string extension, string historyKey)
        {
            return FilePathField(new GUIContent(label), path, panelTitle, extension, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(GUIContent label, string path, string panelTitle)
        {
            return FilePathField(label, path, panelTitle, string.Empty, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(GUIContent label, string path, string panelTitle, string extension)
        {
            return FilePathField(label, path, panelTitle, extension, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(GUIContent label, string path, string panelTitle, string extension, string historyKey)
        {
            Rect position = EditorGUILayout.GetControlRect();
            return FilePathField(position, label, path, panelTitle, extension, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(Rect position, string path, string panelTitle)
        {
            return FilePathField(position, new GUIContent(), path, panelTitle, string.Empty, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(Rect position, string path, string panelTitle, string extension)
        {
            return FilePathField(position, new GUIContent(), path, panelTitle, extension, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(Rect position, string path, string panelTitle, string extension, string historyKey)
        {
            return FilePathField(position, new GUIContent(), path, panelTitle, extension, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(Rect position, string label, string path, string panelTitle, string extension, string historyKey)
        {
            return FilePathField(position, new GUIContent(label), path, panelTitle, extension, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle)
        {
            return FilePathField(position, label, path, panelTitle, string.Empty, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string, string)"/>
        public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string extension)
        {
            return FilePathField(position, label, path, panelTitle, extension, null);
        }

        /// <summary>
        /// Draws a text field with a "Browse..." button to select a path to a file. Also draws a "drop zone" that allow you to drag and
        /// drop a file from the project view to extract its path.
        /// </summary>
        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)" />
        /// <param name="extension">The extension of the file you want to seleect. If null or empty, files are not filtered.</param>
        public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string extension, string historyKey)
        {
            string previousPath = path;
            path = PathField(position, label, path, () => PathEditorUtility.OpenFilePanel(historyKey, panelTitle, extension, false));

            // Draw a drop zone that accept the given extension (if applicable)
            DropZone.Draw<Object>(position, obj =>
            {
                path = AssetDatabase.GetAssetPath(obj[0]);
                return true;
            }, obj =>
            {
                // Always allow drag and drop if no specific extension is expected
                if (string.IsNullOrEmpty(extension))
                    return true;

                // Else, allow drag and drop only if the object has the expected extension
                string tmpPath = AssetDatabase.GetAssetPath(obj);
                return tmpPath.EndsWith($".{extension}");
            });

            if (path != previousPath)
                PathsHistory.AddPath(historyKey, path);

            return path;
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(string path, string panelTitle, string[] filters)
        {
            return FilePathField(new GUIContent(), path, panelTitle, filters, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(string path, string panelTitle, string[] filters, string historyKey)
        {
            return FilePathField(new GUIContent(), path, panelTitle, filters, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(string label, string path, string panelTitle, string[] filters, string historyKey)
        {
            return FilePathField(new GUIContent(label), path, panelTitle, filters, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(GUIContent label, string path, string panelTitle, string[] filters)
        {
            return FilePathField(label, path, panelTitle, filters, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(GUIContent label, string path, string panelTitle, string[] filters, string historyKey)
        {
            Rect position = EditorGUILayout.GetControlRect();
            return FilePathField(position, label, path, panelTitle, filters, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(Rect position, string path, string panelTitle, string[] filters)
        {
            return FilePathField(position, new GUIContent(), path, panelTitle, filters, null);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(Rect position, string path, string panelTitle, string[] filters, string historyKey)
        {
            return FilePathField(position, new GUIContent(), path, panelTitle, filters, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(Rect position, string label, string path, string panelTitle, string[] filters, string historyKey)
        {
            return FilePathField(position, new GUIContent(label), path, panelTitle, filters, historyKey);
        }

        /// <inheritdoc cref="FilePathField(Rect, GUIContent, string, string, string[], string)"/>
        public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string[] filters)
        {
            return FilePathField(position, label, path, panelTitle, filters, null);
        }

        /// <inheritdoc cref="FolderPathField(Rect, GUIContent, string, string, string)" />
        /// <param name="filters">The accepted extensions of the file to select. Must follow this pattern: { "Image files", "png,jpg,jpeg",
        /// "All files", "*" }</param>
        public static string FilePathField(Rect position, GUIContent label, string path, string panelTitle, string[] filters, string historyKey)
        {
            string previousPath = path;
            path = PathField(position, label, path, () => PathEditorUtility.OpenFilePanel(historyKey, panelTitle, filters, false));

            // Get allowed extensions
            List<string> extensions = new List<string>();
            for (int i = 1; i < filters.Length; i += 2)
                extensions.AddRange(filters[i].Split(","));

            // Cancel if all extensions are allowed
            if (extensions.Contains("*"))
                return path;

            // Draw a drop zone that accept only the allowed extensions from filters
            DropZone.Draw<Object>(position, obj =>
            {
                path = AssetDatabase.GetAssetPath(obj[0]);
                return true;
            }, obj =>
            {
                string tmpPath = AssetDatabase.GetAssetPath(obj);
                // For each allowed extensions
                foreach (string ext in extensions)
                {
                    // Stop if the object's file path contains one of the allowed extensions
                    if (tmpPath.EndsWith($".{ext}"))
                        return true;
                }
                return false;
            });

            if (path != previousPath)
                PathsHistory.AddPath(historyKey, path);

            return path;
        }

        /// <summary>
        /// Draws a text field with a "Browse..." button to select a path.
        /// </summary>
        /// <param name="position">The available space for drawing the field.</param>
        /// <param name="label">The label of the field. If null, the label is omitted.</param>
        /// <param name="path">The currently selected path.</param>
        /// <param name="historyKey">The key string used to get/set the default path from the <see cref="PathsHistory"/>.</param>
        /// <param name="onBrowse">Called when the user clicks on "Browse..." button.</param>
        /// <returns>Returns the new selected path.</returns>
        private static string PathField(Rect position, GUIContent label, string path, SelectPathDelegate onBrowse)
        {
            string newPath = path;

            Rect rect = new Rect(position);
            // Label
            if (label != null && !string.IsNullOrEmpty(label.text))
            {
                rect.width = EditorGUIUtility.labelWidth;
                EditorGUI.LabelField(rect, label);

                position.width -= rect.width + MoreGUI.H_MARGIN;
                position.x += rect.width + MoreGUI.H_MARGIN;
                rect = position;
            }

            // Text field
            rect.width = position.width - MoreGUI.WIDTH_S - MoreGUI.H_MARGIN;
            newPath = EditorGUI.DelayedTextField(rect, newPath);

            // Browse button
            rect.x += rect.width + MoreGUI.H_MARGIN;
            rect.width = MoreGUI.WIDTH_S;
            if (GUI.Button(rect, "Browse..."))
            {
                // Losing the focus ensures that the field is updated as expected
                GUI.FocusControl(null);
                newPath = onBrowse();
                if (string.IsNullOrEmpty(newPath))
                    newPath = path;
            }

            return newPath;
        }

        #endregion


        #region Lists

        /// <inheritdoc cref="DrawList{T}(IList{T}, GUIContent, AddItemDelegate{T}, DrawItemDelegate{T})"/>
        public static void DrawList<T>(IList<T> list, GUIContent label, DrawItemDelegate<T> onDrawItem)
        {
            DrawList
            (
                list,
                label,
                l => l.Add(default),
                onDrawItem
            );
        }

        /// <summary>
        /// Draws a non-reorderable list of items, with "Add" control in the header.
        /// </summary>
        /// <typeparam name="T">The type of the items.</typeparam>
        /// <param name="list">The list itself.</param>
        /// <param name="label">The label of the list field.</param>
        /// <param name="onAdd">Called when the "Add" button is clicked.</param>
        /// <param name="onDrawItem">Called when an item is drawn on the GUI. Note that you must draw the "Remove" control yourself as
        /// needed in your GUI.</param>
        public static void DrawList<T>(IList<T> list, GUIContent label, AddItemDelegate<T> onAdd, DrawItemDelegate<T> onDrawItem)
        {
            // Draws the label and addd button
            using (new EditorGUILayout.HorizontalScope())
            {
                if (label == null)
                    label = new GUIContent();

                EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
                if (GUILayout.Button("Add", MoreGUI.WidthS))
                    onAdd?.Invoke(list);
            }

            using (new IndentedScope())
            {
                int indexToRemove = -1;
                // Draw the items list
                for (int i = 0; i < list.Count; i++)
                {
                    if (onDrawItem != null && onDrawItem.Invoke(list, i))
                        indexToRemove = i;
                }

                // Remove the selected item if applicable
                if (indexToRemove >= 0)
                    list.RemoveAt(indexToRemove);
            }
        }

        #endregion


        #region Others

        /// <summary>
        /// Draws an horizontal line.
        /// </summary>
        /// <param name="size">The height of the separator.</param>
        /// <param name="color">The color of the separator.</param>
        /// <param name="wide">If enabled, the separator will use the full view width. This is designed to draw a separator that doesn't
        /// use the margins in the inspector window.</param>
        public static void HorizontalSeparator(float size, Color color, bool wide = false)
        {
            Rect rect = EditorGUILayout.GetControlRect(false, size);

            if (wide && rect.width < EditorGUIUtility.currentViewWidth)
            {
                rect.x = 0;
                rect.width = EditorGUIUtility.currentViewWidth;
            }

            EditorGUI.DrawRect(rect, color);
        }

        /// <inheritdoc cref="HorizontalSeparator(float, Color, bool)"/>
        public static void HorizontalSeparator(float size, bool wide = false)
        {
            HorizontalSeparator(size, SeparatorColor, wide);
        }

        /// <inheritdoc cref="HorizontalSeparator(float, Color, bool)"/>
        public static void HorizontalSeparator(bool wide = false)
        {
            HorizontalSeparator(DEFAULT_SEPARATOR_SIZE, SeparatorColor, wide);
        }

        /// <summary>
        /// Draws a vertical line.
        /// </summary>
        /// <param name="size">The width of the separator.</param>
        /// <param name="color">The color of the separator.</param>
        public static void VerticalSeparator(float size, Color color)
        {
            Rect rect = EditorGUILayout.GetControlRect(false, GUILayout.Width(size));
            EditorGUI.DrawRect(rect, color);
        }

        /// <inheritdoc cref="VerticalSeparator(float, Color)"/>
        public static void VerticalSeparator(float size)
        {
            VerticalSeparator(size, SeparatorColor);
        }

        /// <inheritdoc cref="VerticalSeparator(float, Color)"/>
        public static void VerticalSeparator()
        {
            VerticalSeparator(DEFAULT_SEPARATOR_SIZE, SeparatorColor);
        }

        #endregion


        #region Pagination

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(ref Pagination pagination)
        {
            PaginationField(EditorGUILayout.GetControlRect(false), ref pagination);
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(string label, ref Pagination pagination)
        {
            PaginationField(EditorGUILayout.GetControlRect(false), label, ref pagination);
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(GUIContent label, ref Pagination pagination)
        {
            PaginationField(EditorGUILayout.GetControlRect(false), label, ref pagination);
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(Rect position, ref Pagination pagination)
        {
            float mainFieldWidth = position.width * PAGINATION_MAIN_FIELD_WIDTH_RATIO;
            float controlsAvailableWidth = (position.width - mainFieldWidth - MoreGUI.H_MARGIN * 2) / 2;
            float largeButtonWidth = controlsAvailableWidth * PAGINATION_BUTTON_WIDTH_RATIO;
            float smallButtonWidth = controlsAvailableWidth - largeButtonWidth - MoreGUI.H_MARGIN;

            Rect rect = new Rect(position);

            // Draw "previous" controls
            using (new EnabledScope(pagination.Page > 0))
            {
                rect.width = smallButtonWidth;
                if (GUI.Button(rect, "<<"))
                    pagination.Page = 0;

                rect.x += rect.width + MoreGUI.H_MARGIN;
                rect.width = largeButtonWidth;
                if (GUI.Button(rect, "<"))
                    pagination.Page--;
            }

            // Draw page index
            int indexPlus1 = pagination.Page + 1;
            rect.x += rect.width + MoreGUI.H_MARGIN;
            rect.width = mainFieldWidth / 2;
            indexPlus1 = EditorGUI.IntField(rect, indexPlus1, EditorStyles.textField.TextAlignment(TextAnchor.MiddleCenter));
            pagination.Page = indexPlus1 - 1;

            // Draw separator
            rect.x += rect.width;
            rect.width = PAGINATION_MAIN_FIELD_SEPARATOR_WIDTH;
            EditorGUI.LabelField(rect, "/");

            // Draw pages count
            rect.x += rect.width + MoreGUI.H_MARGIN;
            rect.width = mainFieldWidth / 2 - MoreGUI.H_MARGIN - PAGINATION_MAIN_FIELD_SEPARATOR_WIDTH;
            EditorGUI.LabelField(rect, pagination.PagesCount.ToString(), EditorStyles.label.TextAlignment(TextAnchor.MiddleCenter));

            rect.x += rect.width + MoreGUI.H_MARGIN;
            // Draw "next" controls
            using (new EnabledScope(pagination.Page < pagination.PagesCount - 1))
            {
                rect.width = largeButtonWidth;
                if (GUI.Button(rect, ">"))
                    pagination.Page++;

                rect.x += rect.width + MoreGUI.H_MARGIN;
                rect.width = smallButtonWidth;
                if (GUI.Button(rect, ">>"))
                    pagination.Page = pagination.PagesCount;
            }
        }

        /// <inheritdoc cref="PaginationField(Rect, GUIContent, ref Pagination)"/>
        public static void PaginationField(Rect position, string label, ref Pagination pagination)
        {
            PaginationField(position, new GUIContent(label), ref pagination);
        }

        /// <summary>
        /// Draws a field to edit the given pagination value.
        /// </summary>
        /// <param name="label">The label of the field.</param>
        /// <param name="position">The position and size of the field.</param>
        /// <param name="pagination">The pagination value to edit.</param>
        public static void PaginationField(Rect position, GUIContent label, ref Pagination pagination)
        {
            Rect rect = new Rect(position);

            // Draw label
            if (label != null && !string.IsNullOrEmpty(label.text))
            {
                rect.width = EditorGUIUtility.labelWidth;
                EditorGUI.LabelField(rect, label);

                // Draw pagination field
                rect.x += rect.width + MoreGUI.H_MARGIN;
                rect.width -= rect.width + MoreGUI.H_MARGIN;
            }

            // Draw pagination field
            PaginationField(rect, ref pagination);
        }

        #endregion

    }

}