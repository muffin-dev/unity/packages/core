/**
 * Muffin Dev (c) 2023
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Displays progress indicators from <see cref="Progress"/> utility.
    /// </summary>
    /// <inheritdoc cref="Progress"/>
    public class ProgressWindow : EditorWindow
    {

        #region Fields

        private const string WINDOW_TITLE = "Progress";
        private const string MENU_ITEM = EditorConstants.EDITOR_WINDOW_MENU + "/" + WINDOW_TITLE;

        private static Dictionary<Progress.Status, Texture> s_statusIcons = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this window is opened.
        /// </summary>
        private void OnEnable()
        {
            if (s_statusIcons == null)
            {
                s_statusIcons = new Dictionary<Progress.Status, Texture>
                {
                    { Progress.Status.Running, EditorGUIUtility.IconContent("console.infoicon").image },
                    { Progress.Status.Succeeded, EditorGUIUtility.IconContent("console.infoicon").image },
                    { Progress.Status.Failed, EditorGUIUtility.IconContent("console.erroricon").image },
                    { Progress.Status.Canceled, EditorGUIUtility.IconContent("console.warnicon").image },
                    { Progress.Status.Paused, EditorGUIUtility.IconContent("console.warnicon").image },
                };
            }

            Progress.OnUpdate += Repaint;
        }

        /// <summary>
        /// Called when this window is closed.
        /// </summary>
        private void OnDisable()
        {
            Progress.OnUpdate -= Repaint;
        }

        #endregion


        #region Public API

        /// <summary>
        /// Opens this editor window.
        /// </summary>
        [MenuItem(MENU_ITEM)]
        public static ProgressWindow Open()
        {
            ProgressWindow window = GetWindow<ProgressWindow>(false, WINDOW_TITLE, true);
            window.Show();
            return window;
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this window GUI on screen.
        /// </summary>
        private void OnGUI()
        {
            using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar, GUILayout.ExpandWidth(true)))
            {
                if (GUILayout.Button("Clear Inactive", EditorStyles.toolbarButton, MoreGUI.WidthL))
                    Progress.ClearInactiveIndicators();
            }

            foreach (Progress.ProgressIndicator indicator in Progress.Indicators)
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    Rect iconRect = EditorGUILayout.GetControlRect(false, 32, GUILayout.Width(32));
                    GUI.DrawTexture(iconRect, s_statusIcons[indicator.status]);

                    using (new EditorGUILayout.VerticalScope())
                    {
                        using (new EditorGUILayout.HorizontalScope())
                        {
                            EditorGUILayout.LabelField(indicator.name, EditorStyles.boldLabel, MoreGUI.WidthL);
                            Rect progressBarRect = EditorGUILayout.GetControlRect(false, MoreGUI.HEIGHT_XS);
                            EditorGUI.ProgressBar(progressBarRect, Mathf.Clamp(indicator.progress, 0, 1), Mathf.FloorToInt(indicator.progress * 100f) + "%");
                        }

                        if (indicator.status == Progress.Status.Running)
                            EditorGUILayout.LabelField(indicator.description);
                        else
                            EditorGUILayout.LabelField(indicator.status.ToString());
                    }
                }
                
                MoreEditorGUI.HorizontalSeparator();
            }
        }

        #endregion

    }

}