/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Draws an horizontal split view that can be resized on GUI.
    /// </summary>
    [System.Serializable]
    public class HorizontalSplitView : SplitViewBase
    {

        #region Fields

        public const float RESIZE_CURSOR_WIDTH = 2f;
        public const float RESIZE_CURSOR_CONTROL_AREA_WIDTH = 8f;

        /// <summary>
        /// Caches the X position of the cursor at the moment the user started dragging it.
        /// </summary>
        private float _cursorAnchorX = 0f;

        /// <summary>
        /// Caches the size of the left side of this split view before the user started resizing it.
        /// </summary>
        private float _previousLeftSize = 0f;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="resizable">If enabled, this split view can be resized.</param>
        public HorizontalSplitView(bool resizable = true)
            : base(resizable) { }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="leftSideWidth">The width of the left side of this split view.</param>
        /// <param name="resizable">If enabled, the left side of this split view can be resized.</param>
        public HorizontalSplitView(float leftSideWidth,  bool resizable = true)
            : base(leftSideWidth, resizable) { }

        #endregion


        #region Public API

        /// <summary>
        /// Draws this split view on GUI.<br/>
        /// Note that it must be drawn in a vertical scope to avoid unexpected layout.
        /// </summary>
        /// <param name="onDrawLeftSide">Called when the left side of this split view is drawn.</param>
        /// <param name="onDrawRightSide">Called when the right side of this split view is drawn.</param>
        /// <param name="onResize">Called when this split view is resized.</param>
        /// <param name="noScrollView">By default, each side is wrapped in a scroll view. If this option is enabled, the scroll views are
        /// not drawn, so you must draw it and manage the scroll positions yourself if needed.</param>
        public void DoLayout(Action onDrawLeftSide, Action onDrawRightSide, Action onResize, bool noScrollView = false)
        {
            Rect controlRect = EditorGUILayout.GetControlRect(false, 1f, GUILayout.ExpandWidth(true));

            using (new EditorGUILayout.HorizontalScope())
            {
                // Draw left side
                if (noScrollView)
                {
                    using (new EditorGUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.Width(Size)))
                    {
                        onDrawLeftSide?.Invoke();
                    }
                }
                else
                {
                    FirstSideScrollPosition = EditorGUILayout.BeginScrollView(FirstSideScrollPosition, GUILayout.Width(Size));
                    onDrawLeftSide?.Invoke();
                    EditorGUILayout.EndScrollView();
                }

                // Draw cursor
                Rect rect = EditorGUILayout.GetControlRect(false, GUILayout.Width(RESIZE_CURSOR_WIDTH), GUILayout.ExpandHeight(true));
                Color cursorColor = CursorColor;
                if (!Resizable)
                {
                    cursorColor.a /= 2;
                }
                EditorGUI.DrawRect(rect, cursorColor);
                if (Resizable)
                {
                    rect.x -= (RESIZE_CURSOR_CONTROL_AREA_WIDTH - rect.width) / 2;
                    rect.width = RESIZE_CURSOR_CONTROL_AREA_WIDTH;
                    if (HandleResizeEvents(rect, controlRect.width, MouseCursor.SplitResizeLeftRight))
                    {
                        onResize?.Invoke();
                    }
                }

                // Draw right side
                if (noScrollView)
                {
                    using (new EditorGUILayout.VerticalScope(GUILayout.ExpandHeight(true)))
                    {
                        onDrawRightSide?.Invoke();
                    }
                }
                else
                {
                    SecondSideScrollPosition = EditorGUILayout.BeginScrollView(SecondSideScrollPosition);
                    onDrawRightSide?.Invoke();
                    EditorGUILayout.EndScrollView();
                }
            }
        }

        /// <inheritdoc cref="DoLayout(Action, Action, Action, bool)"/>
        public void DoLayout(Action onDrawLeftSide, Action onDrawRightSide, bool noScrollView = false)
        {
            DoLayout(onDrawLeftSide, onDrawRightSide, null, noScrollView);
        }

        #endregion


        #region Protected API

        /// <inheritdoc cref="SplitViewBase.BeginResize"/>
        protected override void BeginResize()
        {
            _cursorAnchorX = Event.current.mousePosition.x;
            _previousLeftSize = Size;
        }

        /// <summary>
        /// Applies the resize operation.
        /// </summary>
        /// <param name="availableWidth">The available space for resizing the split view.</param>
        protected override void ApplyResize(float availableWidth)
        {
            Size = Mathf.Clamp(_previousLeftSize + (Event.current.mousePosition.x - _cursorAnchorX), MIN_SIZE, availableWidth - MIN_SIZE);
        }

        #endregion

    }

}