/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Draws a vertical split view that can be resized on GUI.
    /// </summary>
    [System.Serializable]
    public class VerticalSplitView : SplitViewBase
    {

        #region Fields

        public const float RESIZE_CURSOR_HEIGHT = 2f;
        public const float RESIZE_CURSOR_CONTROL_AREA_HEIGHT = 8f;

        /// <summary>
        /// Caches the Y position of the cursor at the moment the user started dragging it.
        /// </summary>
        private float _cursorAnchorY = 0f;

        /// <summary>
        /// Caches the size of the top side of this split view before the user started resizing it.
        /// </summary>
        private float _previousTopSize = 0f;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="resizable">If enabled, this split view can be resized.</param>
        public VerticalSplitView(bool resizable = true)
            : base(resizable) { }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="topSideheight">The height of the top side of this split view.</param>
        /// <param name="resizable">If enabled, this split view can be resized.</param>
        public VerticalSplitView(float topSideheight,  bool resizable = true)
            : base(topSideheight, resizable) { }

        #endregion


        #region Public API

        /// <summary>
        /// Draws this split view on GUI.
        /// </summary>
        /// <param name="onDrawTopSide">Called when the top side of this split view is drawn.</param>
        /// <param name="onDrawBottomSide">Called when the bottom side of this split view is drawn.</param>
        /// <param name="onResize">Called when this split view is resized.</param>
        /// <param name="noScrollView">By default, each side is wrapped in a scroll view. If this option is enabled, the scroll views are
        /// not drawn, so you must draw it and manage the scroll positions yourself if needed.</param>
        public void DoLayout(Action onDrawTopSide, Action onDrawBottomSide, Action onResize, bool noScrollView = false)
        {
            Rect controlRect = EditorGUILayout.GetControlRect(false, GUILayout.ExpandHeight(true), GUILayout.Width(1f));

            using (new EditorGUILayout.VerticalScope())
            {
                // Draw top side
                if (noScrollView)
                {
                    using (new EditorGUILayout.VerticalScope(GUILayout.ExpandWidth(true), GUILayout.Height(Size)))
                    {
                        onDrawTopSide?.Invoke();
                    }
                }
                else
                {
                    FirstSideScrollPosition = EditorGUILayout.BeginScrollView(FirstSideScrollPosition, GUILayout.Height(Size));
                    onDrawTopSide?.Invoke();
                    EditorGUILayout.EndScrollView();
                }

                // Draw cursor
                Rect rect = EditorGUILayout.GetControlRect(false, RESIZE_CURSOR_HEIGHT, GUILayout.ExpandWidth(true));
                Color cursorColor = CursorColor;
                if (!Resizable)
                {
                    cursorColor.a /= 2;
                }
                EditorGUI.DrawRect(rect, cursorColor);
                if (Resizable)
                {
                    rect.y -= (RESIZE_CURSOR_CONTROL_AREA_HEIGHT - rect.height) / 2;
                    rect.height = RESIZE_CURSOR_CONTROL_AREA_HEIGHT;
                    if (HandleResizeEvents(rect, controlRect.height, MouseCursor.SplitResizeUpDown))
                    {
                        onResize?.Invoke();
                    }
                }

                // Draw bottom side
                if (noScrollView)
                {
                    using (new EditorGUILayout.VerticalScope(GUILayout.ExpandWidth(true)))
                    {
                        onDrawBottomSide?.Invoke();
                    }
                }
                else
                {
                    SecondSideScrollPosition = EditorGUILayout.BeginScrollView(SecondSideScrollPosition);
                    onDrawBottomSide?.Invoke();
                    EditorGUILayout.EndScrollView();
                }
            }
        }

        /// <inheritdoc cref="DoLayout(Action, Action, Action, bool)"/>
        public void DoLayout(Action onDrawTopSide, Action onDrawBottomSide, bool noScrollView = false)
        {
            DoLayout(onDrawTopSide, onDrawBottomSide, null, noScrollView);
        }

        #endregion


        #region Protected API

        /// <inheritdoc cref="SplitViewBase.BeginResize"/>
        protected override void BeginResize()
        {
            _cursorAnchorY = Event.current.mousePosition.y;
            _previousTopSize = Size;
        }

        /// <summary>
        /// Applies the resize operation.
        /// </summary>
        /// <param name="availableHeight">The available space for resizing the split view.</param>
        protected override void ApplyResize(float availableHeight)
        {
            Size = Mathf.Clamp(_previousTopSize + (Event.current.mousePosition.y - _cursorAnchorY), MIN_SIZE, availableHeight - MIN_SIZE);
        }

        #endregion

    }

}