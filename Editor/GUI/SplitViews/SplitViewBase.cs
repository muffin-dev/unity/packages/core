/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Base class for drawing resizable split views on GUI.
    /// </summary>
    [System.Serializable]
    public abstract class SplitViewBase
    {

        #region Delegates

        /// <summary>
        /// Called when this split view is resized.
        /// </summary>
        /// <param name="size">The new size of the first side of this split view.</param>
        public delegate void ResizeDelegate(float size);

        #endregion


        #region Fields

        public const float DEFAULT_SIZE = 180f;
        public const float MIN_SIZE = 60f;

        /// <inheritdoc cref="ResizeDelegate"/>
        public event ResizeDelegate OnResize;

        [SerializeField]
        [Tooltip("The current size of the first side of this split view.")]
        private float _firstSideSize = DEFAULT_SIZE;

        [SerializeField]
        [Tooltip("Defines if this split view can be resized.")]
        private bool _resizable = true;

        [SerializeField]
        [Tooltip("The color of the vertical separator bewteen this split view sides.")]
        private Color _resizeCursorColor = new Color(1, 1, 1, .4f);

        [SerializeField]
        [Tooltip("The current scroll position of the first side of this split view.")]
        private Vector2 _firstSideScrollPosition = Vector2.zero;

        [SerializeField]
        [Tooltip("The current scroll position of the second side of this split view.")]
        private Vector2 _secondSideScrollPosition = Vector2.zero;

        /// <summary>
        /// Defines if the user is currently resizing the split view (holding mouse button).
        /// </summary>
        private bool _isResizing = false;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <inheritdoc cref="Init(float, bool)"/>
        public SplitViewBase(bool resizable = true)
        {
            Init(DEFAULT_SIZE, resizable);
        }

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <inheritdoc cref="Init(float, bool)"/>
        public SplitViewBase(float firstSideWidth, bool resizable = true)
        {
            Init(firstSideWidth, resizable);
        }

        /// <summary>
        /// Initializes this split view.
        /// </summary>
        /// <param name="firstSideWidth">The width of the first side of this split view.</param>
        /// <param name="resizable">If enabled, the first side of this split view can be resized.</param>
        private void Init(float firstSideWidth, bool resizable)
        {
            Size = firstSideWidth;
            Resizable = resizable;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_firstSideSize"/>
        public float Size
        {
            get => _firstSideSize;
            set
            {
                value = Mathf.Max(value, MIN_SIZE);
                if (_firstSideSize != value)
                {
                    _firstSideSize = value;
                    OnResize?.Invoke(_firstSideSize);
                }
            }
        }

        /// <inheritdoc cref="_resizable"/>
        public bool Resizable
        {
            get => _resizable;
            set => _resizable = value;
        }

        /// <inheritdoc cref="_resizeCursorColor"/>
        public Color CursorColor
        {
            get => _resizeCursorColor;
            set => _resizeCursorColor = value;
        }

        /// <inheritdoc cref="_firstSideScrollPosition"/>
        public Vector2 FirstSideScrollPosition
        {
            get => _firstSideScrollPosition;
            set => _firstSideScrollPosition = value;
        }

        /// <inheritdoc cref="_secondSideScrollPosition"/>
        public Vector2 SecondSideScrollPosition
        {
            get => _secondSideScrollPosition;
            set => _secondSideScrollPosition = value;
        }

        #endregion


        #region Protected API

        /// <summary>
        /// Handle mouse events for the resize cursor.
        /// </summary>
        /// <param name="cursorRect">The rect used for drawing the cursor.</param>
        /// <param name="availableSpace">The available space for resizing the split view.</param>
        /// <param name="cursor">The cursor to display when hovering the resize area.</param>
        /// <returns>Returns true if the split view has been resized, otherwise false.</returns>
        protected bool HandleResizeEvents(Rect cursorRect, float availableSpace, MouseCursor cursor)
        {
            EditorGUIUtility.AddCursorRect(cursorRect, cursor);

            if (Event.current.type == EventType.MouseUp)
            {
                if (_isResizing)
                {
                    ApplyResize(availableSpace);
                    EndResize();
                    _isResizing = false;
                    return true;
                }
                return false;
            }

            if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && cursorRect.Contains(Event.current.mousePosition))
            {
                if (!_isResizing)
                {
                    _isResizing = true;
                    BeginResize();
                }
            }

            if (_isResizing)
            {
                ApplyResize(availableSpace);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Called when the user starts dragging the cursor.
        /// </summary>
        protected virtual void BeginResize() { }

        /// <summary>
        /// Called each frame while the user is dragging the cursor. It updates the first side's size value.
        /// </summary>
        /// <param name="availableSpace">The available space for resizing the split view.</param>
        protected abstract void ApplyResize(float availableSpace);

        /// <summary>
        /// Called when the user stops dragging the cursor.
        /// </summary>
        protected virtual void EndResize() { }

        #endregion

    }

}