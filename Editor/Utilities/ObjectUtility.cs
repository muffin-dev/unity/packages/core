/**
 * Muffin Dev (c) 2023
 * Author: contact@muffindev.com
 */

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using Object = UnityEngine.Object;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous utility functions for working with <see cref="Object"/> instances and assets.
    /// </summary>
    public static class ObjectUtility
    {

        /// <summary>
        /// Gets the GUID string of the given <see cref="Object"/>.<br/>
        /// Note that GUIDs are assigned only to assets, not to scene objects. So if this <see cref="Object"/> is a scene object, this
        /// function will return null.
        /// </summary>
        /// <param name="obj">The <see cref="Object"/> whose GUID you want to get.</param>
        /// <returns>Returns the GUID string of this <see cref="Object"/>, or null if this <see cref="Object"/> is not an asset (meaning
        /// it's a scene object).</returns>
        public static string GetGUID(Object obj)
        {
            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(obj, out string guid, out long localId);
            return localId != 0 ? guid : null;
        }

        /// <summary>
        /// Checks if the given <see cref="Object"/> is an asset (and not a scene object).
        /// </summary>
        /// <param name="obj">The <see cref="Object"/> you want to check.</param>
        /// <returns>Returns true if the given <see cref="Object"/> is an asset, otherwise false.</returns>
        public static bool IsAsset(Object obj)
        {
            return GetGUID(obj) != null;
        }

        /// <summary>
        /// Gets the unique name of an asset to create in the same directory than the given one.
        /// </summary>
        /// <param name="asset">The asset you want to process.</param>
        /// <param name="expectedName">The name you want to set on the asset.</param>
        /// <returns>Returns the processed unique asset name.</returns>
        public static string GetUniqueAssetName(Object asset, string expectedName)
        {
            string path = AssetDatabase.GetAssetPath(asset);
            // Cancel if the given asset is not an asset
            if (string.IsNullOrEmpty(path))
                return null;

            string basePath = System.IO.Path.GetDirectoryName(path);
            string uniquePath = AssetDatabase.GenerateUniqueAssetPath(basePath + System.IO.Path.DirectorySeparatorChar + expectedName + System.IO.Path.GetExtension(path));
            return System.IO.Path.GetFileName(uniquePath.Substring(0, uniquePath.Length - System.IO.Path.GetExtension(path).Length));
        }

        /// <summary>
        /// Gets the relative path to the given asset from the root directory of the current Unity project.
        /// </summary>
        /// <param name="asset">The asset of which you want to get the path.</param>
        /// <returns>Returns the found relative asset path, or null if the given asset is not an asset.</returns>
        public static string GetAssetPath(Object asset)
        {
            string path = AssetDatabase.GetAssetPath(asset);
            // Cancel if the given object is not an asset (and so the path can't be found)
            if (string.IsNullOrEmpty(path))
                return null;
            return path;
        }

        /// <summary>
        /// Gets the absolute path to the given asset.
        /// </summary>
        /// <inheritdoc cref="GetAssetPath(Object)"/>
        /// <returns>Returns the found absolute asset path, or null if the asset is not an asset.</returns>
        public static string GetAbsoluteAssetPath(Object asset)
        {
            string path = GetAssetPath(asset);
            return !string.IsNullOrEmpty(path) ? path.AbsolutePath() : null;
        }

        /// <summary>
        /// Gets the first subasset of the given type in the given main asset.
        /// </summary>
        /// <param name="asset">The main asset of which you want to get the subasset.</param>
        /// <param name="type">The expected type of the subasset.</param>
        /// <param name="includeMainAsset">If enabled, if the main asset has the expected type, it's the one being returned.</param>
        /// <returns>Returns the found subasset with the expected type.</returns>
        public static Object FindSubassetOfType(Object asset, Type type, bool includeMainAsset = false)
        {
            Object[] subassets = FindAllSubassetsOfType(asset, type, includeMainAsset);
            return subassets[0];
        }

        /// <inheritdoc cref="FindSubassetOfType(Object, Type, bool)"/>
        /// <typeparam name="T">The expected type of the subasset.</typeparam>
        public static T FindSubassetOfType<T>(Object asset, bool includeMainAsset = false)
            where T : Object
        {
            return FindSubassetOfType(asset, typeof(T), includeMainAsset) as T;
        }

        /// <summary>
        /// Gets the subassets of the given type in the given main asset.
        /// </summary>
        /// <param name="asset">The main asset of which you want to get the subassets.</param>
        /// <param name="type">The expected type of the subassets.</param>
        /// <param name="includeMainAsset">If enabled, if the main asset has the expected type, it's included in the returned
        /// array.</param>
        /// <returns>Returns the found subassets with the expected type.</returns>
        public static Object[] FindAllSubassetsOfType(Object asset, Type type, bool includeMainAsset = false)
        {
            string path = AssetDatabase.GetAssetPath(asset);
            if (string.IsNullOrEmpty(path))
                return null;

            List<Object> subassets = new List<Object>();

            // For each sub object
            foreach (Object subObj in AssetDatabase.LoadAllAssetsAtPath(path))
            {
                // Skip if the current sub object is the main one
                if (subObj == asset && !includeMainAsset)
                    continue;

                // Add the asset to the list if it has the expected type
                if (type.IsAssignableFrom(subObj.GetType()))
                    subassets.Add(subObj);
            }

            return subassets.ToArray();
        }

        /// <inheritdoc cref="FindAllSubassetsOfType(Object, Type, bool)"/>
        /// <typeparam name="T">The expected type of the subassets.</typeparam>
        public static T[] FindAllSubassetsOfType<T>(Object asset, bool includeMainAsset = false)
            where T : Object
        {
            List<Object> assets = new List<Object>(FindAllSubassetsOfType(asset, typeof(T), includeMainAsset));
            return assets.ConvertAll(a => a as T).ToArray();
        }

        /// <summary>
        /// Find an asset of the given type in the project.
        /// </summary>
        /// <param name="type">The type of the asset you want to get.</param>
        /// <param name="querySubassets">If enabled, this function will also search in the assets attached to other ones.</param>
        /// <returns>Returns the found asset.</returns>
        public static Object FindAssetOfType(Type type, bool querySubassets = false)
        {
            Object[] assets = FindAllAssetsOfType(type, querySubassets);
            return assets.Length > 0 ? assets[0] : null;
        }

        /// <inheritdoc cref="FindAssetOfType(Type, bool)"/>
        /// <typeparam name="T">The type of the asset you want to get.</typeparam>
        public static T FindAssetOfType<T>(bool querySubassets = false)
            where T : Object
        {
            return FindAssetOfType(typeof(T), querySubassets) as T;
        }

        /// <summary>
        /// Find all assets of the given type in the project.
        /// </summary>
        /// <param name="type">The type of assets you want to find.</param>
        /// <param name="querySubassets">If enabled, this function will also search in the assets attached to other ones.</param>
        /// <returns>Returns the found assets.</returns>
        public static Object[] FindAllAssetsOfType(Type type, bool querySubassets = false)
        {
            string[] guids = AssetDatabase.FindAssets($"t:{type.Name}");
            List<Object> assets = new List<Object>();

            // For each found assets of the expected type
            foreach (string guid in guids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                Object mainAsset = AssetDatabase.LoadAssetAtPath<Object>(path);

                // If the query is extended to attached assets
                if (querySubassets)
                {
                    // For each sub object
                    foreach (Object subObj in AssetDatabase.LoadAllAssetsAtPath(path))
                    {
                        // Skip if the current sub object is the main one
                        if (subObj == mainAsset)
                            continue;

                        // Add the asset to the list if it has the expected type
                        if (type.IsAssignableFrom(subObj.GetType()))
                            assets.Add(subObj);
                    }
                }

                // Add the main asset to the list if it has the expected type
                // This check must be done because AssetDatabase.FindAssets() will also query assets that contains attached assets of the
                // expected type.
                if (type.IsAssignableFrom(mainAsset.GetType()))
                    assets.Add(mainAsset);
            }
            return assets.ToArray();
        }

        /// <inheritdoc cref="FindAllAssetsOfType(Type, bool)"/>
        /// <typeparam name="T">The type of asset you want to find.</typeparam>
        public static T[] FindAllAssetsOfType<T>(bool querySubassets = false)
            where T : Object
        {
            List<Object> assets = new List<Object>(FindAllAssetsOfType(typeof(T), querySubassets));
            return assets.ConvertAll(a => a as T).ToArray();
        }

    }

}