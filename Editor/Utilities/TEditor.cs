/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Helper for making a custom editor with "typed" targets.
    /// </summary>
    public abstract class TEditor<T> : Editor
        where T : Object
    {

#pragma warning disable IDE1006 // Naming Styles
        /// <summary>
        /// The object being inspected.
        /// </summary>
        public new T target => base.target as T;

        /// <summary>
        /// An array of all the object being inspected.
        /// </summary>
        public new T[] targets => base.targets as T[];
#pragma warning restore IDE1006 // Naming Styles

    }

}