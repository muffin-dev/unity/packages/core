/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Reflection;

using UnityEngine;
using UnityEditor;

using MuffinDev.Core.Reflection;

using Object = UnityEngine.Object;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous functions for working in the editor context.
    /// </summary>
    public static class EditorHelpers
    {

        #region Fields

        /// <summary>
        /// Caches the object to focus (using <see cref="FocusObject(Object, bool, bool)"/>).
        /// </summary>
        private static Object s_objectToFocus = null;

        /// <summary>
        /// Caches the object to select (using <see cref="FocusObject(Object, bool, bool)"/>).
        /// </summary>
        private static Object s_objectToSelect = null;

        #endregion


        #region Public API

        /// <summary>
        /// Highlights and select an object. If the object is an asset, focus the project window and highlight it from project view. If
        /// it's an object in a scene, highlight it from the hierarchy.
        /// </summary>
        /// <param name="obj">The object you want to focus.</param>
        /// <param name="pingObject">Should the object be highlighted in the Project view?</param>
        /// <param name="selectObject">Should the object be selected?</param>
        public static void FocusObject(Object obj, bool pingObject = true, bool selectObject = true)
        {
            /**
             * This function uses <see cref="EditorApplication.delayCall"/> to wait for all the active editor classes to update. This
             * prevent some exceptions to spawn in the console when the selection or highlight changes between two updates of the
             * inspectors.
             */

            if (pingObject)
            {
                s_objectToFocus = obj;
                EditorApplication.delayCall += PingObject;
            }

            if (selectObject)
            {
                s_objectToSelect = obj;
                EditorApplication.delayCall += SelectObject;
            }
        }

        /// <summary>
        /// Gets the <see cref="EditorWindow"/> instance of the named window.<br/>
        /// Note that this function will only find opened (or loaded) windows.
        /// </summary>
        /// <param name="windowTitle">The title of the window you want to find.</param>
        /// <returns>Returns the found window.</returns>
        public static EditorWindow FindEditorWindow(string windowTitle)
        {
            EditorWindow[] windows = Resources.FindObjectsOfTypeAll<EditorWindow>();
            foreach (EditorWindow w in windows)
            {
                if (w.titleContent.text == windowTitle)
                    return w;
            }
            return null;
        }

        /// <inheritdoc cref="ObjectUtility.FindSubassetOfType(Object, Type, bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindSubassetOfType))]
        public static Object FindSubassetOfType(Object asset, Type type, bool includeMainAsset = false)
        {
            return ObjectUtility.FindSubassetOfType(asset, type, includeMainAsset);
        }

        /// <inheritdoc cref="ObjectUtility.FindSubassetOfType{T}(Object, bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindSubassetOfType))]
        public static T FindSubassetOfType<T>(Object asset, bool includeMainAsset = false)
            where T : Object
        {
            return ObjectUtility.FindSubassetOfType<T>(asset, includeMainAsset);
        }

        /// <inheritdoc cref="ObjectUtility.FindAllSubassetsOfType(Object, Type, bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindAllSubassetsOfType))]
        public static Object[] FindAllSubassetsOfType(Object asset, Type type, bool includeMainAsset = false)
        {
            return ObjectUtility.FindAllSubassetsOfType(asset, type, includeMainAsset);
        }

        /// <inheritdoc cref="ObjectUtility.FindAllSubassetsOfType{T}(Object, bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindAllSubassetsOfType))]
        public static T[] FindAllSubassetsOfType<T>(Object asset, bool includeMainAsset = false)
            where T : Object
        {
            return ObjectUtility.FindAllSubassetsOfType<T>(asset, includeMainAsset);
        }

        /// <inheritdoc cref="ObjectUtility.FindAssetOfType(Type, bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindAssetOfType))]
        public static Object FindAssetOfType(Type type, bool querySubassets = false)
        {
            return ObjectUtility.FindAssetOfType(type, querySubassets);
        }

        /// <inheritdoc cref="ObjectUtility.FindAssetOfType{T}(bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindAssetOfType))]
        public static T FindAssetOfType<T>(bool querySubassets = false)
            where T : Object
        {
            return ObjectUtility.FindAssetOfType<T>(querySubassets);
        }

        /// <inheritdoc cref="ObjectUtility.FindAllAssetsOfType(Type, bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindAllAssetsOfType))]
        public static Object[] FindAllAssetsOfType(Type type, bool querySubassets = false)
        {
            return ObjectUtility.FindAllAssetsOfType(type, querySubassets);
        }

        /// <inheritdoc cref="ObjectUtility.FindAllAssetsOfType{T}(bool)"/>
        [Obsolete("Replaced by " + nameof(ObjectUtility.FindAllAssetsOfType))]
        public static T[] FindAllAssetsOfType<T>(bool querySubassets = false)
            where T : Object
        {
            return ObjectUtility.FindAllAssetsOfType<T>(querySubassets);
        }

        /// <inheritdoc cref="GetLabel(object, string, string, BindingFlags)"/>
        public static GUIContent GetLabel(object obj, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            return GetLabel(obj.GetType(), fieldOrPropertyName, null, bindingFlags);
        }

        /// <param name="obj">The object from which you want to get the data.</param>
        /// <inheritdoc cref="GetLabel(Type, string, string, BindingFlags)"/>
        public static GUIContent GetLabel(object obj, string fieldOrPropertyName, string customLabel, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            return GetLabel(obj.GetType(), fieldOrPropertyName, customLabel, bindingFlags);
        }

        /// <inheritdoc cref="GetLabel{T}(string, string, BindingFlags)"/>
        public static GUIContent GetLabel<T>(string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            return GetLabel(typeof(T), fieldOrPropertyName, null, bindingFlags);
        }

        /// <typeparam name="T">The type of the object from which you want to get the data.</typeparam>
        /// <inheritdoc cref="GetLabel(Type, string, string, BindingFlags)"/>
        public static GUIContent GetLabel<T>(string fieldOrPropertyName, string customLabel, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            return GetLabel(typeof(T), fieldOrPropertyName, customLabel, bindingFlags);
        }

        /// <inheritdoc cref="GetLabel(Type, string, string, BindingFlags)"/>
        public static GUIContent GetLabel(Type type, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            return GetLabel(type, fieldOrPropertyName, null, bindingFlags);
        }

        /// <summary>
        /// Creates a <see cref="GUIContent"/> value with the expected property's label and tooltip.
        /// </summary>
        /// <param name="type">The type of the object from which you want to get the data.</param>
        /// <param name="fieldOrPropertyName">The name of the field or the property you want to display.</param>
        /// <param name="customLabel">If defined, this text is used as is for the <see cref="GUIContent"/> label.</param>
        /// <param name="bindingFlags">Filters to use for finding the field or property. By default, targets only instance data, public and
        /// non-public.</param>
        /// <returns>Returns the processed <see cref="GUIContent"/> value.</returns>
        public static GUIContent GetLabel(Type type, string fieldOrPropertyName, string customLabel, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            string tooltip = GetTooltip(type, fieldOrPropertyName, bindingFlags);
            return new GUIContent
            {
                text = string.IsNullOrEmpty(customLabel) ? ObjectNames.NicifyVariableName(fieldOrPropertyName) : customLabel,
                tooltip = tooltip
            };
        }

        /// <param name="obj">The object of which you want to get the data.</param>
        /// <inheritdoc cref="GetTooltip(Type, string, BindingFlags)"/>
        public static string GetTooltip(object obj, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            return GetTooltip(obj.GetType(), fieldOrPropertyName, bindingFlags);
        }

        /// <typeparam name="T">The type of the object you want to get the data</typeparam>
        /// <inheritdoc cref="GetTooltip(Type, string, BindingFlags)"/>
        public static string GetTooltip<T>(string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            return GetTooltip(typeof(T), fieldOrPropertyName, bindingFlags);
        }

        /// <summary>
        /// Gets the Unity tooltip of the named field or property.
        /// </summary>
        /// <param name="type">The type of the object you want to get the data.</param>
        /// <param name="fieldOrPropertyName">The name of the field or property you want to get the tooltip. Note that this method will
        /// search for the named field first, then search for the property.</param>
        /// <param name="bindingFlags">Filters to use for finding the field or property. By default, targets only instance data, public and
        /// non-public.</param>
        /// <returns>Returs the found tooltip value, otherwise <see cref="string.Empty"/>.</returns>
        public static string GetTooltip(Type type, string fieldOrPropertyName, BindingFlags bindingFlags = ReflectionUtility.INSTANCE_FLAGS)
        {
            // Find a field with the given name, marked with the Tooltip attribute
            FieldInfo field = Array.Find(type.GetFields(bindingFlags), f => f.Name == fieldOrPropertyName && f.IsDefined(typeof(TooltipAttribute)));
            if (field != null)
                return field.GetCustomAttribute<TooltipAttribute>().tooltip;

            // Find a property with the given name, marked with the Tooltip attribute
            PropertyInfo property = Array.Find(type.GetProperties(bindingFlags), p => p.Name == fieldOrPropertyName && p.IsDefined(typeof(TooltipAttribute)));
            if (property != null)
                return property.GetCustomAttribute<TooltipAttribute>().tooltip;

            return string.Empty;
        }

        /// <summary>
        /// Tries to get the active folder path from the Project view if it's open.
        /// </summary>
        /// <param name="path">Outputs the relative path to the fuond active folder.</param>
        /// <returns>Returns true if a path has been found, otherwise false.</returns>
        public static bool TryGetActiveFolderPath(out string path)
        {
            MethodInfo tryGetActiveFolderPathFunc = typeof(ProjectWindowUtil).GetMethod("TryGetActiveFolderPath", BindingFlags.Static | BindingFlags.NonPublic);
            object[] args = new object[] { null };
            bool found = (bool)tryGetActiveFolderPathFunc.Invoke(null, args);
            path = (string)args[0];
            return found;
        }

        #endregion


        #region Private API

        /// <summary>
        /// Highlights an object. If the object is an asset, focus the project window and highlight it from project view. If it's an
        /// object in the scene, highlight it from the hierarchy.
        /// </summary>
        private static void PingObject()
        {
            if (s_objectToFocus == null)
                return;

            if (s_objectToFocus.IsAsset())
                EditorUtility.FocusProjectWindow();

            EditorGUIUtility.PingObject(s_objectToFocus);
            s_objectToFocus = null;
        }

        /// <summary>
        /// Selects the stored asset.
        /// </summary>
        private static void SelectObject()
        {
            Selection.activeObject = s_objectToSelect;
            s_objectToSelect = null;
        }

        #endregion

    }

}