/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous functions to display path selection panels using <see cref="PathsHistory"/>.
    /// </summary>
    [InitializeOnLoad]
    public static class PathEditorUtility
    {

        #region Delegates

        /// <summary>
        /// Defines a strategy for getting a path to a file or a folder.
        /// </summary>
        /// <param name="path">The path found in <see cref="PathsHistory"/>.</param>
        /// <returns>Returns the selected absolute path, or invelid (null or empty) string if the selection is canceled.</returns>
        private delegate string GetPathDelegate(string path);

        #endregion


        #region Fields

        /// <summary>
        /// Name of the /Assets directory.
        /// </summary>
        public const string ASSETS_DIR = "Assets";

        /// <summary>
        /// The default path used for opening "save/open file/folder" panels.
        /// </summary>
        private const string DEFAULT_PANEL_DIRECTORY = ASSETS_DIR;

        /// <summary>
        /// The absolute path to this project's /Assets directory.
        /// </summary>
        public static readonly string AssetsPath = null;

        /// <summary>
        /// The absolute path to this project.
        /// </summary>
        public static readonly string ProjectPath = null;

        /// <summary>
        /// The absolute path to the temporary data directory.
        /// </summary>
        public static readonly string TemporaryPath = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Static constructor.
        /// </summary>
        static PathEditorUtility()
        {
            AssetsPath = Application.dataPath;
            ProjectPath = PathUtility.ProjectPath;
            TemporaryPath = Application.temporaryCachePath;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="OpenFolderPanel(string, string, bool)"/>
        public static string OpenFolderPanel(bool forceInProject = false)
        {
            return OpenFolderPanel(null, null, forceInProject);
        }

        /// <inheritdoc cref="OpenFolderPanel(string, string, bool)"/>
        public static string OpenFolderPanel(string panelTitle, bool forceInProject = false)
        {
            return OpenFolderPanel(null, panelTitle, forceInProject);
        }

        /// <summary>
        /// Opens the "open folder" dialog window to select a path to a folder, and save it in <see cref="PathsHistory"/>.
        /// </summary>
        /// <param name="key">The key used to retrieve the previously selected path from <see cref="PathsHistory"/>.</param>
        /// <param name="panelTitle">The title of the dialog window.</param>
        /// <inheritdoc cref="ProcessPath(string, GetPathDelegate, bool)"/>
        public static string OpenFolderPanel(string key, string panelTitle, bool forceInProject = false)
        {
            return ProcessPath(key, p => EditorUtility.OpenFolderPanel(panelTitle, p, null), forceInProject);
        }

        /// <inheritdoc cref="OpenFilePanel(string, string, string, bool)"/>
        public static string OpenFilePanel(bool forceInProject = false)
        {
            return OpenFilePanel(null, null, extension: null, forceInProject);
        }

        /// <inheritdoc cref="OpenFilePanel(string, string, string, bool)"/>
        public static string OpenFilePanel(string extension, bool forceInProject = false)
        {
            return OpenFilePanel(null, null, extension, forceInProject);
        }

        /// <inheritdoc cref="OpenFilePanel(string, string, string, bool)"/>
        public static string OpenFilePanel(string panelTitle, string extension, bool forceInProject = false)
        {
            return OpenFilePanel(null, panelTitle, extension, forceInProject);
        }

        /// <summary>
        /// Opens the "open file" dialog window to select a path to a file, and save it in <see cref="PathsHistory"/>.
        /// </summary>
        /// <param name="extension">The expected extension of the selected file.</param>
        /// <inheritdoc cref="OpenFolderPanel(string, string, bool)"/>
        public static string OpenFilePanel(string key, string panelTitle, string extension, bool forceInProject = false)
        {
            return ProcessPath(key, p => EditorUtility.OpenFilePanel(panelTitle, p, extension), forceInProject);
        }

        /// <inheritdoc cref="OpenFilePanel(string, string, string[], bool)"/>
        public static string OpenFilePanel(string[] filters, bool forceInProject = false)
        {
            return OpenFilePanel(null, null, filters, forceInProject);
        }

        /// <inheritdoc cref="OpenFilePanel(string, string, string[], bool)"/>
        public static string OpenFilePanel(string panelTitle, string[] filters, bool forceInProject = false)
        {
            return OpenFilePanel(null, panelTitle, filters, forceInProject);
        }

        /// <param name="filters">The expected file extensions. This array must follow this format: { "Image files", "png,jpg,jpeg", "All
        /// files", "*" }</param>
        /// <inheritdoc cref="OpenFilePanel(string, string, string, bool)"/>
        public static string OpenFilePanel(string key, string panelTitle, string[] filters, bool forceInProject = false)
        {
            return ProcessPath(key, p => EditorUtility.OpenFilePanelWithFilters(panelTitle, p, filters), forceInProject);
        }

        /// <inheritdoc cref="SaveFolderPanel(string, string, string, bool)"/>
        public static string SaveFolderPanel(bool forceInProject = false)
        {
            return SaveFolderPanel(null, null, null, forceInProject);
        }

        /// <inheritdoc cref="SaveFolderPanel(string, string, string, bool)"/>
        public static string SaveFolderPanel(string panelTitle, bool forceInProject = false)
        {
            return SaveFolderPanel(null, panelTitle, null, forceInProject);
        }

        /// <inheritdoc cref="SaveFolderPanel(string, string, string, bool)"/>
        public static string SaveFolderPanel(string key, string panelTitle, bool forceInProject = false)
        {
            return SaveFolderPanel(key, panelTitle, null, forceInProject);
        }

        /// <summary>
        /// Opens the "save folder" dialog window to select a path to a folder, and save it in <see cref="PathsHistory"/>.
        /// </summary>
        /// <inheritdoc cref="OpenFolderPanel(string, string, bool)"/>
        public static string SaveFolderPanel(string key, string panelTitle, string defaultName, bool forceInProject = false)
        {
            return ProcessPath(key, p => EditorUtility.SaveFolderPanel(panelTitle, p, defaultName), forceInProject);
        }

        /// <inheritdoc cref="SaveFilePanel(string, string, string, string, bool)"/>
        public static string SaveFilePanel(bool forceInProject = false)
        {
            return SaveFilePanel(null, null, null, null, forceInProject);
        }

        /// <inheritdoc cref="SaveFilePanel(string, string, string, string, bool)"/>
        public static string SaveFilePanel(string extension, bool forceInProject = false)
        {
            return SaveFilePanel(null, null, extension, null, forceInProject);
        }

        /// <inheritdoc cref="SaveFilePanel(string, string, string, string, bool)"/>
        public static string SaveFilePanel(string panelTitle, string extension, bool forceInProject = false)
        {
            return SaveFilePanel(null, panelTitle, extension, null, forceInProject);
        }

        /// <inheritdoc cref="SaveFilePanel(string, string, string, string, bool)"/>
        public static string SaveFilePanel(string key, string panelTitle, string extension, bool forceInProject = false)
        {
            return SaveFilePanel(key, panelTitle, extension, null, forceInProject);
        }

        /// <summary>
        /// Opens the "save file" dialog window to select a path to a file, and save it in <see cref="PathsHistory"/>.
        /// </summary>
        /// <inheritdoc cref="OpenFilePanel(string, string, string, bool)"/>
        public static string SaveFilePanel(string key, string panelTitle, string extension, string defaultName, bool forceInProject = false)
        {
            return ProcessPath(key, p => EditorUtility.SaveFilePanel(panelTitle, p, defaultName, extension), forceInProject);
        }

        #endregion


        #region Private API

        /// <summary>
        /// Opens a panel to select a path, and save it in <see cref="PathsHistory"/>.
        /// </summary>
        /// <param name="key">The key used to save the selected path in <see cref="PathsHistory"/>.</param>
        /// <param name="getPathStrategy">Defines the strategy to use for selecting a path.</param>
        /// <param name="forceInProject">If enabled and the selected path leads outside of this project, the selection is canceled.</param>
        /// <returns>Returns the selected path, or an empty path if the selected is not valid (which means that the selection was
        /// canceled).</returns>
        private static string ProcessPath(string key, GetPathDelegate getPathStrategy, bool forceInProject)
        {
            string path = !string.IsNullOrEmpty(key)
                ? PathsHistory.GetPath(key, DEFAULT_PANEL_DIRECTORY)
                : string.Empty;

            // If the previously selected path leads outside of the project but we want to force the selection in the project, uses the
            // default directory instead
            if (forceInProject && !path.IsProjectPath())
                path = DEFAULT_PANEL_DIRECTORY;

            path = getPathStrategy?.Invoke(path).Path();

            // If the selected path is valid
            if (!string.IsNullOrEmpty(path))
            {
                // If the selected path 
                if (forceInProject && !path.IsProjectPath())
                    path = string.Empty;

                PathsHistory.AddPath(key, path);
            }

            return path;
        }

        #endregion

    }

}