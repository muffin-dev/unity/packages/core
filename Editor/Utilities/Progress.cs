/**
* Muffin Dev (c) 2023
* Author: contact@muffindev.com
*/

using System;
using System.Collections.Generic;

using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Utility class to store the progress of background tasks.
    /// </summary>
    /// <remarks>
    /// This custom implementation mimics the common usages of the Progress API available from Unity 2020.1, so you can get an equivalent
    /// behavior to display tasks progress in a similar way in previous versions.
    /// </remarks>
    [System.Serializable]
    [Save(EDataScope.Temp, true)]
    public class Progress : Singleton<Progress>
    {

        #region Enums & Subclasses

        /// <summary>
        /// Defines the status of an indicator.
        /// </summary>
        public enum Status
        {
            Running,
            Succeeded,
            Failed,
            Canceled,
            Paused
        }

        /// <summary>
        /// Defines how an indicator behaves.
        /// </summary>
        [Flags]
        public enum Options
        {
            /// <summary>
            /// A progress indicator that starts with no other options activated. This is the default.
            /// </summary>
            None = 0x0,

            /// <summary>
            /// A sticky progress indicator displays progress information even after the task is complete. The system does not remove it
            /// automatically. You must remove it using a remove operation.
            /// </summary>
            Sticky = 0x1,
            
            /// <summary>
            /// An indefinite progress indicator shows that the associated task is in progress, but does not show how close it is to
            /// completion.
            /// </summary>
            Indefinite = 0x2,

            /// <summary>
            /// A synchronous progress indicator updates the Editor UI as soon as it reports progress. This is the opposite of the default
            /// behavior, which is to report all progress asynchronously.
            /// </summary>
            Synchronous = 0x4,

            /// <summary>
            /// Unity manages progress indicators. When a domain reload happens, the system cancels tasks that support cancellation, and
            /// removes their progress indicators. is the default for progress indicators started from C#.
            /// </summary>
            /// <remarks>This value mimics the original Progress API behavior, but is not supported by now.</remarks>
            Managed = 0x8,

            /// <summary>
            /// An unmanaged progress indicator is one that Unity does not manage. Unity does not cancel unmanaged progress indicators when
            /// a domain reload happens. This is the default behavior for internal-use progress indicators started from C++ code.
            /// </summary>
            /// <remarks>This value mimics the original Progress API behavior, but is not supported by now.</remarks>
            Unmanaged = 0x10
        }

        /// <summary>
        /// Groups the informations and callbacks of a running task.
        /// </summary>
        [System.Serializable]
        internal class ProgressIndicator
        {
            public int id;
            public int parentId = -1;
            public string name;
            public string description;
            public Status status;
            public Options options;

            public float progress;

            public Func<bool> cancelCallback;

            public bool Active => status == Status.Running || status == Status.Paused;
        }

        #endregion


        #region Fields

        /// <summary>
        /// Called when the indicators are updated.
        /// </summary>
        internal static event Action OnUpdate;

        /// <summary>
        /// The last id generated for a progress indicator.
        /// </summary>
        private static int s_lastIndicatorId = 0;

        /// <summary>
        /// Informations about the running or completed tasks.
        /// </summary>
        [SerializeField]
        private List<ProgressIndicator> _indicators = new List<ProgressIndicator>();

        #endregion


        #region Public API

        /// <summary>
        /// Starts a new progress indicator.
        /// </summary>
        /// <param name="name">The name of the progress indicator.</param>
        /// <param name="description">The description of the progress indicator.</param>
        /// <param name="options">The indicator's options.</param>
        /// <param name="parentId">The unique id of the parent progress indicator. -1 means the progress indicator doesn't have any parent.
        /// This mimics the behavior of the original Progress API but is not supported.</param>
        /// <returns>Returnss the created progress indicator's id.</returns>
        public static int Start(string name, string description, Options options, int parentId = -1)
        {
            ProgressIndicator indicator = new ProgressIndicator
            {
                id = NextId,
                parentId = parentId,
                name = name,
                description = description,
                status = Status.Running,
                options = options,
            };
            I._indicators.Insert(0, indicator);

            OnUpdate?.Invoke();

            return indicator.id;
        }

        /// <inheritdoc cref="Report(int, float, string)"/>
        public static void Report(int id, float progress)
        {
            Report(id, progress, null);
        }

        /// <summary>
        /// Reports a running progress indicator's current status.
        /// </summary>
        /// <param name="id">The unique id of the indicator.</param>
        /// <param name="progress">The progress value, between 0 and 1.</param>
        /// <param name="description">The description of the indicator.</param>
        public static void Report(int id, float progress, string description)
        {
            ProgressIndicator indicator = I._indicators.Find(pi => pi.id == id);

            // Cancel if the indicator doesn't exist
            if (indicator == null)
                return;

            indicator.progress = progress;
            if (description != null)
                indicator.description = description;

            OnUpdate?.Invoke();
        }

        /// <summary>
        /// Checks if a progress indicator exists.
        /// </summary>
        /// <param name="id">The unique id of the progress indicator.</param>
        /// <returns>Returns true if the progress indicator exists, otherwise false.</returns>
        public static bool Exists(int id)
        {
            return I._indicators.Exists(pi =>  pi.id == id);
        }

        /// <summary>
        /// Marks a progress indicator as finished.
        /// </summary>
        /// <param name="id">The unique id of the progress indicator.</param>
        /// <param name="status">The new status of the indicator.</param>
        public static void Finish(int id, Status status)
        {
            ProgressIndicator indicator = I._indicators.Find(pi => pi.id == id);

            // Cancel if the indicator doesn't exist
            if (indicator == null)
                return;

            indicator.status = status;
            if (indicator.status == Status.Succeeded)
                indicator.progress = 1f;

            // Remove the indicator unless it's sticky
            if (!indicator.options.HasFlag(Options.Sticky))
                I._indicators.Remove(indicator);

            OnUpdate?.Invoke();
        }

        /// <summary>
        /// Registeres a callback that is called when the user cancels a running progress indicator.
        /// </summary>
        /// <param name="id">The unique id of the progress indicator.</param>
        /// <param name="callback">The function calledd when a progress indicator is canceled.</param>
        public static void RegisterCancelCallback(int id, Func<bool> callback)
        {
            ProgressIndicator indicator = I._indicators.Find(pi => pi.id == id);

            // Cancel if the indicator doesn't exist
            if (indicator == null)
                return;

            indicator.cancelCallback += callback;
        }

        /// <summary>
        /// Opens the progress window.
        /// </summary>
        public static void ShowDetails()
        {
            ProgressWindow.Open();
        }

        /// <summary>
        /// Removes the inactive indicators from the list.
        /// </summary>
        public static void ClearInactiveIndicators()
        {
            for (int i = I._indicators.Count - 1; i >= 0; i--)
            {
                if (!I._indicators[i].Active)
                    I._indicators.RemoveAt(i);
            }
        }

        #endregion


        #region Internal API

        /// <inheritdoc cref="_indicators"/>
        internal static ProgressIndicator[] Indicators => I._indicators.ToArray();

        #endregion


        #region Private API

        /// <summary>
        /// Gets a unique id for a progress indicator.
        /// </summary>
        private static int NextId
        {
            get
            {
                s_lastIndicatorId++;
                if (s_lastIndicatorId < 0)
                    s_lastIndicatorId = 0;

                return s_lastIndicatorId;
            }
        }

        #endregion

    }

}