/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Utility for creating inspectors from settings providers.
    /// </summary>
    public class SettingsProvidersUtility : SOSingleton<SettingsProvidersUtility>
    {

        #region Delegates

        /// <summary>
        /// Function call to draw the default inspector of an object.
        /// </summary>
        public delegate void DrawDefaultInspectorDelegate();

        /// <summary>
        /// Custom implementation of the GUI Handler for a Settings Provider.
        /// </summary>
        /// <param name="drawDefaultInspector">The original call, which draws the default inspector of the object.</param>
        /// <param name="obj">The object being edited in the Settings Provider.</param>
        /// <param name="search">The current search text.</param>
        public delegate void GUIHandlerDelegate(DrawDefaultInspectorDelegate drawDefaultInspector, Object obj, string search);

        #endregion


        #region Fields

        /// <summary>
        /// Caches the created editor used to provide settings from Preferences or Project Settings menus.
        /// </summary>
        private Dictionary<Object, Editor> _loadedEditors = new Dictionary<Object, Editor>();

        #endregion


        #region Public API

        /// <inheritdoc cref="GetSettingsMenu(Object, string, string[], SettingsScope, GUIHandlerDelegate)"/>
        public static SettingsProvider GetSettingsMenu(Object obj, SettingsScope scope)
        {
            return GetSettingsMenu(obj, null, null, scope);
        }

        /// <inheritdoc cref="GetSettingsMenu(Object, string, string[], SettingsScope, GUIHandlerDelegate)"/>
        public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, SettingsScope scope)
        {
            return GetSettingsMenu(obj, menuPath, null, scope);
        }

        /// <inheritdoc cref="GetSettingsMenu(Object, string, string[], SettingsScope, GUIHandlerDelegate)"/>
        public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, string[] keywords, SettingsScope scope)
        {
            return GetSettingsMenu(obj, menuPath, keywords, scope, (drawDefaultInspector, tmpObj, search) => drawDefaultInspector());
        }

        /// <inheritdoc cref="GetSettingsMenu(Object, string, string[], SettingsScope, GUIHandlerDelegate)"/>
        public static SettingsProvider GetSettingsMenu(Object obj, SettingsScope scope, GUIHandlerDelegate customGUIHandler)
        {
            return GetSettingsMenu(obj, null, null, scope, customGUIHandler);
        }

        /// <inheritdoc cref="GetSettingsMenu(Object, string, string[], SettingsScope, GUIHandlerDelegate)"/>
        public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, SettingsScope scope, GUIHandlerDelegate customGUIHandler)
        {
            return GetSettingsMenu(obj, menuPath, null, scope, customGUIHandler);
        }

        /// <summary>
        /// Gets the settings provider for the given object, displaying its default inspector.
        /// </summary>
        /// <param name="obj">The object for which you want to get a settings provider.</param>
        /// <param name="menuPath">The menu path to reach the settings inspector from Preferences or Project Settings tabs.</param>
        /// <param name="keywords">The keywords used to find the settings from the search bar.</param>
        /// <param name="scope">The scope of the settings inspector.</param>
        /// <param name="customGUIHandler">Custom implementation for drawing GUI in the editor.</param>
        /// <returns>Returns the created settings provider.</returns>
        public static SettingsProvider GetSettingsMenu(Object obj, string menuPath, string[] keywords, SettingsScope scope, GUIHandlerDelegate customGUIHandler)
        {
            // If the given menu path is not valid
            if (string.IsNullOrEmpty(menuPath))
            {
                // Split the namespace by "."
                string[] typeNameSplit = obj.GetType().FullName.Split('.');
                // First part of the menu is the main namespace
                menuPath = ObjectNames.NicifyVariableName(typeNameSplit[0]);
                // Add the last namespace as the last part of the menu
                // As an example, if the namespace is "MuffinDev.Core.Reflection", the menu would be "Muffin Dev/Reflection"
                if (typeNameSplit.Length > 1)
                {
                    menuPath += $"/{ObjectNames.NicifyVariableName(typeNameSplit[typeNameSplit.Length - 1])}";
                }
            }

            // Return the settings provider
            return new SettingsProvider(menuPath, scope)
            {
                keywords = keywords != null ? keywords : new string[0],
                deactivateHandler = () =>
                {
                    if (I._loadedEditors.TryGetValue(obj, out Editor editor))
                    {
                        DestroyImmediate(editor);
                        I._loadedEditors.Remove(obj);
                    }
                },
                guiHandler = search => customGUIHandler(() => I.GetEditor(obj).OnInspectorGUI(), obj, search)
            };
        }

        #endregion


        #region Protected API

        /// <inheritdoc cref="SOSingleton{T}.Unset"/>
        protected override void Unset()
        {
            // Destroy all created editors
            foreach (KeyValuePair<Object, Editor> editors in _loadedEditors)
            {
                if (editors.Value != null)
                {
                    DestroyImmediate(editors.Value);
                }
            }
            _loadedEditors.Clear();
        }

        #endregion


        #region Private API

        /// <summary>
        /// Gets the editor for the given object.
        /// </summary>
        /// <param name="obj">The object of which you want to get the editor.</param>
        /// <returns>Returns the editor for the given object.</returns>
        private Editor GetEditor(Object obj)
        {
            if (!_loadedEditors.TryGetValue(obj, out Editor editor))
            {
                editor = Editor.CreateEditor(obj);
                _loadedEditors.Add(obj, editor);
            }
            return editor;
        }

        #endregion

    }

}