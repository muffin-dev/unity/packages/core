/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Handles editor events and asset modifications to save static classes and loaded assets marked with <see cref="SaveAttribute"/>, so
    /// they can be saved on disk automatically.
    /// </summary>
    [InitializeOnLoad]
    public class SaveProcessor : UnityEditor.AssetModificationProcessor
    {

        /// <summary>
        /// Static constructor.
        /// </summary>
        static SaveProcessor()
        {
            AssemblyReloadEvents.beforeAssemblyReload += SaveUtility.SaveAll;
            EditorApplication.playModeStateChanged += _ => SaveUtility.SaveAll();
        }

        /// <summary>
        /// Called when assets are saved in the editor.
        /// </summary>
        private static string[] OnWillSaveAssets(string[] paths)
        {
            SaveUtility.SaveAll();
            return paths;
        }

    }


}