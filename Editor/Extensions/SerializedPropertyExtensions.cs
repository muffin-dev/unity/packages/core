/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;
using System.Reflection;
using System.Collections;

using UnityEditor;
using UnityEngine;

using MuffinDev.Core.Reflection;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Extension functions for <see cref="SerializedProperty"/> instances.
    /// </summary>
    public static class SerializedPropertyExtensions
    {

        #region Public API

        /// <summary>
        /// Gets the target object of this <see cref="SerializedProperty"/>.<br/>
        /// This function is inspired by the SpacePuppy Unity Framework:
        /// https://github.com/lordofduct/spacepuppy-unity-framework-4.0/blob/master/Framework/com.spacepuppy.core/Editor/src/EditorHelper.cs
        /// </summary>
        /// <param name="property">The property of which you want to get the target object.</param>
        /// <returns>Returns the found target object.</returns>
        public static object GetTarget(this SerializedProperty property)
        {
            // The object is for now the serialized container of the represented property
            object obj = property.serializedObject.targetObject;
            return ReflectionUtility.GetNestedObject(obj, property.propertyPath);
        }

        /// <inheritdoc cref="GetTarget(SerializedProperty)"/>
        /// <typeparam name="T">The expected target type.</typeparam>
        public static T GetTarget<T>(this SerializedProperty property)
        {
            return (T)GetTarget(property);
        }

        #endregion

    }

}