/**
 * Muffin Dev (c) 2023
 * Author: contact@muffindev.com
 */

using System;

using Object = UnityEngine.Object;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous extension functions for <see cref="Object"/> and assets.
    /// </summary>
    public static class ObjectExtensions
    {

        /// <inheritdoc cref="ObjectUtility.GetGUID(Object)"/>
        public static string GetGUID(this Object obj)
        {
            return ObjectUtility.GetGUID(obj);
        }

        /// <inheritdoc cref="ObjectUtility.IsAsset(Object)"/>
        public static bool IsAsset(this Object obj)
        {
            return ObjectUtility.IsAsset(obj);
        }

        /// <inheritdoc cref="ObjectUtility.GetUniqueAssetName(Object, string)"/>
        public static string GetUniqueAssetName(this Object asset, string expectedName)
        {
            return ObjectUtility.GetUniqueAssetName(asset, expectedName);
        }

        /// <inheritdoc cref="ObjectUtility.GetAssetPath(Object)"/>
        public static string GetAssetPath(this Object asset)
        {
            return ObjectUtility.GetAssetPath(asset);
        }

        /// <inheritdoc cref="ObjectUtility.GetAbsoluteAssetPath(Object)"/>
        public static string GetAbsoluteAssetPath(this Object asset)
        {
            return ObjectUtility.GetAbsoluteAssetPath(asset);
        }

        /// <inheritdoc cref="ObjectUtility.FindSubassetOfType(Object, Type, bool)"/>
        public static Object FindSubassetOfType(this Object asset, Type type, bool includeMainAsset = false)
        {
            return ObjectUtility.FindSubassetOfType(asset, type, includeMainAsset);
        }

        /// <inheritdoc cref="ObjectUtility.FindSubassetOfType{T}(Object, bool)"/>
        public static T FindSubassetOfType<T>(this Object asset, bool includeMainAsset = false)
            where T : Object
        {
            return ObjectUtility.FindSubassetOfType<T>(asset, includeMainAsset);
        }

        /// <inheritdoc cref="ObjectUtility.FindAllSubassetsOfType(Object, Type, bool)"/>
        public static Object[] FindAllSubassetsOfType(this Object asset, Type type, bool includeMainAsset = false)
        {
            return ObjectUtility.FindAllSubassetsOfType(asset, type, includeMainAsset);
        }

        /// <inheritdoc cref="ObjectUtility.FindAllSubassetsOfType{T}(Object, bool)"/>
        public static T[] FindAllSubassetsOfType<T>(this Object asset, bool includeMainAsset = false)
            where T : Object
        {
            return ObjectUtility.FindAllSubassetsOfType<T>(asset, includeMainAsset);
        }

    }

}