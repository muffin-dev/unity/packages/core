/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Extension functions for <see cref="ScriptableObject"/> instances.
    /// </summary>
    public static class ScriptableObjectExtensions
    {

        /// <inheritdoc cref="ScriptableObjectUtility.GetScriptPath(ScriptableObject)"/>
        public static string GetScriptPath(this ScriptableObject obj)
        {
            return ScriptableObjectUtility.GetScriptPath(obj);
        }

    }

}