/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;
using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Indents a property in the inspector.
    /// </summary>
    [CustomPropertyDrawer(typeof(IndentAttribute))]
    public class IndentPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            using (new IndentedScope((attribute as IndentAttribute).Levels))
            {
                EditorGUI.PropertyField(position, property, label);
            }
        }

    }

}