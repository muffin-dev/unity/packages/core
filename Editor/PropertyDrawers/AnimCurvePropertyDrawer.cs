/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Displays a custom curve editor in the inspector view for <see cref="AnimationCurve"/> properties marked with
    /// <see cref="AnimCurveAttribute"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(AnimCurveAttribute))]
    public class AnimCurvePropertyDrawer : PropertyDrawer
    {

        /// <summary>
        /// Draws this property field.
        /// </summary>
        /// <param name="position">The available space for drawing the field.</param>
        /// <param name="property">The property that uses this drawer.</param>
        /// <param name="label">The label of the property</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Display original field when multiple values selected
            if (property.hasMultipleDifferentValues)
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }

            AnimCurveAttribute animCurve = attribute as AnimCurveAttribute;

            if (property.propertyType == SerializedPropertyType.AnimationCurve)
            {
                EditorGUI.CurveField(position, property, animCurve.CurveColor, animCurve.Ranges, label);
                property.serializedObject.ApplyModifiedProperties();
            }
            else
            {
                EditorGUI.PropertyField(position, property, label);
                Debug.LogWarning($"The {nameof(AnimCurveAttribute)} attribute can only be used on {nameof(AnimationCurve)} properties.");
            }
        }

    }

}