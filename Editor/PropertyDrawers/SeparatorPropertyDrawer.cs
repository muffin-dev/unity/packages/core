/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;
using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Draws a separator line before the next property.
    /// </summary>
    [CustomPropertyDrawer(typeof(SeparatorAttribute))]
    public class SeparatorPropertyDrawer : PropertyDrawer
    {

        public const float SPACE = 12f;
        public const float SEPARATOR_TOTAL_HEIGHT = SPACE * 2 + MoreEditorGUI.DEFAULT_SEPARATOR_SIZE;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SeparatorAttribute attr = attribute as SeparatorAttribute;

            Rect rect = new Rect(position);
            rect.height = MoreEditorGUI.DEFAULT_SEPARATOR_SIZE;
            rect.y += SPACE;

            if (attr.Wide)
            {
                rect.width = EditorGUIUtility.currentViewWidth;
                rect.x = 0;
            }

            EditorGUI.DrawRect(rect, MoreEditorGUI.SeparatorColor);
            
            rect.width = position.width;
            rect.y += SPACE;
            rect.height = position.height - SEPARATOR_TOTAL_HEIGHT;

            EditorGUI.PropertyField(rect, property, label);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + SEPARATOR_TOTAL_HEIGHT;
        }

    }

}