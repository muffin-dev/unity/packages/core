/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;
using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Draws a field to select a file path, with a "Browse..." button next to it. 
    /// </summary>
    [CustomPropertyDrawer(typeof(FilePathFieldAttribute))]
    public class FilePathFieldPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.hasMultipleDifferentValues || property.propertyType != SerializedPropertyType.String)
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }

            FilePathFieldAttribute attr = attribute as FilePathFieldAttribute;
            if (!string.IsNullOrEmpty(attr.Extension))
            {
                property.stringValue = MoreEditorGUI.FilePathField
                (
                    position,
                    label,
                    property.stringValue,
                    !string.IsNullOrEmpty(attr.PanelTitle) ? attr.PanelTitle : label.text,
                    attr.Extension,
                    !string.IsNullOrEmpty(attr.HistoryKey) ? attr.HistoryKey : null
                );
            }
            else if (attr.Filters != null && attr.Filters.Length > 0)
            {
                property.stringValue = MoreEditorGUI.FilePathField
                (
                    position,
                    label,
                    property.stringValue,
                    !string.IsNullOrEmpty(attr.PanelTitle) ? attr.PanelTitle : label.text,
                    attr.Filters,
                    !string.IsNullOrEmpty(attr.HistoryKey) ? attr.HistoryKey : null
                );
            }
            else
            {
                property.stringValue = MoreEditorGUI.FilePathField
                (
                    position,
                    label,
                    property.stringValue,
                    !string.IsNullOrEmpty(attr.PanelTitle) ? attr.PanelTitle : label.text,
                    extension: null,
                    !string.IsNullOrEmpty(attr.HistoryKey) ? attr.HistoryKey : null
                );
            }
        }

    }

}