/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Custom property drawer for <see cref="TimerLite"/> properties.
    /// </summary>
    [CustomPropertyDrawer(typeof(TimerLite))]
    public class TimerLitePropertyDrawer : PropertyDrawer
    {

        private const string DURATION_PROP = "_duration";
        private bool _displaySeconds = false;

        /// <summary>
        /// Draws this property field's GUI.
        /// </summary>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty durationProp = property.FindPropertyRelative(DURATION_PROP);

            if (property.hasMultipleDifferentValues || durationProp == null)
            {
                EditorGUI.PropertyField(position, property, label, true);
                return;
            }

            TimerLite target = property.GetTarget() as TimerLite;

            // Draw label
            Rect rect = new Rect(position);
            rect.width = EditorGUIUtility.labelWidth;
            EditorGUI.LabelField(rect, label);

            rect.x += rect.width + MoreGUI.H_MARGIN;
            rect.width = position.width - rect.width - MoreGUI.H_MARGIN;

            float half = (rect.width - MoreGUI.H_MARGIN) / 2;

            if (target != null)
                rect.width = half;

            using (new LabelWidthScope(MoreGUI.WIDTH_S))
            {
                durationProp.floatValue = Mathf.Max(0, EditorGUI.FloatField(rect, new GUIContent("Duration (s)", "The duration of this timer, in seconds."), durationProp.floatValue));
            }

            if (target != null)
            {
                rect.x += half + MoreGUI.H_MARGIN;

                string text = _displaySeconds
                    ? $"Elapsed: {target.ElapsedTime.ToString("F2")}s"
                    : $"Elapsed: {(target.ElapsedTimeRatio * 100).ToString("F2")}%";
                EditorGUI.ProgressBar(rect, target.ElapsedTimeRatio, text);

                // Force refresh
                EditorUtility.SetDirty(property.serializedObject.targetObject);

                if (Event.current.type == EventType.MouseUp && rect.Contains(Event.current.mousePosition))
                {
                    Event.current.Use();
                    _displaySeconds = !_displaySeconds;
                    OnGUI(position, property, label);
                }
            }
        }

        /// <summary>
        /// Gets this property field's height.
        /// </summary>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight;
        }

    }

}