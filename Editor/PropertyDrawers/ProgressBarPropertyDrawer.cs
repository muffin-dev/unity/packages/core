/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;
using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Draws a progress bar next to a numeric field in the editor.
    /// </summary>
    [CustomPropertyDrawer(typeof(ProgressBarAttribute))]
    public class ProgressBarPropertyDrawer : PropertyDrawer
    {

        private const string GUI_CONTROL_NAME_PREFIX = nameof(ProgressBarPropertyDrawer) + "_";
        private const float BACKGROUND_COLOR_MULTIPLIER = .4f;
        private const float GRAYSCALE_CONTRAST_THRESHOLD = .5f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.hasMultipleDifferentValues || (property.propertyType != SerializedPropertyType.Float && property.propertyType != SerializedPropertyType.Integer))
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }

            ProgressBarAttribute attr = attribute as ProgressBarAttribute;
            string controlName = GUI_CONTROL_NAME_PREFIX + label.text;
            bool useFullWidth = attr.ReplaceField && GUI.GetNameOfFocusedControl() != controlName;

            using (new EnabledScope(!attr.Readonly))
            {
                // The bar will take the full field width if the ReplaceField option is enabled and the focused control is not that field
                float barWidth = useFullWidth
                    ? position.width - EditorGUIUtility.labelWidth - MoreGUI.H_MARGIN
                    : (position.width - EditorGUIUtility.labelWidth - MoreGUI.H_MARGIN) / 2;

                float fieldWidth = position.width - barWidth - MoreGUI.H_MARGIN;

                Rect rect = new Rect(position);
                rect.width = useFullWidth ? position.width : fieldWidth;

                GUI.SetNextControlName(controlName);
                EditorGUI.PropertyField(rect, property, label);

                float value = property.propertyType == SerializedPropertyType.Float ? property.floatValue : property.intValue;
                if (attr.Clamp)
                {
                    value = Mathf.Clamp(value, attr.Min, attr.Max);
                    if (property.propertyType == SerializedPropertyType.Float)
                        property.floatValue = value;
                    else if (property.propertyType == SerializedPropertyType.Integer)
                        property.intValue = Mathf.FloorToInt(value);
                    property.serializedObject.ApplyModifiedProperties();
                }

                rect.x += fieldWidth + MoreGUI.H_MARGIN;
                rect.width = barWidth;

                float ratio = Mathf.Clamp01(value.Ratio(attr.Min, attr.Max));
                Color color = attr.Color;
                Color backgroundColor = color * BACKGROUND_COLOR_MULTIPLIER;
                backgroundColor.a = 1f;
                float grayscale = ratio >= .5f ? color.grayscale : backgroundColor.grayscale;
                Color fontColor = grayscale >= .5f ? Color.black : Color.white;

                EditorGUI.DrawRect(rect, backgroundColor);
                EditorGUI.DrawRect(new Rect(rect.x, rect.y, rect.width * ratio, rect.height), color);
                EditorGUI.LabelField(rect, attr.GetLabel(value), EditorStyles.label.TextAlignment(TextAnchor.MiddleCenter).FontColor(fontColor));
            }
        }

    }

}