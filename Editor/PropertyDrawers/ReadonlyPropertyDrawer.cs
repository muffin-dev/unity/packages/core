/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;
using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Renders a property as a disabled field in the inspector.
    /// </summary>
    [CustomPropertyDrawer(typeof(ReadonlyAttribute))]
    public class ReadonlyPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            using (new EnabledScope(false))
            {
                EditorGUI.PropertyField(position, property, label);
            }
        }

    }

}