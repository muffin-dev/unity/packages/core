﻿using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

	///<summary>
	/// Custom editor for Pagination properties.
	///</summary>
    [CustomPropertyDrawer(typeof(Pagination))]
	public class PaginationPropertyDrawer : PropertyDrawer
	{

        private const string PAGE_PROP = "_pageIndex";
        private const string NB_ELEMENTS_PER_PAGE_PROP = "_nbElementsPerPage";

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Avoid strange behaviours when multi-selecting elements
            if (property.hasMultipleDifferentValues)
            {
                EditorGUI.PropertyField(position, property);
                return;
            }

            SerializedProperty pageProperty = property.FindPropertyRelative(PAGE_PROP);
            SerializedProperty nbElementsPerPageProperty = property.FindPropertyRelative(NB_ELEMENTS_PER_PAGE_PROP);

            // Draw label
            Rect rect = new Rect(position);
            rect.width = EditorGUIUtility.labelWidth;
            EditorGUI.LabelField(rect, label, property.prefabOverride ? EditorStyles.boldLabel : EditorStyles.label);

            rect.x += rect.width + MoreGUI.H_MARGIN;
            rect.width = (position.width - EditorGUIUtility.labelWidth - MoreGUI.H_MARGIN * 2) / 2;

            using (new LabelWidthScope(Mathf.Min(rect.width / 2, MoreGUI.WIDTH_S)))
            {
                // Draw current page field
                pageProperty.intValue = EditorGUI.IntField(rect, new GUIContent("Page", "Current page index (0-based)"), pageProperty.intValue);

                // Draw "nb. elements per page" field
                rect.x += rect.width + MoreGUI.H_MARGIN;
                nbElementsPerPageProperty.intValue = EditorGUI.IntField(rect, new GUIContent("Nb. per page", "Number of elements per page"), nbElementsPerPageProperty.intValue);
            }

            property.serializedObject.ApplyModifiedProperties();
        }

    }

}