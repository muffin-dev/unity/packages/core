/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEditor;
using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Draws a field to select a folder path, with a "Browse..." button next to it. 
    /// </summary>
    [CustomPropertyDrawer(typeof(FolderPathFieldAttribute))]
    public class FolderPathFieldPropertyDrawer : PropertyDrawer
    {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.hasMultipleDifferentValues || property.propertyType != SerializedPropertyType.String)
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }

            FolderPathFieldAttribute attr = attribute as FolderPathFieldAttribute;
            property.stringValue = MoreEditorGUI.FolderPathField
            (
                position,
                label,
                property.stringValue,
                !string.IsNullOrEmpty(attr.PanelTitle) ? attr.PanelTitle : label.text,
                !string.IsNullOrEmpty(attr.HistoryKey) ? attr.HistoryKey : null
            );
        }

    }

}