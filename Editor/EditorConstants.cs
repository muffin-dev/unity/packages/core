/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Miscellaneous constant values for editor features.
    /// </summary>
    public static class EditorConstants
    {

        /// <summary>
        /// Prefix used for main toolbar menus. Ends with "/".
        /// </summary>
        public const string TOOLBAR_MENU_PREFIX = "Tools/";

        /// <summary>
        /// Base path used for custom menus in the main toolbar. You must concatenate your own path starting with "/".
        /// </summary>
        public const string TOOLBAR_MENU = TOOLBAR_MENU_PREFIX + Constants.COMPANY_NAME;

        /// <summary>
        /// Base path used for custom menus in the main toolbar for opening custom editor windows. You must concatenate your own path
        /// starting with "/".
        /// </summary>
        public const string EDITOR_WINDOW_MENU = TOOLBAR_MENU;

        /// <summary>
        /// Base path used for custom menus in the main toolbar for opening demo custom editor windows. You must concatenate your own path
        /// starting with "/".
        /// </summary>
        public const string EDITOR_WINDOW_MENU_DEMOS = EDITOR_WINDOW_MENU + Constants.DEMOS_SUBMENU;

        /// <summary>
        /// Base path used for custom Preferences menus. You must concatenate your own path starting with "/".
        /// </summary>
        public const string PREFERENCES = Constants.COMPANY_NAME;

        /// <summary>
        /// Base path used for custom Project Settings menus. You must concatenate your own path starting with "/".
        /// </summary>
        public const string PROJECT_SETTINGS = Constants.COMPANY_NAME;

    }


}