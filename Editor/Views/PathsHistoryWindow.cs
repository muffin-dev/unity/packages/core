/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System;

using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Utility window to see and manage the content of <see cref="PathsHistory"/>.
    /// </summary>
    public class PathsHistoryWindow : EditorWindow
    {

        #region Subclasses

        /// <summary>
        /// A tree view for displaying paths histories.
        /// </summary>
        private class PathsHistoriesTreeView : TreeView
        {

            /// <summary>
            /// Represents a custom tree view item for this tree view.
            /// </summary>
            private abstract class PathsHistoriesTreeViewItem : TreeViewItem
            {
                /// <summary>
                /// Draws this item in the tree view.
                /// </summary>
                public abstract void Draw(RowGUIArgs args);

                /// <summary>
                /// Called when the user uses right-click over this item.
                /// </summary>
                /// <param name="reloadFunc">The function to call when the tree view should be reloaded.</param>
                /// <returns>Returns true if the tree view should be reloaded after the operation, otherwise false.</returns>
                public abstract void ContextClick(Action reloadFunc);
            }

            /// <summary>
            /// Represents an item in this tree view for a <see cref="PathsCollection"/>.
            /// </summary>
            private class PathsCollectionItem : PathsHistoriesTreeViewItem
            {
                public PathsCollection collection = null;

                public PathsCollectionItem(int id, int depth, TreeViewItem rootItem, PathsCollection collection)
                {
                    this.id = id;
                    this.depth = depth;
                    this.parent = rootItem;
                    this.collection = collection;

                    this.displayName = !string.IsNullOrEmpty(collection.Key) ? collection.Key : "Default History";
                }

                /// <inheritdoc cref="PathsHistoriesTreeViewItem.Draw(RowGUIArgs)"/>
                public override void Draw(RowGUIArgs args)
                {
                    Rect rect = args.rowRect;
                    rect.width -= MoreGUI.WIDTH_S + MoreGUI.WIDTH_M + MoreGUI.H_MARGIN * 2;
                    EditorGUI.LabelField(rect, $"{args.label}", EditorStyles.boldLabel);

                    rect.x += rect.width + MoreGUI.H_MARGIN;
                    rect.width = MoreGUI.WIDTH_S;
                    GUI.Label(rect, "Limit: " + collection.Limit, EditorStyles.label.FontStyle(FontStyle.Italic));

                    rect.x += rect.width + MoreGUI.H_MARGIN;
                    rect.width = MoreGUI.WIDTH_M;
                    GUI.Label(rect, "Origin: " + collection.Origin, EditorStyles.label.FontStyle(FontStyle.Italic));
                }

                /// <inheritdoc cref="PathsHistoriesTreeViewItem.ContextClick(Action)"/>
                public override void ContextClick(Action reloadFunc)
                {
                    GenericMenu menu = new GenericMenu();
                    menu.AddItem(new GUIContent("Clear"), false, () =>
                    {
                        PathsHistory.ClearHistory(collection.Key);
                        reloadFunc();
                    });
                    menu.AddItem(new GUIContent("Delete"), false, () =>
                    {
                        PathsHistory.RemoveHistory(collection.Key);
                        reloadFunc();
                    });
                    menu.ShowAsContext();
                }
            }

            /// <summary>
            /// Represents an item in this tree view for a <see cref="PathsCollection.PathInfo"/> entry.
            /// </summary>
            private class PathItem : PathsHistoriesTreeViewItem
            {
                public PathsCollection.PathInfo pathInfo = default;

                public PathItem(int id, int depth, PathsCollectionItem parentCollectionItem, PathsCollection.PathInfo pathInfo)
                {
                    this.id = id;
                    this.depth = depth;
                    this.parent = parentCollectionItem;
                    this.pathInfo = pathInfo;

                    this.displayName = pathInfo.path;
                }

                public PathsCollection Collection => (parent as PathsCollectionItem).collection;

                /// <inheritdoc cref="PathsHistoriesTreeViewItem.Draw(RowGUIArgs)"/>
                public override void Draw(RowGUIArgs args)
                {
                    Rect rect = args.rowRect;
                    rect.width -= MoreGUI.WIDTH_S + MoreGUI.WIDTH_M + MoreGUI.H_MARGIN * 2;
                    EditorGUI.LabelField(rect, args.label);

                    rect.x += rect.width + MoreGUI.H_MARGIN;
                    rect.width = MoreGUI.WIDTH_S + MoreGUI.WIDTH_M + MoreGUI.H_MARGIN;
                    EditorGUI.LabelField(rect, pathInfo.Date.ToString(), EditorStyles.label.FontStyle(FontStyle.Italic).FontSize(10));
                }

                /// <inheritdoc cref="PathsHistoriesTreeViewItem.ContextClick(Action)"/>
                public override void ContextClick(Action reloadFunc)
                {
                    GenericMenu menu = new GenericMenu();
                    menu.AddItem(new GUIContent("Remove"), false, () =>
                    {
                        PathsHistory.RemovePath(Collection.Key, pathInfo.path);
                        reloadFunc();
                    });
                    menu.ShowAsContext();
                }
            }

            /// <summary>
            /// Class constructor.
            /// </summary>
            public PathsHistoriesTreeView(TreeViewState state)
                : base(state) { }

            /// <summary>
            /// Builds this tree view.
            /// </summary>
            /// <returns>Returns the root item of this tree view.</returns>
            protected override TreeViewItem BuildRoot()
            {
                int depth = -1;
                int id = -1;
                TreeViewItem root = new TreeViewItem { id = id++, depth = depth++, displayName = "ROOT" };

                foreach (PathsCollection collection in PathsHistory.Instance)
                {
                    PathsCollectionItem collectionItem = new PathsCollectionItem(id++, depth, root, collection);

                    depth++;
                    foreach (PathsCollection.PathInfo p in collection.PathInfos)
                    {
                        PathItem pathItem = new PathItem(id++, depth, collectionItem, p);
                        collectionItem.AddChild(pathItem);
                    }
                    depth--;

                    root.AddChild(collectionItem);
                }

                return root;
            }

            /// <summary>
            /// Called for drawing a single item in this tree view.
            /// </summary>
            protected override void RowGUI(RowGUIArgs args)
            {
                if (args.item is PathsHistoriesTreeViewItem customItem)
                {
                    float offset = depthIndentWidth * (args.item.depth + 1) + baseIndent;

                    args.rowRect.x += offset;
                    args.rowRect.width -= offset;
                    customItem.Draw(args);
                }
            }

            /// <summary>
            /// Called when an item is context-clicked.
            /// </summary>
            protected override void ContextClickedItem(int id)
            {
                TreeViewItem item = FindItem(id, rootItem);
                if (item != null && item is PathsHistoriesTreeViewItem customItem)
                    customItem.ContextClick(Reload);
            }

        }

        #endregion


        #region Fields

        private const string WINDOW_TITLE = "Paths History";
        private const string MENU_ITEM = EditorConstants.EDITOR_WINDOW_MENU + "/" + WINDOW_TITLE;

        [SerializeField]
        private bool _foldoutTestControls = false;

        [SerializeField]
        private string _newCollectionKey = null;

        [SerializeField]
        private int _newCollectionLimit = 0;

        [SerializeField]
        private EPathOrigin _newCollectionOrigin = EPathOrigin.Source;

        [SerializeField]
        private int _addPathCollectionKeyIndex = 0;

        [SerializeField]
        private bool _addPathDefaulCollection = true;

        [SerializeField]
        private string _addPathValue = null;

        [SerializeField]
        private TreeViewState _pathsTreeViewState = null;

        private PathsHistoriesTreeView _pathsTreeView = null;

        #endregion


        #region Lifecycle

        /// <summary>
        /// Called when this window is open.
        /// </summary>
        private void OnEnable()
        {
            if (_pathsTreeViewState == null)
                _pathsTreeViewState = new TreeViewState();

            _pathsTreeView = new PathsHistoriesTreeView(_pathsTreeViewState);
            _pathsTreeView.Reload();
        }

        #endregion


        #region Public API

        /// <summary>
        /// Opens this editor window.
        /// </summary>
        [MenuItem(MENU_ITEM)]
        public static PathsHistoryWindow Open()
        {
            PathsHistoryWindow window = GetWindow<PathsHistoryWindow>(false, WINDOW_TITLE, true);
            window.Show();
            return window;
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this window GUI on screen.
        /// </summary>
        private void OnGUI()
        {
            _foldoutTestControls = EditorGUILayout.Foldout(_foldoutTestControls, EditorHelpers.GetLabel(this, nameof(_foldoutTestControls), "Test Controls"), true);
            if (_foldoutTestControls)
            {
                using (new IndentedScope())
                {
                    using (new EditorGUILayout.HorizontalScope())
                    {
                        using (new EditorGUILayout.VerticalScope())
                        using (new LabelWidthScope(MoreGUI.WIDTH_S))
                        {
                            EditorGUILayout.LabelField("Create New Paths Collection", EditorStyles.boldLabel);

                            _newCollectionKey = EditorGUILayout.TextField(EditorHelpers.GetLabel(this, nameof(_newCollectionKey), "Key"), _newCollectionKey);
                            _newCollectionLimit = Mathf.Max(0, EditorGUILayout.IntField(EditorHelpers.GetLabel(this, nameof(_newCollectionLimit), "Limit"), _newCollectionLimit));
                            _newCollectionOrigin = (EPathOrigin)EditorGUILayout.EnumPopup(EditorHelpers.GetLabel(this, nameof(_newCollectionKey), "Path Origin"), _newCollectionOrigin);

                            if (GUILayout.Button("Create New Paths Collection"))
                            {
                                if(PathsHistory.CreateHistory(_newCollectionKey, out _, _newCollectionLimit, _newCollectionOrigin))
                                    Debug.Log($"New paths collection created, with key {_newCollectionKey}.");
                                else
                                    Debug.LogWarning($"Failed to create new paths collection with key {_newCollectionKey}.");
                            }
                        }

                        using (new EditorGUILayout.VerticalScope())
                        using (new LabelWidthScope(MoreGUI.WIDTH_S))
                        {
                            EditorGUILayout.LabelField("Add Path To Collection", EditorStyles.boldLabel);

                            using (new EditorGUILayout.HorizontalScope())
                            {
                                EditorGUILayout.LabelField(EditorHelpers.GetLabel(this, nameof(_addPathCollectionKeyIndex), "Key"));
                                using (new EnabledScope(!_addPathDefaulCollection))
                                {
                                    _addPathCollectionKeyIndex = EditorGUILayout.Popup(_addPathCollectionKeyIndex, PathsHistory.PathCollections.Map(pc => pc.Key));
                                }
                                using (new LabelWidthScope(MoreGUI.WIDTH_S))
                                {
                                    _addPathDefaulCollection = EditorGUILayout.ToggleLeft("Default", _addPathDefaulCollection);
                                }
                            }
                            _addPathValue = MoreEditorGUI.FilePathField(EditorHelpers.GetLabel(this, nameof(_addPathValue), "Path"), _addPathValue, "Select File Path");
                            EditorGUILayout.GetControlRect();

                            if (GUILayout.Button("Add Path"))
                            {
                                try
                                {
                                    if (_addPathDefaulCollection)
                                        PathsHistory.I[null].Add(_addPathValue);
                                    else
                                        PathsHistory.I[_addPathCollectionKeyIndex].Add(_addPathValue);

                                    Debug.Log($"Path {_addPathValue} added successfully to collection with key {(_addPathDefaulCollection ? "Default Collection" : PathsHistory.I[_addPathCollectionKeyIndex].Key)}.");
                                }
                                catch (System.Exception e)
                                {
                                    Debug.LogException(e);
                                    Debug.LogWarning($"Failed to add path to collection.");
                                }
                            }
                        }
                    }
                }

                EditorGUILayout.Space();
            }

            MoreEditorGUI.HorizontalSeparator();

            EditorGUILayout.Space();
            Rect rect = EditorGUILayout.GetControlRect(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            _pathsTreeView.OnGUI(rect);
        }

        #endregion

    }

}