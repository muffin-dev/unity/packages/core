/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;
using UnityEditor;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Generate a preferences menu to edit core lib settings.
    /// </summary>
    public class CoreSettingsProvider
    {

        [SettingsProvider]
        private static SettingsProvider RegisterDefaultPreferencesMenu()
        {
            return new SettingsProvider(EditorConstants.PREFERENCES + "/General", SettingsScope.User)
            {
                guiHandler = search => DrawGUI()
            };
        }

        [SettingsProvider]
        private static SettingsProvider RegisterPreferencesMenu()
        {
            return new SettingsProvider(EditorConstants.PREFERENCES, SettingsScope.User)
            {
                guiHandler = search => DrawGUI()
            };
        }

        /// <summary>
        /// Draws the preferences GUI.
        /// </summary>
        private static void DrawGUI()
        {
            bool enableDemos = EditorGUILayout.Toggle(EditorHelpers.GetLabel<CoreEditorSettings>("_enableDemos"), CoreEditorSettings.I.EnableDemos);
            if (CoreEditorSettings.I.EnableDemos != enableDemos)
                CoreEditorSettings.I.EnableDemos = enableDemos;
        }

    }

}