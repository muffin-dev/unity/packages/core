/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

namespace MuffinDev.Core.EditorOnly
{

    /// <summary>
    /// Settings for the core library package.
    /// </summary>
    [Save(EDataScope.User)]
    public class CoreEditorSettings : SOSingleton<CoreEditorSettings>
    {

        #region Fields

        [SerializeField]
        [Tooltip("If enabled, adds a define to your project, so you can see \"Demos\" menus from the main toolbar and in \"Add Component\" popup.")]
        private bool _enableDemos = false;

        #endregion


        #region Lifecycle

        /// <inheritdoc cref="SOSingleton{T}.Set"/>
        protected override void Set()
        {
            _enableDemos = DemosEnabledForCurrentPlatform;
        }

        #endregion


        #region Public API

        /// <inheritdoc cref="_enableDemos"/>
        public bool EnableDemos
        {
            get => _enableDemos;
            set
            {
                DemosEnabledForCurrentPlatform = value;
                _enableDemos = value;
            }
        }

        #endregion


        #region Private API

        /// <summary>
        /// Checks if the demos define exists in Player Settings.
        /// </summary>
        private bool DemosEnabledForCurrentPlatform
        {
            get => DefinesUtility.IsDefined(Constants.DEMOS_DEFINE);
            set
            {
                if (value)
                    DefinesUtility.AddDefine(Constants.DEMOS_DEFINE);
                else
                    DefinesUtility.RemoveDefine(Constants.DEMOS_DEFINE);
            }
        }

        #endregion

    }

}