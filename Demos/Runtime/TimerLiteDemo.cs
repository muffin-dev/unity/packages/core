/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

#if MUFFINDEV_DEMOS

using UnityEngine;

using MuffinDev.Core;

namespace MuffinDev.Demos.Core
{

    /// <summary>
    /// Demonstrates how to use the <see cref="TimerLite"/> utility.<br/>
    /// You can configure the duration of the timer from the inspector. When you run the game, the timer is updated each frame until it's
    /// elapsed. By then, the events <see cref="TimerLite.OnElapsed"/> and <see cref="TimerLite.OnElapsedPayload"/> are invoked, so our
    /// demo component can react to it!
    /// </summary>
    [AddComponentMenu(Constants.ADD_COMPONENT_MENU_DEMOS + "/Core/Timer Lite Demo")]
    [HelpURL(Constants.BASE_HELP_URL)]
    public class TimerLiteDemo : MonoBehaviour
    {

        [SerializeField]
        private TimerLite _timerLite = new TimerLite(3f);

        [SerializeField]
        private int _payloadValue = 50;

        private void OnEnable()
        {
            _timerLite.OnElapsed += () => Debug.Log("Timer elapsed!");
            _timerLite.OnElapsedPayload += payload => Debug.Log("Timer elapsed! Received data: " + (int)payload);
        }

        private void Start()
        {
            _timerLite.Start((object)_payloadValue);
        }

        private void Update()
        {
            if(_timerLite.Update(Time.deltaTime))
            {
                Debug.Log($"Timer updated: {_timerLite.ElapsedTime.ToString("F2")}/{_timerLite.Duration.ToString("F2")} ({(_timerLite.ElapsedTimeRatio * 100).ToString("F2")}%)");
            }
        }

    }

}

#endif