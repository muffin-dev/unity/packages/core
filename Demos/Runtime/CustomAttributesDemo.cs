/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

#if MUFFINDEV_DEMOS

using UnityEngine;

using MuffinDev.Core;

namespace MuffinDev.Demos.Core
{

    /// <summary>
    /// Demo component that illustrates the use of custom attributes from this framework.
    /// </summary>
    [AddComponentMenu(Constants.ADD_COMPONENT_MENU_DEMOS + "/Core/Custom Attributes Demo")]
    [HelpURL(Constants.BASE_HELP_URL)]
    public class CustomAttributesDemo : MonoBehaviour
    {

        [AnimCurve(EColor.Cyan)]
        [Tooltip("Simple curve with no bounds.")]
        public AnimationCurve Curve0To1 = AnimationCurve.EaseInOut(0, 0, 1, 1);

        [AnimCurve(5, 10, EColor.Orange)]
        [Tooltip("Curve with time between 0 and 5, and value between 0 and 10.")]
        public AnimationCurve CurveTime5Value10 = AnimationCurve.EaseInOut(0, 0, 5, 10);

        [AnimCurve(0, 1, -5, 5, EColor.Purple)]
        [Tooltip("Curve with time between 0 and 1, and value between -5 and 5.")]
        public AnimationCurve CurveNegative5To5 = AnimationCurve.EaseInOut(0, -5, 1, 5);

        [Tooltip("Indented property field.")]
        [Indent(1)]
        public string IndentOnce = "Indent Level = 1";

        [Tooltip("Indented property field.")]
        [Indent(2)]
        public string IndentTwice = "Indent Level = 2";

        [Tooltip("Readonly property, displayed as a disabled field in the inspector.")]
        [Readonly]
        public string ReadonlyProperty = "Not editable!";

        [Tooltip("Simple float field between 0 and 1, unclamped, displayed as a progress bar.")]
        [ProgressBar]
        public float ProgressBarRatio = .8f;

        [Tooltip("Float field representing a percentage, unclamped, displayed as a progress bar.")]
        [ProgressBar(100, "%", EColor.Green)]
        public float ProgressBarPercents = 25f;

        [Tooltip("Int field from -100 to 100, clamped, displayed as a progress bar.")]
        [ProgressBar(-100, 100, EColor.Red, true, true)]
        public int ProgressBarNegative100ToPositive100 = 50;

        [Separator]

        [Tooltip("Field preceeded by horizontal separator.")]
        public string SeparatedField = string.Empty;

        [Tooltip("Field that contain a path to a folder.")]
        [FolderPathField]
        public string FolderPathField;

        [Tooltip("Field that contain a path to a *.json file.")]
        [FilePathField("json")]
        public string FilePathField;

    }

}

#endif