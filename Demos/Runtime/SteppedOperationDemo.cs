/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

#if MUFFINDEV_DEMOS

using System.Collections;

using UnityEngine;

using MuffinDev.Core;

namespace MuffinDev.Demos.Core
{

    /// <summary>
    /// This demo component will create sphere primitives along a segment represented by an origin and an end point.
    /// </summary>
    [AddComponentMenu(Constants.ADD_COMPONENT_MENU_DEMOS + "/Core/Stepped Operation Demo")]
    [HelpURL(Constants.BASE_HELP_URL)]
    public class SteppedOperationDemo : SteppedOperationComponent
    {

        [SerializeField]
        private int _nbInstances = 10;

        [SerializeField]
        private Vector3 _origin = Vector3.zero;

        [SerializeField]
        private Vector3 _target = new Vector3(10, 5, 0);

        private void Start()
        {
            Init(InstantiatePoints());
        }

        protected override void Update()
        {
            if (Input.GetMouseButtonDown(0))
                Step();

            base.Update();
        }

        private IEnumerator InstantiatePoints()
        {
            for (int i = 0; i < _nbInstances; i++)
            {
                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                obj.name = $"Primitive [{i}]";
                obj.transform.position = Vector3.Lerp(_origin, _target, i / (_nbInstances - 1f));
                yield return null;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(_origin, .05f);

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(_target, .05f);
        }

    }

}

#endif