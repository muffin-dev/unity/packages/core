/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

#if MUFFINDEV_DEMOS

using MuffinDev.Core;

namespace MuffinDev.Demos.Core
{

    /// <summary>
    /// This script illustrates how to create a config asset accessibe from settings menus such as Project Settings or Preferences.<br/>
    /// Check the SettingsProviderDemoEditor class to see how the <see cref="UnityEditor.SettingsProvider"/> are actually registered in
    /// Unity menus.
    /// </summary>
    [Save(EDataScope.Temp)]
    public class SettingsProviderDemo : SOSingleton<SettingsProviderDemo>
    {

        public string mainSceneName;
        public float defaultPlayerSpeed;
        public float defaultGameDuration;

        public void Reset()
        {
            mainSceneName = "GameScene";
            defaultPlayerSpeed = 6f;
            defaultGameDuration = 120f;
        }

    }

}

#endif