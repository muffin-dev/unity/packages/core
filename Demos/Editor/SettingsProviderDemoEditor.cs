/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

#if MUFFINDEV_DEMOS

using UnityEngine;
using UnityEditor;

using MuffinDev.Demos.Core;
using MuffinDev.Core.EditorOnly;

namespace MuffinDev.Demos.Core.EditorOnly
{

    /// <summary>
    /// This script illustrates how to create a config asset accessibe from settings menus such as Project Settings or Preferences.
    /// </summary>
    public class SettingsProviderDemoEditor
    {

        // This will create a "Muffin Dev/Demos/Game Default Values" menu from Edit > Preferences, displaying the default inspector of the
        // asset.
        [SettingsProvider]
        private static SettingsProvider RegisterPreferencesMenu()
        {
            return SettingsProvidersUtility.GetSettingsMenu(SettingsProviderDemo.I, "Muffin Dev/Demos/Game Default Values", SettingsScope.User);
        }

        // This will create a "Muffin Dev/Demos/Game Default Values" menu from Edit > Project Settings, displaying the default inspector of
        // the asset, and an additional control button.
        [SettingsProvider]
        private static SettingsProvider RegisterProjectSettingsMenu()
        {
            return SettingsProvidersUtility.GetSettingsMenu(SettingsProviderDemo.I, "Muffin Dev/Demos/Game Default Values", SettingsScope.Project, (drawDefaultInspector, obj, search) =>
            {
                drawDefaultInspector();

                EditorGUILayout.Space();
                if (GUILayout.Button("Reset", GUILayout.Height(36f)))
                {
                    SettingsProviderDemo.I.Reset();
                }
            });
        }

    }

}

#endif